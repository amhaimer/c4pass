#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include "devices_Hp.h"

/*
 * Class:     devices_Hp
 * Method:    init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Hp_init
(JNIEnv *env , jobject obj)
{
  int file = open("/dev/hp",0x02);
  if (file < 0)
    printf("[C] : error");
  write(file, "0", sizeof("0"));
  return file;
}

/*
 * Class:     devices_Hp
 * Method:    niveau
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Hp_niveau
(JNIEnv *env, jobject obj, jstring string)
{
  jint handle;
  jclass hp = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, hp, "handle", "I");
  const char *value = (*env)->GetStringUTFChars(env, string, 0);

  handle = (*env)->GetIntField(env, obj, fidHandle);
  write(handle, value, sizeof(value));
  (*env)->ReleaseStringUTFChars(env, string, value);
}

/*
 * Class:     devices_Hp
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Hp_exit
  (JNIEnv *env, jobject obj)
{
  jint handle;
  jclass hp = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, hp, "handle", "I");

  if (fidHandle == NULL)
    return;
  handle = (*env)->GetIntField(env, obj, fidHandle);
  close(handle);
}
