#include <jni.h>
#include <string.h>
#include <stdio.h>
#include "sip_SipClient.h"

/*
 * Class:     sip_SipClient
 * Method:    System
 * Signature: (Ljava/lang/String;)V
 */

JNIEXPORT void JNICALL Java_sip_SipClient_System
(JNIEnv *env, jobject obj, jstring string){
  const char *command = (*env)->GetStringUTFChars(env, string, 0);
  system(command);
}
