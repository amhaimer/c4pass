#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include "devices_Buzzer.h"

/*
 * Class:     devices_Buzzer
 * Method:    init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Buzzer_init
(JNIEnv *env , jobject obj)
{
  int file = open("/dev/buzzer",0x02);
  if (file < 0)
    printf("[C] : error");
  return file;
}

/*
 * Class:     devices_Buzzer
 * Method:    execute
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Buzzer_execute
(JNIEnv *env, jobject obj, jstring time)
{
  jint handle;
  jclass buzzer = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, buzzer, "handle", "I");
  const char *_time = (*env)->GetStringUTFChars(env, time, 0);
  
  if (fidHandle == NULL)
    return;
  handle = (*env)->GetIntField(env, obj, fidHandle);
  puts(_time);
  write(handle, _time, sizeof(_time));
  (*env)->ReleaseStringUTFChars(env, time, _time);
}

/*
 * Class:     devices_Buzzer
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Buzzer_exit
  (JNIEnv *env, jobject obj)
{
  jint handle;
  jclass buzzer = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, buzzer, "handle", "I");

  if (fidHandle == NULL)
    return;

  handle = (*env)->GetIntField(env, obj, fidHandle);
  close(handle);
}
