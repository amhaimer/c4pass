#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include "devices_Bouton_Appel.h"

/*
 * Class:     devices_Bouton_Appel
 * Method:    init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Bouton_1Appel_init
(JNIEnv *env , jobject obj)
{
  int file = open("/dev/bouton_appel",0x02);
  if (file < 0)
    printf("[C] : error");
  return file;
}

/*
 * Class:     devices_Bouton_Appel
 * Method:    status
 * Signature: ()Z
 */
JNIEXPORT jstring JNICALL Java_devices_Bouton_1Appel_status
(JNIEnv *env, jobject obj)
{
  jint handle;
  char buff[1];
  jclass bouton_appel = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, bouton_appel, "handle", "I");

  if (fidHandle == NULL)
    return;
  handle = (*env)->GetIntField(env, obj, fidHandle);
  read(handle, buff, 1);
  buff[1] = '\0';
  return (*env)->NewStringUTF(env,buff);
    
}

/*
 * Class:     devices_Bouton_Appel
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Bouton_1Appel_exit
  (JNIEnv *env, jobject obj)
{
  jint handle;
  jclass bouton_appel = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, bouton_appel, "handle", "I");

  if (fidHandle == NULL)
    return;

  handle = (*env)->GetIntField(env, obj, fidHandle);
  close(handle);
}
