#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include "devices_Relais.h"

/*
 * Class:     devices_Relais
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT jint JNICALL Java_devices_Relais_init
(JNIEnv *env , jobject obj)
{
  int file = open("/dev/relais",0x02);
  if (file < 0)
    printf("[C] : error");
  return file;
}

/*
 * Class:     devices_Relais
 * Method:    status
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Relais_status
(JNIEnv *env, jobject obj, jboolean c, jboolean b)
{
  jint handle;
  jclass relais = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, relais, "handle", "I");

  if (fidHandle == NULL)
    return;
  handle = (*env)->GetIntField(env, obj, fidHandle);
  if (!c)
    {
      if (b == JNI_TRUE)
{
	write(handle, "1", sizeof("1"));

puts("relais0");}
      else
	write(handle, "0", sizeof("0"));
      
    }
  else if (c)
    {
      if (b == JNI_TRUE)
	write(handle, "3", sizeof("3"));
      else
	write(handle, "2", sizeof("2"));
      puts("relais1");
    }

}

/*
 * Class:     devices_Relais
 * Method:    getStatusUn                                                                          * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Relais_getStatusUn
(JNIEnv *env, jobject obj)
{
  jint handle;
  jclass relais = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, relais, "handle", "I");
  char buf[1];
  
  if (fidHandle == NULL)
    return;
  handle = (*env)->GetIntField(env, obj, fidHandle);
  return (buf[0] == '1' ? JNI_TRUE : JNI_FALSE);
}

/*
 * Class:     devices_Relais
 * Method:    getStatusDeux                                                                        * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Relais_getStatusDeux
(JNIEnv *env, jobject obj)
{
  jint handle;
  jclass relais = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, relais, "handle", "I");
  char buf[1];

  if (fidHandle == NULL)
    return;
  handle = (*env)->GetIntField(env, obj, fidHandle);
  read(handle, buf, 1);
  return (buf[0] == '3' ? JNI_TRUE : JNI_FALSE);
}

/*
 * Class:     devices_Relais
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Relais_exit
  (JNIEnv *env, jobject obj)
{
  jint handle;
  jclass relais = (*env)->GetObjectClass(env, obj);
  jfieldID fidHandle = (*env)->GetFieldID(env, relais, "handle", "I");

  if (fidHandle == NULL)
    return;

  handle = (*env)->GetIntField(env, obj, fidHandle);
  close(handle);
}
