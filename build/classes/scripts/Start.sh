#!/bin/bash

# Lancer ce script au démarrage du RaspBerry :
# pi: chmod +x Start.sh && cp Start.sh /etc/init.d

sudo cp /home/pi/NetBeansProjects/C4PASS/index.php /var/www/html/stream/
#sudo motion
echo '--# Lancement de la C4PASS'
sudo service daemontools restart
# Place le CLASSPATH dans l'environnement
export CLASSPATH="../library/mysql-connector-java-5.1.39-bin.jar:../library/json-simple-1.1.1.jar:/opt/pi4j/lib/*:."
echo '--# CLASSPATH mit à jour'


while true
do
    ps auxw | grep Djava.library.path=/jni/ | grep -v grep > /dev/null
    if [ $? != 0 ]; then
	echo '--# Demarrage de la C4Pass'	
	sudo /usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/bin/java  -Dfile.encoding=UTF-8 -Djava.library.path=/home/pi/NetBeansProjects/C4PASS/dist/lib/jni/  -Xincgc  -Xms384m -Xmx450m   "-XX:OnOutOfMemoryError=kill -9 %p" -jar /home/pi/NetBeansProjects/C4PASS/dist/C4PASS.jar

	echo '--# Arrêt de la C4Pass'
    fi
done
