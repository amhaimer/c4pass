#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <pjsua-lib/pjsua.h>
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

/* Prototypes fonctions afficheur I2C */

void lcd_clear(void);
void lcd_init(void);
void lcdLoc(int);
void lcd_toggle_enable(int);
void lcd_byte(int, int);
void lcd_setCursor(int, int);
void lcd_print(const char *, bool);


/* Macros utilisation afficheur I2C */

#define  I2C_ADDR 0x3F
#define  LCD_CHR 1
#define  LCD_CMD 0
#define  LINE1 0x80
#define  LINE2 0xC0
#define  LCD_BACKLIGHT 0x08
#define  ENABLE 0b00000100


/* Prototypes fonctions Pjsip */

static void on_app(void);
static void on_comm(void);
void endCall(void);
static void on_call_media_state(pjsua_call_id);
static int error_exit(const char *, pj_status_t);
static void call_on_dtmf_callback(pjsua_call_id, int);
static void on_call_state(pjsua_call_id, pjsip_event *);
static void on_incoming_call(pjsua_acc_id, pjsua_call_id, pjsip_rx_data *);

char *intToArray(int);
void buzzer_execute(int);

#define  THIS_FILE "device_Pjsip.c"

enum AccountType {
    CLIENT = 0, OVH, ASTERISK, DIESE
};

/* Compte SIP */
typedef struct s_Account {
    pjsua_acc_id acc_id;
    enum AccountType type;
} Account;

/* Parametres PJSip */
typedef struct s_PJParametres {
    Account account[4];
    pjsua_call_id id_call[2];
    int vid_idx;
    pjsua_vid_win_id wid;
} PJParametres;

clock_t temps;

/* File Descriptor des drivers */
typedef struct s_Drivers {
    int relais;
    int buzzer;
    int lcd;
} Drivers;

/* Parametres ouverture de porte */
typedef struct s_TempsPorte {
    int porte1;
    int porte2;
} TempsPorte;

/* Parametres touches DTMF */
typedef struct s_TouchesDtmf {
    char ouverturePorte1;
    char ouverturePorte2;
    char prolongerAppel;
    char raccrocherAppel;
} TouchesDtmf;

/* Resident */
typedef struct s_Resident {
    char *code;
    char *nom;
} Resident;

/* Parametres d'appel */
typedef struct s_Appel {
    int tempsAppel;
    int tempsCommunication;
    Resident resident;
    double tempsActuel;
    double tempsDebut;
    bool prolonger;
} Appel;

/* Etat de l'appel */
typedef struct s_Etat {
    bool destroy;
    bool porteOuverte;
    bool statuscom;
    bool on_commu;
    bool statusdiscon;
    bool hasBeenConnected;
} Etat;

int destroy = 0, on_open = 0, statuscom = 0, on_commu = 0, statusdiscon = 0;

const size_t MAX_SIP_ID_LENGTH = 50;
const size_t MAX_SIP_REG_URI_LENGTH = 50;
