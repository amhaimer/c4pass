#include <jni.h>
#include <string.h>
#include <unistd.h>  
#include <fcntl.h>   
#include <termios.h>
#include <errno.h>
#include "devices_Lecteur.h"

/*                                                                                                
 * Class:     devices_Lecteur                                                                     
 * Method:    init                                                                                
 * Signature: ()I                                                                                 
 */
JNIEXPORT jint JNICALL Java_devices_Lecteur_init
(JNIEnv *env, jobject obj) {
    char buf[35];
    int file = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY);
    return file;
}

/*                                                                                                
 * Class:     devices_Lecteur                                                                     
 * Method:    getTrameJNI                                                                         
 * Signature: ()Ljava/lang/String;                                                                
 */
JNIEXPORT jstring JNICALL Java_devices_Lecteur_getTrameJNI
(JNIEnv *env, jobject obj) {
    jint handle;
    jclass poussoir = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, poussoir, "handle", "I");

    if (fidHandle == NULL)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);
    if (handle == -1)
        return NULL;

    char buff[27] = "";
    char str_tmp[27] = "";
    unsigned char rx_buffer[27];
    int rx_length = read(handle, (void*) rx_buffer, 27);
    int i, j;
    if (rx_length > 0) {
        printf("%d\n", rx_length);
        rx_buffer[rx_length] = '\0';
        for (i = 0; rx_buffer[i] != '\0'; i++) {
            while (!((rx_buffer[i] >= '0' && rx_buffer[i] <= '9') ||
                    (rx_buffer[i] >= 'A' && rx_buffer[i] <= 'Z') ||
                    (rx_buffer[i] == '>') ||
                    (rx_buffer[i] == '\0'))) {
                for (j = i; rx_buffer[j] != '\0'; j++)
                    rx_buffer[j] = rx_buffer[j + 1];
                rx_buffer[j] = '\0';
            }
        }
        if (strlen(rx_buffer) < 27 || strncmp(rx_buffer, "0000000000", 10)) {
            strcpy(rx_buffer, "");
            return NULL;
        }
        char buf[27];

        strncpy(buf, rx_buffer, 27);
        buf[27] = '\0';
        printf("\nLength: %d\n", rx_length);
        printf("\nTrame: %s\n", buf);
        strcpy(rx_buffer, "");

        strcat(str_tmp, rx_buffer);
        char * position = strchr(str_tmp, '\n');
        if (position > 0) {
            strncpy(buff, str_tmp, position - &str_tmp[0]);
            position = 0;
            strcpy(buff, "");
            strcpy(str_tmp, "");
            puts(rx_buffer);
            return (*env)->NewStringUTF(env, rx_buffer);
        }
    }
    return NULL;
}
