#include "devices_Diodes.h"

/*
 * Class:     devices_Diodes
 * Method:    execute
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Diodes_execute
(JNIEnv *env, jobject obj, jboolean status) {
    jint handle;
    jclass buzzer = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, buzzer, "handle", "I");

    if (fidHandle == NULL)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);
    write(handle, status ? "1" : "0", 1);
}

/*
 * Class:     devices_Diodes
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Diodes_exit
(JNIEnv *env, jobject obj) {
    int handle;
    jclass buzzer = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, buzzer, "handle", "I");

    if (fidHandle == NULL)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);
    close(handle);
}

/*
 * Class:     devices_Diodes
 * Method:    init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Diodes_init
(JNIEnv *env, jobject obj) {
    int file = open("/dev/diodes", 0x02);
    if (file < 0)
        printf("[C] : error");
    return file;
}
