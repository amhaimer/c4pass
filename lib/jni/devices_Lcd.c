#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>
#include "devices_Lcd.h"

// Define some device parameters
//#define I2C_ADDR   0x27 // I2C device address
#define I2C_ADDR   0x3F // I2C device address


// Define some device constants
#define LCD_CHR  1 // Mode - Sending data
#define LCD_CMD  0 // Mode - Sending command

#define LINE1  0x80 // 1st line
#define LINE2  0xC0 // 2nd line

#define LCD_BACKLIGHT   0x08  // On
// LCD_BACKLIGHT = 0x00  # Off

#define ENABLE  0b00000100 // Enable bit

void lcd_init(void);
void lcd_byte(int bits, int mode);
void lcd_toggle_enable(int bits);

// added by Lewis
void setCursor(int ligne, int collumn);
void lcdLoc(int line); //move cursor
void ClrLcd(void); // clr LCD return home
int fd;

void lcd_byte(int bits, int mode) {

    //Send byte to data pins
    // bits = the data
    // mode = 1 for data, 0 for command
    int bits_high;
    int bits_low;
    // uses the two half byte writes to LCD
    bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT;
    bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT;

    // High bits
    wiringPiI2CReadReg8(fd, bits_high);
    lcd_toggle_enable(bits_high);

    // Low bits
    wiringPiI2CReadReg8(fd, bits_low);
    lcd_toggle_enable(bits_low);
}

void lcdLoc(int line) {
    lcd_byte(line, LCD_CMD);
}

void lcd_toggle_enable(int bits) {
    // Toggle enable pin on LCD display
    delayMicroseconds(500);
    wiringPiI2CReadReg8(fd, (bits | ENABLE));
    delayMicroseconds(500);
    wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
    delayMicroseconds(500);
}

/*
 * Class:     devices_Lcd
 * Method:    init
 * Signature: ()I
 */


JNIEXPORT jint JNICALL Java_devices_Lcd_init
(JNIEnv *env, jobject obj) {
    fd = wiringPiI2CSetup(I2C_ADDR);
    // Initialise display
    lcd_byte(0x33, LCD_CMD); // Initialise
    lcd_byte(0x32, LCD_CMD); // Initialise
    lcd_byte(0x06, LCD_CMD); // Cursor move direction
    lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
    lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
    lcd_byte(0x01, LCD_CMD); // Clear display
    delayMicroseconds(500);

}

JNIEXPORT jint JNICALL Java_devices_Lcd_initS
(JNIEnv *env, jobject obj) {

    // Initialise display
    lcd_byte(0x33, LCD_CMD); // Initialise
    lcd_byte(0x32, LCD_CMD); // Initialise
    lcd_byte(0x06, LCD_CMD); // Cursor move direction
    lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
    lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
    lcd_byte(0x01, LCD_CMD); // Clear display
    delayMicroseconds(500);

}

/*
 * Class:     devices_Lcd
 * Method:    blinkMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_blinkMode
(JNIEnv *env, jobject obj, jboolean b) {
    if (b == JNI_TRUE)
        lcd_byte(0x0F, LCD_CMD);
    else
        lcd_byte(0x0C, LCD_CMD);

}

/*
 * Class:     devices_Lcd
 * Method:    clearLcd
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_clearLcd
(JNIEnv *env, jobject obj) {
    lcd_byte(0x01, LCD_CMD);
    lcd_byte(0x02, LCD_CMD);

}

/*
 * Class:     devices_Lcd
 * Method:    setCursor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_setCursor
(JNIEnv *env, jobject obj, jint ligne, jint collumn) {
    int position = 0;
    if (ligne == 1)
        position = LINE1;
    else
        position = LINE2;
    position = position + collumn;
    //printf("%p\n",position);
    lcdLoc(position);
}

/*
 * Class:     devices_Lcd
 * Method:    print
 * Signature: (Ljava/lang/String;Z)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_print
(JNIEnv *env, jobject obj, jstring string, jboolean b) {

    if (b == JNI_TRUE) {
        lcd_byte(0x01, LCD_CMD);
        lcd_byte(0x02, LCD_CMD);

    }
    const char *str = (*env)->GetStringUTFChars(env, string, 0);
    while (*str) {
        if (*(str) == '\n') {
            lcdLoc(LINE2);
            *(str++);

        } else
            lcd_byte(*(str++), LCD_CHR);

    }

}

JNIEXPORT void JNICALL Java_devices_Lcd_saisie
(JNIEnv *env, jobject obj, jboolean b) {
    if (b == JNI_TRUE)
        lcd_byte(0x0F, LCD_CMD);
    else
        lcd_byte(0x0C, LCD_CMD);

}
