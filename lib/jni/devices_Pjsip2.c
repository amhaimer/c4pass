#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <pjsua-lib/pjsua.h>
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

#include "devices_Pjsip2.h"

#define THIS_FILE "XCPjsua.c"

#define I2C_ADDR 0x27
#define LCD_CHR 1
#define LCD_CMD 0
#define LINE1 0x80
#define LINE2 0xC0
#define LCD_BACKLIGHT 0x08
#define ENABLE 0b00000100

void lcd_init(void);
void lcd_byte(int bits, int mode);
void lcd_toggle_enable(int bits);
void lcd_print(const char *str, bool clear);
void lcd_setCursor(int line, int column);
void lcdLoc(int line);
void ClrLcd(void);
void buzzer_execute(int n);
clock_t temps;
char *intToArray(int nb);

static pjsua_acc_id acc_id;
static pjsua_call_id id_call;
static int vid_idx;
static pjsua_vid_win_id wid;

static int relais = -1, buzzer = -1, lcd = -1;
int destroy = 0, on_open = 0, statuscom = 0, on_commu = 0, statusdiscon = 0;
static char _dtmf1, _dtmf2, *_resident;
static int _tempsOuverture1, _tempsOuverture2, _tempsComm, _tempsAppel, _code2;

const size_t MAX_SIP_ID_LENGTH = 50;
const size_t MAX_SIP_REG_URI_LENGTH = 50;

static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata);
static void call_on_dtmf_callback(pjsua_call_id call_id, int dtmf);
static void on_call_state(pjsua_call_id call_id, pjsip_event *e);
static void on_call_media_state(pjsua_call_id call_id);
static void error_exit(const char *title, pj_status_t status);
static void on_comm();
static void on_app();

/*
 * Class:     devices_Pjsip
 * Method:    register
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip2_register
(JNIEnv *env, jobject obj, jstring user, jstring passwd, jstring domain) {
    const char *_user = (*env)->GetStringUTFChars(env, user, 0);
    const char *_passwd = (*env)->GetStringUTFChars(env, passwd, 0);
    const char *_domain = (*env)->GetStringUTFChars(env, domain, 0);

    relais = open("/dev/relais", 0x02);
    buzzer = open("/dev/buzzer", 0x02);
    lcd_init();

    pj_status_t status;
    status = pjsua_create();
    if (status != PJ_SUCCESS)
        error_exit("Error in pjsua_create()", status);

    // Initialise la librairie
    {
        pjsua_config cfg;
        pjsua_config_default(&cfg);
        cfg.cred_info[0].realm = pj_str((char *) "*");
        cfg.cred_info[0].scheme = pj_str((char *) "digest");
        cfg.cb.on_incoming_call = &on_incoming_call;
        cfg.cb.on_call_media_state = &on_call_media_state;
        cfg.cb.on_call_state = &on_call_state;
        cfg.cb.on_dtmf_digit = &call_on_dtmf_callback;
        pjsua_logging_config log_cfg;
        pjsua_logging_config_default(&log_cfg);
        log_cfg.console_level = 4;
        status = pjsua_init(&cfg, &log_cfg, NULL);
        if (status != PJ_SUCCESS)
            error_exit("Error in pjsua_init()", status);
    }

    // Ajoute le transport UDP
    {
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        cfg.port = 0;
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
        if (status != PJ_SUCCESS)
            error_exit("Error creating transport", status);
    }
    // Ajoute le transport TCP
    {
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        cfg.port = 0;
        status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &cfg, NULL);
        if (status != PJ_SUCCESS)
            error_exit("Error creating transport", status);
    }
    status = pjsua_start();
    if (status != PJ_SUCCESS)
        error_exit("Error starting pjsua", status);

    // Enregistre le compte SIP sur le serveur
    {
        pjsua_acc_config cfg;
        pjsua_acc_config_default(&cfg);

        cfg.vid_in_auto_show = PJ_TRUE;

        char sipId[MAX_SIP_ID_LENGTH];
        sprintf(sipId, "sip:%s@%s", _user, _domain);
        cfg.id = pj_str(sipId);
        char regUri[MAX_SIP_REG_URI_LENGTH];
        sprintf(regUri, "sip:%s", _domain);
        cfg.reg_uri = pj_str(regUri);
        cfg.cred_count = 1;
        cfg.cred_info[0].realm = pj_str((char *) "*");
        //cfg.cred_info[0].realm = pj_str((char *)_domain);
        cfg.cred_info[0].scheme = pj_str("digest");
        cfg.cred_info[0].username = pj_str((char *) _user);
        cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
        cfg.cred_info[0].data = pj_str((char *) _passwd);
        status = pjsua_acc_add(&cfg, PJ_TRUE, &acc_id);
        if (status != PJ_SUCCESS)
            error_exit("Error adding account", status);
    }

    (*env)->ReleaseStringUTFChars(env, user, _user);
    (*env)->ReleaseStringUTFChars(env, passwd, _passwd);
    (*env)->ReleaseStringUTFChars(env, domain, _domain);

    return 0;
}

/*
 * Class:     devices_Pjsip
 * Method:    makeCall
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip2_makeCall
(JNIEnv *env, jobject obj, jstring uri) {

    char *_uri = (*env)->GetStringUTFChars(env, uri, 0);
    pj_status_t status;
    pj_str_t pj_uri = pj_str(_uri);

    if (pjsua_verify_sip_url(_uri) != 0)
        return;
    status = pjsua_call_make_call(acc_id, &pj_uri, 0, NULL, NULL, NULL);
    if (status != PJ_SUCCESS)
        error_exit("Error making call", status);

    char outcode[4];

    sprintf(outcode, "%d", _code2);

    lcd_setCursor(LINE1, 0);
    lcd_print(_resident, true);
    lcd_setCursor(LINE2, 0);
    lcd_print("  APPEL DU ", false);
    if (_code2 < 10) {
        lcd_print("00", false);
        lcd_print(outcode, false);
    } else {
        lcd_print(outcode, false);
    }

    //on_app();

}

void on_app() {
    // pour temps d'appel 
    double t11 = (double) time(NULL);
    double t22 = t11;
    char outcode[4];

    sprintf(outcode, "%d", _code2);




    while ((double) time(NULL) - t11 < _tempsAppel && on_commu == 0 && on_open == 0) {

        if (t22 != (double) time(NULL) && on_commu == 0 && on_open == 0) {
            t22 = (double) time(NULL);

            //lcd_print("En Appel ", false);
            char output1[3];

            if ((double) time(NULL) - t11 < 10)
                snprintf(output1, 2, "%f", (double) time(NULL) - t11);
            else
                snprintf(output1, 3, "%f", (double) time(NULL) - t11);
            lcd_setCursor(LINE1, 0);
            lcd_print(_resident, true);

            lcd_setCursor(LINE2, 0);
            lcd_print("APPEL ", false);
            if (_code2 < 10) {
                lcd_print("00", false);
                lcd_print(outcode, false);
            } else {
                lcd_print(outcode, false);
            }
            lcd_print(" ", false);
            lcd_print(output1, false);
            lcd_print("s", false);
        }

    }

}

/*
 * Class:     devices_Pjsip
 * Method:    destroy
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip2_destroy
(JNIEnv *env, jobject obj) {
    destroy = 1;
    statuscom = 0;
    //statusdiscon=1;
    pjsua_destroy();
}

JNIEXPORT jint JNICALL Java_devices_Pjsip2_status
(JNIEnv *env, jobject obj) {
    return statuscom;
}

JNIEXPORT jint JNICALL Java_devices_Pjsip2_statusdisconnected
(JNIEnv *env, jobject obj) {
    return statusdiscon;
}

void endCall() {
    pjsua_call_hangup_all();
}

static void on_call_media_state(pjsua_call_id call_id) {
    pjsua_call_info ci;

    pjsua_call_get_info(call_id, &ci);

    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
        pjsua_conf_connect(ci.conf_slot, 0);
        pjsua_conf_connect(0, ci.conf_slot);

    }

}

static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata) {
    pjsua_call_info ci;

    PJ_UNUSED_ARG(acc_id);
    PJ_UNUSED_ARG(rdata);
    pjsua_call_get_info(call_id, &ci);
    wid = ci.media[vid_idx].stream.vid.win_in;
    PJ_LOG(3, (THIS_FILE, "WID:%d", wid));
    PJ_LOG(3, (THIS_FILE, "Incoming call from %.*s!!",
            (int) ci.remote_info.slen,
            ci.remote_info.ptr));

    printf("%s", ci.remote_info.ptr);
    id_call = call_id;
}

static void on_call_state(pjsua_call_id call_id, pjsip_event *e) {
    pjsua_call_info ci;

    PJ_UNUSED_ARG(e);

    pjsua_call_get_info(call_id, &ci);
    PJ_LOG(3, (THIS_FILE, "Call %d state=%.*s", call_id,
            (int) ci.state_text.slen,
            ci.state_text.ptr));
    temps = clock();



    if (ci.state == 5) {
        statuscom = 1;

        destroy = 0;
        ClrLcd();
        on_comm();

    }

    /*if(ci.state==6)
    {
   
    destroy=1;
    }*/
    if (ci.state == 6) {
        statusdiscon = 1;
        //endCall(); 
        destroy = 1;
    }
}

static void on_comm() {
    on_commu = 1;
    double t1 = (double) time(NULL);
    double t2 = t1;
    char outcode[4];

    sprintf(outcode, "%d", _code2);

    while ((double) time(NULL) - t1 < _tempsComm && destroy == 0) {

        if (t2 != (double) time(NULL) && on_open == 0) {
            t2 = (double) time(NULL);
            printf(" temps ecoulé:%f", (double) time(NULL) - t1);
            if (_tempsComm - ((double) time(NULL) - t1) > 5) {
                lcd_setCursor(LINE1, 0);
                lcd_print(_resident, false);
                lcd_setCursor(LINE2, 0);
                // lcd_print("123", false);
                //lcd_print("  ", false);
                if (_code2 < 10) {
                    lcd_print("00", false);
                    lcd_print(outcode, false);
                } else {
                    lcd_print(outcode, false);
                }
                lcd_print("--PARLEZ-", false);
                char output[3];

                /*if((double)time(NULL)-t1<10)
                snprintf(output,2,"%f",(double)time(NULL)-t1);
                else*/
                snprintf(output, 3, "%f", _tempsComm - ((double) time(NULL) - t1));
                lcd_print(output, false);
                lcd_print("s", false);
            } else {
                lcd_setCursor(LINE1, 0);
                lcd_print("-----FIN DE-----", false);
                lcd_setCursor(LINE2, 0);
                lcd_print("LA COMMUNICATION", false);
            }
        }

    }
    on_commu = 0;
}

char *intToArray(int nb) {
    int n = log10(nb) + 1, i;
    char *str = calloc(n, sizeof (char));
    i = n;
    for (i = n; i > 0; --i, nb /= 10)
        str[i - 1] = nb % 10 + '0';
    return str;
}

void ouverture_porte(char dtmf) {
    on_open = 1;
    write(relais, dtmf == _dtmf1 ? "1" : "3", sizeof ("1"));
    lcd_print(dtmf == _dtmf1 ? "Porte 1 Ouverte" : "Porte 2 Ouverte", true);
    write(buzzer, "400", sizeof ("400"));
    int current = 0;
    for (current = dtmf == _dtmf1 ? _tempsOuverture1 : _tempsOuverture2;
            current > 0; current--) {

        lcd_setCursor(LINE2, 0);
        char *test = intToArray(current);
        write(buzzer, "0", sizeof ("0"));
        lcd_print(test, false);
        lcd_print(" secondes", false);
        sleep(1);
    }
    write(buzzer, "800", sizeof ("800"));
    write(relais, dtmf == _dtmf1 ? "0" : "2", sizeof ("0"));
    //  lcd_print(_resident, true);
    //  lcd_setCursor(LINE2, 0);
    // lcd_print("Appel en cours", false);
    ClrLcd();
    on_open = 0;
}

static void call_on_dtmf_callback(pjsua_call_id call_id, int dtmf) {
    PJ_LOG(3, (THIS_FILE, "Incoming DTMF on call %d: %c", call_id, dtmf));
    if (dtmf == _dtmf1 || dtmf == _dtmf2)

        ouverture_porte(dtmf);

}

static void error_exit(const char *title, pj_status_t status) {
    pjsua_perror(THIS_FILE, title, status);
    pjsua_destroy();
    exit(1);
}

/*
 * Class:     devices_Pjsip
 * Method:    init_params
 * Signature: (CCIILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip2_init_1params
(JNIEnv *env, jobject obj, jchar dtmf1, jchar dtmf2, jint tempsOuverture1,
        jint tempsOuverture2, jstring resident, jint code2, jint tempsComm, jint tempsAppel) {
    _resident = (*env)->GetStringUTFChars(env, resident, 0);
    _code2 = code2;
    _dtmf1 = dtmf1;
    _dtmf2 = dtmf2;
    _tempsOuverture1 = tempsOuverture1;
    _tempsOuverture2 = tempsOuverture2;
    _tempsComm = tempsComm;
    _tempsAppel = tempsAppel;
}

void lcd_init() {
    lcd = wiringPiI2CSetup(I2C_ADDR);
    lcd_byte(0x33, LCD_CMD);
    lcd_byte(0x32, LCD_CMD);
    lcd_byte(0x06, LCD_CMD);
    lcd_byte(0x0C, LCD_CMD);
    lcd_byte(0x28, LCD_CMD);
    lcd_byte(0x01, LCD_CMD);
    delayMicroseconds(500);
}

void ClrLcd() {
    lcd_byte(0x01, LCD_CMD);
    lcd_byte(0x02, LCD_CMD);
}

void lcd_setCursor(int line, int column) {
    lcdLoc(line + column);
}

void lcd_clear() {
    lcd_byte(0x01, LCD_CMD);
    lcd_byte(0x02, LCD_CMD);
}

void lcd_print(const char *str, bool clear) {
    if (clear)
        lcd_clear();
    while (*str)
        if (*(str) == '\n') {
            lcdLoc(LINE2);
            *(str++);
        } else
            lcd_byte(*(str++), LCD_CHR);
}

void lcd_toggle_enable(int bits) {
    delayMicroseconds(500);
    wiringPiI2CReadReg8(lcd, (bits | ENABLE));
    delayMicroseconds(500);
    wiringPiI2CReadReg8(lcd, (bits & ~ENABLE));
    delayMicroseconds(500);
}

void lcd_byte(int bits, int mode) {
    int bits_high, bits_low;

    bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT;
    bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT;
    wiringPiI2CReadReg8(lcd, bits_high);
    lcd_toggle_enable(bits_high);
    wiringPiI2CReadReg8(lcd, bits_low);
    lcd_toggle_enable(bits_low);
}

void lcdLoc(int line) {
    lcd_byte(line, LCD_CMD);
}

