#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include "devices_Poussoir.h"

/*
 * Class:     devices_Poussoir
 * Method:    init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Poussoir_init
(JNIEnv *env, jobject obj) {
    int file = open("/dev/poussoir", 0x02);
    if (file < 0)
        printf("[C] : error");
    return file;
}

/*
 * Class:     devices_Poussoir
 * Method:    status
 * Signature: ()Z
 */
JNIEXPORT jstring JNICALL Java_devices_Poussoir_status
(JNIEnv *env, jobject obj) {
    jint handle;
    char buff[1];
    jclass poussoir = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, poussoir, "handle", "I");

    if (fidHandle == NULL)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);
    read(handle, buff, 1);
    buff[1] = '\0';
    return (*env)->NewStringUTF(env, buff);

}

/*
 * Class:     devices_Poussoir
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Poussoir_exit
(JNIEnv *env, jobject obj) {
    jint handle;
    jclass poussoir = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, poussoir, "handle", "I");

    if (fidHandle == NULL)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);
    close(handle);
}
