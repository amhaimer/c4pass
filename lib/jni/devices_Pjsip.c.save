#rtpinclude "DiesePjsip.h"
#include "devices_Pjsip.h"
#include "devices_Pjsip_AccountType.h"

static PJParametres     parametres;
static Drivers          drivers;
static TouchesDtmf      dtmf;
static TempsPorte       tempsPorte;
static Appel            appel;
static Etat		etat;


bool			initialized = false;
static int current_account = 0;
static int nbcalls = 0;
static bool addtime = false;
static int porteOuverte = 0;

/*
 * Class:     devices_Pjsip
 * Method:    register
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip_register
(JNIEnv *env, jobject obj, jstring user, jstring passwd, jstring domain, jint type)
{
  pj_status_t status;

  const char *_user = (*env)->GetStringUTFChars(env, user, 0);
  const char *_passwd = (*env)->GetStringUTFChars(env, passwd, 0);
  const char *_domain = (*env)->GetStringUTFChars(env, domain, 0);

  if (!initialized && Pjsip_initialize() == 1)
    return 1;

  // Enregistre le compte SIP sur le serveur
  {
    pjsua_acc_config cfg;
    char *userC =  (char *)_user;
    char sipId[MAX_SIP_ID_LENGTH];
    char regUri[MAX_SIP_REG_URI_LENGTH];
    pj_str_t codec_s;

    pjsua_acc_config_default(&cfg);
    cfg.vid_out_auto_transmit = PJ_TRUE;
    //    cfg.vid_in_auto_show = PJ_TRUE;
    sprintf(sipId, "sip:%s@%s", _user, _domain);
    cfg.id = pj_str(sipId);
    sprintf(regUri, "sip:%s", _domain);
    cfg.reg_uri = pj_str(regUri);
    cfg.cred_count = 1;
    cfg.cred_info[0].realm = pj_str((char *)"*");
    //cfg.cred_info[0].realm = pj_str((char *)_domain);
    cfg.cred_info[0].scheme = pj_str("digest");
    cfg.cred_info[0].username = pj_str((char *)_user);
    cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    cfg.cred_info[0].data = pj_str((char *)_passwd);
 

    pjsua_codec_set_priority(pj_cstr(&codec_s, "speex/16000/1"), PJMEDIA_CODEC_PRIO_HIGHEST);
    
    if (current_account < 4) {
      if(current_account == 3){
		current_account--;
		pjsua_acc_del(parametres.account[current_account].acc_id);
		printf("le compte courrant %s",userC);
      }
      parametres.account[current_account].type = (enum AccountType)type;
      status = pjsua_acc_add(&cfg, PJ_TRUE, &parametres.account[current_account].acc_id);
      if (status != PJ_SUCCESS)
	return error_exit("Error adding account", status);
      current_account++;
    }
    else
      return error_exit("More than 4 account configured", 0);
  }

  (*env)->ReleaseStringUTFChars(env, user, _user);
  (*env)->ReleaseStringUTFChars(env, passwd, _passwd);
  (*env)->ReleaseStringUTFChars(env, domain, _domain);
  return 0;
}

int Pjsip_initialize() {
  drivers.relais = open("/dev/relais", 0x02);
  drivers.buzzer = open("/dev/buzzer", 0x02);
  lcd_init();

  pj_status_t status;
  status = pjsua_create();
  if (status != PJ_SUCCESS)
    error_exit("Error in pjsua_create()", status);

  // Initialise la librairie
  {
    pjsua_config cfg;
    pjsua_config_default (&cfg);
    cfg.cred_info[0].realm = pj_str((char *)"*");
    cfg.cred_info[0].scheme = pj_str((char *)"digest");
    cfg.cb.on_incoming_call = &on_incoming_call;
    cfg.cb.on_call_media_state = &on_call_media_state;
    cfg.cb.on_call_state = &on_call_state;
    cfg.cb.on_dtmf_digit = &call_on_dtmf_callback;
    pjsua_logging_config log_cfg;
    pjsua_logging_config_default(&log_cfg);
    log_cfg.console_level = 4;
    status = pjsua_init(&cfg, &log_cfg, NULL);
    if (status != PJ_SUCCESS)
      return error_exit("Error in pjsua_init()", status);
  }

  // Ajoute le transport UDP
  {
    pjsua_transport_config cfg;
    pjsua_transport_config_default(&cfg);
    cfg.port = 0;
    status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
    if (status != PJ_SUCCESS)
      return error_exit("Error creating transport", status);
  }
  // Ajoute le transport TCP
  /*
    {
    pjsua_transport_config cfg;
    pjsua_transport_config_default(&cfg);
    cfg.port =0;
    status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &cfg, NULL);
    if (status != PJ_SUCCESS)
      return error_exit("Error creating transport", status);
  }
  */
  status = pjsua_start();
  if (status != PJ_SUCCESS)
    return error_exit("Error starting pjsua", status);

  initialized = true;
  return 0;
}

/*
 * Class:     devices_Pjsip
 * Method:    resetAddTime
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_resetAddTime
(JNIEnv *env, jobject obj)
{
  addtime = false;
}

/*
 * Class:     devices_Pjsip
 * Method:    addTime
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_addTime
(JNIEnv *env, jobject obj)
{
  return addtime;
}

/*
 * Class:     devices_Pjsip
 * Method:    resetOuverture
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_resetOuverture
(JNIEnv *env, jobject obj)
{
  porteOuverte = 0;
}

/*
 * Class:     devices_Pjsip
 * Method:    ouverturePorte
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip_ouverturePorte
(JNIEnv *env, jobject obj)
{
  return porteOuverte;
}

/*
 * Class:     devices_Pjsip
 * Method:    connectedToServer
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_connectedToServer
(JNIEnv *env, jobject obj, jint type)
{
  int i;
  pjsua_acc_info info;

  for (i = 0;i < current_account;i++) {
    pjsua_acc_get_info(parametres.account[i].acc_id, &info);
    if (parametres.account[i].type == (enum AccountType)type && info.status == 200)
      return true;
  }
  return false;
}

/*
 * Class:     devices_Pjsip
 * Method:    onCommunication
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_onCommunication
(JNIEnv *env, jobject obj)
{
  return etat.on_commu;
}

void communicationEnCours() {
  appel.tempsDebut = (double)time(NULL);
  appel.tempsActuel = appel.tempsDebut;

  while ((double)time(NULL) - appel.tempsDebut < appel.tempsCommunication && etat.on_commu == 1) {
    if (appel.tempsActuel != (double)time(NULL) && etat.porteOuverte == 0) {
      appel.tempsActuel=(double)time(NULL);
      double tempsRestant = appel.tempsCommunication - ((double)time(NULL) - appel.tempsDebut);
      char output[3];
      snprintf(output, tempsRestant > 9 ? 3 : 2,"%f", tempsRestant);
      printf(output);
      if(tempsRestant > 9 && etat.on_commu)
	{
	  if (tempsRestant == appel.tempsCommunication - 1) {
	    lcd_print(appel.resident.nom, true);
	    lcd_setCursor(LINE2, 0);
	    lcd_print(appel.resident.code,false);
	    lcd_print(" PARLEZ ",false);
	    lcd_setCursor(LINE2, 16);
	    lcd_print("s", false);
	  }
	  lcd_setCursor(LINE2, 11);
	  lcd_print(output, false);
	  lcd_print("s", false);
	}
      else {
	if (tempsRestant == 9) {
	  write(drivers.buzzer, "200", sizeof("200"));
	  lcd_print("COMMUNICATION\nTERMINEE DANS ", true);
	  lcd_setCursor(LINE2, 15);
	  lcd_print("s", false);
	}
	lcd_setCursor(LINE2, 14);
	lcd_print(output, false);
      }
    }
  }
  endCall();
  etat.on_commu = false;
}

void appelEnCours(void) {
  double tempsDebut = (double)time(NULL);
  double tempsActuel = tempsDebut;

  while ((double)time(NULL) - tempsDebut < appel.tempsAppel && etat.on_commu == 0 && !statusdiscon) {
    if(tempsActuel != (double)time(NULL) && etat.on_commu == 0 && etat.porteOuverte == 0) {
      char secondes[3];

      tempsActuel = (double)time(NULL);
      snprintf(secondes,3,"%f",(double)time(NULL) - tempsDebut);
      lcd_setCursor(LINE1, 0);
      lcd_print(appel.resident.nom, true);
      lcd_setCursor(LINE2, 0);
      lcd_print("APPEL ", false);
      lcd_print(appel.resident.code, false);
      lcd_print(" ", false);
      lcd_print(secondes, false);
      lcd_print("s", false);
    }
  }
  if (etat.on_commu)
    communicationEnCours();
  endCall();
}

/*
 * Class:     devices_Pjsip
 * Method:    makeCall
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_makeCall
(JNIEnv *env, jobject obj, jstring uri, jint type, jstring uri2, jint type2)
{
  char *_uri = (char *)(*env)->GetStringUTFChars(env, uri, 0);
  char *_uri2 = (char *)(*env)->GetStringUTFChars(env, uri2, 0);
  pj_str_t pj_uri = pj_str(_uri);
  pj_str_t pj_uri2 = pj_str(type2 != -1 ? _uri2 : "");
  int acc_id = getAccount(type);
  int acc_id2 = getAccount(type2);
  pjsua_call_info info;

  nbcalls = type2 == -1 ? 1 : 2;
  statusdiscon = 0;
  etat.hasBeenConnected = 0;
  if ((pjsua_verify_sip_url(_uri) != 0) || type2 != -1 && pjsua_verify_sip_url(_uri2) != 0)
    return;
  if (pjsua_call_make_call(acc_id, &pj_uri, 0, NULL, NULL, &parametres.id_call[0]) != PJ_SUCCESS)
    return;
  if (type2 != -1)
    if (pjsua_call_make_call(acc_id2, &pj_uri2, 0, NULL, NULL, &parametres.id_call[1]) != PJ_SUCCESS)
      return;
  write(drivers.buzzer, "400", sizeof("400"));
  //appelEnCours();
}

int getAccount(int type) {
  int id;

  for (id = 0;id < 4;id++)
    if (type == parametres.account[id].type)
      return id;
  return -1;
}

/*
 * Class:     devices_Pjsip
 * Method:    destroy
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip_destroy
(JNIEnv *env, jobject obj)
{
  destroy=1;
  statuscom=0;
  //statusdiscon=1;
  pjsua_destroy();
}

JNIEXPORT jint JNICALL Java_devices_Pjsip_hasBeenConnected
(JNIEnv *env, jobject obj)
{
  return etat.hasBeenConnected;
}

JNIEXPORT jint JNICALL Java_devices_Pjsip_statusdisconnected
(JNIEnv *env, jobject obj)
{
  return statusdiscon;
}

/*
 * Class:     devices_Pjsip
 * Method:    endCall
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_endCall
(JNIEnv *env, jobject obj)
{
  pjsua_call_hangup_all();
}

void endCall()
{
  pjsua_call_hangup_all();
}

static void on_call_media_state(pjsua_call_id call_id) {
  pjsua_call_info ci;

  pjsua_call_get_info(call_id, &ci);
  if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
    pjsua_conf_connect(ci.conf_slot, 0);
    pjsua_conf_connect(0, ci.conf_slot);
  }
}

static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata) {
  pjsua_call_info ci;

  PJ_UNUSED_ARG(acc_id);
  PJ_UNUSED_ARG(rdata);
  pjsua_call_get_info(call_id, &ci);
  parametres.wid = ci.media[parametres.vid_idx].stream.vid.win_in;
  PJ_LOG(3,(THIS_FILE, "WID:%d", parametres.wid));
  PJ_LOG(3,(THIS_FILE, "Incoming call from %.*s!!",
	    (int)ci.remote_info.slen,
	    ci.remote_info.ptr));

  printf("%s", ci.remote_info.ptr);
}

static void on_call_state(pjsua_call_id call_id, pjsip_event *e) {
  pjsua_call_info ci;
  pj_status_t status;
  static int nbdiscon = 0;

  PJ_UNUSED_ARG(e);
  pjsua_call_get_info(call_id, &ci);
  PJ_LOG(3,(THIS_FILE, "Call %d state=%.*s", call_id,
	    (int)ci.state_text.slen,
	    ci.state_text.ptr));

  if(ci.state == 5) {
    if (nbcalls == 2)
      pjsua_call_hangup(call_id == parametres.id_call[0] ? parametres.id_call[1] : parametres.id_call[0], 0, NULL, NULL);
    etat.hasBeenConnected = true;
    etat.on_commu = true;
  }
  else if(ci.state == PJSIP_INV_STATE_DISCONNECTED) {
    nbdiscon++;

    if (nbdiscon >= nbcalls) {
      statusdiscon = 1;
      etat.on_commu = false;
      write(drivers.buzzer, "800", sizeof("800"));
      nbdiscon = 0;
    }
  }
}

char *intToArray(int nb) {
  int n = log10(nb) + 1, i;
  char *str = calloc(n, sizeof(char));
  i = n;
  for (i = n; i > 0; --i, nb /= 10)
    str[i - 1] = nb % 10 + '0';
  return str;
}

void ouverture_porte(int touche) {
  etat.porteOuverte = 1;
  write(drivers.relais, touche == dtmf.ouverturePorte1 ? "1" : "3", sizeof("1"));
  lcd_print(touche == dtmf.ouverturePorte1 ? "Porte 1 Ouverte" : "Porte 2 Ouverte", true);
  write(drivers.buzzer, "400", sizeof("400"));
  int current = 0;
  for (current = touche == dtmf.ouverturePorte1 ? tempsPorte.porte1 : tempsPorte.porte2;
       current > 0; current--) {

    lcd_setCursor(LINE2,0);
    char *temps = intToArray(current);
    write(drivers.buzzer, "0", sizeof("0"));
    lcd_print(temps, false);
    lcd_print(" secondes", false);
    sleep(1);
  }
  write(drivers.buzzer, "800", sizeof("800"));
  write(drivers.relais, touche == dtmf.ouverturePorte1 ? "0" : "2", sizeof("0"));
  //  lcd_print(_resident, true);
  //  lcd_setCursor(LINE2, 0);
  // lcd_print("Appel en cours", false);
  lcd_clear();
  etat.porteOuverte = 0;
}


static void call_on_dtmf_callback(pjsua_call_id call_id, int touche)
{
  PJ_LOG(3,(THIS_FILE, "Incoming DTMF on call %d: %c", call_id, touche));
  if (touche == dtmf.ouverturePorte1 || touche == dtmf.ouverturePorte2) {
    porteOuverte = touche == dtmf.ouverturePorte1 ? 1 : 2;
    usleep(100);
    ouverture_porte(touche);
    porteOuverte = 0;
  }
  if (touche == dtmf.prolongerAppel) {
    appel.tempsDebut = (double)time(NULL);
    addtime = true;
  }
}

static int error_exit(const char *title, pj_status_t status) {
  pjsua_perror(THIS_FILE, title, status);
  //pjsua_destroy();
  return 1;
}

 /*
  * Class:     devices_Pjsip
  * Method:    init_params
  * Signature: (CCIILjava/lang/String;)V
  */
JNIEXPORT void JNICALL Java_devices_Pjsip_init_1params
(JNIEnv *env, jobject obj, jchar dtmf1, jchar dtmf2, jchar dtmf3, jint tempsOuverture1,
 jint tempsOuverture2, jstring resident, jstring code,jint tempsComm,jint tempsAppel)
{
  appel.resident.nom = (char *)(*env)->GetStringUTFChars(env, resident, 0);
  appel.resident.code = (char *)(*env)->GetStringUTFChars(env, code, 0);
  dtmf.ouverturePorte1 = dtmf1;
  dtmf.ouverturePorte2 = dtmf2;
  dtmf.prolongerAppel = dtmf3;
  tempsPorte.porte1 = tempsOuverture1;
  tempsPorte.porte2 = tempsOuverture2;
  appel.tempsAppel = tempsAppel;
  appel.tempsCommunication = tempsComm;
}

void lcd_init() {
  drivers.lcd = wiringPiI2CSetup(I2C_ADDR);
  lcd_byte(0x33, LCD_CMD);
  lcd_byte(0x32, LCD_CMD);
  lcd_byte(0x06, LCD_CMD);
  lcd_byte(0x0C, LCD_CMD);
  lcd_byte(0x28, LCD_CMD);
  lcd_byte(0x01, LCD_CMD);
  delayMicroseconds(500);
}

void lcd_setCursor(int line, int column) {
  lcdLoc(line + column);
}

void lcd_clear() {
  lcd_byte(0x01, LCD_CMD);
  lcd_byte(0x02, LCD_CMD);
}

void lcd_print(const char *str, bool clear) {
  if(clear)
    lcd_clear();
  while (*str)
    if (*(str) == '\n')
      {
	lcdLoc(LINE2);
	*(str++);
      }
    else
      lcd_byte(*(str++), LCD_CHR);
}

void lcd_toggle_enable(int bits) {
  delayMicroseconds(500);
  wiringPiI2CReadReg8(drivers.lcd, (bits | ENABLE));
  delayMicroseconds(500);
  wiringPiI2CReadReg8(drivers.lcd, (bits & ~ENABLE));
  delayMicroseconds(500);
}

void lcd_byte(int bits, int mode) {
  int bits_high, bits_low;

  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT;
  bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT;
  wiringPiI2CReadReg8(drivers.lcd, bits_high);
  lcd_toggle_enable(bits_high);
  wiringPiI2CReadReg8(drivers.lcd, bits_low);
  lcd_toggle_enable(bits_low);
}

void lcdLoc(int line) {
  lcd_byte(line, LCD_CMD);
}
