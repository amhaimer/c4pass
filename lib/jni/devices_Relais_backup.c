#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include "devices_Relais.h"

/*
 * Class:     devices_Relais
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT jint JNICALL Java_devices_Relais_init
(JNIEnv *env, jobject obj) {
    int file = open("/dev/relais", 0x02);
    if (file < 0)
        printf("[C] : error");
    return file;
}

/*
 * Class:     devices_Relais
 * Method:    status
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Relais_status
(JNIEnv *env, jobject obj, jboolean b) {
    jint handle;
    jclass relais = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, relais, "handle", "I");

    if (fidHandle == NULL)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);
    if (b == JNI_TRUE)
        write(handle, "1", sizeof ("1"));
    else
        write(handle, "0", sizeof ("0"));
}

/*
 * Class:     devices_Relais
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Relais_exit
(JNIEnv *env, jobject obj) {
    jint handle;
    jclass relais = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, relais, "handle", "I");

    if (fidHandle == NULL)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);
    close(handle);
}
