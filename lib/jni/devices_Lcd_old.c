#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include "devices_Lcd.h"

/*
 * Class:     Lcd
 * Method:    init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Lcd_init
(JNIEnv *env, jobject obj) {
    int file = open("/dev/lcd", 0x02);
    if (file < 0) {
        printf("[C] : error");
    }
    return file;
}

/*
 * Class:     Lcd
 * Method:    blinkMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_blinkMode
(JNIEnv *env, jobject obj, jboolean b) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);

    if (b == JNI_FALSE)
        write(handle, "/Cblink:0", sizeof ("/Cblink:0"));
    else
        write(handle, "/Cblink:1", sizeof ("/Cblink:1"));
}

/*
 * Class:     Lcd
 * Method:    cursorMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_cursorMode
(JNIEnv * env, jobject obj, jboolean b) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);

    if (b == JNI_FALSE)
        write(handle, "/Cshow:0", sizeof ("/Cshow:0"));
    else
        write(handle, "/Cshow:1", sizeof ("/Cshow:1"));

}

/*
 * Class:     Lcd
 * Method:    displayMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_displayMode
(JNIEnv * env, jobject obj, jboolean b) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);

    if (b == JNI_FALSE)
        write(handle, "/Dcontrol:0", sizeof ("/Dcontrol:0"));
    else
        write(handle, "/Dcontrol:1", sizeof ("/Dcontrol:1"));

}

/*
 * Class:     Lcd
 * Method:    clearLcd
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_clearLcd
(JNIEnv * env, jobject obj) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);
    write(handle, "/Dclear", sizeof ("/Dclear"));
}

/*
 * Class:     Lcd
 * Method:    setCursor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_setCursor
(JNIEnv * env, jobject obj, jint ligne, jint position) {

    char ligne_string[8];
    char position_string[8];
    char cmd[32];

    jclass lcd = (*env)->GetObjectClass(env, obj);

    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");
    if (NULL == fidHandle) return;

    jint handle = (*env)->GetIntField(env, obj, fidHandle);

    // extrac and convert parametre
    sprintf(ligne_string, "%d", (int) ligne);
    sprintf(position_string, "%d", (int) position);

    strcat(cmd, "/Cposition:");
    strcat(cmd, ligne_string);
    strcat(cmd, ":");
    strcat(cmd, position_string);
    //printf("In C, error ; %s",cmd);

    write(handle, cmd, strlen(cmd) + 1);

}

/*
 * Class:     Lcd
 * Method:    print
 * Signature: (Ljava/lang/String;Z)V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_print
(JNIEnv * env, jobject obj, jstring string, jboolean b) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    const char *str = (*env)->GetStringUTFChars(env, string, 0);
    char underscore[33] = "_";

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);

    if (b == JNI_TRUE) {
        write(handle, str, strlen(str) + 1);
    } else {
        strcat(underscore, str);
        write(handle, underscore, strlen(underscore) + 1);
    }

    (*env)->ReleaseStringUTFChars(env, string, str);
}

/*
 * Class:     Lcd
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Lcd_exit
(JNIEnv * env, jobject obj) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);
    close(handle);
}
