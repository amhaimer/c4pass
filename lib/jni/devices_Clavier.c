#include <jni.h>
#include <string.h>
#include <fcntl.h>
#include "devices_Clavier.h"

/*
 * Class:     Clavier
 * Method:    keyscan
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_devices_Clavier_keyscan
(JNIEnv *env, jobject obj) {

    char buff[32];
    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");


    if (NULL == fidHandle)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);
    read(handle, buff, 32);

    return (*env)->NewStringUTF(env, buff);
}

/*
 * Class:     Clavier
 * Method:    retroEclairage
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_devices_Clavier_retroEclairage
(JNIEnv *env, jobject obj, jboolean b) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");


    if (NULL == fidHandle)
        return;
    handle = (*env)->GetIntField(env, obj, fidHandle);

    if (b == JNI_TRUE) {
        write(handle, "/Retro:1", sizeof ("/Retro:1"));
    } else {
        write(handle, "/Retro:0", sizeof ("/Retro:0"));
    }

}

/*
 * Class:     Clavier
 * Method:    init
 * Signature: ()I
 */

JNIEXPORT jint JNICALL Java_devices_Clavier_init
(JNIEnv *env, jobject obj) {
    int file = open("/dev/clavier", 0x02);
    if (file < 0) {
        printf("[C] :  error");
    }
    return file;
}

/*
 * Class:     Clavier
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Clavier_exit
(JNIEnv * env, jobject obj) {

    jint handle;
    jclass lcd = (*env)->GetObjectClass(env, obj);
    jfieldID fidHandle = (*env)->GetFieldID(env, lcd, "handle", "I");

    if (NULL == fidHandle)
        return;

    handle = (*env)->GetIntField(env, obj, fidHandle);
    close(handle);

}

