#include "DiesePjsip.h"
#include "devices_Pjsip.h"
#include "devices_Pjsip_AccountType.h"
#include <jni.h>
#include <wiringPi.h>
#include <stdio.h>
#include <pthread.h>
#define RELAIS1    13
#define RELAIS2     6
#define BUZZER     19

static PJParametres parametres;
static Drivers drivers;


static Appel appel;
static Etat etat;
pthread_t thread1;

int dtmft = 1, tempsporte1, tempsporte2;
int elstat = 0;
char dtmfporte1, dtmfporte2, dtmfpro, dtmfrac;
bool initialized = false;
static int current_account = 0;
static int nbcalls = 0;
static bool addtime = false;
static bool raccroche = false;
static int porteOuverte = 0;
void open_relais(int i);
void buzz(int i);

void init_relais() {
    wiringPiSetup();
    pinMode(RELAIS1, OUTPUT);
    pinMode(RELAIS2, OUTPUT);
    pinMode(BUZZER, OUTPUT);
}

/*
 * Class:     devices_Pjsip
 * Method:    register
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip_register
(JNIEnv *env, jobject obj, jstring user, jstring passwd, jstring domain, jint type) {
    pj_status_t status;

    const char *_user = (*env)->GetStringUTFChars(env, user, 0);
    const char *_passwd = (*env)->GetStringUTFChars(env, passwd, 0);
    const char *_domain = (*env)->GetStringUTFChars(env, domain, 0);

    if (!initialized && Pjsip_initialize() == 1)
        return 1;

    // Enregistre le compte SIP sur le serveur
    {
        pjsua_acc_config cfg;
        char *userC = (char *) _user;
        char sipId[MAX_SIP_ID_LENGTH];
        char regUri[MAX_SIP_REG_URI_LENGTH];
        pj_str_t codec_s;

        pjsua_acc_config_default(&cfg);
        cfg.vid_out_auto_transmit = PJ_TRUE;
        //    cfg.vid_in_auto_show = PJ_TRUE;
        sprintf(sipId, "sip:%s@%s", _user, _domain);
        cfg.id = pj_str(sipId);

        sprintf(regUri, "sip:%s", _domain);
        cfg.reg_uri = pj_str(regUri);
        cfg.cred_count = 1;
        cfg.cred_info[0].realm = pj_str((char *) "*");
        //cfg.cred_info[0].realm = pj_str((char *)_domain);
        cfg.cred_info[0].scheme = pj_str("digest");
        cfg.cred_info[0].username = pj_str((char *) _user);
        cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
        cfg.cred_info[0].data = pj_str((char *) _passwd);


        pjsua_codec_set_priority(pj_cstr(&codec_s, "GSM/8000/1"), PJMEDIA_CODEC_PRIO_HIGHEST);

        if (current_account < 4) {
            if (current_account == 3) {
                current_account--;
                pjsua_acc_del(parametres.account[current_account].acc_id);
                printf("le compte courrant %s", userC);
            }
            parametres.account[current_account].type = (enum AccountType)type;
            status = pjsua_acc_add(&cfg, PJ_TRUE, &parametres.account[current_account].acc_id);
            if (status != PJ_SUCCESS)
                return error_exit("Error adding account", status);
            current_account++;
        } else
            return error_exit("More than 4 account configured", 0);
    }

    (*env)->ReleaseStringUTFChars(env, user, _user);
    (*env)->ReleaseStringUTFChars(env, passwd, _passwd);
    (*env)->ReleaseStringUTFChars(env, domain, _domain);
    return 0;
}

int Pjsip_initialize() {
    drivers.relais = open("/dev/relais", 0x02);
    drivers.buzzer = open("/dev/buzzer", 0x02);
    lcd_init();

    pj_status_t status;
    status = pjsua_create();
    if (status != PJ_SUCCESS)
        error_exit("Error in pjsua_create()", status);

    // Initialise la librairie
    {
        pjsua_config cfg;
        pjsua_config_default(&cfg);

        cfg.cred_info[0].realm = pj_str((char *) "*");
        cfg.cred_info[0].scheme = pj_str((char *) "digest");
        cfg.cb.on_incoming_call = &on_incoming_call;
        cfg.cb.on_call_media_state = &on_call_media_state;
        cfg.cb.on_call_state = &on_call_state;
        cfg.cb.on_dtmf_digit = &call_on_dtmf_callback;
        pjsua_logging_config log_cfg;
        pjsua_logging_config_default(&log_cfg);
        log_cfg.console_level = 4;

        status = pjsua_init(&cfg, &log_cfg, NULL);
        if (status != PJ_SUCCESS)
            return error_exit("Error in pjsua_init()", status);
    }

    // Ajoute le transport UDP
    {
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        cfg.port = 0;
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
        if (status != PJ_SUCCESS)
            return error_exit("Error creating transport", status);
    }
    // Ajoute le transport TCP
    /*
      {
      pjsua_transport_config cfg;
      pjsua_transport_config_default(&cfg);
      cfg.port =0;
      status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &cfg, NULL);
      if (status != PJ_SUCCESS)
        return error_exit("Error creating transport", status);
    }
     */
    status = pjsua_start();
    if (status != PJ_SUCCESS)
        return error_exit("Error starting pjsua", status);

    initialized = true;
    return 0;
}

/*
 * Class:     devices_Pjsip
 * Method:    resetAddTime
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_resetAddTime
(JNIEnv *env, jobject obj) {
    addtime = false;
}

/*
 * Class:     devices_Pjsip
 * Method:    addTime
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_addTime
(JNIEnv *env, jobject obj) {
    return addtime;
}

/*
 * Class:     devices_Pjsip
 * Method:    RessetRaccrocher
 * Signature: ()Z
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_resetRaccroche
(JNIEnv *env, jobject obj) {
    raccroche = false;
}

/*
 * Class:     devices_Pjsip
 * Method:    Raccrocher
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_raccroche
(JNIEnv *env, jobject obj) {
    return raccroche;
}

/*
 * Class:     devices_Pjsip
 * Method:    resetOuverture
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_resetOuverture
(JNIEnv *env, jobject obj) {
    porteOuverte = 0;
}

/*
 * Class:     devices_Pjsip
 * Method:    ouverturePorte
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip_ouverturePorte
(JNIEnv *env, jobject obj) {
    return porteOuverte;
}

/*
 * Class:     devices_Pjsip
 * Method:    connectedToServer
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_connectedToServer
(JNIEnv *env, jobject obj, jint type) {
    int i;
    pjsua_acc_info info;

    for (i = 0; i < current_account; i++) {
        pjsua_acc_get_info(parametres.account[i].acc_id, &info);
        if (parametres.account[i].type == (enum AccountType)type && info.status == 200)
            return true;
    }
    return false;
}

/*
 * Class:     devices_Pjsip
 * Method:    onCommunication
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_devices_Pjsip_onCommunication
(JNIEnv *env, jobject obj) {
    return etat.on_commu;
}

void communicationEnCours() {
    appel.tempsDebut = (double) time(NULL);
    appel.tempsActuel = appel.tempsDebut;

    while ((double) time(NULL) - appel.tempsDebut < appel.tempsCommunication && etat.on_commu == 1) {
        if (appel.tempsActuel != (double) time(NULL) && etat.porteOuverte == 0) {
            appel.tempsActuel = (double) time(NULL);
            double tempsRestant = appel.tempsCommunication - ((double) time(NULL) - appel.tempsDebut);
            char output[3];
            snprintf(output, tempsRestant > 9 ? 3 : 2, "%f", tempsRestant);
            printf(output);
            if (tempsRestant > 9 && etat.on_commu) {
                if (tempsRestant == appel.tempsCommunication - 1) {
                    lcd_print(appel.resident.nom, true);
                    lcd_setCursor(LINE2, 0);
                    lcd_print(appel.resident.code, false);
                    lcd_print(" PARLEZ ", false);
                    lcd_setCursor(LINE2, 16);
                    lcd_print("s", false);
                }
                lcd_setCursor(LINE2, 11);
                lcd_print(output, false);
                lcd_print("s", false);
            } else {
                if (tempsRestant == 9) {
                    write(drivers.buzzer, "200", sizeof ("200"));
                    lcd_print("COMMUNICATION\nTERMINEE DANS ", true);
                    lcd_setCursor(LINE2, 15);
                    lcd_print("s", false);
                }
                lcd_setCursor(LINE2, 14);
                lcd_print(output, false);
            }
        }
    }
    endCall();
    etat.on_commu = false;
}

void appelEnCours(void) {
    double tempsDebut = (double) time(NULL);
    double tempsActuel = tempsDebut;

    while ((double) time(NULL) - tempsDebut < appel.tempsAppel && etat.on_commu == 0 && !statusdiscon) {
        if (tempsActuel != (double) time(NULL) && etat.on_commu == 0 && etat.porteOuverte == 0) {
            char secondes[3];

            tempsActuel = (double) time(NULL);
            snprintf(secondes, 3, "%f", (double) time(NULL) - tempsDebut);
            lcd_setCursor(LINE1, 0);
            lcd_print(appel.resident.nom, true);
            lcd_setCursor(LINE2, 0);
            lcd_print("APPEL ", false);
            lcd_print(appel.resident.code, false);
            lcd_print(" ", false);
            lcd_print(secondes, false);
            lcd_print("s", false);
        }
    }
    if (etat.on_commu)
        communicationEnCours();
    endCall();
}

/*
 * Class:     devices_Pjsip
 * Method:    makeCall
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_makeCall
(JNIEnv *env, jobject obj, jstring uri, jint type, jstring uri2, jint type2, jstring uri3, jint type3, jstring uri4, jint type4, jstring uri5, jint type5, jstring uri6, jint type6, jstring uri7, jint type7, jstring uri8, jint type8, jstring uri9, jint type9, jstring uri10, jint type10, jint statusel) {

    char *_uri = (char *) (*env)->GetStringUTFChars(env, uri, 0);
    char *_uri2 = (char *) (*env)->GetStringUTFChars(env, uri2, 0);
    char *_uri3 = (char *) (*env)->GetStringUTFChars(env, uri3, 0);
    char *_uri4 = (char *) (*env)->GetStringUTFChars(env, uri4, 0);
    char *_uri5 = (char *) (*env)->GetStringUTFChars(env, uri5, 0);
    char *_uri6 = (char *) (*env)->GetStringUTFChars(env, uri6, 0);
    char *_uri7 = (char *) (*env)->GetStringUTFChars(env, uri7, 0);
    char *_uri8 = (char *) (*env)->GetStringUTFChars(env, uri8, 0);
    char *_uri9 = (char *) (*env)->GetStringUTFChars(env, uri9, 0);
    char *_uri10 = (char *) (*env)->GetStringUTFChars(env, uri10, 0);

    pj_str_t pj_uri = pj_str(_uri);
    pj_str_t pj_uri2 = pj_str(type2 != -1 ? _uri2 : "");
    pj_str_t pj_uri3 = pj_str(type3 != -1 ? _uri3 : "");
    pj_str_t pj_uri4 = pj_str(type4 != -1 ? _uri4 : "");

    pj_str_t pj_uri5 = pj_str(type5 != -1 ? _uri5 : "");
    pj_str_t pj_uri6 = pj_str(type6 != -1 ? _uri6 : "");
    pj_str_t pj_uri7 = pj_str(type7 != -1 ? _uri7 : "");
    pj_str_t pj_uri8 = pj_str(type8 != -1 ? _uri8 : "");
    pj_str_t pj_uri9 = pj_str(type9 != -1 ? _uri9 : "");
    pj_str_t pj_uri10 = pj_str(type10 != -1 ? _uri10 : "");

    elstat = statusel;
    int acc_id = getAccount(type);
    int acc_id2 = getAccount(type2);
    int acc_id3 = getAccount(type3);
    int acc_id4 = getAccount(type4);
    int acc_id5 = getAccount(type5);
    int acc_id6 = getAccount(type6);
    int acc_id7 = getAccount(type7);
    int acc_id8 = getAccount(type8);
    int acc_id9 = getAccount(type9);
    int acc_id10 = getAccount(type10);

    pjsua_call_info info;

    //nbcalls = type2 == -1 ? 1 : 2;
    nbcalls = type2 == -1 ? 1 : (type3 == -1 ? 2 : (type4 == -1 ? 3 : (type5 == -1 ? 4 : (type6 == -1 ? 5 : (type7 == -1 ? 6 : (type8 == -1 ? 7 : (type9 == -1 ? 8 : (type10 == -1 ? 9 : 10))))))));

    statusdiscon = 0;
    etat.hasBeenConnected = 0;
    if ((pjsua_verify_sip_url(_uri) != 0) || (type2 != -1 && pjsua_verify_sip_url(_uri2) != 0) || (type3 != -1 && pjsua_verify_sip_url(_uri3) != 0) || (type4 != -1 && pjsua_verify_sip_url(_uri4) != 0) || (type5 != -1 && pjsua_verify_sip_url(_uri5) != 0) || (type6 != -1 && pjsua_verify_sip_url(_uri6) != 0) || (type7 != -1 && pjsua_verify_sip_url(_uri7) != 0) || (type8 != -1 && pjsua_verify_sip_url(_uri8) != 0) || (type9 != -1 && pjsua_verify_sip_url(_uri9) != 0) || (type10 != -1 && pjsua_verify_sip_url(_uri10) != 0))
        return;


    if (pjsua_call_make_call(acc_id, &pj_uri, 0, NULL, NULL, &parametres.id_call[0]) != PJ_SUCCESS)
        return;


    if (type2 != -1)
        pjsua_call_make_call(acc_id2, &pj_uri2, 0, NULL, NULL, &parametres.id_call[1]);

    if (type3 != -1)
        pjsua_call_make_call(acc_id3, &pj_uri3, 0, NULL, NULL, &parametres.id_call[2]);


    if (type4 != -1)
        pjsua_call_make_call(acc_id4, &pj_uri4, 0, NULL, NULL, &parametres.id_call[3]);

    if (type5 != -1)
        pjsua_call_make_call(acc_id5, &pj_uri5, 0, NULL, NULL, &parametres.id_call[4]);


    if (type6 != -1)
        pjsua_call_make_call(acc_id6, &pj_uri6, 0, NULL, NULL, &parametres.id_call[5]);



    if (type7 != -1)
        pjsua_call_make_call(acc_id7, &pj_uri7, 0, NULL, NULL, &parametres.id_call[6]);

    if (type8 != -1)
        pjsua_call_make_call(acc_id8, &pj_uri8, 0, NULL, NULL, &parametres.id_call[7]);


    if (type9 != -1)
        pjsua_call_make_call(acc_id9, &pj_uri9, 0, NULL, NULL, &parametres.id_call[8]);


    if (type10 != -1)
        pjsua_call_make_call(acc_id10, &pj_uri10, 0, NULL, NULL, &parametres.id_call[9]);

    write(drivers.buzzer, "400", sizeof ("400"));
    //appelEnCours();
}

int getAccount(int type) {
    int id;

    for (id = 0; id < 4; id++)
        if (type == parametres.account[id].type)
            return id;
    return -1;
}

/*
 * Class:     devices_Pjsip
 * Method:    destroy
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_devices_Pjsip_destroy
(JNIEnv *env, jobject obj) {
    destroy = 1;
    statuscom = 0;
    //statusdiscon=1;
    pjsua_destroy();
}

JNIEXPORT jint JNICALL Java_devices_Pjsip_hasBeenConnected
(JNIEnv *env, jobject obj) {
    return etat.hasBeenConnected;
}

JNIEXPORT jint JNICALL Java_devices_Pjsip_statusdisconnected
(JNIEnv *env, jobject obj) {
    return statusdiscon;
}

/*
 * Class:     devices_Pjsip
 * Method:    endCall
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_endCall
(JNIEnv *env, jobject obj) {
    pjsua_call_hangup_all();
}

void endCall() {
    pjsua_call_hangup_all();
}

static void on_call_media_state(pjsua_call_id call_id) {
    pjsua_call_info ci;

    pjsua_call_get_info(call_id, &ci);
    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
        pjsua_conf_connect(ci.conf_slot, 0);
        pjsua_conf_connect(0, ci.conf_slot);
    }
}

static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata) {
    pjsua_call_info ci;

    PJ_UNUSED_ARG(acc_id);
    PJ_UNUSED_ARG(rdata);
    pjsua_call_get_info(call_id, &ci);
    parametres.wid = ci.media[parametres.vid_idx].stream.vid.win_in;
    PJ_LOG(3, (THIS_FILE, "WID:%d", parametres.wid));
    PJ_LOG(3, (THIS_FILE, "Incoming call from %.*s!!",
            (int) ci.remote_info.slen,
            ci.remote_info.ptr));

    printf("%s", ci.remote_info.ptr);
}

static void on_call_state(pjsua_call_id call_id, pjsip_event *e) {
    pjsua_call_info ci;
    pj_status_t status;



    PJ_UNUSED_ARG(e);
    pjsua_call_get_info(call_id, &ci);
    PJ_LOG(3, (THIS_FILE, "Call %d state=%.*s", call_id,
            (int) ci.state_text.slen,
            ci.state_text.ptr));

    if (ci.state == PJSIP_INV_STATE_CONFIRMED) {
        /*  if (nbcalls == 2)
            pjsua_call_hangup(call_id == parametres.id_call[0] ? parametres.id_call[1] : parametres.id_call[0], 0, NULL, NULL);*/
        etat.hasBeenConnected = true;
        etat.on_commu = true;
        for (int count = 0; count <= pjsua_call_get_count(); count++) {

            if (call_id != parametres.id_call[count])
                pjsua_call_hangup(parametres.id_call[count], 0, NULL, NULL);

        }

    } else if (ci.state == PJSIP_INV_STATE_DISCONNECTED) {


        if (pjsua_call_get_count() <= 1) {
            statusdiscon = 1;
            pjsua_call_hangup_all();
            etat.on_commu = false;
            //write(drivers.buzzer, "800", sizeof("800"));

        }
    }
}

char *intToArray(int nb) {
    int n = log10(nb) + 1, i;
    char *str = calloc(n, sizeof (char));
    i = n;
    for (i = n; i > 0; --i, nb /= 10)
        str[i - 1] = nb % 10 + '0';
    return str;
}

void *ouverture_porte(void *p) {

    printf("ouverture porte");
    int touche = dtmft;
    etat.porteOuverte = 1;
    porteOuverte = dtmft;
    if (elstat == 0) {
        open_relais(touche == dtmfporte1 ? 1 : 3);
        //  lcd_print(touche == dtmf.ouverturePorte1 ? "    Porte 1     " : "    Porte 2     ", true);
        // write(drivers.buzzer, "400", sizeof("400"));
        buzz(400);
        int current = 0;
        for (current = touche == dtmfporte1 ? tempsporte1 : tempsporte2;
                current > 0; current--) {

            lcd_setCursor(LINE2, 0);


            //  lcd_print(temps, false);
            //    lcd_print("    Ouverte", false);
            sleep(1);
        }
        //write(drivers.buzzer, "800", sizeof("800"));
        buzz(800);
        open_relais(touche == dtmfporte1 ? 0 : 2);
    } else if (elstat == 1 && touche == dtmfporte2) {
        open_relais(touche == dtmfporte1 ? 1 : 3);
        //  lcd_print(touche == dtmf.ouverturePorte1 ? "    Porte 1     " : "    Porte 2     ", true);
        // write(drivers.buzzer, "400", sizeof("400"));
        buzz(400);
        int current = 0;
        for (current = touche == dtmfporte1 ? tempsporte1 : tempsporte2;
                current > 0; current--) {

            lcd_setCursor(LINE2, 0);


            //  lcd_print(temps, false);
            //    lcd_print("    Ouverte", false);
            sleep(1);
        }
        //write(drivers.buzzer, "800", sizeof("800"));
        buzz(800);
        open_relais(touche == dtmfporte1 ? 0 : 2);
    } else if (elstat == 2 && touche == dtmfporte1) {
        open_relais(touche == dtmfporte1 ? 1 : 3);
        //  lcd_print(touche == dtmf.ouverturePorte1 ? "    Porte 1     " : "    Porte 2     ", true);
        // write(drivers.buzzer, "400", sizeof("400"));
        buzz(400);
        int current = 0;
        for (current = touche == dtmfporte1 ? tempsporte1 : tempsporte2;
                current > 0; current--) {

            lcd_setCursor(LINE2, 0);


            //  lcd_print(temps, false);
            //    lcd_print("    Ouverte", false);
            sleep(1);
        }
        //write(drivers.buzzer, "800", sizeof("800"));
        buzz(800);
        open_relais(touche == dtmfporte1 ? 0 : 2);
    }
    //lcd_print("parlez", true);
    //  lcd_setCursor(LINE2, 0);
    // lcd_print("Appel en cours", false);
    // lcd_clear();
    etat.porteOuverte = 0;
    porteOuverte = 0;

    pthread_exit(NULL);
}

void open_relais(int i) {
    switch (i) {
        case 0:
            digitalWrite(RELAIS1, LOW);
            break;
        case 1:
            digitalWrite(RELAIS1, HIGH);
            break;
        case 2:
            digitalWrite(RELAIS2, LOW);
            break;
        case 3:
            digitalWrite(RELAIS2, HIGH);
            break;

    }
}

void buzz(int i) {
    digitalWrite(BUZZER, HIGH);
    usleep(100);
    digitalWrite(BUZZER, LOW);
}

static void call_on_dtmf_callback(pjsua_call_id call_id, int touche) {
    PJ_LOG(3, (THIS_FILE, "Incoming DTMF on call %d: %c", call_id, touche));



    if (touche == dtmfporte1 || touche == dtmfporte2) {
        porteOuverte = touche == dtmfporte1 ? 1 : 2;
        usleep(100);
        dtmft = touche;
        pthread_create(&thread1, NULL, ouverture_porte, NULL);
        //pthread_join(thread1, NULL)

        //porteOuverte = 0;
    }
    if (touche == dtmfpro) {
        appel.tempsDebut = (double) time(NULL);
        addtime = true;
    }

    if (touche == dtmfrac) {
        raccroche = true;
    }
}

static int error_exit(const char *title, pj_status_t status) {
    pjsua_perror(THIS_FILE, title, status);
    //pjsua_destroy();
    return 1;
}

/*
 * Class:     devices_Pjsip
 * Method:    init_params
 * Signature: (CCIILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_devices_Pjsip_init_1params
(JNIEnv *env, jobject obj, jint dtmf1, jint dtmf2, jint dtmf3, jint dtmf4, jint tempsOuverture1,
        jint tempsOuverture2, jstring resident, jstring code, jint tempsComm, jint tempsAppel) {
    appel.resident.nom = (char *) (*env)->GetStringUTFChars(env, resident, 0);
    appel.resident.code = (char *) (*env)->GetStringUTFChars(env, code, 0);
    dtmfporte1 = dtmf1;
    dtmfporte2 = dtmf2;

    dtmfpro = dtmf3;
    dtmfrac = dtmf4;
    tempsporte1 = tempsOuverture1;
    tempsporte2 = tempsOuverture2;
    appel.tempsAppel = tempsAppel;
    appel.tempsCommunication = tempsComm;
}

void lcd_init() {
    drivers.lcd = wiringPiI2CSetup(I2C_ADDR);
    lcd_byte(0x33, LCD_CMD);
    lcd_byte(0x32, LCD_CMD);
    lcd_byte(0x06, LCD_CMD);
    lcd_byte(0x0C, LCD_CMD);
    lcd_byte(0x28, LCD_CMD);
    lcd_byte(0x01, LCD_CMD);
    delayMicroseconds(500);
}

void lcd_setCursor(int line, int column) {
    lcdLoc(line + column);
}

void lcd_clear() {
    lcd_byte(0x01, LCD_CMD);
    lcd_byte(0x02, LCD_CMD);
}

void lcd_print(const char *str, bool clear) {
    if (clear)
        lcd_clear();
    while (*str)
        if (*(str) == '\n') {
            lcdLoc(LINE2);
            *(str++);
        } else
            lcd_byte(*(str++), LCD_CHR);
}

void lcd_toggle_enable(int bits) {
    delayMicroseconds(500);
    wiringPiI2CReadReg8(drivers.lcd, (bits | ENABLE));
    delayMicroseconds(500);
    wiringPiI2CReadReg8(drivers.lcd, (bits & ~ENABLE));
    delayMicroseconds(500);
}

void lcd_byte(int bits, int mode) {
    int bits_high, bits_low;

    bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT;
    bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT;
    wiringPiI2CReadReg8(drivers.lcd, bits_high);
    lcd_toggle_enable(bits_high);
    wiringPiI2CReadReg8(drivers.lcd, bits_low);
    lcd_toggle_enable(bits_low);
}

void lcdLoc(int line) {
    lcd_byte(line, LCD_CMD);
}
