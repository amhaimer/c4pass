/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class devices_Pjsip2 */

#ifndef _Included_devices_Pjsip2
#define _Included_devices_Pjsip2
#ifdef __cplusplus
extern "C" {
#endif
    /*
     * Class:     devices_Pjsip2
     * Method:    status
     * Signature: ()I
     */
    JNIEXPORT jint JNICALL Java_devices_Pjsip2_status
    (JNIEnv *, jobject);

    /*
     * Class:     devices_Pjsip2
     * Method:    statusdisconnected
     * Signature: ()I
     */
    JNIEXPORT jint JNICALL Java_devices_Pjsip2_statusdisconnected
    (JNIEnv *, jobject);

    /*
     * Class:     devices_Pjsip2
     * Method:    register
     * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
     */
    JNIEXPORT jint JNICALL Java_devices_Pjsip2_register
    (JNIEnv *, jobject, jstring, jstring, jstring);

    /*
     * Class:     devices_Pjsip2
     * Method:    makeCall
     * Signature: (Ljava/lang/String;)V
     */
    JNIEXPORT void JNICALL Java_devices_Pjsip2_makeCall
    (JNIEnv *, jobject, jstring);

    /*
     * Class:     devices_Pjsip2
     * Method:    destroy
     * Signature: ()I
     */
    JNIEXPORT jint JNICALL Java_devices_Pjsip2_destroy
    (JNIEnv *, jobject);

    /*
     * Class:     devices_Pjsip2
     * Method:    init_params
     * Signature: (CCIILjava/lang/String;III)V
     */
    JNIEXPORT void JNICALL Java_devices_Pjsip2_init_1params
    (JNIEnv *, jobject, jchar, jchar, jint, jint, jstring, jint, jint, jint);

#ifdef __cplusplus
}
#endif
#endif
