package utils;

import java.io.*;
import sql.DataBase;
import sql.data.Porte;

/**
 * classe user manager
 *
 * @author Abdellah
 */
public class UsersManager {

    private Process p;
    private ProcessBuilder pb;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();

    /**
     * constructeur de classe
     */
    public UsersManager() {
        porte = Porte.getInstance();
    }

    /**
     * add
     *
     * @param id identifiant
     */
    public void add(String id) {
        if (!isVOIP()) {
            return;
        }
        try {
            pb = new ProcessBuilder("sudo", "bash", "-c", genString(id));
            p = pb.start();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }

    /**
     * remove
     *
     * @param id identifiant
     */
    public void remove(String id) {
        if (!isVOIP()) {
            return;
        }
        try {
            pb = new ProcessBuilder("sudo", "bash", "-c", "sed -i -e '/" + id + "/,+2d' /etc/asterisk/users.conf");
            p = pb.start();
            System.out.println("sudo sed -i -e '/[" + id + "]/,+2d' /etc/asterisk/users.conf");
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }

    /**
     * voip
     *
     * @return true or false
     */
    private boolean isVOIP() {
        return (porte.getInterphone().charAt(0) == '4');
    }

    /**
     *
     * @param id identifiant
     * @return chaine
     */
    private String genString(String id) {
        String string = new String();
        string = "echo -e \"";
        string += "\\n\\n[" + id + "1" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "2" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "3" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "4" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "5" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "6" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "7" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "8" + "](template)\\nsecret=9999\\n";
        string += "[" + id + "9" + "](template)\\nsecret=9999\\n";
        string += "\"";
        string += " >> /etc/asterisk/users.conf";
        System.out.println(string);
        return string;
    }

}
