package utils;

/**
 * **************** Algorithme de cryptage ************
 */
public class Cryptage {

    /**
     * constructeur de classe
     */
    public Cryptage() {

    }

    /**
     * encryptage
     *
     * @param s chaine à crypter
     * @return résultat
     */
    public static String encrypt(String s) {
        char[] arr = s.toCharArray();
        int a, b, c, d;
        int max = 9, min = 0;
        int randi = (int) (Math.random() * (max - min));
        int randi2 = (int) (Math.random() * (max - min));
        char A, B, C, D;
        a = Character.getNumericValue(arr[0]);
        b = Character.getNumericValue(arr[1]);
        c = Character.getNumericValue(arr[2]);
        d = Character.getNumericValue(arr[3]);
        a = a ^ 7;
        b = b ^ 2;
        c = c ^ 3;
        d = d ^ randi;
        A = trans(a);
        B = trans(b);
        C = trans(c);
        D = trans(d);
        String sf = randi + "" + D + "X" + B + "" + C + "" + randi2 + "X" + A;
        return sf;
    }

    /**
     * transfet caractere
     *
     * @param i le caractere
     * @return résultat
     */
    public static char trans(int i) {
        char c = 'm';
        switch (i) {
            case 0:
                c = '0';
                break;
            case 1:
                c = '1';
                break;
            case 2:
                c = '2';
                break;
            case 3:
                c = '3';
                break;
            case 4:
                c = '4';
                break;
            case 5:
                c = '5';
                break;
            case 6:
                c = '6';
                break;
            case 7:
                c = '7';
                break;
            case 8:
                c = '8';
                break;
            case 9:
                c = '9';
                break;
            case 10:
                c = 'P';
                break;
            case 11:
                c = 'X';
                break;
            case 12:
                c = 'K';
                break;
            case 13:
                c = 'H';
                break;
            case 14:
                c = 'A';
                break;
            case 15:
                c = 'L';
                break;
            default:
                break;
        }
        return c;
    }
}
