/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Abdellah
 */
public class Eth0Plug {

    /**
     * vérifier si ethernet est branché
     *
     * @return true or false
     */
    public boolean isPlugged() {
        String madr;

        Process ifeth0 = null;

        madr = "";
        try {
            ifeth0 = Runtime.getRuntime().exec("sudo mii-tool eth0");
        } catch (IOException e) {
            System.out.println(e);
        }
        BufferedReader addrifeth0 = new BufferedReader(new InputStreamReader(ifeth0.getInputStream()));

        String sadr;

        try {
            while ((sadr = addrifeth0.readLine()) != null) {
                madr += sadr;

            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return madr.contains("no link");
    }
}
