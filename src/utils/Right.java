package utils;

/**
 * aligner à droite
 *
 * @author Abdellah
 */
public class Right implements TextAlign {

    /**
     * aligner la chaine a droite
     *
     * @param s la chaine a aligner
     * @return la chaine aligné
     */
    @Override
    public String align(String s) {
        String tmp;
        int begin = 0;

        while (s.charAt(begin) == ' ') {
            begin++;
        }
        tmp = s.substring(begin);
        return tmp;
    }
}
