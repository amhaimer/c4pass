package utils;

/**
 * classe center
 *
 * @author Abdellah
 */
public class Center implements TextAlign {

    private static int sizeLine = 16;

    /**
     * align
     *
     * @param s chaine
     * @return renvoi la chaine
     */
    @Override
    public String align(String s) {
        String tmp;
        int begin = 0;

        while (s.charAt(begin) == ' ') {
            begin++;
        }
        tmp = s.substring(begin);

        if (tmp.length() < sizeLine) {
            boolean reverse = true;
            while (tmp.length() < sizeLine) {
                if (reverse) {
                    tmp = tmp + " ";
                    reverse = false;
                } else {
                    tmp = " " + tmp;
                    reverse = true;
                }
            }
        }
        return tmp;

    }
}
