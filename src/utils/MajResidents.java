package utils;

import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;
import sql.data.Evenement;
import java.util.*;
import devices.Lcd;

/**
 * Maj des donnees
 *
 * @author Abdellah
 */
public class MajResidents {

    Lcd lcd = Lcd.getInstance();

    /**
     * maj des residents
     */
    public void maj() {

        DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        Connection coon = new Connection();
        String s = coon.conn(Constante.URL_WEB_MAJ + "/java_php/getEtatPorteResident.php?porte=" + porte.getNumPorte());

        if (s.indexOf(";") >= 0) {
            String chaineTest = s.substring(0, s.indexOf(";"));
            String idEnvoie = s.substring(s.indexOf(";") + 1, s.lastIndexOf(";"));

            if (chaineTest.contains("200")) {
                Resident res;

                //Requet sql pour lecture Residents
                /*
		  String url = "http://logidiese.com/java_php/updateResidents.php?id=140&droit=122&code=111&nom=azertyuiopqsdfgh&telephone1=0787878787&renvoi_appel=1&telephone2=0808090805&list_rouge=1&Identifian\
		  t=1111111111&visio_tel1=1&visio_tel2=2&validation_cle=14455879&cle1=12345678&clem1=12345678&cle2=12345678&clem2=12345678&cle3=12345678&clem3=12345678&cle4=12345678&clem4=12345678&cle5=12345678&clem5=12345678&cle6=12345678&cle \
		  m6=12345678&cle7=12345678&clem7=12345678&cle8=12345678&clem8=12345678&cle9=12345678&clem9=12345678&cle10=12345678&clem10=12345678&couleur_badge=1234567890&identitel=1234&temp_appeldirect=100&appel_int-ext_tel1=1appel_int-ext_ \
		  tel2=2&tempo_renvoi_appel=100&temp_renvoi_appel=100&temp_cle=11&jour_debut=1111111&jour_fin=1111111th1_debut=11:50&th1_fin=12:50&th2_debut=13:50&th2_fin=14:59&jour=1111111&th1_debut_sem2=15/06/2012&th1_fin_sem2=05/07/2018&th2 \
		  _debut_sem2=06/11/2011&th2_fin_sem2=08/07/2014&jour_sem2=1111111&profession_liberale=111111111&is_refresh=1";
		  
                 */
                String url = Constante.URL_WEB_MAJ + "/java_php/updateResidents.php?id=140&droit=122&code=111&nom=azertyuiopqsdfgh&telephone1=0787878787&renvoi_appel=1&telephone2=0808090805&list_rouge=1&Identifiant=1111111111&visio_tel1=1&visio_tel2=2&validation_cle=14455879&cle1=12345678&clem1=12345678&cle2=12345678&clem2=12345678&cle3=12345678&clem3=12345678&cle4=12345678&clem4=12345678&cle5=12345678&clem5=12345678&cle6=12345678&clem6=12345678&cle7=12345678&clem7=12345678&cle8=12345678&clem8=12345678&cle9=12345678&clem9=12345678&cle10=12345678&clem10=12345678&couleur_badge=1234567890&identitel=1234&temp_appeldirect=100&appel_int-ext_tel1=1appel_int-ext_tel2=2&tempo_renvoi_appel=100&temp_renvoi_appel=100&temp_cle=11&jour_debut=1111111&jour_fin=1111111th1_debut=11:50&th1_fin=12:50&th2_debut=13:50&th2_fin=14:59&jour=1111111&th1_debut_sem2=15/06/2012&th1_fin_sem2=05/07/2018&th2_debut_sem2=06/11/2011&th2_fin_sem2=08/07/2014&jour_sem2=1111111&profession_liberale=111111111&is_refresh=1";

                List<Resident> residents = data.getResidentsMaj();
                ListIterator itr = residents.listIterator();

                while (itr.hasNext()) {

                    res = (Resident) itr.next();

                    if (res.getEtat().equals("0")) {
                        url = Constante.URL_WEB_MAJ + "/java_php/updateResidents.php?porte=" + porte.getNumPorte()
                                + "&etat=" + res.getEtat() + "&id=" + res.getId()
                                + "&req=INSERT INTO residents ( droit, code, nom, telephone1, renvoi_appel, telephone2, list_rouge, Identifiant, techno_appel, validation_cle, cle1, clem1, cle2, clem2, cle3, clem3, cle4, clem4, cle5, clem5, cle6, clem6, cle7, clem7, cle8, clem8, cle9, clem9, cle10, clem10, identitel, temp_appeldirect, temp_renvoi_appel, temp_cle, profession_liberale, is_refresh, sipphone1, sipphone2, tempo_renvoi_sipphone, tempo_renvoi_telephone, code_renvoi, id_bluetooth1, id_bluetooth2, id_bluetooth3, bip1, bip2, bip3, temp_radio, temp_identitooth, temp_identitelSIP, temp_profession_liberale, temp_liste_rouge) VALUES ('"
                                + res.getDroit() + "','"
                                + res.getCode() + "','"
                                + res.getName() + "','"
                                + res.getTelephone1() + "','"
                                + res.getRenvoiAppel() + "','"
                                + res.getTelephone2() + "','"
                                + res.getListeRouge() + "','"
                                + res.getIdentifiant() + "','"
                                + res.getTechnoAppel() + "','"
                                + res.getValidationCle() + "','"
                                + res.getCle1() + "','"
                                + res.getClem1() + "','"
                                + res.getCle2() + "','"
                                + res.getClem2() + "','"
                                + res.getCle3() + "','"
                                + res.getClem3() + "','"
                                + res.getCle4() + "','"
                                + res.getClem4() + "','"
                                + res.getCle5() + "','"
                                + res.getClem5() + "','"
                                + res.getCle6() + "','"
                                + res.getClem6() + "','"
                                + res.getCle7() + "','"
                                + res.getClem7() + "','"
                                + res.getCle8() + "','"
                                + res.getClem8() + "','"
                                + res.getCle9() + "','"
                                + res.getClem9() + "','"
                                + res.getCle10() + "','"
                                + res.getClem10() + "','"
                                + res.getIdentitel() + "','"
                                + res.getAppelDirect() + "','"
                                + res.getTempRenvoi() + "','"
                                + res.getTempCle() + "','"
                                + res.getProfessionLiberale() + "','1','"
                                + res.getSipPhoneUn() + "','"
                                + res.getSipPhoneDeux() + "','"
                                + res.getRenvoiSip() + "','"
                                + res.getRenvoiTel() + "','"
                                + res.getCodeRenvoi() + "','"
                                + res.getId_bluetooth1() + "','"
                                + res.getId_bluetooth2() + "','"
                                + res.getId_bluetooth3() + "','"
                                + res.getBip1() + "','"
                                + res.getBip2() + "','"
                                + res.getBip3() + "','"
                                + res.getTempRadio() + "','"
                                + res.getTempIdentitooth() + "','"
                                + res.getTempIdentitelSIP() + "','"
                                + res.getTempProfessionLiberale() + "','"
                                + res.getTempListeRouge() + "')";
                    } else {

                        url = Constante.URL_WEB_MAJ + "/java_php/updateResidents.php?porte="
                                + porte.getNumPorte()
                                + "&etat=" + res.getEtat()
                                + "&id=" + res.getId()
                                + "&req=UPDATE residents SET "
                                + "droit='" + res.getDroit()
                                + "',code='" + res.getCode()
                                + "',nom='" + res.getName()
                                + "',telephone1='" + res.getTelephone1()
                                + "',renvoi_appel='" + res.getRenvoiAppel()
                                + "',telephone2='" + res.getTelephone2()
                                + "',list_rouge='" + res.getListeRouge()
                                + "',Identifiant='" + res.getIdentifiant()
                                + "',techno_appel='" + res.getTechnoAppel()
                                + "',validation_cle='" + res.getValidationCle()
                                + "',cle1='" + res.getCle1()
                                + "',clem1='" + res.getClem1()
                                + "',cle2='" + res.getCle2()
                                + "',clem2='" + res.getClem2()
                                + "',cle3='" + res.getCle3()
                                + "',clem3='" + res.getClem3()
                                + "',cle4='" + res.getCle4()
                                + "',clem4='" + res.getClem4()
                                + "',cle5='" + res.getCle5()
                                + "',clem5='" + res.getClem5()
                                + "',cle6='" + res.getCle6()
                                + "',clem6='" + res.getClem6()
                                + "',cle7='" + res.getCle7()
                                + "',clem7='" + res.getClem7()
                                + "',cle8='" + res.getCle8()
                                + "',clem8='" + res.getClem8()
                                + "',cle9='" + res.getCle9()
                                + "',clem9='" + res.getClem9()
                                + "',cle10='" + res.getCle10()
                                + "',clem10='" + res.getClem10()
                                + "',sipphone1='" + res.getSipPhoneUn()
                                + "',sipphone2='" + res.getSipPhoneDeux()
                                + "',identitel='" + res.getIdentitel()
                                + "',temp_appeldirect='" + res.getAppelDirect()
                                + "',temp_renvoi_appel='" + res.getTempRenvoi()
                                + "',temp_cle='" + res.getTempCle()
                                + "',profession_liberale='" + res.getProfessionLiberale()
                                + "',is_refresh='1',tempo_renvoi_sipphone='" + res.getRenvoiSip()
                                + "',tempo_renvoi_telephone='" + res.getRenvoiTel()
                                + "',code_renvoi='" + res.getCodeRenvoi()
                                + "',bip1='" + res.getBip1()
                                + "',bip2='" + res.getBip2()
                                + "',bip3='" + res.getBip3()
                                + "',id_bluetooth1='" + res.getId_bluetooth1()
                                + "',id_bluetooth2='" + res.getId_bluetooth2()
                                + "',id_bluetooth3='" + res.getId_bluetooth3()
                                + "',temp_radio='" + res.getTempRadio()
                                + "',temp_identitooth='" + res.getTempIdentitooth()
                                + "',temp_identitelSIP='" + res.getTempIdentitelSIP()
                                + "',temp_profession_liberale='" + res.getTempProfessionLiberale()
                                + "',temp_liste_rouge='" + res.getTempListeRouge()
                                + "' WHERE id=" + res.getId();
                    }

                    url = url.replace(" ", "%20");
                    System.out.println(url);

                    String jso = coon.conn(url);
                    jso = jso.trim();
                    System.out.println(jso + " " + res.getEtat());

                    if (res.getEtat().equals("0")) {
                        data.updateId(res.getId(), jso);
                    }
                }
                url = Constante.URL_WEB_MAJ + "/java_php/updateResidents.php?etat=15&porte=" + idEnvoie;
                url = url.replace(' ', ',');
                String jso = coon.conn(url);
                data.addEvent("TRANSFERT LOGI#", "Lecture donnees", "Residents", " ", " ");
                lcd.clearLcd();
                lcd.print(" LOGIDIESE.COM\n LECTURE RESID.", true);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    /**
     * maj de la porte
     *
     * @param ip param
     */
    public void majPorte(int ip) {

        DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        Connection coon = new Connection();
        String s = coon.conn(Constante.URL_WEB_MAJ + "/java_php/getEtatPorteResident.php?porte=" + porte.getNumPorte());
        if (s.indexOf(";") >= 0) {
            String chaineTest = s.substring(0, s.indexOf(";"));
            String idEnvoie = s.substring(s.indexOf(";") + 1, s.lastIndexOf(";"));

            if (chaineTest.contains("100") || ip == 1) {
                System.out.println("connexion web:" + porte.getTimeConnexionWeb()
                        + "valid_connexion:" + porte.getValidConnexionWeb());

                String requete = "UPDATE porte SET "
                        + "nom='" + porte.getNomAcces()
                        + "',id_interphone='" + porte.getInterphone()
                        + "',Version='" + porte.getVersion()
                        //+"',id_teamviewer='"+porte.getIdTeamViewer()		    
                        + "',sim='" + porte.getNumeroSim()
                        + "',Tranches_horaires='" + porte.getTranchesHoraires()
                        + "',clavier_code1='" + porte.getClavierCodeUn()
                        + "',choix_cc1='" + porte.getChoixClavierCodeUn()
                        + "',temp_cc1='" + porte.getTemporaireClavierCodeUn()
                        + "',clavier_code2='" + porte.getClavierCodeDeux()
                        + "',choix_cc2='" + porte.getChoixClavierCodeDeux()
                        + "',temp_cc2='" + porte.getTemporaireClavierCodeDeux()
                        + "',entre_libre1='" + porte.getChoixEntreLibreUn()
                        + "',entre_libre2='" + porte.getChoixEntreLibreUn()
                        + "',temps_appel='" + porte.getTempsAppel()
                        + "',temps_conversation='" + porte.getTempsCommunication()
                        + "',appel_abrege='" + porte.getAppelAbrege()
                        + "',haut_parleur='" + porte.getNiveauHP()
                        + "',mess_bienvenue='" + porte.getMessageBienvenue()
                        + "',mess_utilisation='" + porte.getMessageUtilisation()
                        + "',affichage_etage='" + porte.getAffichageEtage()
                        + "',prior_affichage1='" + porte.getPrioriteAffichageUn()
                        + "',prior_affichage2='" + porte.getPrioriteAffichageDeux()
                        + "',prior_affichage3='" + porte.getPrioriteAffichageTrois()
                        + "',tempo_porte1='" + porte.getTempsOuvertureUn()
                        + "',tempo_porte2='" + porte.getTempsOuvertureDeux()
                        + "',tempo_porte3='" + porte.getTempsOuvertureTrois()
                        + "',tempo_porte4='" + porte.getTempsOuvertureQuatre()
                        + "',tempo_porte5='" + porte.getTempsOuvertureCinq()
                        + "',tempo_porte6='" + porte.getTempsOuvertureSix()
                        + "',tempo_porte7='" + porte.getTempsOuvertureSept()
                        + "',tempo_porte8='" + porte.getTempsOuvertureHuit()
                        + "',signal_sonore='" + porte.getSignalSonore()
                        + "',nomporte1='" + porte.getNomPorte1()
                        + "',lecteur_porte1='" + porte.getLecteurPorte1()
                        + "',nomporte2='" + porte.getNomPorte2()
                        + "',lecteur_porte2='" + porte.getLecteurPorte2()
                        + "',nomporte3='" + porte.getNomPorte3()
                        + "',lecteur_porte3='" + porte.getLecteurPorte3()
                        + "',nomporte4='" + porte.getNomPorte4()
                        + "',lecteur_porte4='" + porte.getLecteurPorte4()
                        + "',nomporte5='" + porte.getNomPorte5()
                        + "',lecteur_porte5='" + porte.getLecteurPorte5()
                        + "',nomporte6='" + porte.getNomPorte6()
                        + "',lecteur_porte6='" + porte.getLecteurPorte6()
                        + "',nomporte7='" + porte.getNomPorte7()
                        + "',lecteur_porte7='" + porte.getLecteurPorte7()
                        + "',nomporte8='" + porte.getNomPorte8()
                        + "',lecteur_porte8='" + porte.getLecteurPorte8()
                        + "',valid_temp_badge='" + porte.getValidTempBadge()
                        + "',valid_temp_ident='" + porte.getValidTempIdentitel()
                        + "',code_gestionnaire='" + porte.getCode()
                        + "',ipclient='" + porte.getIpClient()
                        + "',user_client='" + porte.getUserClient()
                        + "',mdp_client='" + porte.getMdpClient()
                        + "',iplocal='" + porte.getIpInterne()
                        + "',user_externe='" + porte.getUserExterne()
                        + "',mdp_externe='" + porte.getMdpExterne()
                        + "',ipexterne='" + porte.getIpExterne()
                        + "',code_accueil='" + porte.getCodeAccueil()
                        + "',gestion_couleur='" + porte.getGestionCouleur()
                        + "',valid_visuel_cle='" + porte.getVisuelBadge()
                        + "',lecteur='" + porte.getLecteur()
                        + "',id_badge='" + porte.getIdBadge()
                        + "',langue='" + porte.getLangue()
                        + "',id_wifi='" + porte.getIdWifi()
                        + "',mdp_wifi='" + porte.getMdpWifi()
                        + "',saisie_manuelle='" + porte.getSaisieManuelle()
                        + "',serveur_IP='" + porte.getServeurIP()
                        + "',event_auto='" + porte.getEvenementAutomatique()
                        + "',valid_connexion_web='" + porte.getValidConnexionWeb()
                        + "',time_connexion_web='" + porte.getTimeConnexionWeb()
                        + "',valid_web_Client='" + porte.getValidWebClient()
                        + "',valid_dhcp='" + porte.getValidServeurClientDhcp()
                        + "',ipplatine='" + porte.getIpPlatine()
                        + "',netmask='" + porte.getNetmask()
                        + "',passerelle='" + porte.getPasserelle()
                        + "',Valid_radio='" + porte.getValidRadio()
                        + "',Valid_Identitooth='" + porte.getValidIdentitooth()
                        + "',Valid_IdentitelSIP='" + porte.getValidIdentitelSIP()
                        + "',mac_ethernet='" + porte.getMacEthernet()
                        + "',mac_wifi='" + porte.getMacWifi()
                        + "',valid_uart='" + porte.getValidUart()
                        + "',temp_radio='" + porte.getTempRadio()
                        + "',temp_identitooth='" + porte.getTempIdentitooth()
                        + "',temp_identitelSIP='" + porte.getTempIdentitelSIP()
                        + "',mdp_wifi_pi='" + porte.getMdpWifiPi()
                        + "',valid_wifi_pi='" + porte.getValidWifiPi()
                        + "',tempo_vpn='" + porte.getTempoVpn()
                        + "',tempo_sip='" + porte.getTempoSip()
                        + "',valid_vpn='" + porte.getValidVpn()
                        + "'WHERE id=" + porte.getNumPorte();

                requete = requete.replace(" ", "%20");
                // url=url.replace("=","%3d");
                requete = requete.replace("<<", "gauche");
                requete = requete.replace(">>", "droite");
                // System.out.println(url);		
                System.out.println(requete);

                String url = Constante.URL_WEB_MAJ + "/java_php/updatePorte.php?id=" + porte.getNumPorte() + "&req=" + requete;
                System.out.println("\nL'URL :    \n" + url + "\n\n");

                String jso = coon.conn(url);
                url = Constante.URL_WEB_MAJ + "/java_php/updateResidents.php?etat=15&porte=" + idEnvoie;
                url = url.replace(' ', ',');
                jso = coon.conn(url);
                data.addEvent("TRANSFERT LOGI#", "Lecture donnees", "Portes", " ", " ");
                lcd.clearLcd();
                lcd.print(" LOGIDIESE.COM\n LECTURE PORTE", true);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    /**
     * maj des events
     *
     * @return true or false
     */
    public boolean majEvent() {

        DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        Connection coon = new Connection();
        String s = coon.conn(Constante.URL_WEB_MAJ + "/java_php/getEtatPorteResident.php?porte=" + porte.getNumPorte());
        if (s.indexOf(";") >= 0) {
            String chaineTest = s.substring(0, s.indexOf(";"));
            String idEnvoie = s.substring(s.indexOf(";") + 1, s.lastIndexOf(";"));

            if (s.contains("300") || porte.getEvenementAutomatique().contains("1")) {

                List<Evenement> evenement = data.getEvenementsToSend();
                ListIterator itr = evenement.listIterator();
                Evenement eve;
                while (itr.hasNext()) {

                    eve = (Evenement) itr.next();

                    String url = Constante.URL_WEB_MAJ + "/java_php/updateEvent.php?req=INSERT INTO eventporte (id_porte, date_event, lcd_ligne1, lcd_ligne2, type, lcd_ligne3, lcd_ligne4, toSend) VALUES ('"
                            + eve.getIdPorte() + "', '"
                            + eve.getDate() + "', '"
                            + eve.getLigne1() + "', '"
                            + eve.getLigne2() + "', '"
                            + eve.getType() + "', '"
                            + eve.getLigne3() + "','"
                            + eve.getLigne4() + "','1')";

                    url = url.replace(" ", "%20");
                    url = url.replace("#", "%20");
                    // url=url.replace("=","%3d");
                    System.out.println(url);
                    String jso = coon.conn(url);
                    if (jso == null) {
                        return false;
                    }

                }

                String url = Constante.URL_WEB_MAJ + "/java_php/updateEvent.php?id=" + idEnvoie + "&req=15";
                url = url.replace(" ", "%20");
                url = url.replace("#", "%20");
                url = url.replace("<<", "gauche");
                url = url.replace(">>", "droite");
                // url=url.replace("=","%3d");
                System.out.println(url);
                String jso = coon.conn(url);
                data.addEvent("TRANSFERT LOGI#", "Lecture donnees", "Evenement", " ", " ");
                lcd.clearLcd();
                lcd.print(" LOGIDIESE.COM\n LECTURE EVENT.", true);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }
        return true;
    }
}
