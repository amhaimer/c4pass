package utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sql.DataBase;
import static utils.Constante.VERSIONC4PASS;

/**
 *
 * @author Abdellah
 */
public class DownloadVersion {

    int vserver;

    /**
     * telecharger la nouvelle version du programme
     */
    public void download() {
        if (Connection.trycnx()) {

            if (NeedUpdate()) {
                try {

                    RunCMD("sudo /home/pi/NetBeansProjects/C4PASS/Majsitebd.sh ");
                    UpdateBD();
                    RunCMD("sudo /home/pi/NetBeansProjects/C4PASS/Majobd.sh");
                    downloadMusic();
                    URL website = new URL(Constante.URL_WEB_MAJ + "/releases/C4PASS-" + vserver + ".jar");
                    ReadableByteChannel rbc = null;
                    try {
                        rbc = Channels.newChannel(website.openStream());
                    } catch (IOException ex) {
                        Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream("C4PASS.jar.tmp");
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

                    } catch (IOException ex) {
                        Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            fos.close();
                            rbc.close();
                        } catch (IOException ex) {
                            Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } catch (MalformedURLException ex) {
                    Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
                }

                DataBase base = DataBase.getInstance();
                base.setVersion(String.valueOf(vserver));
                CopyReboot();
            }
        }
    }

    /**
     * vérifier si nouvelle version disponible
     *
     * @return
     */
    private boolean NeedUpdate() {

        if (tryParseInt(VERSIONC4PASS)) {

            if (tryParseInt(getVersionServer().substring(0, 3))) {

                vserver = parseInt(getVersionServer().substring(0, 3));
                if (parseInt(VERSIONC4PASS) < parseInt(getVersionServer().substring(0, 3))) {
                    return true;
                }
            }
        }
        return false;

    }

    /**
     * renvoi la version du serveur
     *
     * @return chaine
     */
    private String getVersionServer() {

        String url = Constante.URL_WEB_MAJ + "/java_php/getVersion.php";

        Connection coon = new Connection();
        String jso = coon.conn(url);

        return jso;
    }

    /**
     * convertir chaine to int
     *
     * @param value chaine
     * @return entier
     */
    static boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            System.err.print(e.getMessage());
            return false;
        }
    }

    /**
     * executer la commande
     *
     * @param cmd la commande à executer
     */
    public static void RunCMD(String cmd) {
        try {
            Runtime rt = Runtime.getRuntime();

            Process pr = rt.exec(cmd);
        } catch (IOException ex) {
            System.err.print(ex.getMessage());
        }

    }

    /**
     * redemarrer la platine
     */
    private void CopyReboot() {
        RunCMD("sudo /home/pi/NetBeansProjects/C4PASS/MajC4PASS.sh");
    }

    /**
     * mettre à jour la base de données
     */
    private void UpdateBD() {
        try {
            DataBase data = DataBase.getInstance();
            String versiondb = data.getDBVersion();

            List<String> lines = Arrays.asList("for i in `seq " + String.valueOf(parseInt(versiondb) + 1) + " 100`; \n"
                    + "do\n"
                    + "	cat gibraltar.evol.$i.sql >> gibraltar_full.sql\n"
                    + "done\n"
                    + "cat traduction.sql >> gibraltar_full.sql \n"
                    + "gzip gibraltar_full.sql");

            Path file = Paths.get("/home/pi/website/DIGICALL_Web_Prod/_mysql/generatebd.sh");
            Files.write(file, lines, Charset.forName("UTF-8"));

            RunCMD("sudo chmod 777 /home/pi/website/DIGICALL_Web_Prod/_mysql/generatebd.sh");
            RunCMD("sudo rm /home/pi/website/DIGICALL_Web_Prod/_mysql/gibraltar_full.sql.gz");

        } catch (IOException ex) {
            Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * telecharger les derniers syntheses vocale
     */
    private void downloadMusic() {
        // RunCMD("sudo rm lib/signaux_sonore.zip");
        try {
            URL website = new URL(Constante.URL_WEB_MAJ + "/releases/signaux_sonore.zip");
            ReadableByteChannel rbc = null;
            try {
                rbc = Channels.newChannel(website.openStream());
            } catch (IOException ex) {
                Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
            }
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream("signaux_sonore.zip");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

            } catch (IOException ex) {
                Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                    rbc.close();
                } catch (IOException ex) {
                    Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(DownloadVersion.class.getName()).log(Level.SEVERE, null, ex);
        }
        RunCMD("sudo rm -R dist/lib/signaux_sonore");
        RunCMD("sudo unzip signaux_sonore.zip -d dist/lib");
        // RunCMD("sudo chmod -R 777 dist/lib/signaux_sonore/*");
    }
}
