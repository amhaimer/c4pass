package utils;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sql.DataBase;
import sql.data.*;
import java.io.*;
import devices.Lcd;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * json parser
 *
 * @author Abdellah
 */
public class JsonParse {

    Lcd lcd = Lcd.getInstance();
    private static Process p;
    private static ProcessBuilder pb;

    public static boolean conn = false;
    public static boolean checkOver = true;

    /**
     * vérifier internet
     *
     * @return true or false
     */
    public static boolean tryConnection() {

        //System.out.println("searching for internet ");
        checkOver = false;
        conn = Connection.trycnx();
        checkOver = true;
        return conn;
    }

    /**
     * notifier le telephone ios
     *
     * @param id le token
     */
    public static void notifIOS(String id) {

        String url = "https://ipv.cdvi.com/json/sipnotif/" + id + "/Platine";
        Connection coon = new Connection();
        String jso = coon.conn(url);

    }

    /**
     * mettre à jour les residents
     */
    public static void updateResidents() {

        Lcd lcd = Lcd.getInstance();
        DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        String langue = porte.getTraduction();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        SimpleDateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");
        LocalDate date = LocalDate.now();
        String tempsCourrant = date.format(formatDate) + "  " + formatHeure.format(Calendar.getInstance().getTime());

        try {
            String url = Constante.URL_WEB_MAJ + "/java_php/getResidents.php?porte_id=" + porte.getNumPorte();
            /*
    * if(ind==1) url="http://logidiese.com/java_php/getPorte.php?porte_id=320";
             */
            Connection coon = new Connection();
            String jso = coon.conn(url);
            if (jso.length() > 6) {
                if (jso.startsWith("[{")) {

                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("connexion", langue), data.getTraduction("web", langue), data.getTraduction("resident_ecrit", langue), "");
                    lcd.clearLcd();
                    lcd.print(" LOGIDIESE.COM\n" + data.getTraduction("resident_ecrit", langue), true);
                    System.out.println("--#" + data.getTraduction("assistance", langue) + "  " + data.getTraduction("connexion", langue) + "  " + data.getTraduction("web", langue) + "  " + data.getTraduction("resident_ecrit", langue));

                    String str1 = "\"";
                    String str2 = "\\";
                    str2 += "\"";
                    // System.out.println(str1+" "+str2);
                    jso = jso.replaceAll(str1, str2);
                    jso = jso.substring(1, jso.length() - 6);
                    System.out.println(jso);
                    jso = "{\"residents\": [" + jso + "]}";
                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject;
                    jsonObject = (JSONObject) jsonParser.parse(jso);

                    // get a String from the JSON object
                    // String firstName = (String) jsonObject.get("id");
                    // System.out.println("id: " + firstName);
                    // get an array from the JSON object
                    JSONArray lang = (JSONArray) jsonObject.get("residents");
                    /*
      ** take the elements of the json array for(int i=0; i<lang.size(); i++){
      ** System.out.println("The " + i + " element of the array: "+lang.get(i)); }
                     */
                    Iterator i = lang.iterator();

                    String id;
                    String droit;
                    String code;
                    String nom;
                    String telephone1;
                    String renvoi_appel;
                    String telephone2;
                    String list_rouge;
                    String identifiant;
                    String technoappel;
                    String validationCle;
                    String cle1;
                    String clem1;
                    String cle2;
                    String clem2;
                    String cle3;
                    String clem3;
                    String cle4;
                    String clem4;
                    String cle5;
                    String clem5;
                    String cle6;
                    String clem6;
                    String cle7;
                    String clem7;
                    String cle8;
                    String clem8;
                    String cle9;
                    String clem9;
                    String cle10;
                    String clem10;
                    String identitel;
                    String appelDirect;
                    String tempRenvoiAppel;
                    String tempCle;
                    String professionLiberale;
                    String sipphone1;
                    String sipphone2;
                    String renvoi_sip;
                    String renvoi_tel;

                    String code_renvoi;
                    String id_bluetooth1;
                    String id_bluetooth2;
                    String id_bluetooth3;
                    String bip1;
                    String bip2;
                    String bip3;
                    String tempRadio;
                    String tempIdentitooth;
                    String tempIdentitelSIP;
                    String tempProfessionLiberale;
                    String tempListeRouge;

                    String ecriture;
                    // take each value from the json array separately
                    while (i.hasNext()) {
                        JSONObject innerObj = (JSONObject) i.next();
                        id = (String) innerObj.get("id");
                        droit = (String) innerObj.get("droit");
                        code = (String) innerObj.get("code");
                        nom = (String) innerObj.get("nom");
                        telephone1 = (String) innerObj.get("telephone1");
                        renvoi_appel = (String) innerObj.get("renvoi_appel");
                        telephone2 = (String) innerObj.get("telephone2");
                        list_rouge = (String) innerObj.get("list_rouge");
                        identifiant = (String) innerObj.get("Identifiant");
                        technoappel = (String) innerObj.get("techno_appel");
                        cle1 = (String) innerObj.get("cle1");
                        clem1 = (String) innerObj.get("clem1");
                        cle2 = (String) innerObj.get("cle2");
                        clem2 = (String) innerObj.get("clem2");
                        cle3 = (String) innerObj.get("cle3");
                        clem3 = (String) innerObj.get("clem3");
                        cle4 = (String) innerObj.get("cle4");
                        clem4 = (String) innerObj.get("clem4");
                        cle5 = (String) innerObj.get("cle5");
                        clem5 = (String) innerObj.get("clem5");
                        cle6 = (String) innerObj.get("cle6");
                        clem6 = (String) innerObj.get("clem6");
                        cle7 = (String) innerObj.get("cle7");
                        clem7 = (String) innerObj.get("clem7");
                        cle8 = (String) innerObj.get("cle8");
                        clem8 = (String) innerObj.get("clem8");
                        cle9 = (String) innerObj.get("cle9");
                        clem9 = (String) innerObj.get("clem9");
                        cle10 = (String) innerObj.get("cle10");
                        clem10 = (String) innerObj.get("clem10");
                        identitel = (String) innerObj.get("identitel");
                        appelDirect = (String) innerObj.get("temp_appeldirect");
                        tempRenvoiAppel = (String) innerObj.get("temp_renvoi_appel");
                        tempCle = (String) innerObj.get("temp_cle");
                        professionLiberale = (String) innerObj.get("profession_liberale");
                        sipphone1 = (String) innerObj.get("sipphone1");
                        sipphone2 = (String) innerObj.get("sipphone2");
                        renvoi_sip = (String) innerObj.get("tempo_renvoi_sipphone");
                        renvoi_tel = (String) innerObj.get("tempo_renvoi_telephone");

                        code_renvoi = (String) innerObj.get("code_renvoi");
                        bip1 = (String) innerObj.get("bip1");
                        bip2 = (String) innerObj.get("bip2");
                        bip3 = (String) innerObj.get("bip3");
                        id_bluetooth1 = (String) innerObj.get("id_bluetooth1");
                        id_bluetooth2 = (String) innerObj.get("id_bluetooth2");
                        id_bluetooth3 = (String) innerObj.get("id_bluetooth3");
                        tempRadio = (String) innerObj.get("temp_radio");
                        tempIdentitooth = (String) innerObj.get("temp_identitooth");
                        tempIdentitelSIP = (String) innerObj.get("temp_identitelSIP");
                        tempProfessionLiberale = (String) innerObj.get("temp_profession_liberale");
                        tempListeRouge = (String) innerObj.get("temp_liste_rouge");
                        ecriture = (String) innerObj.get("id_ecriture");
                        validationCle = (String) innerObj.get("autorisation_badge");
                        Resident resident = new Resident();
                        resident.setId(id);
                        resident.setDroit(droit);
                        resident.setCode(code);
                        resident.setName(nom);
                        resident.setTelephone1(telephone1);
                        resident.setRenvoiAppel(renvoi_appel);
                        resident.setTelephone2(telephone2);
                        resident.setListeRouge(list_rouge);
                        resident.setIdentifiant(identifiant);
                        resident.setTechnoAppel(technoappel);
                        resident.setValidationCle(validationCle);
                        resident.setCle1(cle1);
                        resident.setClem1(clem1);
                        resident.setCle2(cle2);
                        resident.setClem2(clem2);
                        resident.setCle3(cle3);
                        resident.setClem3(clem3);
                        resident.setCle4(cle4);
                        resident.setClem4(clem4);
                        resident.setCle5(cle5);
                        resident.setClem5(clem5);
                        resident.setCle6(cle6);
                        resident.setClem6(clem6);
                        resident.setCle7(cle7);
                        resident.setClem7(clem7);
                        resident.setCle8(cle8);
                        resident.setClem8(clem8);
                        resident.setCle9(cle9);
                        resident.setClem9(clem9);
                        resident.setCle10(cle10);
                        resident.setClem10(clem10);
                        resident.setIdentitel(identitel);
                        resident.setAppelDirect(appelDirect);
                        resident.setTempRenvoi(tempRenvoiAppel);
                        resident.setProfessionLiberale(professionLiberale);
                        resident.setTempCle(tempCle);
                        resident.setSipPhoneUn(sipphone1);
                        resident.setSipPhoneDeux(sipphone2);
                        resident.setRenvoiSip(renvoi_sip);
                        resident.setRenvoiTel(renvoi_tel);

                        resident.setCodeRenvoi(code_renvoi);
                        resident.setId_bluetooth1(id_bluetooth1);
                        resident.setId_bluetooth2(id_bluetooth2);
                        resident.setId_bluetooth3(id_bluetooth3);
                        resident.setBip1(bip1);
                        resident.setBip2(bip2);
                        resident.setBip3(bip3);
                        resident.setTempRadio(tempRadio);
                        resident.setTempIdentitooth(tempIdentitooth);
                        resident.setTempIdentitelSIP(tempIdentitelSIP);
                        resident.setTempProfessionLiberale(tempProfessionLiberale);
                        resident.setTempListeRouge(tempListeRouge);
                        System.out.println("envoi reussi");
                        String idd = data.getResident(code).getId();
                        if (idd == null) {
                            data.insert(resident);
                        } else {
                            resident.setId(idd);
                            data.update(resident);
                        }
                        String jso2 = coon.conn(Constante.URL_WEB_MAJ + "/java_php/updateEtatResident.php?id=" + ecriture);
                        System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("effectuee", langue));
                        data.addEvent("TRANSFERT LOGI#", "Ecriture donnees", "Residents", " ", " ");
                        lcd.clearLcd();
                        lcd.print(" LOGIDIESE.COM\n ECRITURE RESID.", true);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            System.err.print(e.getMessage());
                        }
                    }
                    // take each value from the json array separately
                } else {
                    System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("echouee", langue));
                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requetteinternet", langue), data.getTraduction("logidiesecom", langue), data.getTraduction("echouee", langue), " ");
                }
            }
        } catch (ParseException ex) {
            System.err.print(ex.getMessage());
        }
    }

    /**
     * update porte
     */
    public static void updatePorte() {
        boolean modifWifi = false;
        Lcd lcd = Lcd.getInstance();
        DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        String langue = porte.getTraduction();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        SimpleDateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");
        LocalDate date = LocalDate.now();
        String tempsCourrant = date.format(formatDate) + "  " + formatHeure.format(Calendar.getInstance().getTime());

        try {
            String url = Constante.URL_WEB_MAJ + "/java_php/getPorte.php?porte_id=" + porte.getNumPorte();
            Connection coon = new Connection();
            String jso = coon.conn(url);
            if (jso.length() > 11) {
                if (jso.startsWith("[{")) {

                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("connexion", langue), data.getTraduction("web", langue), data.getTraduction("porte_ecrit", langue), "");
                    lcd.clearLcd();
                    lcd.print(" LOGIDIESE.COM\n" + data.getTraduction("porte_ecrit", langue), true);
                    System.out.println("--#" + data.getTraduction("assistance", langue) + "  " + data.getTraduction("connexion", langue) + "  " + data.getTraduction("web", langue) + "  " + data.getTraduction("porte_ecrit", langue));

                    String str1 = "\"";
                    String str2 = "\\";
                    str2 += "\"";
                    jso = jso.replaceAll(str1, str2);
                    jso = jso.substring(1, jso.length() - 11);

                    System.out.println("Parsing");
                    System.out.println("test");
                    System.out.println(jso);

                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject;

                    jsonObject = (JSONObject) jsonParser.parse(jso);

                    // get a String from the JSON object
                    String envoi = (String) jsonObject.get("id_envoi");
                    String nomAcces = (String) jsonObject.get("nom");
                    String interphone = (String) jsonObject.get("id_interphone");
                    String version = (String) jsonObject.get("Version");
                    String sim = (String) jsonObject.get("sim");
                    String dateModification = (String) jsonObject.get("date_modification");
                    String tranchesHoraires = (String) jsonObject.get("Tranches_horaires");
                    String ClavierCodeUn = (String) jsonObject.get("clavier_code1");
                    String choixClavierCodeUn = (String) jsonObject.get("choix_cc1");
                    String temporaireClavierCodeUn = (String) jsonObject.get("temp_cc1");
                    String ClavierCodeDeux = (String) jsonObject.get("clavier_code2");
                    String choixClavierCodeDeux = (String) jsonObject.get("choix_cc2");
                    String temporaireClavierCodeDeux = (String) jsonObject.get("temp_cc2");
                    String choixEntreLibreUn = (String) jsonObject.get("entre_libre1");
                    String choixEntreLibreDeux = (String) jsonObject.get("entre_libre2");
                    String tempsAppel = (String) jsonObject.get("temps_appel");
                    String TempsCommunication = (String) jsonObject.get("temps_conversation");
                    String appelAbrege = (String) jsonObject.get("appel_abrege");
                    String niveauHP = (String) jsonObject.get("haut_parleur");
                    String messageBienvenue = (String) jsonObject.get("mess_bienvenue");
                    String messageUtilisation = (String) jsonObject.get("mess_utilisation");
                    String affichageEtage = (String) jsonObject.get("affichage_etage");
                    String prioriteAffichageUn = (String) jsonObject.get("prior_affichage1");
                    String prioriteAffichageDeux = (String) jsonObject.get("prior_affichage2");
                    String prioriteAffichageTroix = (String) jsonObject.get("prior_affichage3");
                    String tempsOuvertureUn = (String) jsonObject.get("tempo_porte1");
                    String tempsOuvertureDeux = (String) jsonObject.get("tempo_porte2");
                    String tempsOuvertureTrois = (String) jsonObject.get("tempo_porte3");
                    String tempsOuvertureQuatre = (String) jsonObject.get("tempo_porte4");
                    String tempsOuvertureCinq = (String) jsonObject.get("tempo_porte5");
                    String tempsOuvertureSix = (String) jsonObject.get("tempo_porte6");
                    String tempsOuvertureSept = (String) jsonObject.get("tempo_porte7");
                    String tempsOuvertureHuit = (String) jsonObject.get("tempo_porte8");
                    String signalSonore = (String) jsonObject.get("signal_sonore");
                    String nomPorte1 = (String) jsonObject.get("nomporte1");
                    String lecteurPorte1 = (String) jsonObject.get("lecteur_porte1");
                    String nomPorte2 = (String) jsonObject.get("nomporte2");
                    String lecteurPorte2 = (String) jsonObject.get("lecteur_porte2");
                    String nomPorte3 = (String) jsonObject.get("nomporte3");
                    String lecteurPorte3 = (String) jsonObject.get("lecteur_porte3");
                    String nomPorte4 = (String) jsonObject.get("nomporte4");
                    String lecteurPorte4 = (String) jsonObject.get("lecteur_porte4");
                    String nomPorte5 = (String) jsonObject.get("nomporte5");
                    String lecteurPorte5 = (String) jsonObject.get("lecteur_porte5");
                    String nomPorte6 = (String) jsonObject.get("nomporte6");
                    String lecteurPorte6 = (String) jsonObject.get("lecteur_porte6");
                    String nomPorte7 = (String) jsonObject.get("nomporte7");
                    String lecteurPorte7 = (String) jsonObject.get("lecteur_porte7");
                    String nomPorte8 = (String) jsonObject.get("nomporte8");
                    String lecteurPorte8 = (String) jsonObject.get("lecteur_porte8");
                    String validTempBadge = (String) jsonObject.get("valid_temp_badge");
                    String validTempIdentitel = (String) jsonObject.get("valid_temp_ident");
                    String code = (String) jsonObject.get("code_gestionnaire");
                    String ipClient = (String) jsonObject.get("ipclient");
                    String userClient = (String) jsonObject.get("user_client");
                    String mdpClient = (String) jsonObject.get("mdp_client");
                    String ipInterne = (String) jsonObject.get("iplocal");
                    String ipExterne = (String) jsonObject.get("ipexterne");
                    String userExterne = (String) jsonObject.get("user_externe");
                    String mdpExterne = (String) jsonObject.get("mdp_externe");
                    String codeAccueil = (String) jsonObject.get("code_accueil");
                    String gestionCouleur = (String) jsonObject.get("gestion_couleur");
                    String validVisuelCle = (String) jsonObject.get("valid_visuel_cle");
                    String lecteur = (String) jsonObject.get("lecteur");
                    String idBadge = (String) jsonObject.get("id_badge");

                    String idWifi = (String) jsonObject.get("id_wifi");
                    String mdpWifi = (String) jsonObject.get("mdp_wifi");
                    String saisieManuelle = (String) jsonObject.get("saisie_manuelle");
                    String serveurIP = (String) jsonObject.get("serveur_IP");
                    String eventAuto = (String) jsonObject.get("event_auto");
                    String validConnexionWeb = (String) jsonObject.get("valid_connexion_web");
                    String timeConnexionWeb = (String) jsonObject.get("time_connexion_web");
                    String validWebClient = (String) jsonObject.get("valid_web_client");
                    String validServeurClientDhcp = (String) jsonObject.get("valid_dhcp");
                    String ipPlatine = (String) jsonObject.get("ipplatine");
                    String netmask = (String) jsonObject.get("netmask");
                    String passerelle = (String) jsonObject.get("passerelle");
                    String validRadio = (String) jsonObject.get("valid_radio");

                    String validIdentitelSIP = (String) jsonObject.get("valid_identitelSIP");
                    String macEthernet = (String) jsonObject.get("mac_ethernet");
                    String macWifi = (String) jsonObject.get("mac_wifi");
                    String validUart = (String) jsonObject.get("valid_uart");
                    String tempRadio = (String) jsonObject.get("temp_radio");
                    String tempIdentitooth = (String) jsonObject.get("temp_identitooth");
                    String tempIdentitelSIP = (String) jsonObject.get("temp_identitelSIP");
                    String validWifiPi = (String) jsonObject.get("valid_wifi_pi");
                    String mdpWifiPi = (String) jsonObject.get("mdp_wifi_pi");
                    String tempoVpn = (String) jsonObject.get("tempo_vpn");
                    String tempoSip = (String) jsonObject.get("tempo_sip");
                    String validVpn = (String) jsonObject.get("valid_vpn");
                    String validSipUn = (String) jsonObject.get("valid_sip1");
                    String validSipDeux = (String) jsonObject.get("valid_sip2");
                    String tempoConnect = (String) jsonObject.get("tempo_connect");

                    porte.setNomAcces(nomAcces);
                    porte.setInterphone(interphone);
                    porte.setVersion(version);
                    porte.setNumeroSim(sim);
                    porte.setDateModification(dateModification);
                    porte.setTranchesHoraires(tranchesHoraires);
                    porte.setClavierCodeUn(ClavierCodeUn);
                    porte.setChoixClavierCodeUn(choixClavierCodeUn);
                    porte.setTemporaireClavierCodeUn(temporaireClavierCodeUn);
                    porte.setClavierCodeDeux(ClavierCodeDeux);
                    porte.setChoixClavierCodeDeux(choixClavierCodeDeux);
                    porte.setTemporaireClavierCodeDeux(temporaireClavierCodeDeux);
                    porte.setChoixEntreLibreUn(choixEntreLibreUn);
                    porte.setChoixEntreLibreDeux(choixEntreLibreDeux);
                    porte.setTempsAppel(tempsAppel);
                    porte.setTempsCommunication(TempsCommunication);
                    porte.setAppelAbrege(appelAbrege);

                    if (!porte.getNiveauHP().trim().equals(niveauHP.trim())) {
                        porte.setNiveauHP(niveauHP);

                    }
                    porte.setMessageBienvenue(messageBienvenue);
                    porte.setMessageUtilisation(messageUtilisation);
                    porte.setAffichageEtage(affichageEtage);
                    porte.setPrioriteAffichageUn(prioriteAffichageUn);
                    porte.setPrioriteAffichageDeux(prioriteAffichageDeux);
                    porte.setPrioriteAffichageTrois(prioriteAffichageTroix);
                    porte.setTempsOuvertureUn(tempsOuvertureUn);
                    porte.setTempsOuvertureDeux(tempsOuvertureDeux);
                    porte.setTempsOuvertureTrois(tempsOuvertureTrois);
                    porte.setTempsOuvertureQuatre(tempsOuvertureQuatre);
                    porte.setTempsOuvertureCinq(tempsOuvertureCinq);
                    porte.setTempsOuvertureSix(tempsOuvertureSix);
                    porte.setTempsOuvertureSept(tempsOuvertureSept);
                    porte.setTempsOuvertureHuit(tempsOuvertureHuit);
                    porte.setSignalSonore(signalSonore);
                    porte.setNomPorte1(nomPorte1);
                    porte.setLecteurPorte1(lecteurPorte1);
                    porte.setNomPorte2(nomPorte2);
                    porte.setLecteurPorte2(lecteurPorte2);
                    porte.setNomPorte3(nomPorte3);
                    porte.setLecteurPorte3(lecteurPorte3);
                    porte.setNomPorte4(nomPorte4);
                    porte.setLecteurPorte4(lecteurPorte4);
                    porte.setNomPorte5(nomPorte5);
                    porte.setLecteurPorte5(lecteurPorte5);
                    porte.setNomPorte6(nomPorte6);
                    porte.setLecteurPorte6(lecteurPorte6);
                    porte.setNomPorte7(nomPorte7);
                    porte.setLecteurPorte7(lecteurPorte7);
                    porte.setNomPorte8(nomPorte8);
                    porte.setLecteurPorte8(lecteurPorte8);
                    porte.setValidTempBadge(validTempBadge);
                    porte.setValidTempIdentitel(validTempIdentitel);
                    porte.setCode(code);
                    porte.setIpClient(ipClient);
                    porte.setUserClient(userClient);
                    porte.setMdpClient(mdpClient);
                    porte.setIpInterne(ipInterne);
                    porte.setUserExterne(userExterne);
                    porte.setMdpExterne(mdpExterne);
                    porte.setIpExterne(ipExterne);
                    porte.setCodeAccueil(codeAccueil);
                    porte.setGestionCouleur(gestionCouleur);
                    porte.setVisuelBadge(validVisuelCle);
                    porte.setLecteur(lecteur);
                    porte.setIdBadge(idBadge);
                    porte.setLangue(langue);
                    if (!porte.getIdWifi().equals(idWifi)) {
                        porte.setIdWifi(idWifi);
                        modifWifi = true;
                    }
                    if (!porte.getMdpWifi().equals(mdpWifi)) {
                        porte.setMdpWifi(mdpWifi);
                        modifWifi = true;
                    }
                    porte.setSaisieManuelle(saisieManuelle);
                    porte.setServeurIP(serveurIP);
                    porte.setEvenementAutomatique(eventAuto);
                    porte.setValidConnexionWeb(validConnexionWeb);
                    porte.setTimeConnexionWeb(timeConnexionWeb);
                    porte.setValidWebClient(validWebClient);
                    porte.setValidServeurClientDhcp(validServeurClientDhcp);
                    porte.setIpPlatine(ipPlatine);
                    porte.setNetmask(netmask);
                    porte.setPasserelle(passerelle);
                    porte.setValidRadio(validRadio);
                    porte.setValidIdentitooth(validRadio);
                    porte.setValidIdentitelSIP(validIdentitelSIP);
                    porte.setMacEthernet(macEthernet);
                    porte.setMacWifi(macWifi);
                    porte.setValidUart(validUart);
                    porte.setTempRadio(tempRadio);
                    porte.setTempIdentitooth(tempIdentitooth);
                    porte.setTempIdentitelSIP(tempIdentitelSIP);
                    porte.setValidWifiPi(validWifiPi);
                    porte.setMdpWifiPi(mdpWifiPi);
                    porte.setTempoVpn(tempoVpn);
                    porte.setTempoSip(tempoSip);
                    porte.setValidVpn(validVpn);
                    porte.setValidSipUn(validSipUn);
                    porte.setValidSipDeux(validSipDeux);
                    porte.setTempoConnect(tempoConnect);
                    /*
       ** porte.setCodePorte2(codePorte2);
       ** porte.setTempoMenu(tempoMenu);
       ** porte.setTelephoneSms(telephoneSms);
       ** porte.setActivationVisio(activationVisio);		
                     */

                    data.update(porte);
                    data.update_ClavierCodeUn(porte);
                    data.update_ClavierCodeDeux(porte);
                    data.update_Badge(porte);
                    data.update_EntreLibreUn(porte);
                    data.update_EntreLibreDeux(porte);
                    if (modifWifi) {
                        try {
                            pb = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's/.*ssid.*/ssid=\"" + idWifi.trim() + "\"/g' /etc/wpa_supplicant/wpa_supplicant.conf");
                            p = pb.start();

                            if (mdpWifi.trim().length() == 0) {
                                pb = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's/.*psk.*/#psk=\"" + mdpWifi.trim() + "\"/g' /etc/wpa_supplicant/wpa_supplicant.conf");
                                p = pb.start();
                                pb = new ProcessBuilder("sudo", "bash", "-c",
                                        "sed -i 's/.*key_mgmt.*/key_mgmt=NONE/g' /etc/wpa_supplicant/wpa_supplicant.conf");
                                p = pb.start();
                            } else {
                                pb = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's/.*psk.*/psk=\"" + mdpWifi.trim() + "\"/g' /etc/wpa_supplicant/wpa_supplicant.conf");
                                p = pb.start();
                                pb = new ProcessBuilder("sudo", "bash", "-c",
                                        "sed -i 's/.*key_mgmt.*/#key_mgmt=\"\"/g' /etc/wpa_supplicant/wpa_supplicant.conf");
                                p = pb.start();
                            }
                        } catch (IOException e) {
                            System.out.println("ERROR");
                        }
                        lcd.clearLcd();
                        lcd.print("CONFIGURATION DE\n WIFI ...", true);

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                        }

                        try {

                            Runtime.getRuntime().exec("sudo ifup wlan0");

                        } catch (IOException e) {
                            System.out.println(e);
                        }
                        lcd.setCursor(2, 0);
                        lcd.print("REDEMARRAGE ...  ", false);
                        data.addEvent("ASSISTANCE", "REDEMARRAGE", "jsonparse", " ", " ");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                        try {
                            pb = new ProcessBuilder("sudo", "bash", "-c", "reboot -f");
                            p = pb.start();
                        } catch (IOException e) {
                            System.out.println("ERROR rebooting");
                        }
                    }
                    System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requete_internet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("effectuee", langue));
                    String jso2 = coon.conn(Constante.URL_WEB_MAJ + "/java_php/updateEtatPorte.php?id=" + envoi);
                    data.addEvent("TRANSFERT LOGI#", "Ecriture donnees", "Porte", " ", " ");

                    lcd.clearLcd();
                    lcd.print(" LOGIDIESE.COM\n ECRITURE PORTE", true);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }

            }
        } catch (ParseException | NullPointerException ex) {
            System.err.print(ex.getMessage());
        }
    }

    /**
     * update profil
     */
    public static void updateProfil() {

        DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        String langue = porte.getTraduction();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        SimpleDateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");
        LocalDate date = LocalDate.now();
        String tempsCourrant = date.format(formatDate) + "  " + formatHeure.format(Calendar.getInstance().getTime());

        try {
            String url = Constante.URL_WEB_MAJ + "/java_php/getProfil.php?id=" + porte.getNumPorte();
            String url2 = Constante.URL_WEB_MAJ + "/java_php/getProfilTemporel.php?id=" + porte.getNumPorte();
            String url3 = Constante.URL_WEB_MAJ + "/java_php/getJourFerie.php?id=" + porte.getNumPorte();
            Connection coon = new Connection();
            String jso = coon.conn(url);
            if (jso.length() > 11) {
                if (jso.startsWith("[{")) {
                    String str1 = "\"";
                    String str2 = "\\";
                    str2 += "\"";
                    // System.out.println(str1+" "+str2);
                    jso = jso.replaceAll(str1, str2);
                    jso = jso.substring(1, jso.length() - 7);

                    System.out.println("Parsing");
                    System.out.println(jso);

                    JSONParser jsonParser = new JSONParser();

                    JSONObject jsonObject;

                    jsonObject = (JSONObject) jsonParser.parse(jso);

                    // get a String from the JSON object
                    String id = (String) jsonObject.get("id");
                    String idResidence = (String) jsonObject.get("id_residence");
                    String nom = (String) jsonObject.get("nom");
                    String jourDebut = (String) jsonObject.get("jour_debut");
                    String jourFin = (String) jsonObject.get("jour_fin");
                    String etat = (String) jsonObject.get("etat");

                    String idd = data.getProfil(id).getId();
                    if (idd == null) {
                        data.insert(new Profil(id, idResidence, jourDebut, jourFin, nom, etat));
                    } else {
                        data.update(new Profil(id, idResidence, jourDebut, jourFin, nom, etat));
                    }

                    String jso2 = coon.conn(Constante.URL_WEB_MAJ + "/java_php/updateEtatProfilTemporel.php?id=" + id + "&code=" + "1");
                    System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("effectuee", langue));
                    data.addEvent("TRANSFERT LOGI#", "Ecriture donnees", "profil", " ", " ");
                } else {
                    System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("echouee", langue));
                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requetteinternet", langue), data.getTraduction("logidiesecom", langue), data.getTraduction("echouee", langue), " ");
                }
            } else {
                jso = coon.conn(url2);
                if (jso.length() > 11) {
                    if (jso.startsWith("[{")) {
                        String str1 = "\"";
                        String str2 = "\\";
                        str2 += "\"";
                        // System.out.println(str1+" "+str2);
                        jso = jso.replaceAll(str1, str2);
                        jso = jso.substring(1, jso.length() - 7);

                        System.out.println("Parsing");
                        System.out.println(jso);

                        JSONParser jsonParser = new JSONParser();

                        JSONObject jsonObject;

                        jsonObject = (JSONObject) jsonParser.parse(jso);

                        // get a String from the JSON object
                        String id = (String) jsonObject.get("id");
                        String idProfil = (String) jsonObject.get("id_profil");
                        String jour = (String) jsonObject.get("jour");
                        String heureDebut = (String) jsonObject.get("heure_debut");
                        String heureFin = (String) jsonObject.get("heure_fin");
                        String etat = (String) jsonObject.get("etat");

                        String idd = data.getProfilTemporel(id).getId();
                        if (idd == null) {
                            data.insert(new ProfilTemporel(id, idProfil, heureDebut, heureFin, jour, etat));
                        } else {
                            data.update(new ProfilTemporel(id, idProfil, heureDebut, heureFin, jour, etat));
                        }

                        String jso2 = coon.conn(Constante.URL_WEB_MAJ + "/java_php/updateEtatProfilTemporel.php?id=" + id + "&code=" + "2");
                        data.addEvent("TRANSFERT LOGI#", "Ecriture donnees", "profil temporel", " ", " ");
                        System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("effectuee", langue));

                    } else {
                        System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("echouee", langue));
                        data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requetteinternet", langue), data.getTraduction("logidiesecom", langue), data.getTraduction("echouee", langue), " ");
                    }
                } else {
                    jso = coon.conn(url3);
                    if (jso.length() > 11) {
                        if (jso.startsWith("[{")) {
                            String str1 = "\"";
                            String str2 = "\\";
                            str2 += "\"";
                            // System.out.println(str1+" "+str2);
                            jso = jso.replaceAll(str1, str2);
                            jso = jso.substring(1, jso.length() - 7);

                            System.out.println("Parsing");
                            System.out.println(jso);

                            JSONParser jsonParser = new JSONParser();

                            JSONObject jsonObject;

                            jsonObject = (JSONObject) jsonParser.parse(jso);

                            // get a String from the JSON object
                            String id = (String) jsonObject.get("id");
                            String idResidence = (String) jsonObject.get("id_residence");
                            String jourDebut = (String) jsonObject.get("jour_debut");
                            String jourFin = (String) jsonObject.get("jour_fin");
                            String etat = (String) jsonObject.get("etat");

                            String idd = data.getJourFerie(id).getId();
                            if (idd == null) {
                                data.insert(new JourFerie(id, idResidence, jourDebut, jourFin, etat));
                            } else {
                                data.update(new JourFerie(id, idResidence, jourDebut, jourFin, etat));
                            }

                            String jso2 = coon.conn(Constante.URL_WEB_MAJ + "/java_php/updateEtatProfilTemporel.php?id=" + id + "&code=" + "3");
                            System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("effectuee", langue));
                            data.addEvent("TRANSFERT LOGI#", "Ecriture donnees", "jour ferie", " ", " ");
                        } else {
                            System.out.println("-- # " + tempsCourrant + "  " + data.getTraduction("requetteinternet", langue) + " " + data.getTraduction("logidiesecom", langue) + " " + data.getTraduction("echouee", langue));
                            data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requetteinternet", langue), data.getTraduction("logidiesecom", langue), data.getTraduction("echouee", langue), " ");
                        }
                    }
                }
            }
        } catch (ParseException | NullPointerException ex) {
            System.err.print(ex.getMessage());
        }
    }
}
