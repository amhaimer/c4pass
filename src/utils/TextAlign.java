//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file TextAlign.java Controle de l'afficheur Lcd
/**
 * Package utilitaires contient toute les classes utile a application java tels le centrage des caractere.
 */
package utils;

/**
 * interface text align
 *
 * @author Abdellah
 */
public interface TextAlign {

    /**
     * methode d'alignement
     *
     * @param s chaine à aligner
     * @return résultat
     */
    public String align(String s);
}
