package utils;

/**
 * aligner a gauche
 *
 * @author Abdellah
 */
public class Left implements TextAlign {

    private static int sizeLine = 16;

    /**
     * aligner
     *
     * @param s la chaine à aligner
     * @return le résultat
     */
    @Override
    public String align(String s) {
        while (s.length() < sizeLine) {
            s = " " + s;
        }
        return s;
    }
}
