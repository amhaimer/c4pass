package utils;

import java.io.File;

import java.io.IOException;

import javax.sound.sampled.*;

/**
 * classe qui permet de jouer des synthese vocale
 *
 * @author Abdellah
 */
public class Sound {

    static final String directory = "/home/pi/NetBeansProjects/C4PASS/dist/lib/signaux_sonore/";
    static boolean playing = false;
    static Clip clip = null;

    /**
     * play
     *
     * @param filename le son a jouer
     */
    public synchronized void play(final String filename) {

        if (!playing) {
            new Thread(() -> {
                if (!playing) {
                    File file = new File(directory + filename);

                    try {
                        Runtime.getRuntime().exec("sudo aplay " + directory + filename);
                        playing = true;
                        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
                        AudioFormat format = audioInputStream.getFormat();
                        long audioFileLength = file.length();
                        int frameSize = format.getFrameSize();
                        float frameRate = format.getFrameRate();
                        float durationInSeconds = (audioFileLength / (frameSize * frameRate));

                        Thread.sleep((long) ((durationInSeconds * 1000) + 100));
                        playing = false;
                    } catch (IOException | InterruptedException | UnsupportedAudioFileException e) {
                        System.err.print(e);
                    }
                }
            }).start();
        }
    }

}
