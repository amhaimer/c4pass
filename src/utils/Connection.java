package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * classe connection
 *
 * @author Abdellah
 */
public class Connection {

    TrustManager[] trustAllCerts = new TrustManager[]{
        new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
            }
        }
    };

    /**
     * installer la certif pour le ssl
     */
    public void installtrust() {
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            System.err.print(e.getMessage());
        }
    }

    /**
     * tester la connection internet
     *
     * @return true or false
     */
    public static boolean trycnx() {

        try {
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(5000);
            conn.connect();
            //conn.setConnectTimeout(15000);
            return true;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            return false;
        }
    }

    public String conn(String url) {

        if (trycnx()) {
            String s = "";
            URL u = null;
            try {
                u = new URL(url);
            } catch (MalformedURLException e) {
                System.err.print(e.getMessage());
            }
            URLConnection c = null;
            try {
                c = u.openConnection();
            } catch (java.net.UnknownHostException em) {
                return null;
            } catch (IOException e) {
                return null;
            }

            InputStream r;
            try {
                r = c.getInputStream();
            } catch (IOException e) {
                System.err.print(e.getMessage());
                return null;
            } catch (IllegalArgumentException e) {
                return null;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(r));
            try {
                for (String line; (line = reader.readLine()) != null;) {
                    s += line;
                }
            } catch (IOException e) {
                System.err.print(e.getMessage());
            }
            return s;
        }
        return "null";
    }
}
