package utils;

import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * gestion des dtmf (obsolete )
 *
 * @author Abdellah
 */
public class DTMFManager {

    String ouverture1, ouverture2, timer;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    Process rm, dumpcap;

    /**
     * init
     */
    public void init() {

        porte = Porte.getInstance();
        ouverture1 = toHex(porte.getTouchesDTMF().charAt(0));
        ouverture2 = toHex(porte.getTouchesDTMF().charAt(1));
        timer = toHex(porte.getTouchesDTMF().charAt(2));
        try {
            rm = Runtime.getRuntime()
                    .exec("sudo rm /tmp/dumpcap");
            dumpcap = Runtime.getRuntime()
                    .exec("sudo dumpcap -w /tmp/dumpcap -q");
        } catch (IOException e) {
            System.err.print(e.getMessage());
        }

    }

    /**
     * vérifier les portes
     *
     * @return entier
     */
    public int check() {

        if (check_ouverture1(ouverture1)) {
            return 1;
        } else if (check_ouverture2(ouverture2)) {
            return 2;
        } else if (check_timer(timer)) {
            return 3;
        } else {
            return 0;
        }
    }

    /**
     * exit
     */
    public void exit() {
        dumpcap.destroy();
    }

    /**
     * reset
     */
    public void reset() {
        dumpcap.destroy();

        try {
            dumpcap = Runtime.getRuntime()
                    .exec("sudo dumpcap -w /tmp/dumpcap -q");
        } catch (IOException e) {
            System.err.print(e.getMessage());
        }
        for (int i = 0; i < 9999999; i++);
    }

    /**
     * check ouverture 1
     *
     * @param ouv1 chaine ouv
     * @return true or false
     */
    private boolean check_ouverture1(String ouv1) {

        try {
            ProcessBuilder ps = new ProcessBuilder("/bin/sh", "-c", "sudo hd /tmp/dumpcap | grep -A2 \"80 65\" | grep \"" + ouv1 + "\"");
            ps.redirectErrorStream(true);
            Process process = ps.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;

            while ((s = reader.readLine()) != null) {
                rm = Runtime.getRuntime()
                        .exec("sudo truncate -s0 /tmp/dumpcap");
                for (int i = 0; i < 9999999; i++);
                return true;

            }
        } catch (IOException e) {
            System.err.print(e.getMessage());
        }
        return false;
    }

    /**
     * check ouverture 2
     *
     * @param ouv1 chaine ouv
     * @return true or false
     */
    private boolean check_ouverture2(String ouv2) {
        try {
            ProcessBuilder ps = new ProcessBuilder("/bin/sh", "-c", "sudo hd /tmp/dumpcap | grep -A2 \"80 65\" | grep -A2 \"" + ouv2 + "\"");
            ps.redirectErrorStream(true);
            Process process = ps.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;
            while ((s = reader.readLine()) != null) {
                rm = Runtime.getRuntime()
                        .exec("sudo truncate -s0 /tmp/dumpcap");
                for (int i = 0; i < 9999999; i++);
                return true;
            }
        } catch (IOException e) {
            System.err.print(e.getMessage());
        }
        return false;
    }

    /**
     * check timer
     *
     * @param timer chaine timer
     * @return true or false
     */
    private boolean check_timer(String timer) {
        try {
            ProcessBuilder ps = new ProcessBuilder("/bin/sh", "-c", "sudo hd /tmp/dumpcap | grep -A2 \"80 65\" | grep -A2 \"" + timer + "\"");
            ps.redirectErrorStream(true);
            Process process = ps.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;
            while ((s = reader.readLine()) != null) {
                rm = Runtime.getRuntime()
                        .exec("sudo truncate -s0 /tmp/dumpcap");
                for (int i = 0; i < 9999999; i++);
                return true;
            }
        } catch (IOException e) {
            System.err.print(e.getMessage());
        }
        return false;
    }

    /**
     * convert to hex
     *
     * @param c charactere
     * @return chaine
     */
    private String toHex(char c) {
        switch (c) {
            case '0':
                return "00 0a";
            case '1':
                return "01 0a";
            case '2':
                return "02 0a";
            case '3':
                return "03 0a";
            case '4':
                return "04 0a";
            case '5':
                return "05 0a";
            case '6':
                return "06 0a";
            case '7':
                return "07 0a";
            case '8':
                return "08 0a";
            case '9':
                return "09 0a";
            case '*':
                return "0a 0a";
            case '#':
                return "0b 0a";
            default:
                return "unknown";
        }
    }
}
