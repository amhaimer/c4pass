//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKeyValid.java Event corespondant la touche 0 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier valid
 *
 * @author Abdellah
 */
public class EventKeyValid extends Event {

    /**
     * constructeur de classe
     */
    public EventKeyValid() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "Event : Key Valid pressed \n"
                + "time : " + time + "\n";

    }

}
