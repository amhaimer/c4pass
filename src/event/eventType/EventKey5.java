package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 5
 *
 * @author Abdellah
 */
public class EventKey5 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey5() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "5";

    }

}
