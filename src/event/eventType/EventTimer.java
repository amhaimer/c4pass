package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du fin du timer
 *
 * @author Abdellah
 */
public class EventTimer extends Event {

    long interval;

    /**
     * constructeur de classe
     *
     * @param interval l'intervale en sec
     */
    public EventTimer(long interval) {
        super();
        this.interval = interval;
    }

    @Override
    public String toString() {
        return interval == 2001 ? "switch" : "timer";
    }
}
