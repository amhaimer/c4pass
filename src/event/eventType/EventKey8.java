//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKey8.java Event corespondant la touche 8 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 8
 *
 * @author Abdellah
 */
public class EventKey8 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey8() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "8";
    }

}
