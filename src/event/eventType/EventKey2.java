package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 2
 *
 * @author Abdellah
 */
public class EventKey2 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey2() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "2";

    }

}
