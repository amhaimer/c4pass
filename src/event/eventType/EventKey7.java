//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKey7.java Event corespondant la touche 7 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 7
 *
 * @author Abdellah
 */
public class EventKey7 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey7() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "7";
    }

}
