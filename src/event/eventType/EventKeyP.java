//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKeyP.java Event corespondant la touche 0 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier P
 *
 * @author Abdellah
 */
public class EventKeyP extends Event {

    /**
     * constructeur de classe
     */
    public EventKeyP() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "Event : Key P pressed \n"
                + "time : " + time + "\n";

    }

}
