//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKey6.java Event corespondant la touche 6 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 6
 *
 * @author Abdellah
 */
public class EventKey6 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey6() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "6";

    }

}
