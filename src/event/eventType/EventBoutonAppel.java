package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton d'appel
 *
 * @author Abdellah
 */
public class EventBoutonAppel extends Event {

    /**
     * constructeur de classe
     */
    public EventBoutonAppel() {
        super();
    }

    @Override
    public String toString() {
        return "Event : Bouton Appel pressed \n"
                + "time : " + time + "\n";

    }

}
