//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKey9.java Event corespondant la touche 9 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 9
 *
 * @author Abdellah
 */
public class EventKey9 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey9() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "9";
    }

}
