//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKeyLeft.java Event corespondant la touche 0 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier gauche
 *
 * @author Abdellah
 */
public class EventKeyLeft extends Event {

    /**
     * constructeur de classe
     */
    public EventKeyLeft() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "Event : Key Left pressed \n"
                + "time : " + time + "\n";

    }

}
