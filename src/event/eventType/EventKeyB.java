//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKey0.java Event corespondant la touche 0 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier B
 *
 * @author Abdellah
 */
public class EventKeyB extends Event {

    /**
     * constructeur de classe
     */
    public EventKeyB() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "Event : Key B pressed \n"
                + "time : " + time + "\n";

    }

}
