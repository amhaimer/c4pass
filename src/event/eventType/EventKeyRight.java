//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventRight.java Event corespondant la touche 0 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier droite
 *
 * @author Abdellah
 */
public class EventKeyRight extends Event {

    /**
     * constructeur de classe
     */
    public EventKeyRight() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "Event : Key Right pressed \n"
                + "time : " + time + "\n";
    }
}
