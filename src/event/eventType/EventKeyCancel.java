//! \author Henon.Julien
//! \date 10 Mars 2016
//! \file EventKeyCancel.java Event corespondant la touche 0 apuyé
package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier cancel
 *
 * @author Abdellah
 */
public class EventKeyCancel extends Event {

    /**
     * constructeur de classe
     */
    public EventKeyCancel() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "Event : Key Cancel pressed \n"
                + "time : " + time + "\n";
    }
}
