package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton poussoir
 *
 * @author Abdellah
 */
public class EventPoussoir extends Event {

    /**
     * constructeur de classe
     */
    public EventPoussoir() {
        super();
    }

    @Override
    public String toString() {
        return "Event : Poussoir pressed \n"
                + "time : " + time + "\n";

    }

}
