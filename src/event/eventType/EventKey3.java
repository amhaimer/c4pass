package event.eventType;

import event.Event;

/**
 * renvoie l'evenement du bouton du clavier 3
 *
 * @author Abdellah
 */
public class EventKey3 extends Event {

    /**
     * constructeur de classe
     */
    public EventKey3() {
        super();
    }

    //! \brief Methode toString retourne la touche appuyé et la date d'appuye
    //! \details Methode surchargé
    @Override
    public String toString() {
        return "3";

    }

}
