package event;

import event.eventType.*;
import devices.*;
import java.util.*;
import sql.data.Porte;
import sql.DataBase;
import utils.Connection;
import menu.PorteManager;
import menu.action.Call;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * la gestion des evenements
 *
 * @see getEvent
 * @author Abdellah
 */
public class EventManager {

    private static EventManager INSTANCE = null;
    private Clavier clavier;
    private Poussoir poussoir;
    private Pjsip pjsip;

    private Buzzer buzzer;
    //   private Lecteur lecteur;
    private ArrayList<devices.TimerLocal> timers;
    private Lcd lcd;
    private Bouton_Appel boutonAppel;
    DataBase data = DataBase.getInstance();
    Porte porte;
    PorteManager portemanager;
    Connection connection = new Connection();
    private Process reboot;
    private static int count_soft = 0;
    private static int count_hard = 0;
    int etatbp = 0;
    int etatbp2 = 0;
    char key = 'n';
    char keytwo = 'n';
    char Lastkey = 'n';
    boolean bpRelache = true;
    boolean bp2Relache = true;

    private EventManager() {

//        lecteur = lecteur.getInstance();
        clavier = Clavier.getInstance();
        poussoir = Poussoir.getInstance();
        buzzer = Buzzer.getInstance();
        portemanager = PorteManager.getInstance();
        lcd = Lcd.getInstance();
        pjsip = Pjsip.getInstance();

        boutonAppel = Bouton_Appel.getInstance();
        timers = new ArrayList<devices.TimerLocal>();
    }

    /**
     * Retour le pointeur sur l'unique instance de la classe EventManager
     *
     * @return l'instance du manageur des evenemnt
     */
    public static EventManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EventManager();
        }
        return INSTANCE;
    }

    /**
     * Retourne L'évenemnt produit
     *
     * @return l'evenemenet qui a ete produit sinonn null
     */
    public Event getEvent() {

        //    key = clavier.getKey();

        /*        if (porte.getInterphone().equals("33") && (key == 'V' || key == '<')) {
            if (key == 'V') {
                key = '<';
            } else {
                key = 'V';
            }
        }*/
        if (key != 'n') {
            System.out.println("Touche: " + key);
        }

        int bouton = poussoir.scan();
        int boutonBP2 = poussoir.scanBP2();
        int boutonAppelValue = boutonAppel.scan();

        int status = pjsip.hasBeenConnected();
        Call.confirmed = status;
        int statusdisconnected = pjsip.statusdisconnected();
        Call.disconnected = statusdisconnected;

        // lecteur.getTrame();
        // DataBase data=getInstance();
        porte = Porte.getInstance();

        if (count_soft >= 15 || (count_soft >= 5 && porte.getInterphone().equals("31"))) {
            String t_serveur = new String();
            t_serveur = porte.getServeurIP();
            char t_clavier = porte.getInterphone().charAt(1);
            String ts_clavier = new String();

            /*
             ** if (t_techno == '3')
             ** ts_techno = "Audi";
             ** else if (t_techno == '4')
             ** ts_techno = "VOIP";
             ** else if (t_techno == '5')
             ** ts_techno = "SIP";
             ** else if (t_techno == '6')
             ** ts_techno = "AuVo";
             ** else if (t_techno == '7')
             ** ts_techno = "AuSI";
             ** else if (t_techno == '8')
             ** ts_techno = "VoSI";
             ** else if (t_techno == '9')
             ** ts_techno = "TOUT";
             ** else
             ** ts_techno = "----"; 
             */
            switch (t_clavier) {
                case '1':
                    ts_clavier = "MA";
                    break;
                case '2':
                    ts_clavier = "HA";
                    break;
                case '3':
                    ts_clavier = "BO";
                    break;
                case '4':
                    ts_clavier = "G1";
                    break;
                case '5':
                    ts_clavier = "G3";
                    break;
                default:
                    ts_clavier = "--";
                    break;
            }
            lcd.print("R" + porte.getIdResidence()
                    + "-P" + porte.getNumPorte()
                    + "-" + porte.getVersion(), true);
            lcd.setCursor(2, 0);

            if (porte.getValidConnexionWeb().equals("1")) {
                lcd.print(ts_clavier + "-"
                        + t_serveur + "-"
                        + data.getNbResidents() + "R-", false);
            } else {
                lcd.print(ts_clavier + "-"
                        + t_serveur + "-"
                        + data.getNbResidents() + "R-" + "X",
                        false);
            }

            data.addEvent("ASSISTANCE", "Reset clavier", "A-P programme", " ", " ");
            try {
                Thread.sleep(4500);
            } catch (InterruptedException e) {
            }
            lcd.clearLcd();
            lcd.print("VOIP:" + (porte.getValidConnexionWeb().charAt(0) == '1' ? "@" : "L")
                    + "DATA:" + (porte.getValidConnexionWeb().charAt(0) == '1' ? porte.getTimeConnexionWeb() : "INTRA"), false);
            lcd.setCursor(2, 0);
            Process adresseIp = null;
            try {
                adresseIp = Runtime.getRuntime().exec("hostname -I");
            } catch (IOException e) {
                System.out.println(e);
            }

            BufferedReader adresse = new BufferedReader(new InputStreamReader(adresseIp.getInputStream()));
            lcd.setCursor(2, 0);
            String s;
            String m = "";

            try {
                while ((s = adresse.readLine()) != null) {
                    System.out.println(adresse.readLine());
                    m += s;
                }
            } catch (IOException e) {
                System.out.println(e);
            }
            lcd.print("IP:-----", false);
            lcd.print(m, false);
            System.out.println(m + "\n");
            porte.setIpPlatine(m);
            data.update(porte);
            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
            }
            lcd.clearLcd();
            lcd.setCursor(1, 0);
            lcd.print("REDEMARRAGE...", false);
            System.exit(0);
        }
        if (count_hard >= 15 || (count_hard >= 5 && porte.getInterphone().equals("31"))) {
            String t_serveur = new String();
            t_serveur = porte.getServeurIP();
            char t_clavier = porte.getInterphone().charAt(1);
            String ts_clavier = new String();

            switch (t_clavier) {
                case '1':
                    ts_clavier = "MA";
                    break;
                case '2':
                    ts_clavier = "HA";
                    break;
                case '3':
                    ts_clavier = "BO";
                    break;
                case '4':
                    ts_clavier = "G1";
                    break;
                case '5':
                    ts_clavier = "G3";
                    break;
                default:
                    break;
            }
            lcd.print("R" + porte.getIdResidence()
                    + "-P" + porte.getNumPorte()
                    + "-" + porte.getVersion(), true);
            lcd.setCursor(2, 0);

            if (porte.getValidConnexionWeb().equals("1")) {
                lcd.print(ts_clavier + "-"
                        + t_serveur + "-"
                        + data.getNbResidents() + "R-",
                        false);
            } else {
                lcd.print(ts_clavier + "-"
                        + t_serveur + "-"
                        + data.getNbResidents() + "R-" + "X",
                        false);
            }

            data.addEvent("ASSISTANCE", "Reset clavier", "A-P programme", " ", " ");
            try {
                Thread.sleep(4500);
            } catch (InterruptedException e) {
            }
            lcd.clearLcd();
            lcd.print("VOIP:" + (porte.getValidConnexionWeb().charAt(0) == '1' ? "@" : "L")
                    + "DATA:" + (porte.getValidConnexionWeb().charAt(0) == '1' ? porte.getTimeConnexionWeb() : "INTRA"), false);
            lcd.setCursor(2, 0);
            Process adresseIp = null;
            try {
                adresseIp = Runtime.getRuntime().exec("hostname -I");
            } catch (IOException e) {
                System.out.println(e);
            }
            BufferedReader adresse = new BufferedReader(new InputStreamReader(adresseIp.getInputStream()));
            lcd.setCursor(2, 0);
            String s;
            String m = "";
            try {
                while ((s = adresse.readLine()) != null) {
                    System.out.println(adresse.readLine());
                    m += s;
                }
            } catch (IOException e) {
                System.out.println(e);
            }
            System.out.println(m + "\n");
            lcd.print("IP:-----", false);
            lcd.print(m, false);
            porte.setIpPlatine(m);
            data.update(porte);
            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
            }
            lcd.clearLcd();
            lcd.setCursor(1, 0);
            lcd.print("REDEMARRAGE...", false);
            data.addEvent("ASSISTANCE", "Reset clavier", "APPEL-1 OS", " ", " ");
            try {
                reboot = Runtime.getRuntime().exec("sudo reboot -f");
            } catch (IOException e) {
                System.out.println("IOE");
            }
        }
        if (bouton == 0 && bpRelache) {

            bpRelache = false;
            etatbp = 1;
            new Thread(() -> {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (poussoir.scan() == 0) {
                    portemanager.init();
                    portemanager.ouvertureBP(1, "BP", "");
                }
            }).start();
            return new EventPoussoir();

        }
        if (boutonBP2 == 0 && bp2Relache) {

            bp2Relache = false;
            etatbp2 = 1;
            new Thread(() -> {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (poussoir.scanBP2() == 0) {
                    portemanager.init();
                    portemanager.ouvertureBP(2, "BP", "");
                }
            }).start();
            return new EventPoussoir();

        } else if (boutonAppelValue == 1) {
            // return new EventBoutonAppel();
            return new EventKey1();
        } else if (boutonAppelValue == 2) {
            // return new EventBoutonAppel(); 
            return new EventKey2();
        } else if (boutonAppelValue == 3) {
            // return new EventBoutonAppel(); 
            return new EventKey3();
        } else if (key != 'n' && Lastkey != key) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            keytwo = clavier.getKey();
            if (key == keytwo) {
                Lastkey = key;
                buzzer.buzz(1);

                if (key == 'P') {
                    count_soft++;
                } else {
                    count_soft = 0;
                }
                if (key == 'B' || key == 'X') {
                    count_hard++;
                } else {
                    count_hard = 0;
                }

                if (key == 'V') {
                    if (porte.getInterphone().charAt(1) == '2') {
                        return new EventKeyLeft();
                    } else {
                        return new EventKeyValid();
                    }
                }
                if (key == '<') {
                    if (porte.getInterphone().charAt(1) == '2') {
                        return new EventKeyValid();
                    } else {
                        return new EventKeyLeft();
                    }
                }
                if (key == '>') {
                    return new EventKeyRight();
                }
                if (key == '0') {
                    return new EventKey0();
                }
                if (key == '1') {
                    return new EventKey1();
                }
                if (key == '2') {
                    return new EventKey2();
                }
                if (key == '3') {
                    return new EventKey3();
                }
                if (key == '4') {
                    return new EventKey4();
                }
                if (key == '5') {
                    return new EventKey5();
                }
                if (key == '6') {
                    return new EventKey6();
                }
                if (key == '7') {
                    return new EventKey7();
                }
                if (key == '8') {
                    return new EventKey8();
                }
                if (key == '9') {
                    return new EventKey9();
                }
                if (key == 'P') {
                    return new EventKeyP();
                }
                if (key == 'B') {
                    return new EventKeyB();
                }
            }
        }

        Lastkey = 'X';
        if (bouton == 1 && etatbp == 1) {

            if (poussoir.scan() == 1) {
                bpRelache = true;
                etatbp = 0;
            }
        }
        if (boutonBP2 == 1 && etatbp2 == 1) {

            if (poussoir.scanBP2() == 1) {
                bp2Relache = true;
                etatbp2 = 0;
            }
        }
        for (devices.TimerLocal timer : timers) {
            if (timer.isTimerEnd()) {
                return new EventTimer(timer.getInterval());
            }
        }

        return null;

        /*
	** verifie si call sip terminer	
         */
    }

    /**
     * Retour le pointeur sur un timer nouvellemnt creer
     *
     * @param temps fixe l'intervalle de temps associé au timer.
     * @param unit unité utilisé pour le timer seconde , minutes , ou
     * milliseconde.
     * @return Retourne l'instance du timer nouvellement crée.
     */
    public devices.TimerLocal requestTimer(int temps, devices.TimerLocal.Unit unit) {
        devices.TimerLocal timer = new devices.TimerLocal(temps, unit);
        timers.add(timer);
        return timer;
    }

    /**
     * uprime le timer donné en parametre
     *
     * @param timer pointeur du timer a suprimer
     */
    public void releaseTimer(devices.TimerLocal timer) {

        timers.remove(timer);
        timers.clear();
    }

    /**
     * renvoi temps actuel en sec
     *
     * @return temps entier
     */
    public static long getTime() {

        return System.currentTimeMillis();
    }

    private char valueof(int boutonAppelValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
