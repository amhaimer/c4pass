package event;

/**
 * classe mère des evenements
 *
 * @author Abdellah
 */
public abstract class Event {

    protected long time;

    /**
     * Constructeur des évenement.
     *
     */
    public Event() {
        time = EventManager.getTime();
    }

    /**
     * Retourne sa date de création.
     *
     * @return temps en entier
     */
    public long getTime() {
        return time;
    }

}
