package devices;

import java.util.Map;
import java.util.HashMap;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * driver qui permet de controller le volume du HP et du micro par alsamixer
 *
 * @see VOlumHP
 * @see VOlumMicro
 * @author Abdellah
 */
public class HpMicro {

    private static HpMicro INSTANCE = null;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static HpMicro getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new HpMicro();
        }
        return INSTANCE;
    }

    /**
     * contructeur de classe
     */
    public HpMicro() {
        init();
    }

    /**
     * permet de définir le volume du HP via ALSAMIXER avec une commande cmd
     *
     * @param niveau 1-8
     */
    public void VOlumHP(String niveau) {
        System.out.println("sudo amixer -c 1 set Speaker " + niveau + "%");
        cmdexec("sudo amixer -c 1 set Speaker " + niveau + "%");

    }

    /**
     * permet de définir le volume du Micro via ALSAMIXER avec une commande cmd
     *
     * @param niveau 1-8
     */
    public void VOlumMicro(String niveau) {
        cmdexec("sudo amixer -c 1 set Capture " + niveau + "%");
    }

    /**
     * fonction init
     */
    private void init() {

    }

    /**
     * fonction exit
     */
    public void exit() {

    }

    public static void cmdexec(String cmd) {
        Process p2;
        ProcessBuilder pb2;
        try {
            pb2 = new ProcessBuilder("sudo", "bash", "-c", cmd);
            p2 = pb2.start();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }
}
