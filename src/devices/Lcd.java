package devices;

/**
 * Driver du LCD fait en C ici il y'a la définition des méthodes qui sont
 * redéfini sur le fichier LCD.c JNI
 *
 * @author Abdellah
 */
public class Lcd {

    private static Lcd INSTANCE = null;
    private int handle;

    /**
     * init
     *
     * @return return 1 si réussis
     */
    public native int init();

    /**
     * init S
     *
     * @return return 1 si réussis
     */
    public native int initS();

    /**
     * Clignotment du curseur
     *
     * @param b true:visible --------- false : invisible
     */
    public native void blinkMode(boolean b);

    /**
     * clear LCD
     */
    public native void clearLcd();

    /**
     * mettre curseur sur une ligne et position
     *
     * @param ligne la ligne
     * @param position la position
     */
    public native void setCursor(int ligne, int position);

    /**
     * fonction pour affichage
     *
     * @param message message à afficher
     * @param reset_cursor remetre le cursor à la 1er ligne position 0
     */
    public native void print(String message, boolean reset_cursor);

    /**
     * clignotement curseur
     *
     * @param boo vrai ou faux
     */
    public native void saisie(boolean boo);

    /**
     * constructeur de classe
     */
    private Lcd() {
        handle = init();
    }

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Lcd getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Lcd();
        }
        return INSTANCE;
    }

}
