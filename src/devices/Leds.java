package devices;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
initialize  sudo i2cset -y 1 0x41 0x03 0x00
controle led :sudo i2cset -y 1 0x41 0x01 0x01
 */
/**
 * driver des leds en I2C
 *
 * @author Abdellah
 */
public class Leds {

    /**
     * ADDRESS l'adresse du composant sur le bus i2c REGINIT l'adresse du reg
     * init REGWRITE l'adresse du reg write INITVAL la valeur d'initialisation
     * EXITVAL la valeur d'exit ALLOFF etteinde tous les leds ALLON allumer tous
     * les leds
     */
    public static final byte ADDRESS = (byte) 0x41;
    public static final byte REGINIT = (byte) 0x03;
    public static final byte REGWRITE = (byte) 0x01;
    public static final byte INITVAL = (byte) 0x00;
    public static final byte EXITVAL = (byte) 0x0f;
    public static final byte ALLOFF = (byte) 0x00;
    public static final byte ALLON = (byte) 0x0f;
    public static final byte WHITE = (byte) 0x01;
    public static final byte GREEN = (byte) 0x08;//0x08
    public static final byte WHITEGREEN = (byte) 0x09;//0x09
    public static final byte ORANGE = (byte) 0x04;
    public static final byte WHITEORANGE = (byte) 0x05;
    public static final byte BLUEORANGE = (byte) 0x0c;//0x0c
    public static final byte WHITEORANGEGREEN = (byte) 0x0d;
    public static final byte BLUE = (byte) 0x02;
    public static final byte WHITEBLUE = (byte) 0x03;
    public static final byte BLUEGREEN = (byte) 0x0a;
    public static final byte WHITEBLUEGREEN = (byte) 0x0b;
    public static final byte ORANGEGREEN = (byte) 0x06;
    public static final byte WHITEORANGEBLUE = (byte) 0x07;//0x07
    public static final byte BLUEORANGEGREEN = (byte) 0x0e;

    private static Leds INSTANCE = null;
    public static I2CDevice device;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Leds getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Leds();
        }
        return INSTANCE;
    }

    /**
     * constructeur de classe
     */
    public Leds() {
        init();
    }

    /**
     * allumer tous les leds
     */
    public void AllumeAll() {
        try {
            device.write(REGWRITE, ALLON);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * allumer les leds blanc
     */
    public void AllumeWHITE() {
        try {
            device.write(REGWRITE, WHITE);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * allumer les leds blanc +blue
     */
    public void AllumeWHITEBLUE() {
        try {
            device.write(REGWRITE, WHITEBLUE);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * allumer les leds blanc +orange
     */
    public void AllumeWHITEORANGE() {
        try {
            device.write(REGWRITE, WHITEORANGE);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * allumer les leds blanc +vert
     */
    public void AllumeWHITEGREEN() {
        try {
            device.write(REGWRITE, WHITEGREEN);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * allumer les leds blanc +orange+vert
     */
    public void AllumeWHITEORANGEGREEN() {
        try {
            device.write(REGWRITE, WHITEORANGEGREEN);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * etteindre tous les leds
     */
    public void eteint() {

        try {
            device.write(REGWRITE, ALLOFF);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * init
     */
    public void init() {
        try {
            I2CBus i2c = null;
            try {
                i2c = I2CFactory.getInstance(I2CBus.BUS_1);
            } catch (I2CFactory.UnsupportedBusNumberException | IOException ex) {
                Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Create device object
            device = i2c.getDevice(ADDRESS);

            try {
                device.write(REGINIT, INITVAL);
            } catch (IOException ex) {
                Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * exit
     */
    void exit() {
        try {
            device.write(REGINIT, EXITVAL);
        } catch (IOException ex) {
            Logger.getLogger(Leds.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
