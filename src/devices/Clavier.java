package devices;

import com.pi4j.wiringpi.Gpio;

/**
 * classe clavier obsoléte sur la nouvelle version (phoenix)
 *
 * @see getKey
 * @author Abdellah
 */
public class Clavier {

    char[][] KEYPAD
            = {
                {'1', '2', '3', 'O'},
                {'4', '5', '6', 'V'},
                {'7', '8', '9', '>'},
                {'P', '0', 'B', '<'}
            };// les touches du clavier 
    int ROW[] = {58, 58, 58, 58};// GPIO des pins pour les lignes 
    int COLUMN[] = {58, 58, 58, 58};// GPIO des pins pour les columns 

    private static Clavier INSTANCE = null;
    private long TimeStartAntiRebond;
    private final long TimeAntiRebond = 50;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Clavier getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Clavier();
        }
        return INSTANCE;
    }

    /**
     * permet de scanner les GPIO des lignes et colonne pour récuperer la touche
     * appuyé
     *
     * @return le caractére appuyé
     */
    public char getKey() {
        int rowVal = -1, colVal = -1, i, j, tmpRead;
        //check temps anti rebond
        long millis = System.currentTimeMillis();

        // Provisions the pin.
        int pin = Gpio.wiringPiSetupGpio();

        // lignes en sortie  colonne en entree
        for (i = 0; i < 4; i++) {
            Gpio.pinMode(ROW[i], Gpio.OUTPUT);
            Gpio.digitalWrite(ROW[i], Gpio.LOW);
            Gpio.pinMode(COLUMN[i], Gpio.INPUT);
            Gpio.pullUpDnControl(COLUMN[i], Gpio.PUD_DOWN);
        }

        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            System.err.print(e.getMessage());
        }

        for (i = 0; i < 4; i++) {
            Gpio.digitalWrite(ROW[i], Gpio.HIGH);
            for (j = 0; j < 4; j++) {
                tmpRead = Gpio.digitalRead(COLUMN[j]);
                if (tmpRead == 1) {
                    rowVal = i;
                    colVal = j;
                    //break;
                }
            }

            //if(tmpRead==0) break;
            Gpio.digitalWrite(ROW[i], Gpio.LOW);
        }
        if (rowVal + colVal < 0) {
            quit();
            return 'n';
        }
        quit();
        /*	System.out.println("the row is : "+rowVal+"the column is : "+colVal);
		System.out.println("the char is:   "+KEYPAD[rowVal][colVal]);
		System.out.println("break");*/
        if (millis - TimeStartAntiRebond > TimeAntiRebond) {
            TimeStartAntiRebond = millis;
        } else {
            return 'n';
        }
        return KEYPAD[rowVal][colVal];
    }

    /**
     * Restet des gpio à la sortie
     */
    void quit() {
        int i;
        for (i = 0; i < 4; i++) {
            Gpio.pinMode(ROW[i], Gpio.INPUT);
            Gpio.pinMode(COLUMN[i], Gpio.INPUT);
        }
    }
}
