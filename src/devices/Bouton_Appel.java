package devices;

import com.pi4j.wiringpi.Gpio;

/**
 * Driver en JAVA pour les 3 boutons d'appels
 *
 * @see scan
 * @author Abdellah
 */
public class Bouton_Appel {

    int GPIO1 = 26;//gpio bouton d'appel 1
    int GPIO2 = 5;//gpio bouton d'appel 2
    int GPIO3 = 0;//gpio bouton d'appel 3
    private static Bouton_Appel INSTANCE = null;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Bouton_Appel getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Bouton_Appel();
        }
        return INSTANCE;
    }

    /**
     * contructeur de classe
     */
    public Bouton_Appel() {
        init();
    }

    /**
     * Initialisation des GPIO
     */
    private void init() {
        int pin = Gpio.wiringPiSetupGpio();
        Gpio.pinMode(GPIO1, Gpio.INPUT);
        Gpio.pullUpDnControl(GPIO1, Gpio.PUD_DOWN);
        Gpio.pinMode(GPIO2, Gpio.INPUT);
        Gpio.pullUpDnControl(GPIO2, Gpio.PUD_DOWN);
        Gpio.pinMode(GPIO3, Gpio.INPUT);
        Gpio.pullUpDnControl(GPIO3, Gpio.PUD_DOWN);
    }

    /**
     * Reset des GPIO
     */
    public void exit() {
        Gpio.digitalWrite(GPIO1, Gpio.LOW);
        Gpio.pinMode(GPIO1, Gpio.OUTPUT);
        Gpio.digitalWrite(GPIO2, Gpio.LOW);
        Gpio.pinMode(GPIO2, Gpio.OUTPUT);
        Gpio.digitalWrite(GPIO2, Gpio.LOW);
        Gpio.pinMode(GPIO2, Gpio.OUTPUT);
    }

    /**
     * methode qui scan les GPIO
     *
     * @return
     * <ul>
     * <li>:0= aucun bouton appuyé </li>
     * <li>:1= bouton appel 1</li>
     * <li>:2= bouton appel 2</li>
     * <li>:3= bouton appel 3</li>
     * </ul>
     */
    public int scan() {
        int theb = 0;
        if (Gpio.digitalRead(GPIO1) == 0) {
            theb = 1;
        } else if (Gpio.digitalRead(GPIO2) == 0) {
            theb = 2;
        } else if (Gpio.digitalRead(GPIO3) == 0) {
            theb = 3;
        }
        return theb;
    }
}
