package devices;

/**
 * classe PJSIP
 *
 * @author Abdellah
 */
public class Pjsip {

    /**
     * different type de serveur
     */
    public enum AccountType {
        CLIENT, OVH, ASTERISK, DIESE;
    };

    private static Pjsip INSTANCE = null;

    /**
     * reset add time
     */
    public native void resetAddTime();

    /**
     * rajouter temps appels
     *
     * @return true or false
     */
    public native boolean addTime();

    /**
     * reset raccroche
     */
    public native void resetRaccroche();

    /**
     * raccroché
     *
     * @return true si reussis
     */
    public native boolean raccroche();

    /**
     * reset ouverture
     */
    public native void resetOuverture();

    /**
     * ouverture de la porte
     *
     * @return true si reussis
     */
    public native int ouverturePorte();

    /**
     * en communication
     *
     * @return true si on est en communication
     */
    public native int hasBeenConnected();

    /**
     * retourne l'etat de connection à un serveur passé en parametres
     *
     * @param type de serveur
     * @return true si on est enregistré sur le serveur
     */
    public native boolean connectedToServer(int type);

    /**
     * en communication
     *
     * @return true or false
     */
    public native boolean onCommunication();

    /**
     * status de deconnection
     *
     * @return true or false
     */
    public native int statusdisconnected();

    /**
     * methode d'enregistrement sur un serveur
     *
     * @param user username
     * @param passwd pwd
     * @param domain host
     * @param type type de serveur
     * @return true or false
     */
    public native int register(String user, String passwd, String domain, int type);

    /**
     * appel vers 10 telephone
     *
     * @param uri1 urisip
     * @param type1 type du serveur
     * @param uri2 urisip
     * @param type2 type du serveur
     * @param uri3 urisip
     * @param type3 type du serveur
     * @param uri4 urisip
     * @param type4 type du serveur
     * @param uri5 urisip
     * @param type5 type du serveur
     * @param uri6 urisip
     * @param type6 type du serveur
     * @param uri7 urisip
     * @param type7 type du serveur
     * @param uri8 urisip
     * @param type8 type du serveur
     * @param uri9 urisip
     * @param type9 type du serveur
     * @param uri10 urisip
     * @param type10 type du serveur
     * @param statusel status de l'entree libre
     */
    public native void makeCall(String uri1, int type1, String uri2, int type2, String uri3, int type3, String uri4, int type4, String uri5, int type5, String uri6, int type6, String uri7, int type7, String uri8, int type8, String uri9, int type9, String uri10, int type10, int statusel);

    /**
     * terminer l'appel
     */
    public native void endCall();

    /**
     * detrue l'objet pjsip
     *
     * @return true or false
     */
    public native int destroy();

    /**
     * l'init des parametres
     *
     * @param dtmf1
     * @param dtmf2
     * @param dtmf3
     * @param dtmf4
     * @param tempsOuverture1
     * @param tempsOuverture2
     * @param resident
     * @param code
     * @param tempsComm
     * @param TempsAppel
     */
    public native void init_params(int dtmf1, int dtmf2, int dtmf3, int dtmf4, int tempsOuverture1,
            int tempsOuverture2, String resident, String code, int tempsComm, int TempsAppel);

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Pjsip getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Pjsip();
        }
        return INSTANCE;
    }
}
