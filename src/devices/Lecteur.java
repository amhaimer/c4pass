package devices;

/**
 * Lecteur MIFARE not used on phoenix
 *
 * @author Abdellah
 */
public class Lecteur {

    private int handle;
    private String currentTrame;
    private static Lecteur INSTANCE = null;

    private native String getTrameJNI();

    /**
     * init
     *
     * @return 1 si réussis
     */
    public native int init();

    /**
     * constructeur de classe
     */
    private Lecteur() {
        handle = init();
    }

    /**
     * envoyer une trame sur le lecteur
     *
     * @param trame la trame a envoyer
     */
    private void setTrame(String trame) {
        currentTrame = trame;
    }

    /**
     * receptionner une trame sur le lecteur
     *
     */
    public void getTrame() {
        String trame = getTrameJNI();
        setTrame(trame);
        //	System.out.println("Trame: " + trame);
    }

    /**
     * fin trame
     *
     * @return string fin tram
     */
    public String getFin() {
        return currentTrame.substring(29, 33);
    }

    /**
     * recuper numéro du badge
     *
     * @return numéro du badge
     */
    public String getBadge() {
        return currentTrame.substring(22, 29);
    }

    /**
     * si id badge =99999999 le badge appartient au facteur
     *
     * @return true or false
     */
    public boolean isFacteur() {
        return currentTrame.substring(0, 10).equals("9999999999");
    }

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Lecteur getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Lecteur();
        }
        return INSTANCE;
    }
}
