package devices;

/**
 * classe timer basé sur System.currentmillis()
 *
 * @author Abdellah
 */
public class TimerLocal {

    long interval;
    public long startTimer;

    /**
     * les different unités de temps
     */
    public enum Unit {
        millis,
        sec,
        min;
    }

    Unit unit;

    /**
     * l'intervale du temps avec l'unité
     *
     * @param interval l'interval
     * @param unit son unité
     */
    public TimerLocal(long interval, Unit unit) {
        if (Unit.millis.equals(unit)) {
            this.interval = interval;
        }
        if (Unit.sec.equals(unit)) {
            this.interval = interval * 1000;
        }
        if (Unit.min.equals(unit)) {
            this.interval = interval * 1000 * 60;
        }

        this.unit = unit;
        startTimer = System.currentTimeMillis();

    }

    /**
     * renvoie le nombre de secondes restants
     *
     * @return nombre de secondes
     */
    public long getRemainingSecond() {
        return ((((System.currentTimeMillis() - startTimer) / 1000) - (interval / 1000)) * -1);
    }

    /**
     * renvoi le temps passé par le timer
     *
     * @return nombre de secondes
     */
    public long getSpentSecond() {
        return (((startTimer - System.currentTimeMillis()) / 1000) * -1);
    }

    /**
     * l'etat du timer
     *
     * @return timer si il a ecoulé
     */
    public boolean isTimerEnd() {
        long millis = System.currentTimeMillis();

        return millis - startTimer > interval; //startTimer = millis;

    }

    /**
     * renvoi l'interval en cours
     *
     * @return l'intervale en socondes
     */
    public long getInterval() {
        return interval;
    }

    /**
     * renvoi l'unité utilisé
     *
     * @return enum unit
     */
    public Unit getUnit() {
        return unit;
    }
}
