package devices;

import com.pi4j.wiringpi.Gpio;

/**
 * driver des boutons poussoir
 *
 * @author Abdellah
 */
public class Poussoir {

    int GPIO = 11;
    int GPIOBP2 = 8;

    private static Poussoir INSTANCE = null;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Poussoir getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Poussoir();
        }
        return INSTANCE;
    }

    /**
     * constructeur de classe
     */
    public Poussoir() {
        init();
    }

    /**
     * init
     */
    void init() {
        int pin = Gpio.wiringPiSetupGpio();
        Gpio.pinMode(GPIO, Gpio.INPUT);
        Gpio.pullUpDnControl(GPIO, Gpio.PUD_UP);
        Gpio.pinMode(GPIOBP2, Gpio.INPUT);
        Gpio.pullUpDnControl(GPIOBP2, Gpio.PUD_UP);
    }

    /**
     * exit
     */
    public void exit() {
        Gpio.digitalWrite(GPIO, Gpio.LOW);
        Gpio.pinMode(GPIO, Gpio.OUTPUT);
        Gpio.digitalWrite(GPIOBP2, Gpio.LOW);
        Gpio.pinMode(GPIOBP2, Gpio.OUTPUT);
    }

    /**
     * scan du BP1
     *
     * @return l'etat du bouton
     */
    public int scan() {
        return Gpio.digitalRead(GPIO);
    }

    /**
     * scan du BP2
     *
     * @return l'etat du bouton
     */
    public int scanBP2() {
        return Gpio.digitalRead(GPIOBP2);
    }
}
