package devices;

import com.pi4j.wiringpi.Gpio;

/**
 * Driver pour le buzzer
 *
 * @see buzz
 * @author Abdellah
 */
public class Buzzer {

    int GPIO = 16;// gpio buzzer 
    boolean is_BUZ = false;//indice de l'etat du buzzer 
    private static Buzzer INSTANCE = null;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Buzzer getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Buzzer();
        }
        return INSTANCE;
    }

    /**
     * contructeur de classe
     */
    public Buzzer() {
        init();
    }

    /**
     * fonction qui permet d'activer le buzzer pendant un laps de temps
     *
     * @param temps temps d'activation du buzzer
     */
    public void buzz(int temps) {
        for (int i = 0; i < temps; i++) {
            Gpio.digitalWrite(GPIO, Gpio.HIGH);
            is_BUZ = true;
            waitMICRO();
            Gpio.digitalWrite(GPIO, Gpio.LOW);

            is_BUZ = true;
            waitMICRO();

        }
    }

    /**
     * Initialisation du GPIO
     */
    private void init() {
        int pin = Gpio.wiringPiSetupGpio();
        Gpio.pinMode(GPIO, Gpio.OUTPUT);

    }

    /**
     * Reset du GPIO
     */
    void exit() {
        Gpio.digitalWrite(GPIO, Gpio.LOW);
        Gpio.pinMode(GPIO, Gpio.INPUT);
    }

    /**
     * wait time en nano
     */
    public void waitMICRO() {
        final long INTERVAL = 125000;
        long start = System.nanoTime();
        long end;
        do {
            end = System.nanoTime();
        } while (start + INTERVAL >= end);

    }
}
