package devices;

import com.pi4j.wiringpi.Gpio;

/**
 * driver des relais
 *
 * @author Abdellah
 */
public class Relais {

    int GPIO_R1 = 13;
    int GPIO_R2 = 9;

    private static Relais INSTANCE = null;

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Relais getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Relais();
        }
        return INSTANCE;
    }

    /**
     * constructeur de classe
     */
    public Relais() {
        init();
    }

    /**
     * init
     */
    public void init() {
        int pin = Gpio.wiringPiSetupGpio();
        Gpio.pinMode(GPIO_R1, Gpio.OUTPUT);
        Gpio.pinMode(GPIO_R2, Gpio.OUTPUT);
    }

    /**
     * exit
     */
    public void exit() {
        Gpio.digitalWrite(GPIO_R1, Gpio.LOW);
        Gpio.digitalWrite(GPIO_R2, Gpio.LOW);
        Gpio.pinMode(GPIO_R1, Gpio.INPUT);
        Gpio.pinMode(GPIO_R2, Gpio.INPUT);
    }

    /**
     * ouvrir le relais
     *
     * @param door l'identifiant du relais
     */
    public void OpenDoor(int door) {
        if (door == 1) {
            Gpio.digitalWrite(GPIO_R1, Gpio.HIGH);
            System.out.println("########## door is open ##########");

        } else if (door == 2) {
            Gpio.digitalWrite(GPIO_R2, Gpio.HIGH);
            System.out.println("########## door 2 is open ##########");

        }
    }

    /**
     * fermer le relais
     *
     * @param door l'identifiant du relais
     */
    public void CloseDoor(int door) {

        if (door == 1) {
            Gpio.digitalWrite(GPIO_R1, Gpio.LOW);

        } else if (door == 2) {
            Gpio.digitalWrite(GPIO_R2, Gpio.LOW);

        }
    }

    /**
     * renvoi l'etat du premier relais
     *
     * @return true or false
     */
    public boolean getStatusRelais1() {
        //return statusRelais1 == 1;
        return Gpio.digitalRead(GPIO_R1) == 1;
    }

    /**
     * renvoi l'etat du deuxieme relais
     *
     * @return true or false
     */
    public boolean getStatusRelais2() {
        return Gpio.digitalRead(GPIO_R2) == 1;
    }

}
