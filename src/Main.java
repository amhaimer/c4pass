
import devices.*;
import event.Event;
import event.EventManager;
import java.io.*; // pour detecter evenemnt du clavier

import java.net.SocketException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import menu.*;
import menu.action.ActionSwitchTime;

import sql.DataBase;
import sql.data.Porte; // pour afficher le menu
import utils.*;
import static utils.Constante.VERSIONC4PASS;

/**
 * <b>la classe Main c'est la classe principale du programme </b>
 *
 *
 * @see main
 *
 *
 *
 *
 * @author A.M
 *
 */
public class Main {

    static boolean ledsoff = false;

    /**
     *
     * methode qui fait l'init globale et lance la boucle principale du
     * programme
     *
     * @param args argument de la classe main
     * @throws SocketException throw socket exception
     */
    public static void main(String[] args) throws SocketException {

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
            @Override
            public void run() {
                // your code here
                if (!ledsoff) {
                    System.exit(1);
                }
            }
        },
                1000 * 60
        );

        System.loadLibrary("devices_Lcd");
        //  System.loadLibrary("devices_Clavier");
        //  System.loadLibrary("devices_Relais");
        // System.loadLibrary("devices_Poussoir");
        // System.loadLibrary("devices_Buzzer");
        //  System.loadLibrary("devices_Lecteur");
        //  System.loadLibrary("devices_Hp");
        System.loadLibrary("devices_Pjsip");
//	System.loadLibrary("devices_Diodes");
        //  System.loadLibrary("devices_Bouton_Appel");

        // System.loadLibrary("sip_SipClient");
        Timer t = new Timer();

        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                // -	télécharger la dernière version du programme 
                DownloadVersion d = new DownloadVersion();
                d.download();

                System.out.println("************ every day ***************");
            }
        }, 24 * 60 * 60 * 1000, 24 * 60 * 60 * 1000);
        Connection json = new Connection();
        json.installtrust();
        Porte porte;
        EventManager manager = EventManager.getInstance();
        Lcd lcd = Lcd.getInstance();
        Eth0Plug eth0plug = new Eth0Plug();
        Menu menu = Menu.getInstance();
        Item curentItem = menu.getCurrentItem();
        // SipClient sip = new SipClient();
        long i = 0;

        DataBase data = DataBase.getInstance();
        PorteManager portemanager = PorteManager.getInstance();
        portemanager.initRel();

        Pjsip pjsip = Pjsip.getInstance();
        porte = Porte.getInstance();
        ActionSwitchTime.DHCP = porte.getValidServeurClientDhcp().equals("1");
        ActionSwitchTime.HOSTIP = porte.getIpInterne();
        ActionSwitchTime.SERVERIP = porte.getPasserelle();
        ActionSwitchTime.MASK = porte.getNetmask();
        ActionSwitchTime.SSID = porte.getIdWifi();
        ActionSwitchTime.PWD = porte.getMdpWifi();
        ActionSwitchTime.CHOIXIPBX = porte.getServeurIP();
        ActionSwitchTime.DGW = porte.getDefaultGateway();

        Leds diodes = Leds.getInstance();
        diodes.AllumeAll();
        Sound snd = new Sound();

        // cmdVpn("sudo amixer set Capture 50%");

        /* t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
              // entree libre
               portemanager.init();
        portemanager.entreeLibre();
            }
        },  60 * 1000,60 * 1000);*/

 /*  Hp hp = Hp.getInstance();
hp.init();*/
        int techno = getTypeTechno(1);
        String mdr = porte.getIdTeamViewer();
        System.out.println(mdr);
        if (porte.getInterphone().charAt(0) == '3') {

            System.out.println("\u001B[32m" + "---# " + getUsername(techno) + "  " + getPassword(techno) + "  " + getDomain(techno) + "   " + techno + "\u001B[0m");
            pjsip.register(getUsername(techno), getPassword(techno), getDomain(techno), techno);

            if (!porte.getServeurIP().contains("-")) {
                int techno2 = getTypeTechno(2);
                System.out.println("\u001B[32m" + "---# " + getUsername(techno2) + "  " + getPassword(techno2) + "  " + getDomain(techno2) + "   " + techno2 + "\u001B[0m");
                pjsip.register(getUsername(techno2), getPassword(techno2), getDomain(techno2), techno2);

            }
        }

        // String t_serveur = new String();
        //  t_serveur = porte.getServeurIP();
        //  char t_clavier = porte.getInterphone().charAt(1);
        //String ts_clavier = new String();
        lcd.initS();
        lcd.clearLcd();
        /*switch (t_clavier) {
            case '1':
                ts_clavier = "MA";
                break;
            case '2':
                ts_clavier = "HA";
                break;
            case '3':
                ts_clavier = "BO";
                break;
            case '4':
                ts_clavier = "G1";
                break;
            case '5':
                ts_clavier = "G3";
                break;
            default:
                break;
        }*/
 /*   try {
            ProcessBuilder pb2 = new ProcessBuilder("sudo", "amixer", "set", "PCM", "--", "100%");
            Process p2 = pb2.start();
           
         
        } catch (IOException e) {
            System.out.println("ERROR");
        }*/
        //lcd.print("R" + porte.getIdResidence() +
        //"-P" + porte.getNumPorte() +
        //"-" + porte.getVersion(), true);
        //lcd.setCursor(2,0);
        if (porte.getValidConnexionWeb().equals("1")) {
            //lcd.print(ts_clavier + "-" +
            //t_serveur + "-" +
            //data.getNbResidents() + "R-" +
            //(connection.trycnx() == true ? "@" : "X") +
            //(EventManager.isUartRunning()? "U" : "X") +
            //(EventManager.isRadioRunning()? "R" : "X") +
            //(EventManager.isIdentitoothRunning()? "O" : "X") +
            //(EventManager.isIdentitelRunning()? "I" : "X"), false);
        } else {
            //lcd.print(ts_clavier + "-" +
            //t_serveur + "-" +
            //data.getNbResidents() + "R-" + "X" +
            //(EventManager.isUartRunning()? "U" : "X") +
            //(EventManager.isRadioRunning()? "R" : "X") +
            //(EventManager.isIdentitoothRunning()? "O" : "X") +
            //(EventManager.isIdentitelRunning()? "I" : "X"), false);
        }

        data.addEvent("ASSISTANCE", "Reset clavier", "A-P programme", " ", " ");
        //try {
        //Thread.sleep(2500);
        //}
        //catch (InterruptedException e) {
        //}
        //lcd.clearLcd();
        //lcd.print("L"+porte.getTimeConnexionWeb()+"V"+porte.getTempoVpn()+"S"+porte.getTempoSip()+"C"+porte.getTempoConnect() , false);
        //lcd.print("VOIP:" + (porte.getValidConnexionWeb().charAt(0) == '1' ? "@" : "L") +
        //	  "DATA:" + (porte.getValidConnexionWeb().charAt(0) == '1' ? porte.getTimeConnexionWeb() : "INTRA"), false);
        //lcd.setCursor(2, 0);
        Process adresseIp = null;
        Process macEth = null;
        Process macWifi = null;
        Process eth0 = null;
        Process eth1 = null;
        Process wlan0 = null;

        if (eth0plug.isPlugged()) {
            lcd.clearLcd();
            lcd.print("DEFAUT DE   \n   CONNEXION", false);

        }
        try {
            Thread.sleep(100);  //  Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        lcd.clearLcd();

        try {
            adresseIp = Runtime.getRuntime().exec("hostname -I");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            eth0 = Runtime.getRuntime().exec("ifconfig eth0");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            eth1 = Runtime.getRuntime().exec("ifconfig eth1");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            wlan0 = Runtime.getRuntime().exec("ifconfig wlan0");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            macEth = Runtime.getRuntime().exec("cat /sys/class/net/eth0/address");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            macWifi = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address");
        } catch (IOException e) {
            System.out.println(e);
        }
        Buzzer buz1 = new Buzzer();
        buz1.buzz(2000);

        BufferedReader adresse = new BufferedReader(new InputStreamReader(adresseIp.getInputStream()));
        BufferedReader adrMacEth = new BufferedReader(new InputStreamReader(macEth.getInputStream()));
        BufferedReader adrMacWifi = new BufferedReader(new InputStreamReader(macWifi.getInputStream()));
        BufferedReader ifaceEth0 = new BufferedReader(new InputStreamReader(eth0.getInputStream()));
        BufferedReader ifaceEth1 = new BufferedReader(new InputStreamReader(eth1.getInputStream()));
        BufferedReader ifaceWlan0 = new BufferedReader(new InputStreamReader(wlan0.getInputStream()));
        //lcd.setCursor(2,0);
        String s, m = "", s1, m1 = "", s2, m2 = "", s3, m3 = "", se, me = "", sw, mw = "";

        try {
            while ((s = adresse.readLine()) != null) {
                m += s;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s1 = ifaceEth0.readLine()) != null) {
                m1 += s1;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s2 = ifaceEth1.readLine()) != null) {
                m2 += s2;
            }

        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s3 = ifaceWlan0.readLine()) != null) {
                m3 += s3;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((se = adrMacEth.readLine()) != null) {
                me += se;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((sw = adrMacWifi.readLine()) != null) {
                mw += sw;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        System.out.println(m + "\n");
        System.out.println("ethernet mac : " + me + "\n");
        System.out.println("ethernet wifi : " + mw + "\n");
        porte.setMacEthernet(me);
        porte.setMacWifi(mw);
        try {
            ifaceWlan0.close();
            adresse.close();
            adrMacEth.close();
            adrMacWifi.close();
            ifaceEth0.close();
            ifaceEth1.close();

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (m.length() < 17) {
            //une seule addresse ip	

            if (m.length() == 0) {
                //  lcd.print("AUCUNE CONNEXION:" + m, false);
                //      porte.setIpPlatine(m);
            } else if (m.contains("169.244.222.1")) {

                lcd.print("R" + m, false);
                porte.setMdpWifiPi(m);
            } else if (m1.contains(m)) {
                lcd.print("E" + m, false);
                porte.setIpPlatine(m);
            } else if (m2.contains(m)) {
                lcd.print("G" + m, false);
                porte.setIpPlatine(m);
            } else if (m3.contains(m)) {

                lcd.print("W" + m, false);
                porte.setMdpWifiPi(m);
            }

        } else if (m.length() > 17) {

            String[] ips = m.split(" ");
            String ip1 = ips[0];
            String ip2 = ips[1];

            if (ip1.contains("169.244.222.1")) {
                lcd.print("R" + ip1, false);
                porte.setMdpWifiPi(ip1);
            } else if (m1.contains(ip1)) {
                lcd.print("E" + ip1, false);
                porte.setIpPlatine(ip1);
            } else if (m2.contains(ip1)) {
                lcd.print("G" + ip1, false);
                porte.setIpPlatine(ip1);
            } else if (m3.contains(ip1)) {
                lcd.print("W" + ip1, false);
                porte.setMdpWifiPi(ip1);
            }

            lcd.setCursor(2, 0);
            if (ip2.contains("169.244.222.1")) {
                lcd.print("R" + ip2, false);
                porte.setMdpWifiPi(ip2);
            } else if (m1.contains(ip2)) {
                lcd.print("E" + ip2, false);
                porte.setIpPlatine(ip2);
            } else if (m2.contains(ip2)) {
                lcd.print("G" + ip2, false);
                porte.setIpPlatine(ip2);
            } else if (m3.contains(ip2)) {
                lcd.print("W" + ip2 + "-V" + VERSIONC4PASS, false);
                porte.setMdpWifiPi(ip2);
            }
        }

        //	lcd.print("IP:-----", false);
        //porte.setIpPlatine(m);
        data.update(porte);

        try {
            Thread.sleep(8500);
        } catch (InterruptedException e) {
            System.err.print(e.getMessage());
        }
        diodes.eteint();

        ledsoff = true;
        lcd.saisie(false);
        lcd.print(menu.toString(), true);
        //    sip.init();

        Event event;
        while (true) {

            try {
                event = manager.getEvent();
                if (event != null) {
                    curentItem.execute(event);
                }
                curentItem = menu.getCurrentItem();

            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.exit(1);

            }
        }
    }

    /**
     * fonction qui permet de récuperer le serveur pour l'enregistrement
     *
     * @param nb position du techno à récuperer
     * @return indice du serveur pour s'enregistrer
     */
    public static int getTypeTechno(int nb) {
        Porte porte = Porte.getInstance();

        int type = -1;
        char techno = porte.getServeurIP().charAt(nb == 1 ? 0 : 1);
        if (techno == '-') {
            techno = porte.getServeurIP().charAt(0);
        }
        switch (techno) {
            case 'C':
                type = 0; // client
                break;
            case 'O':
                type = 1; // ovh
                break;
            case 'D':
                type = 2; // asterisk
                break;
            case 'V':
                type = 3; // diese
                break;
            default:
                break;
        }
        return type;
    }

    private static String getUsername(int techno) {
        Porte porte = Porte.getInstance();
        String username = "";
        switch (techno) {
            case 0:
                username = porte.getUserClient();
                break;
            case 1:
                //  username = "8" + (porte.getId().length() == 1 ? "00" + porte.getId() : (porte.getId().length() == 2 ? "0" + porte.getId() : porte.getId()));
                username = "Portier_" + porte.getId();//"0033972137830";////"vMIVsfORIR";
                break;
            case 2:
                username = "DIGIPcall";
                break;
            case 3:
                username = "Portier_" + porte.getId();//Portier_Diese_"+porte.getId();
                break;
            default:
                break;
        }
        return username;
    }

    private static String getPassword(int techno) {
        Porte porte = Porte.getInstance();
        String password = "";

        switch (techno) {
            case 0:
                password = porte.getMdpClient();
                break;
            case 1:
                password = "9999";//toorcdvic4pass2019";//"icKZdIOQDW";//"toorcdvic4pass2019";////
                break;
            case 2:
                password = "9999";
                break;
            case 3:
                password = "9999";//"Diesetel92";//"9999";
                break;
            default:
                break;
        }
        return password;
    }

    private static String getDomain(int techno) {
        Porte porte = Porte.getInstance();
        String domain = "";

        switch (techno) {
            case 0:
                domain = porte.getIpClient();
                break;
            case 1:
                domain = "192.168.1.108";//sip5.ovh.fr";//"94.143.87.216";//"sip5.ovh.fr";////
                break;
            case 2:
                domain = "127.0.0.1";
                break;
            case 3:
                domain = "192.168.1.100";//178.33.26.236";//sip.linphone.org";//192.168.168.103";
                break;
            default:
                break;
        }
        return domain;
    }

    /**
     * execute une commande cmd
     *
     * @param cmd la commande à executer
     */
    public static void cmdVpn(String cmd) {
        Process p2;
        ProcessBuilder pb2;
        try {
            pb2 = new ProcessBuilder("sudo", "bash", "-c", cmd);
            p2 = pb2.start();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }

}
