package sql;

import event.Event;
import java.util.List;

/**
 * interface events
 *
 * @author Abdellah
 */
interface IDataBaseEvent {

    /**
     * renvoi list des evenements
     *
     * @return list events
     */
    List<Event> getEvents();

}
