package sql.data;

import sql.DataBase;

/**
 * classe porte table porte
 *
 * @author Abdellah
 */
public class Porte {

    String id;
    String idResidence;
    String nomAcces;
    String interphone;
    String version;
    String idTeamViewer;
    String numeroSim;
    String dateModification;
    String tranchesHoraires;
    String ClavierCodeUn;
    String choixClavierCodeUn;
    String temporaireClavierCodeUn;
    String ClavierCodeDeux;
    String choixClavierCodeDeux;
    String temporaireClavierCodeDeux;
    String choixEntreLibreUn;
    String choixEntreLibreDeux;
    String tempsAppel;
    String tempsCommunication;
    String appelAbrege;
    String niveauHP;
    String niveauMicro;
    String messageBienvenue;
    String messageUtilisation;
    String affichageEtage;
    String prioriteAffichage1;
    String prioriteAffichage2;
    String prioriteAffichage3;
    String tempsOuvertureUn;
    String tempsOuvertureDeux;
    String tempsOuvertureTrois;
    String tempsOuvertureQuatre;
    String tempsOuvertureCinq;
    String tempsOuvertureSix;
    String tempsOuvertureSept;
    String tempsOuvertureHuit;
    String codeDTMF;
    String signalSonore;
    String nomPorte1;
    String lecteurPorte1;
    String nomPorte2;
    String lecteurPorte2;
    String nomPorte3;
    String lecteurPorte3;
    String nomPorte4;
    String lecteurPorte4;
    String nomPorte5;
    String lecteurPorte5;
    String nomPorte6;
    String lecteurPorte6;
    String nomPorte7;
    String lecteurPorte7;
    String nomPorte8;
    String lecteurPorte8;
    String validTempBadge;
    String validTempIdentitel;
    String code;
    String ipInterne;
    String ipClient;
    String userClient;
    String mdpClient;
    String ipExterne;
    String userExterne;
    String mdpExterne;
    String codeAccueil;
    String gestionCouleur;
    String visuelBadge;
    String lecteur;
    String idBadge;
    String traduction;

    String idWifi;
    String mdpWifi;
    String saisieManuelle;
    String serveurIP;
    String evenementAutomatique;
    String validConnexionWeb;
    String timeConnexionWeb;
    String validWebClient;
    String validServeurClientDhcp;
    String ipPlatine;
    String netmask;
    String passerelle;
    String validRadio;
    String validIdentitooth;
    String validIdentitelSIP;
    String macEthernet;
    String macWifi;
    String validUart;
    String tempRadio;
    String tempIdentitooth;
    String tempIdentitelSIP;
    String validWifiPi;
    String mdpWifiPi;

    String activationVisio;
    String codeAppel;
    String codePorte2;
    String jourDebutBadge;
    String jourFinBadge;
    String th1BadgeDebut;
    String th1BadgeFin;
    String th2BadgeDebut;
    String th2BadgeFin;
    String jourBadge;
    String th1BadgeDebutSem2;
    String th1BadgeFinSem2;
    String th2BadgeDebutSem2;
    String th2BadgeFinSem2;
    String jourBadgeSem2;
    String jourDebutIdentitel;
    String jourFinIdentitel;
    String th1IdnetitelDebut;
    String th1IdnetitelFin;
    String th2IdnetitelDebut;
    String th2IdnetitelFin;
    String jourIdnetitel;
    String th1IdnetitelDebutSem2;
    String th1IdnetitelFinSem2;
    String th2BIdnetitelDebutSem2;
    String th2IdnetitelFinSem2;
    String jourIdnetitelSem2;
    String validVisio;
    String telephoneSms;
    String tempoMenu;
    String horaireUnDebutClavierCodeUn;
    String horaireUnFinClavierCodeUn;
    String horaireDeuxDebutClavierCodeUn;
    String horaireDeuxFinClavierCodeUn;
    String horaireTroisDebutClavierCodeUn;
    String horaireTroisFinClavierCodeUn;
    String horaireQuatreDebutClavierCodeUn;
    String horaireQuatreFinClavierCodeUn;
    String joursUnClavierCodeUn;
    String joursDeuxClavierCodeUn;
    String debutClavierCodeDeux;
    String finClavierCodeDeux;
    String horaireUnDebutClavierCodeDeux;
    String horaireUnFinClavierCodeDeux;
    String horaireDeuxDebutClavierCodeDeux;
    String horaireDeuxFinClavierCodeDeux;
    String horaireTroisDebutClavierCodeDeux;
    String horaireTroisFinClavierCodeDeux;
    String horaireQuatreDebutClavierCodeDeux;
    String horaireQuatreFinClavierCodeDeux;
    String joursUnClavierCodeDeux;
    String joursDeuxClavierCodeDeux;
    String temporaireEntreLibreUn;
    String debutEntreLibreUn;
    String finEntreLibreUn;
    String horaireUnDebutEntreLibreUn;
    String horaireUnFinEntreLibreUn;
    String horaireDeuxDebutEntreLibreUn;
    String horaireDeuxFinEntreLibreUn;
    String horaireTroisDebutEntreLibreUn;
    String horaireTroisFinEntreLibreUn;
    String horaireQuatreDebutEntreLibreUn;
    String horaireQuatreFinEntreLibreUn;
    String joursUnEntreLibreUn;
    String joursDeuxEntreLibreUn;
    String temporaireEntreLibreDeux;
    String debutEntreLibreDeux;
    String finEntreLibreDeux;
    String horaireUnDebutEntreLibreDeux;
    String horaireUnFinEntreLibreDeux;
    String horaireDeuxDebutEntreLibreDeux;
    String horaireDeuxFinEntreLibreDeux;
    String horaireTroisDebutEntreLibreDeux;
    String horaireTroisFinEntreLibreDeux;
    String horaireQuatreDebutEntreLibreDeux;
    String horaireQuatreFinEntreLibreDeux;
    String joursUnEntreLibreDeux;
    String joursDeuxEntreLibreDeux;
    String langue;

    String tempoVpn;
    String tempoSip;
    String validVpn;

    String validSipUn;
    String validSipDeux;
    String tempoConnect;

    String numPorte;
    String nbResidents;
    String typeTechno;
    char typeClavier;
    String defaultGateway;
    public static Porte INSTANCE = null;

    /**
     * renvoi instance de la porte (singleton)
     *
     * @return
     */
    public static Porte getInstance() {
        if (INSTANCE == null) {
            INSTANCE = DataBase.getInstance().getPorte();

        }
        return INSTANCE;
    }

    /**
     * constructeur de classe
     */
    public Porte() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdResidence() {
        return idResidence;
    }

    public void setIdResidence(String idResidence) {
        this.idResidence = idResidence;
    }

    public String getNomAcces() {
        return nomAcces;
    }

    public void setNomAcces(String nom) {
        this.nomAcces = nom;
    }

    public String getInterphone() {
        return interphone;
    }

    public void setInterphone(String interphone) {
        this.interphone = interphone;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIdTeamViewer() {
        return idTeamViewer;
    }

    public void setIdTeamViewer(String idTeamViewer) {
        this.idTeamViewer = idTeamViewer;
    }

    public String getNumeroSim() {
        return numeroSim;
    }

    public void setNumeroSim(String numeroSim) {

        if (numeroSim.length() != 10) {
            numeroSim = "0000000000";
        }
        this.numeroSim = numeroSim;
    }

    public String getDateModification() {
        return dateModification;
    }

    public void setDateModification(String dateModification) {
        this.dateModification = dateModification;
    }

    public String getTranchesHoraires() {
        return tranchesHoraires;
    }

    public void setTranchesHoraires(String tranchesHoraires) {
        this.tranchesHoraires = tranchesHoraires;
    }

    public String getClavierCodeUn() {
        return ClavierCodeUn;
    }

    public void setClavierCodeUn(String code) {
        this.ClavierCodeUn = code;
    }

    public String getChoixClavierCodeUn() {
        return choixClavierCodeUn;
    }

    public void setChoixClavierCodeUn(String choix) {
        this.choixClavierCodeUn = choix;
    }

    public String getTemporaireClavierCodeUn() {
        return temporaireClavierCodeUn;
    }

    public void setTemporaireClavierCodeUn(String choix) {
        this.temporaireClavierCodeUn = choix;
    }

    public String getClavierCodeDeux() {
        return ClavierCodeDeux;
    }

    public void setClavierCodeDeux(String code) {
        this.ClavierCodeDeux = code;
    }

    public String getChoixClavierCodeDeux() {
        return choixClavierCodeDeux;
    }

    public void setChoixClavierCodeDeux(String choix) {
        this.choixClavierCodeDeux = choix;
    }

    public String getTemporaireClavierCodeDeux() {
        return temporaireClavierCodeDeux;
    }

    public void setTemporaireClavierCodeDeux(String choix) {
        this.temporaireClavierCodeDeux = choix;
    }

    public String getChoixEntreLibreUn() {
        return choixEntreLibreUn;
    }

    public void setChoixEntreLibreUn(String choix) {
        this.choixEntreLibreUn = choix;
    }

    public String getChoixEntreLibreDeux() {
        return choixEntreLibreDeux;
    }

    public void setChoixEntreLibreDeux(String choix) {
        this.choixEntreLibreDeux = choix;
    }

    public String getTempsAppel() {
        return tempsAppel;
    }

    public void setTempsAppel(String tempsAppel) {
        if (tempsAppel.length() < 2) {
            this.tempsAppel = "00" + tempsAppel;
        } else if (tempsAppel.length() < 3) {
            this.tempsAppel = "0" + tempsAppel;
        } else {
            this.tempsAppel = tempsAppel;
        }
    }

    public String getTempsCommunication() {
        return tempsCommunication;
    }

    public void setTempsCommunication(String tempsCommunication) {
        if (tempsCommunication.length() < 2) {
            this.tempsCommunication = "00" + tempsCommunication;
        } else if (tempsCommunication.length() < 3) {
            this.tempsCommunication = "0" + tempsCommunication;
        } else {
            this.tempsCommunication = tempsCommunication;
        }
    }

    public String getAppelAbrege() {
        return appelAbrege;
    }

    public void setAppelAbrege(String appelAbrege) {
        this.appelAbrege = appelAbrege;
    }

    public String getNiveauHP() {
        return niveauHP;
    }

    public void setNiveauHP(String niveau) {
        this.niveauHP = niveau;
    }

    public String getNiveauMicro() {
        return niveauMicro;
    }

    public void setNiveauMicro(String niveau) {
        this.niveauMicro = niveau;
    }

    public String getMessageBienvenue() {
        return messageBienvenue;
    }

    public void setMessageBienvenue(String msg) {
        this.messageBienvenue = msg;
    }

    public String getMessageUtilisation() {
        return messageUtilisation;
    }

    public void setMessageUtilisation(String msg) {
        this.messageUtilisation = msg;
    }

    public String getAffichageEtage() {
        return affichageEtage;
    }

    public void setAffichageEtage(String bool) {
        this.affichageEtage = bool;
    }

    public String getPrioriteAffichageUn() {
        return prioriteAffichage1;
    }

    public void setPrioriteAffichageUn(String id) {
        this.prioriteAffichage1 = id;
    }

    public String getPrioriteAffichageDeux() {
        return prioriteAffichage2;
    }

    public void setPrioriteAffichageDeux(String id) {
        this.prioriteAffichage2 = id;
    }

    public String getPrioriteAffichageTrois() {
        return prioriteAffichage3;
    }

    public void setPrioriteAffichageTrois(String id) {
        this.prioriteAffichage3 = id;
    }

    public String getTempsOuvertureUn() {
        return tempsOuvertureUn;
    }

    public void setTempsOuvertureUn(String temps) {
        this.tempsOuvertureUn = temps;
    }

    public String getTempsOuvertureDeux() {
        return tempsOuvertureDeux;
    }

    public void setTempsOuvertureDeux(String temps) {
        this.tempsOuvertureDeux = temps;
    }

    public String getTempsOuvertureTrois() {
        return tempsOuvertureTrois;
    }

    public void setTempsOuvertureTrois(String temps) {
        this.tempsOuvertureTrois = temps;
    }

    public String getTempsOuvertureQuatre() {
        return tempsOuvertureQuatre;
    }

    public void setTempsOuvertureQuatre(String temps) {
        this.tempsOuvertureQuatre = temps;
    }

    public String getTempsOuvertureCinq() {
        return tempsOuvertureCinq;
    }

    public void setTempsOuvertureCinq(String temps) {
        this.tempsOuvertureCinq = temps;
    }

    public String getTempsOuvertureSix() {
        return tempsOuvertureSix;
    }

    public void setTempsOuvertureSix(String temps) {
        this.tempsOuvertureSix = temps;
    }

    public String getTempsOuvertureSept() {
        return tempsOuvertureSept;
    }

    public void setTempsOuvertureSept(String temps) {
        this.tempsOuvertureSept = temps;
    }

    public String getTempsOuvertureHuit() {
        return tempsOuvertureHuit;
    }

    public void setTempsOuvertureHuit(String temps) {
        this.tempsOuvertureHuit = temps;
    }

    public String getTouchesDTMF() {
        return codeDTMF;
    }

    public void setTouchesDTMF(String code) {
        this.codeDTMF = code;
    }

    public String getSignalSonore() {
        return signalSonore;
    }

    public void setSignalSonore(String string) {
        this.signalSonore = string;
    }

    public String getLecteurPorte1() {
        return lecteurPorte1;
    }

    public void setLecteurPorte1(String lecteurPorte1) {
        this.lecteurPorte1 = lecteurPorte1;
    }

    public String getNomPorte1() {
        return nomPorte1;
    }

    public void setNomPorte1(String nomPorte1) {
        this.nomPorte1 = nomPorte1;
    }

    public String getLecteurPorte2() {
        return lecteurPorte2;
    }

    public void setLecteurPorte2(String lecteurPorte2) {
        this.lecteurPorte2 = lecteurPorte2;
    }

    public String getNomPorte2() {
        return nomPorte2;
    }

    public void setNomPorte2(String nomPorte2) {
        this.nomPorte2 = nomPorte2;
    }

    public String getLecteurPorte3() {
        return lecteurPorte3;
    }

    public void setLecteurPorte3(String lecteurPorte3) {
        this.lecteurPorte3 = lecteurPorte3;
    }

    public String getNomPorte3() {
        return nomPorte3;
    }

    public void setNomPorte3(String nomPorte3) {
        this.nomPorte3 = nomPorte3;
    }

    public String getLecteurPorte4() {
        return lecteurPorte4;
    }

    public void setLecteurPorte4(String lecteurPorte4) {
        this.lecteurPorte4 = lecteurPorte4;
    }

    public String getNomPorte4() {
        return nomPorte4;
    }

    public void setNomPorte4(String nomPorte4) {
        this.nomPorte4 = nomPorte4;
    }

    public String getLecteurPorte5() {
        return lecteurPorte5;
    }

    public void setLecteurPorte5(String lecteurPorte5) {
        this.lecteurPorte5 = lecteurPorte5;
    }

    public String getNomPorte5() {
        return nomPorte5;
    }

    public void setNomPorte5(String nomPorte5) {
        this.nomPorte5 = nomPorte5;
    }

    public String getLecteurPorte6() {
        return lecteurPorte6;
    }

    public void setLecteurPorte6(String lecteurPorte6) {
        this.lecteurPorte6 = lecteurPorte6;
    }

    public String getNomPorte6() {
        return nomPorte6;
    }

    public void setNomPorte6(String nomPorte6) {
        this.nomPorte6 = nomPorte6;
    }

    public String getLecteurPorte7() {
        return lecteurPorte7;
    }

    public void setLecteurPorte7(String lecteurPorte7) {
        this.lecteurPorte7 = lecteurPorte7;
    }

    public String getNomPorte7() {
        return nomPorte7;
    }

    public void setNomPorte7(String nomPorte7) {
        this.nomPorte7 = nomPorte7;
    }

    public String getLecteurPorte8() {
        return lecteurPorte8;
    }

    public void setLecteurPorte8(String lecteurPorte8) {
        this.lecteurPorte8 = lecteurPorte8;
    }

    public String getNomPorte8() {
        return nomPorte8;
    }

    public void setNomPorte8(String nomPorte8) {
        this.nomPorte8 = nomPorte8;
    }

    public String getValidTempBadge() {
        return validTempBadge;
    }

    public void setValidTempBadge(String validTempBadge) {
        this.validTempBadge = validTempBadge;
    }

    public String getValidTempIdentitel() {
        return validTempIdentitel;
    }

    public void setValidTempIdentitel(String validTempIdentitel) {
        this.validTempIdentitel = validTempIdentitel;
    }

    public String getCode() {
        return code;
    }

    public Porte(String code) {
        this.code = code;
    }

    public String getIpInterne() {
        return ipInterne;
    }

    public void setIpInterne(String ipInterne) {
        this.ipInterne = ipInterne;
    }

    public String getIpClient() {
        return ipClient;
    }

    public void setIpClient(String ipClient) {
        this.ipClient = ipClient;
    }

    public String getUserClient() {
        return userClient;
    }

    public void setUserClient(String userClient) {
        this.userClient = userClient;
    }

    public String getMdpClient() {
        return mdpClient;
    }

    public void setMdpClient(String mdpClient) {
        this.mdpClient = mdpClient;
    }

    public String getIpExterne() {
        return ipExterne;
    }

    public void setIpExterne(String ipExterne) {
        this.ipExterne = ipExterne;
    }

    public String getUserExterne() {
        return userExterne;
    }

    public void setUserExterne(String user) {
        this.userExterne = user;
    }

    public String getMdpExterne() {
        return mdpExterne;
    }

    public void setMdpExterne(String mdp) {
        this.mdpExterne = mdp;
    }

    public String getCodeAccueil() {
        return codeAccueil;
    }

    public void setCodeAccueil(String codeAccueil) {
        this.codeAccueil = codeAccueil;
    }

    public String getGestionCouleur() {
        return gestionCouleur;
    }

    public void setGestionCouleur(String gestionCouleur) {
        this.gestionCouleur = gestionCouleur;
    }

    public String getVisuelBadge() {
        return visuelBadge;
    }

    public void setVisuelBadge(String choix) {
        this.visuelBadge = choix;
    }

    public String getLecteur() {
        return lecteur;
    }

    public void setLecteur(String lecteur) {
        this.lecteur = lecteur;
    }

    public String getIdBadge() {
        return idBadge;
    }

    public void setIdBadge(String idBadge) {
        this.idBadge = idBadge;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getTraduction() {
        return traduction;
    }

    public void setTraduction(String traduction) {
        this.traduction = traduction;
    }

    public String getIdWifi() {
        return idWifi;
    }

    public void setIdWifi(String idWifi) {
        this.idWifi = idWifi;
    }

    public String getMdpWifi() {
        return mdpWifi;
    }

    public void setMdpWifi(String mdpWifi) {
        this.mdpWifi = mdpWifi;
    }

    public String getSaisieManuelle() {
        return saisieManuelle;
    }

    public void setSaisieManuelle(String saisieManuelle) {
        this.saisieManuelle = saisieManuelle;
    }

    public String getServeurIP() {
        return serveurIP;
    }

    public void setServeurIP(String serveurIP) {
        this.serveurIP = serveurIP;
    }

    public String getEvenementAutomatique() {
        return evenementAutomatique;
    }

    public void setEvenementAutomatique(String evenementAutomatique) {
        this.evenementAutomatique = evenementAutomatique;
    }

    public String getValidConnexionWeb() {
        return validConnexionWeb;
    }

    public void setValidConnexionWeb(String bool) {
        this.validConnexionWeb = bool;
    }

    public String getTimeConnexionWeb() {
        return timeConnexionWeb;
    }

    public void setTimeConnexionWeb(String delai) {
        this.timeConnexionWeb = delai;
    }

    public String getValidWebClient() {
        return validWebClient;
    }

    public void setValidWebClient(String validWebClient) {
        this.validWebClient = validWebClient;
    }

    public String getValidServeurClientDhcp() {
        return validServeurClientDhcp;
    }

    public void setValidServeurClientDhcp(String validdhcp) {
        this.validServeurClientDhcp = validdhcp;
    }

    public String getIpPlatine() {
        return ipPlatine;
    }

    public void setIpPlatine(String ipplatine) {
        this.ipPlatine = ipplatine;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    public String getPasserelle() {
        return passerelle;
    }

    public void setPasserelle(String pass) {
        this.passerelle = pass;
    }

    public String getValidRadio() {
        return validRadio;
    }

    public void setValidRadio(String validRadio) {
        this.validRadio = validRadio;
    }

    public String getValidIdentitooth() {
        return validIdentitooth;
    }

    public void setValidIdentitooth(String validIdentitooth) {
        this.validIdentitooth = validIdentitooth;
    }

    public String getValidIdentitelSIP() {
        return validIdentitelSIP;
    }

    public void setValidIdentitelSIP(String validIdentitelSIP) {
        this.validIdentitelSIP = validIdentitelSIP;
    }

    public String getMacEthernet() {
        if (macEthernet == null) {
            macEthernet = "xx-xx-xx-xx-xx-xx";
        }
        return macEthernet;
    }

    public void setMacEthernet(String macEthernet) {
        this.macEthernet = macEthernet;
    }

    public String getMacWifi() {
        return macWifi;
    }

    public void setMacWifi(String macwifi) {
        this.macWifi = macwifi;
    }

    public String getValidUart() {
        return validUart;
    }

    public void setValidUart(String validUart) {
        this.validUart = validUart;
    }

    public String getTempRadio() {
        return tempRadio;
    }

    public void setTempRadio(String tempRadio) {
        this.tempRadio = tempRadio;
    }

    public String getTempIdentitooth() {
        return tempIdentitooth;
    }

    public void setTempIdentitooth(String tempIdentitooth) {
        this.tempIdentitooth = tempIdentitooth;
    }

    public String getTempIdentitelSIP() {
        return tempIdentitelSIP;
    }

    public void setTempIdentitelSIP(String tempIdentitelSIP) {
        this.tempIdentitelSIP = tempIdentitelSIP;
    }

    public String getValidWifiPi() {
        return validWifiPi;
    }

    public void setValidWifiPi(String validWifiPi) {
        this.validWifiPi = validWifiPi;
    }

    public String getMdpWifiPi() {
        return mdpWifiPi;
    }

    public void setMdpWifiPi(String mdpWifiPi) {
        this.mdpWifiPi = mdpWifiPi;
    }

    // renvoie le code d'appel
    public String getCodeAppel() {
        return codeAppel;
    }

    // définie le code d'appel
    public void setCodeAppel(String code) {
        this.codeAppel = code;
    }

    // renvoie l'horaire de debut de la tranche horaire 1 du CC1
    public String getHoraireUnDebutClavierCodeUn() {
        return horaireUnDebutClavierCodeUn;
    }

    // définie l'horaire de debut de la tranche horaire 1 du CC1
    public void setHoraireUnDebutClavierCodeUn(String horaire) {
        this.horaireUnDebutClavierCodeUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 1 du CC1

    public String getHoraireUnFinClavierCodeUn() {
        return horaireUnFinClavierCodeUn;
    }

    // définie l'horaire de fin de la tranche horaire 1 du CC1
    public void setHoraireUnFinClavierCodeUn(String horaire) {
        this.horaireUnFinClavierCodeUn = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 2 du CC1

    public String getHoraireDeuxDebutClavierCodeUn() {
        return horaireDeuxDebutClavierCodeUn;
    }

    // définie l'horaire de debut de la tranche horaire 2 du CC1
    public void setHoraireDeuxDebutClavierCodeUn(String horaire) {
        this.horaireDeuxDebutClavierCodeUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 2 du CC1

    public String getHoraireDeuxFinClavierCodeUn() {
        return horaireDeuxFinClavierCodeUn;
    }

    // définie l'horaire de fin de la tranche horaire 2 du CC1
    public void setHoraireDeuxFinClavierCodeUn(String horaire) {
        this.horaireDeuxFinClavierCodeUn = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 3 du CC1

    public String getHoraireTroisDebutClavierCodeUn() {
        return horaireTroisDebutClavierCodeUn;
    }

    // définie l'horaire de debut de la tranche horaire 3 du CC1
    public void setHoraireTroisDebutClavierCodeUn(String horaire) {
        this.horaireTroisDebutClavierCodeUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 3 du CC1

    public String getHoraireTroisFinClavierCodeUn() {
        return horaireTroisFinClavierCodeUn;
    }

    // définie l'horaire de fin de la tranche horaire 3 du CC1
    public void setHoraireTroisFinClavierCodeUn(String horaire) {
        this.horaireTroisFinClavierCodeUn = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 4 du CC1

    public String getHoraireQuatreDebutClavierCodeUn() {
        return horaireQuatreDebutClavierCodeUn;
    }

    // définie l'horaire de debut de la tranche horaire 4 du CC1
    public void setHoraireQuatreDebutClavierCodeUn(String horaire) {
        this.horaireQuatreDebutClavierCodeUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 4 du CC1

    public String getHoraireQuatreFinClavierCodeUn() {
        return horaireQuatreFinClavierCodeUn;
    }

    // définie l'horaire de fin de la tranche horaire 4 du CC1
    public void setHoraireQuatreFinClavierCodeUn(String horaire) {
        this.horaireQuatreFinClavierCodeUn = horaire;
    }

    // renvoie les jours de la semaine 1 du CC1
    public String getJoursUnClavierCodeUn() {
        return joursUnClavierCodeUn;
    }

    // définie les jours de la semaine 1 du CC1
    public void setJoursUnClavierCodeUn(String jours) {
        this.joursUnClavierCodeUn = jours;
    }
    // renvoie les jours de la semaine 2 du CC1

    public String getJoursDeuxClavierCodeUn() {
        return joursDeuxClavierCodeUn;
    }

    // définie les jours de la semaine 2 du CC1
    public void setJoursDeuxClavierCodeUn(String jours) {
        this.joursDeuxClavierCodeUn = jours;
    }

    // renvoie la date de debut du CC1
    public String getDebutClavierCodeDeux() {
        return debutClavierCodeDeux;
    }

    // définie la date de debut du CC1
    public void setDebutClavierCodeDeux(String date) {
        this.debutClavierCodeDeux = date;
    }
    // renvoie la date de fin du CC1

    public String getFinClavierCodeDeux() {
        return finClavierCodeDeux;
    }

    // définie la date de fin du CC1
    public void setFinClavierCodeDeux(String date) {
        this.finClavierCodeDeux = date;
    }
    // renvoie l'horaire de debut de la tranche horaire 1 du CC1

    public String getHoraireUnDebutClavierCodeDeux() {
        return horaireUnDebutClavierCodeDeux;
    }

    // définie l'horaire de debut de la tranche horaire 1 du CC1
    public void setHoraireUnDebutClavierCodeDeux(String horaire) {
        this.horaireUnDebutClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 1 du CC1

    public String getHoraireUnFinClavierCodeDeux() {
        return horaireUnFinClavierCodeDeux;
    }

    // définie l'horaire de fin de la tranche horaire 1 du CC1
    public void setHoraireUnFinClavierCodeDeux(String horaire) {
        this.horaireUnFinClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 2 du CC1

    public String getHoraireDeuxDebutClavierCodeDeux() {
        return horaireDeuxDebutClavierCodeDeux;
    }

    // définie l'horaire de debut de la tranche horaire 2 du CC1
    public void setHoraireDeuxDebutClavierCodeDeux(String horaire) {
        this.horaireDeuxDebutClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 2 du CC1

    public String getHoraireDeuxFinClavierCodeDeux() {
        return horaireDeuxFinClavierCodeDeux;
    }

    // définie l'horaire de fin de la tranche horaire 2 du CC1
    public void setHoraireDeuxFinClavierCodeDeux(String horaire) {
        this.horaireDeuxFinClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 3 du CC1

    public String getHoraireTroisDebutClavierCodeDeux() {
        return horaireTroisDebutClavierCodeDeux;
    }

    // définie l'horaire de debut de la tranche horaire 3 du CC1
    public void setHoraireTroisDebutClavierCodeDeux(String horaire) {
        this.horaireTroisDebutClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 3 du CC1

    public String getHoraireTroisFinClavierCodeDeux() {
        return horaireTroisFinClavierCodeDeux;
    }

    // définie l'horaire de fin de la tranche horaire 3 du CC1
    public void setHoraireTroisFinClavierCodeDeux(String horaire) {
        this.horaireTroisFinClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 4 du CC1

    public String getHoraireQuatreDebutClavierCodeDeux() {
        return horaireQuatreDebutClavierCodeDeux;
    }

    // définie l'horaire de debut de la tranche horaire 4 du CC1
    public void setHoraireQuatreDebutClavierCodeDeux(String horaire) {
        this.horaireQuatreDebutClavierCodeDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 4 du CC1

    public String getHoraireQuatreFinClavierCodeDeux() {
        return horaireQuatreFinClavierCodeDeux;
    }

    // définie l'horaire de fin de la tranche horaire 4 du CC1
    public void setHoraireQuatreFinClavierCodeDeux(String horaire) {
        this.horaireQuatreFinClavierCodeDeux = horaire;
    }

    // renvoie les jours de la semaine 1 du CC1
    public String getJoursUnClavierCodeDeux() {
        return joursUnClavierCodeDeux;
    }

    // définie les jours de la semaine 1 du CC1
    public void setJoursUnClavierCodeDeux(String jours) {
        this.joursUnClavierCodeDeux = jours;
    }
    // renvoie les jours de la semaine 2 du CC1

    public String getJoursDeuxClavierCodeDeux() {
        return joursDeuxClavierCodeDeux;
    }
    // définie les jours de la semaine 2 du CC1

    public void setJoursDeuxClavierCodeDeux(String jours) {
        this.joursDeuxClavierCodeDeux = jours;
    }

    // ENTREE LIBRE 1
    // renvoie le choix du EL1 Temporaire
    public String getTemporaireEntreLibreUn() {
        return temporaireEntreLibreUn;
    }

    // définie le choix du EL1 Temporaire
    public void setTemporaireEntreLibreUn(String choix) {
        this.temporaireEntreLibreUn = choix;
    }
    // renvoie la date de debut du EL1

    public String getDebutEntreLibreUn() {
        return debutEntreLibreUn;
    }

    // définie la date de debut du EL1
    public void setDebutEntreLibreUn(String date) {
        this.debutEntreLibreUn = date;
    }
    // renvoie la date de fin du EL1

    public String getFinEntreLibreUn() {
        return finEntreLibreUn;
    }

    // définie la date de fin du EL1
    public void setFinEntreLibreUn(String date) {
        this.finEntreLibreUn = date;
    }
    // renvoie l'horaire de debut de la tranche horaire 1 du EL1

    public String getHoraireUnDebutEntreLibreUn() {
        return horaireUnDebutEntreLibreUn;
    }

    // définie l'horaire de debut de la tranche horaire 1 du EL1
    public void setHoraireUnDebutEntreLibreUn(String horaire) {
        this.horaireUnDebutEntreLibreUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 1 du EL1

    public String getHoraireUnFinEntreLibreUn() {
        return horaireUnFinEntreLibreUn;
    }

    // définie l'horaire de fin de la tranche horaire 1 du EL1
    public void setHoraireUnFinEntreLibreUn(String horaire) {
        this.horaireUnFinEntreLibreUn = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 2 du EL1

    public String getHoraireDeuxDebutEntreLibreUn() {
        return horaireDeuxDebutEntreLibreUn;
    }

    // définie l'horaire de debut de la tranche horaire 2 du EL1
    public void setHoraireDeuxDebutEntreLibreUn(String horaire) {
        this.horaireDeuxDebutEntreLibreUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 2 du EL1

    public String getHoraireDeuxFinEntreLibreUn() {
        return horaireDeuxFinEntreLibreUn;
    }

    // définie l'horaire de fin de la tranche horaire 2 du EL1
    public void setHoraireDeuxFinEntreLibreUn(String horaire) {
        this.horaireDeuxFinEntreLibreUn = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 3 du EL1

    public String getHoraireTroisDebutEntreLibreUn() {
        return horaireTroisDebutEntreLibreUn;
    }

    // définie l'horaire de debut de la tranche horaire 3 du EL1
    public void setHoraireTroisDebutEntreLibreUn(String horaire) {
        this.horaireTroisDebutEntreLibreUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 3 du EL1

    public String getHoraireTroisFinEntreLibreUn() {
        return horaireTroisFinEntreLibreUn;
    }

    // définie l'horaire de fin de la tranche horaire 3 du EL1
    public void setHoraireTroisFinEntreLibreUn(String horaire) {
        this.horaireTroisFinEntreLibreUn = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 4 du EL1

    public String getHoraireQuatreDebutEntreLibreUn() {
        return horaireQuatreDebutEntreLibreUn;
    }

    // définie l'horaire de debut de la tranche horaire 4 du EL1
    public void setHoraireQuatreDebutEntreLibreUn(String horaire) {
        this.horaireQuatreDebutEntreLibreUn = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 4 du EL1

    public String getHoraireQuatreFinEntreLibreUn() {
        return horaireQuatreFinEntreLibreUn;
    }

    // définie l'horaire de fin de la tranche horaire 4 du EL1
    public void setHoraireQuatreFinEntreLibreUn(String horaire) {
        this.horaireQuatreFinEntreLibreUn = horaire;
    }

    // renvoie les jours de la semaine 1 du EL1
    public String getJoursUnEntreLibreUn() {
        return joursUnEntreLibreUn;
    }

    // définie les jours de la semaine 1 du EL1
    public void setJoursUnEntreLibreUn(String jours) {
        this.joursUnEntreLibreUn = jours;
    }
    // renvoie les jours de la semaine 2 du EL1

    public String getJoursDeuxEntreLibreUn() {
        return joursDeuxEntreLibreUn;
    }

    // définie les jours de la semaine 2 du EL1
    public void setJoursDeuxEntreLibreUn(String jours) {
        this.joursDeuxEntreLibreUn = jours;
    }

    // ENTREE LIBRE 2
    // renvoie le choix du EL2 Temporaire
    public String getTemporaireEntreLibreDeux() {
        return temporaireEntreLibreDeux;
    }

    // définie le choix du EL2 Temporaire
    public void setTemporaireEntreLibreDeux(String choix) {
        this.temporaireEntreLibreDeux = choix;
    }
    // renvoie la date de debut du EL2

    public String getDebutEntreLibreDeux() {
        return debutEntreLibreDeux;
    }

    // définie la date de debut du EL2
    public void setDebutEntreLibreDeux(String date) {
        this.debutEntreLibreDeux = date;
    }
    // renvoie la date de fin du EL2

    public String getFinEntreLibreDeux() {
        return finEntreLibreDeux;
    }

    // définie la date de fin du EL2
    public void setFinEntreLibreDeux(String date) {
        this.finEntreLibreDeux = date;
    }
    // renvoie l'horaire de debut de la tranche horaire 1 du EL2

    public String getHoraireUnDebutEntreLibreDeux() {
        return horaireUnDebutEntreLibreDeux;
    }

    // définie l'horaire de debut de la tranche horaire 1 du EL2
    public void setHoraireUnDebutEntreLibreDeux(String horaire) {
        this.horaireUnDebutEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 1 du EL2

    public String getHoraireUnFinEntreLibreDeux() {
        return horaireUnFinEntreLibreDeux;
    }

    // définie l'horaire de fin de la tranche horaire 1 du EL2
    public void setHoraireUnFinEntreLibreDeux(String horaire) {
        this.horaireUnFinEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 2 du EL2

    public String getHoraireDeuxDebutEntreLibreDeux() {
        return horaireDeuxDebutEntreLibreDeux;
    }

    // définie l'horaire de debut de la tranche horaire 2 du EL2
    public void setHoraireDeuxDebutEntreLibreDeux(String horaire) {
        this.horaireDeuxDebutEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 2 du EL2

    public String getHoraireDeuxFinEntreLibreDeux() {
        return horaireDeuxFinEntreLibreDeux;
    }

    // définie l'horaire de fin de la tranche horaire 2 du EL2
    public void setHoraireDeuxFinEntreLibreDeux(String horaire) {
        this.horaireDeuxFinEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 3 du EL2

    public String getHoraireTroisDebutEntreLibreDeux() {
        return horaireTroisDebutEntreLibreDeux;
    }

    // définie l'horaire de debut de la tranche horaire 3 du EL2
    public void setHoraireTroisDebutEntreLibreDeux(String horaire) {
        this.horaireTroisDebutEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 3 du EL2

    public String getHoraireTroisFinEntreLibreDeux() {
        return horaireTroisFinEntreLibreDeux;
    }

    // définie l'horaire de fin de la tranche horaire 3 du EL2
    public void setHoraireTroisFinEntreLibreDeux(String horaire) {
        this.horaireTroisFinEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de debut de la tranche horaire 4 du EL2

    public String getHoraireQuatreDebutEntreLibreDeux() {
        return horaireQuatreDebutEntreLibreDeux;
    }

    // définie l'horaire de debut de la tranche horaire 4 du EL2
    public void setHoraireQuatreDebutEntreLibreDeux(String horaire) {
        this.horaireQuatreDebutEntreLibreDeux = horaire;
    }
    // renvoie l'horaire de fin de la tranche horaire 4 du EL2

    public String getHoraireQuatreFinEntreLibreDeux() {
        return horaireQuatreFinEntreLibreDeux;
    }

    // définie l'horaire de fin de la tranche horaire 4 du EL2
    public void setHoraireQuatreFinEntreLibreDeux(String horaire) {
        this.horaireQuatreFinEntreLibreDeux = horaire;
    }

    // renvoie les jours de la semaine 1 du EL2
    public String getJoursUnEntreLibreDeux() {
        return joursUnEntreLibreDeux;
    }

    // définie les jours de la semaine 1 du EL2
    public void setJoursUnEntreLibreDeux(String jours) {
        this.joursUnEntreLibreDeux = jours;
    }
    // renvoie les jours de la semaine 2 du EL2

    public String getJoursDeuxEntreLibreDeux() {
        return joursDeuxEntreLibreDeux;
    }

    // définie les jours de la semaine 2 du EL2
    public void setJoursDeuxEntreLibreDeux(String jours) {
        this.joursDeuxEntreLibreDeux = jours;
    }

    // renvoie le numero logidiese de la porte
    public String getNumPorte() {
        return numPorte;
    }
    // definie le numero logidiese de la porte

    public void setNumPorte(String num) {
        this.numPorte = num;
    }
    // renvoie le nombre de residents (BUG)

    public String getNbResidents() {
        System.out.println("ret: " + this.nbResidents);
        return "1234";
    }
    // definie le nombre de residents (BUG)

    public void setNbResidents(String nb) {
        System.out.println(nb);
        this.nbResidents = nb;
        System.out.println(this.nbResidents);
    }

//! \brief setter pour le code
//! @param le code gestionnaire
    public void setCode(String code) {
        this.code = code;
    }

    public String getActivationVisio() {
        return activationVisio;
    }

//! \brief setter pour l'activation visio
//! @param l'activation visio
    public void setActivationVisio(String activationVisio) {
        this.activationVisio = activationVisio;
    }

    public String getCodePorte2() {
        return codePorte2;
    }

    public void setCodePorte2(String codePorte2) {
        this.codePorte2 = codePorte2;
    }

    public String getJourDebutBadge() {
        return (jourDebutBadge != null) ? jourDebutBadge : "2000-01-01";
    }

    public void setJourDebutBadge(String jourDebutBadge) {
        this.jourDebutBadge = jourDebutBadge;
    }

    public String getJourFinBadge() {
        return (jourFinBadge != null) ? jourFinBadge : "2099-12-31";
    }

    public void setJourFinBadge(String jourFinBadge) {
        this.jourFinBadge = jourFinBadge;
    }

    public String getTh1BadgeDebut() {
        return (th1BadgeDebut != null) ? th1BadgeDebut : "00:00:00";
    }

    public void setTh1BadgeDebut(String th1BadgeDebut) {
        this.th1BadgeDebut = th1BadgeDebut;
    }

    public String getTh1BadgeFin() {
        return (th1BadgeFin != null) ? th1BadgeFin : "23:59:59";
    }

    public void setTh1BadgeFin(String th1BadgeFin) {
        this.th1BadgeFin = th1BadgeFin;
    }

    public String getTh2BadgeDebut() {
        return (th2BadgeDebut != null) ? th2BadgeDebut : "00:00:00";
    }

    public void setTh2BadgeDebut(String th2BadgeDebut) {
        this.th2BadgeDebut = th2BadgeDebut;
    }

    public String getTh2BadgeFin() {
        return (th2BadgeFin != null) ? th2BadgeFin : "23:59:59";
    }

    public void setTh2BadgeFin(String th2BadgeFin) {
        this.th2BadgeFin = th2BadgeFin;
    }

    public String getJourBadge() {
        return jourBadge;
    }

    public void setJourBadge(String jourBadge) {
        this.jourBadge = jourBadge;
    }

    public String getTh1BadgeDebutSem2() {
        return (th1BadgeDebutSem2 != null) ? th1BadgeDebutSem2 : "00:00:00";
    }

    public void setTh1BadgeDebutSem2(String th1BadgeDebutSem2) {
        this.th1BadgeDebutSem2 = th1BadgeDebutSem2;
    }

    public String getTh1BadgeFinSem2() {
        return (th1BadgeFinSem2 != null) ? th1BadgeFinSem2 : "23:59:59";
    }

    public void setTh1BadgeFinSem2(String th1BadgeFinSem2) {
        this.th1BadgeFinSem2 = th1BadgeFinSem2;
    }

    public String getTh2BadgeDebutSem2() {
        return (th2BadgeDebutSem2 != null) ? th2BadgeDebutSem2 : "00:00:00";
    }

    public void setTh2BadgeDebutSem2(String th2BadgeDebutSem2) {
        this.th2BadgeDebutSem2 = th2BadgeDebutSem2;
    }

    public String getTh2BadgeFinSem2() {
        return (th2BadgeFinSem2 != null) ? th2BadgeFinSem2 : "23:59:59";
    }

    public void setTh2BadgeFinSem2(String th2BadgeFinSem2) {
        this.th2BadgeFinSem2 = th2BadgeFinSem2;
    }

    public String getJourBadgeSem2() {
        return jourBadgeSem2;
    }

    public void setJourBadgeSem2(String jourBadgeSem2) {
        this.jourBadgeSem2 = jourBadgeSem2;
    }

    public String getJourDebutIdentitel() {
        return jourDebutIdentitel;
    }

    public void setJourDebutIdentitel(String jourDebutIdentitel) {
        this.jourDebutIdentitel = jourDebutIdentitel;
    }

    public String getJourFinIdentitel() {
        return jourFinIdentitel;
    }

    public void setJourFinIdentitel(String jourFinIdentitel) {
        this.jourFinIdentitel = jourFinIdentitel;
    }

    public String getTh1IdentitelDebut() {
        return th1IdnetitelDebut;
    }

    public void setTh1IdentitelDebut(String th1IdnetitelDebut) {
        this.th1IdnetitelDebut = th1IdnetitelDebut;
    }

    public String getTh1IdentitelFin() {
        return th1IdnetitelFin;
    }

    public void setTh1IdentitelFin(String th1IdnetitelFin) {
        this.th1IdnetitelFin = th1IdnetitelFin;
    }

    public String getTh2IdentitelDebut() {
        return th2IdnetitelDebut;
    }

    public void setTh2IdentitelDebut(String th2IdnetitelDebut) {
        this.th2IdnetitelDebut = th2IdnetitelDebut;
    }

    public String getTh2IdentitelFin() {
        return th2IdnetitelFin;
    }

    public void setTh2IdentitelFin(String th2IdnetitelFin) {
        this.th2IdnetitelFin = th2IdnetitelFin;
    }

    public String getJourIdnetitel() {
        return jourIdnetitel;
    }

    public void setJourIdentitel(String jourIdnetitel) {
        this.jourIdnetitel = jourIdnetitel;
    }

    public String getTh1IdentitelDebutSem2() {
        return th1IdnetitelDebutSem2;
    }

    public void setTh1IdentitelDebutSem2(String th1IdnetitelDebutSem2) {
        this.th1IdnetitelDebutSem2 = th1IdnetitelDebutSem2;
    }

    public String getTh1IdentitelFinSem2() {
        return th1IdnetitelFinSem2;
    }

    public void setTh1IdentitelFinSem2(String th1IdnetitelFinSem2) {
        this.th1IdnetitelFinSem2 = th1IdnetitelFinSem2;
    }

    public String getTh2IdentitelDebutSem2() {
        return th2BIdnetitelDebutSem2;
    }

    public void setTh2IdentitelDebutSem2(String th2BIdnetitelDebutSem2) {
        this.th2BIdnetitelDebutSem2 = th2BIdnetitelDebutSem2;
    }

    public String getTh2IdentitelFinSem2() {
        return th2IdnetitelFinSem2;
    }

    public void setTh2IdentitelFinSem2(String th2IdnetitelFinSem2) {
        this.th2IdnetitelFinSem2 = th2IdnetitelFinSem2;
    }

    public String getJourIdentitelSem2() {
        return jourIdnetitelSem2;
    }

    public void setJourIdentitelSem2(String jourIdnetitelSem2) {
        this.jourIdnetitelSem2 = jourIdnetitelSem2;
    }

    public String getValidVisio() {
        return validVisio;
    }

    public void setValidVisio(String validVisio) {
        this.validVisio = validVisio;
    }

    public String getTelephoneSms() {
        return telephoneSms;
    }

    public void setTelephoneSms(String telephoneSms) {
        this.telephoneSms = telephoneSms;
    }

    public String getTempoMenu() {
        return tempoMenu;
    }

    public void setTempoMenu(String tempoMenu) {
        this.tempoMenu = tempoMenu;
    }

    public String getTempoVpn() {
        return tempoVpn;
    }

    public void setTempoVpn(String tempoVpn) {
        this.tempoVpn = tempoVpn;
    }

    public String getTempoSip() {
        return tempoSip;
    }

    public void setTempoSip(String tempoSip) {
        this.tempoSip = tempoSip;
    }

    public String getValidVpn() {
        return validVpn;
    }

    public void setValidVpn(String validVpn) {
        this.validVpn = validVpn;
    }

    public String getValidSipUn() {
        return validSipUn;
    }

    public void setValidSipUn(String validSipUn) {
        this.validSipUn = validSipUn;
    }

    public String getValidSipDeux() {
        return validSipDeux;
    }

    public void setValidSipDeux(String validSipDeux) {
        this.validSipDeux = validSipDeux;
    }

    public String getTempoConnect() {
        return tempoConnect;
    }

    public void setTempoConnect(String tempoConnect) {
        this.tempoConnect = tempoConnect;
    }

    static boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public String getDefaultGateway() {
        return defaultGateway;
    }

    public void setDefaultGateway(String defaultGateway) {
        this.defaultGateway = defaultGateway;
    }
}
