package sql.data;

/**
 * classe des evenements qui represente la table eventporte
 *
 * @author Abdellah
 */
public class Evenement {

    private String id;
    private String idPorte;
    private String date_event;
    private String type;
    private String lcd_ligne1;
    private String lcd_ligne2;
    private String lcd_ligne3;
    private String lcd_ligne4;

    /**
     * constructeur
     *
     * @param id identifiant
     * @param idPorte idporte
     * @param date date
     * @param type type
     * @param ligne1 ligne 1
     * @param ligne2 ligne 2
     * @param ligne3 ligne 3
     * @param ligne4 ligne 4
     */
    public Evenement(String id, String idPorte, String date, String type, String ligne1, String ligne2,
            String ligne3, String ligne4) {
        this.id = id;
        this.idPorte = idPorte;
        this.date_event = date;
        this.lcd_ligne1 = ligne1;
        this.lcd_ligne2 = ligne2;
        this.lcd_ligne3 = ligne3;
        this.lcd_ligne4 = ligne4;
        this.type = type;
    }

    public String getIdPorte() {
        return this.idPorte;
    }

    public String getId() {
        return this.id;
    }

    public String getDate() {
        return this.date_event;
    }

    public String getLigne1() {
        return this.lcd_ligne1;
    }

    public String getLigne2() {
        return this.lcd_ligne2;
    }

    public String getLigne3() {
        return this.lcd_ligne3;
    }

    public String getLigne4() {
        return this.lcd_ligne4;
    }

    public String getType() {
        return this.type;
    }
}
