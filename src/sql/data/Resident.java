package sql.data;

import java.net.*;
import java.util.*;

public class Resident {

    private String id;
    private String droit;
    private String code;
    private String name;
    private String telephone1;
    private String renvoiAppel;
    private String telephone2;
    private String listeRouge;
    private String identifiant;
    private String technoAppel;
    private String validationCle;
    private String cle1;
    private String cle2;
    private String cle3;
    private String cle4;
    private String cle5;
    private String cle6;
    private String cle7;
    private String cle8;
    private String cle9;
    private String cle10;
    private String clem1;
    private String clem2;
    private String clem3;
    private String clem4;
    private String clem5;
    private String clem6;
    private String clem7;
    private String clem8;
    private String clem9;
    private String clem10;
    private String identitel;
    private String appelDirect;
    private String tempRenvoi;
    private String tempCle;
    private String professionLiberale;
    private String isRefresh;
    private String etat;
    private String sipPhone1;
    private String sipPhone2;
    private String renvoiSip;
    private String renvoiTel;

    private String codeRenvoi;
    private String idBluetooth1;
    private String idBluetooth2;
    private String idBluetooth3;
    private String bip1;
    private String bip2;
    private String bip3;
    private String tempRadio;
    private String tempIdentitooth;
    private String tempIdentitelSIP;
    private String tempProfessionLiberale;
    private String tempListeRouge;
    private String idVoip;
    private String badge_temp;
    private String sip;
    private String profil;

    /**
     * constructeur de classe
     */
    public Resident() {
    }

    /**
     * constructeur de classe
     *
     * @param id identifiant
     * @param code code
     * @param name nom
     * @param telephone1 telephone 1
     * @param telephone2 telephone 2
     * @param renvoiAppel renvoi appel
     * @param etat etat
     */
    public Resident(String id,
            String code,
            String name,
            String telephone1,
            String telephone2,
            String renvoiAppel,
            String etat) {

        this.id = id;
        this.code = code;
        this.name = name;
        this.telephone1 = telephone1;
        this.telephone2 = telephone2;
        this.renvoiAppel = renvoiAppel;
        this.etat = etat;
        String ipAdresse;
        Enumeration e = null;

        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException ex) {
            System.err.print(ex.getMessage());
        }
        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                ipAdresse = i.getHostAddress();
                if (ipAdresse.startsWith("127")) {
                    continue;
                }
                if (ipAdresse.startsWith("192.168")) {
                    sip = telephone1 + "@" + ipAdresse;
                }
            }
        }
    }

    public Resident(String id,
            String droit,
            String code,
            String name,
            String telephone1,
            String renvoiAppel,
            String telephone2,
            String listeRouge,
            String identifiant,
            String technoAppel,
            String validationCle,
            String cle1,
            String clem1,
            String cle2,
            String clem2,
            String cle3,
            String clem3,
            String cle4,
            String clem4,
            String cle5,
            String clem5,
            String cle6,
            String clem6,
            String cle7,
            String clem7,
            String cle8,
            String clem8,
            String cle9,
            String clem9,
            String cle10,
            String clem10,
            String identitel,
            String appelDirect,
            String tempRenvoi,
            String tempCle,
            String professionLiberale,
            String isRefresh,
            String etat,
            String sipPhone1,
            String sipPhone2,
            String renvoiSip,
            String renvoiTel,
            String idVoip,
            String codeRenvoi,
            String idBluetooth1,
            String idBluetooth2,
            String idBluetooth3,
            String bip1,
            String bip2,
            String bip3,
            String tempRadio,
            String tempIdentitooth,
            String tempIdentitelSIP,
            String tempProfessionLiberale,
            String tempListeRouge,
            String profil) {

        this.id = id;
        this.droit = droit;
        this.code = code;
        this.name = name;
        this.telephone1 = telephone1;
        this.renvoiAppel = renvoiAppel;
        this.telephone2 = telephone2;
        this.listeRouge = listeRouge;
        this.identifiant = identifiant;
        this.technoAppel = technoAppel;
        this.validationCle = validationCle;
        this.cle1 = cle1;
        this.clem1 = clem1;
        this.cle2 = cle2;
        this.clem2 = clem2;
        this.cle3 = cle3;
        this.clem3 = clem3;
        this.cle4 = cle4;
        this.clem4 = clem4;
        this.cle5 = cle5;
        this.clem5 = clem5;
        this.cle6 = cle6;
        this.clem6 = clem6;
        this.cle7 = cle7;
        this.clem7 = clem7;
        this.cle8 = cle8;
        this.clem8 = clem8;
        this.cle9 = cle9;
        this.clem9 = clem9;
        this.cle10 = cle10;
        this.clem10 = clem10;
        this.identitel = identitel;
        this.appelDirect = appelDirect;
        this.tempRenvoi = tempRenvoi;
        this.tempCle = tempCle;
        this.professionLiberale = professionLiberale;
        this.isRefresh = isRefresh;
        this.etat = etat;
        this.sipPhone1 = sipPhone1;
        this.sipPhone2 = sipPhone2;
        this.renvoiSip = renvoiSip;
        this.renvoiTel = renvoiTel;
        this.idVoip = idVoip;
        this.codeRenvoi = codeRenvoi;
        this.idBluetooth1 = idBluetooth1;
        this.idBluetooth2 = idBluetooth2;
        this.idBluetooth3 = idBluetooth3;
        this.bip1 = bip1;
        this.bip2 = bip2;
        this.bip3 = bip3;
        this.tempRadio = tempRadio;
        this.tempIdentitooth = tempIdentitooth;
        this.tempIdentitelSIP = tempIdentitelSIP;
        this.tempProfessionLiberale = tempProfessionLiberale;
        this.tempListeRouge = tempListeRouge;
        this.profil = profil;
        String ipAdresse;
        Enumeration e = null;

        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException ex) {
            System.err.print(ex.getMessage());
        }
        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                ipAdresse = i.getHostAddress();
                if (ipAdresse.startsWith("127")) {
                    continue;
                }
                if (ipAdresse.startsWith("192.168")) {
                    sip = telephone1 + "@" + ipAdresse;
                }
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDroit() {
        return droit;
    }

    public void setDroit(String droit) {
        this.droit = droit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone1() {

        return telephone1.replace("+", "00");
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getRenvoiAppel() {
        return renvoiAppel;
    }

    public void setRenvoiAppel(String renvoiAppel) {
        this.renvoiAppel = renvoiAppel;
    }

    public String getTelephone2() {
        return telephone2.replace("+", "00");
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getListeRouge() {
        return listeRouge;
    }

    public void setListeRouge(String listeRouge) {
        this.listeRouge = listeRouge;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getTechnoAppel() {
        return technoAppel;
    }

    public void setTechnoAppel(String technoAppel) {
        this.technoAppel = technoAppel;
    }

    public String getValidationCle() {
        return validationCle;
    }

    public void setValidationCle(String validationCle) {
        this.validationCle = validationCle;
    }

    public String getCle1() {
        return cle1;
    }

    public void setCle1(String cle1) {
        this.cle1 = cle1;
    }

    public String getCle2() {
        return cle2;
    }

    public void setCle2(String cle2) {
        this.cle2 = cle2;
    }

    public String getCle3() {
        return cle3;
    }

    public void setCle3(String cle3) {
        this.cle3 = cle3;
    }

    public String getCle4() {
        return cle4;
    }

    public void setCle4(String cle4) {
        this.cle4 = cle4;
    }

    public String getCle5() {
        return cle5;
    }

    public void setCle5(String cle5) {
        this.cle5 = cle5;
    }

    public String getCle6() {
        return cle6;
    }

    public void setCle6(String cle6) {
        this.cle6 = cle6;
    }

    public String getCle7() {
        return cle7;
    }

    public void setCle7(String cle7) {
        this.cle7 = cle7;
    }

    public String getCle8() {
        return cle8;
    }

    public void setCle8(String cle8) {
        this.cle8 = cle8;
    }

    public String getCle9() {
        return cle9;
    }

    public void setCle9(String cle9) {
        this.cle9 = cle9;
    }

    public String getCle10() {
        return cle10;
    }

    public void setCle10(String cle10) {
        this.cle10 = cle10;
    }

    public String getClem1() {
        return clem1;
    }

    public void setClem1(String clem1) {
        this.clem1 = clem1;
    }

    public String getClem2() {
        return clem2;
    }

    public void setClem2(String clem2) {
        this.clem2 = clem2;
    }

    public String getClem3() {
        return clem3;
    }

    public void setClem3(String clem3) {
        this.clem3 = clem3;
    }

    public String getClem4() {
        return clem4;
    }

    public void setClem4(String clem4) {
        this.clem4 = clem4;
    }

    public String getClem5() {
        return clem5;
    }

    public void setClem5(String clem5) {
        this.clem5 = clem5;
    }

    public String getClem6() {
        return clem6;
    }

    public void setClem6(String clem6) {
        this.clem6 = clem6;
    }

    public String getClem7() {
        return clem7;
    }

    public void setClem7(String clem7) {
        this.clem7 = clem7;
    }

    public String getClem8() {
        return clem8;
    }

    public void setClem8(String clem8) {
        this.clem8 = clem8;
    }

    public String getClem9() {
        return clem9;
    }

    public void setClem9(String clem9) {
        this.clem9 = clem9;
    }

    public String getClem10() {
        return clem10;
    }

    public void setClem10(String clem10) {
        this.clem10 = clem10;
    }

    public String getIdentitel() {
        return identitel;
    }

    public void setIdentitel(String identitel) {
        this.identitel = identitel;
    }

    public String getAppelDirect() {
        return appelDirect;
    }

    public void setAppelDirect(String appelDirect) {
        this.appelDirect = appelDirect;
    }

    public String getTempRenvoi() {
        return tempRenvoi;
    }

    public void setTempRenvoi(String temp) {
        this.tempRenvoi = temp;
    }

    public String getTempCle() {
        return tempCle;
    }

    public void setTempCle(String tempCle) {
        this.tempCle = tempCle;
    }

    public String getProfessionLiberale() {
        return professionLiberale;
    }

    public void setProfessionLiberale(String professionLiberale) {
        this.professionLiberale = professionLiberale;
    }

    public String getIsRefresh() {
        return isRefresh;
    }

    public void setIsRefresh(String isRefresh) {
        this.isRefresh = isRefresh;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getSipPhoneUn() {
        return sipPhone1;
    }

    public void setSipPhoneUn(String sipPhone1) {
        this.sipPhone1 = sipPhone1;
    }

    public String getSipPhoneDeux() {
        return sipPhone2;
    }

    public void setSipPhoneDeux(String sipPhone2) {
        this.sipPhone2 = sipPhone2;
    }

    public String getRenvoiSip() {
        return renvoiSip;
    }

    public void setRenvoiSip(String renvoiSip) {
        this.renvoiSip = renvoiSip;
    }

    public String getRenvoiTel() {
        return renvoiTel;
    }

    public void setRenvoiTel(String renvoiTel) {
        this.renvoiTel = renvoiTel;
    }

    public String getIdVoip() {
        return idVoip;
    }

    public void setIdVoip(String idVoip) {
        this.idVoip = idVoip;
    }

    public String getCodeRenvoi() {
        return codeRenvoi;
    }

    public void setCodeRenvoi(String code) {
        this.codeRenvoi = code;
    }

    public String getId_bluetooth1() {
        return idBluetooth1;
    }

    public void setId_bluetooth1(String idBluetooth1) {
        this.idBluetooth1 = idBluetooth1;
    }

    public String getId_bluetooth2() {
        return idBluetooth2;
    }

    public void setId_bluetooth2(String idBluetooth2) {
        this.idBluetooth2 = idBluetooth2;
    }

    public String getId_bluetooth3() {
        return idBluetooth3;
    }

    public void setId_bluetooth3(String idBluetooth3) {
        this.idBluetooth3 = idBluetooth3;
    }

    public String getBip1() {
        return bip1;
    }

    public void setBip1(String bip1) {
        this.bip1 = bip1;
    }

    public String getBip2() {
        return bip2;
    }

    public void setBip2(String bip2) {
        this.bip2 = bip2;
    }

    public String getBip3() {
        return bip3;
    }

    public void setBip3(String bip3) {
        this.bip3 = bip3;
    }

    public String getTempRadio() {
        return tempRadio;
    }

    public void setTempRadio(String tempRadio) {
        this.tempRadio = tempRadio;
    }

    public String getTempIdentitooth() {
        return tempIdentitooth;
    }

    public void setTempIdentitooth(String tempIdentitooth) {
        this.tempIdentitooth = tempIdentitooth;
    }

    public String getTempIdentitelSIP() {
        return tempIdentitelSIP;
    }

    public void setTempIdentitelSIP(String tempIdentitelSIP) {
        this.tempIdentitelSIP = tempIdentitelSIP;
    }

    public String getTempProfessionLiberale() {
        return tempProfessionLiberale;
    }

    public void setTempProfessionLiberale(String tempProfessionLiberale) {
        this.tempProfessionLiberale = tempProfessionLiberale;
    }

    public String getTempListeRouge() {
        return tempListeRouge;
    }

    public void setTempListeRouge(String tempListeRouge) {
        this.tempListeRouge = tempListeRouge;
    }

    public void setBadgeTemporaire(String value) {
        this.badge_temp = value;
    }

    public String getBadgeTemporaire() {
        return badge_temp;
    }

    public String getSipAdress() {
        return sip;
    }

    /**
     * @return the profil
     */
    public String getProfil() {
        return profil;
    }

    /**
     * @param profil the profil to set
     */
    public void setProfil(String profil) {
        this.profil = profil;
    }
}
