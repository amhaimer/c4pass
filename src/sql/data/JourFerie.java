package sql.data;

/**
 * classe jour ferier lié à la table jourferie
 *
 * @author Abdellah
 */
public class JourFerie {

    private String id;
    private String idResidence;
    private String jourDebut;
    private String jourFin;

    private String etat;

    /**
     * constructeur de classe
     *
     * @param id identifiant
     * @param idResidence id residence
     * @param jourDebut jourdebut
     * @param jourFin jourfin
     * @param etat etat
     */
    public JourFerie(String id, String idResidence, String jourDebut, String jourFin, String etat) {
        this.id = id;
        this.idResidence = idResidence;
        this.jourDebut = jourDebut;
        this.jourFin = jourFin;
        this.etat = etat;

    }

    /**
     * constructeur de classe
     */
    public JourFerie() {

    }

    /**
     * getter pour l'identifiant
     *
     * @return identifiant
     */
    public String getId() {
        return id;
    }

    /**
     * setter pour l'identifiant
     *
     * @param id identifiant
     */
    public void setId(String id) {
        this.id = id;
    }

    /*
*getter pour l'id profil
     */
    public String getIdResidence() {
        return idResidence;
    }

    /**
     * setter pour l'id profil
     *
     * @param idResidence idresidence
     */
    public void setIdResidence(String idResidence) {
        this.idResidence = idResidence;
    }

    /**
     * getter pour jour debut
     *
     * @return jourdebut
     */
    public String getJourDebut() {
        return jourDebut;
    }

    /**
     * setter pour jour debut
     *
     * @param jourDebut jourdebut
     */
    public void setJourDebut(String jourDebut) {
        this.jourDebut = jourDebut;
    }

    /**
     * getter pour jour fin
     *
     * @return jourfin
     */
    public String getJourFin() {
        return jourFin;
    }

    /**
     * setter pour jour fin
     *
     * @param jourFin jourifn
     */
    public void setJourFin(String jourFin) {
        this.jourFin = jourFin;
    }

    /**
     * getter pour l'etat
     *
     * @return etat
     */
    public String getEtat() {
        return etat;
    }

    /*
*setter pour l'etat
     */
    public void setEtat(String etat) {
        this.etat = etat;
    }

}
