package sql.data;

/**
 * classe profil temporel table profil temporel
 *
 * @author Abdellah
 */
public class ProfilTemporel {

    private String id;
    private String idProfil;
    private String heureDebut;
    private String heureFin;
    private String jour;
    private String etat;

    /**
     * constructeur de classe
     */
    public ProfilTemporel() {
    }

    /**
     * constructeur de classe
     *
     * @param id identifiant
     * @param idProfil id profil
     * @param heureDebut heure debut
     * @param heureFin heure fin
     * @param jour jour
     * @param etat etat
     */
    public ProfilTemporel(String id, String idProfil, String heureDebut, String heureFin, String jour, String etat) {
        this.id = id;
        this.idProfil = idProfil;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.jour = jour;
        this.etat = etat;

    }

//getter pour l'identifiant
    public String getId() {
        return id;
    }
//setter pour l'identifiant

    public void setId(String id) {
        this.id = id;
    }

//getter pour l'id profil
    public String getIdProfil() {
        return idProfil;
    }
//setter pour l'id profil

    public void setIdProfil(String id) {
        this.idProfil = id;
    }

//getter pour Heure debut
    public String getHeureDebut() {
        return heureDebut;
    }
//setter pour Heure debut

    public void setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
    }

//getter pour Heure fin
    public String getHeureFin() {
        return heureFin;
    }
//setter pour Heure fin

    public void setHeureFin(String V) {
        this.heureFin = V;
    }

//getter pour jour de la semaine
    public String getJour() {
        return jour;
    }
//setter pour jour de la semaine

    public void setJour(String jour) {
        this.jour = jour;
    }

//getter pour l'etat
    public String getEtat() {
        return etat;
    }
//setter pour l'etat

    public void setEtat(String etat) {
        this.etat = etat;
    }

}
