package sql.data;

/**
 * classe profil table profil
 *
 * @author Abdellah
 */
public class Profil {

    private String id;
    private String idResidence;
    private String jourDebut;
    private String jourFin;
    private String nom;
    private String etat;

    /**
     * constructeur de classe
     */
    public Profil() {
    }

    /**
     * constructeur de classe
     *
     * @param id identifiant
     * @param idResidence id residence
     * @param jourDebut jour debut
     * @param jourFin jour fin
     * @param nom nom
     * @param etat etat
     */
    public Profil(String id, String idResidence, String jourDebut, String jourFin, String nom, String etat) {
        this.id = id;
        this.idResidence = idResidence;
        this.jourDebut = jourDebut;
        this.jourFin = jourFin;
        this.nom = nom;
        this.etat = etat;

    }

//getter pour l'identifiant
    public String getId() {
        return id;
    }
//setter pour l'identifiant

    public void setId(String id) {
        this.id = id;
    }

//getter pour l'id 
    public String getIdResidence() {
        return idResidence;
    }
//setter pour 

    public void setIdResidence(String idResidence) {
        this.idResidence = idResidence;
    }

    //getter pour jour debut 
    public String getJourDebut() {
        return jourDebut;
    }
//setter pour jour debut 

    public void setJourDebut(String jourDebut) {
        this.jourDebut = jourDebut;
    }

    //getter pour jour fin 
    public String getJourFin() {
        return jourFin;
    }
//setter pour jour fin 

    public void setJourFin(String jourFin) {
        this.jourFin = jourFin;
    }

//getter pour jour de la semaine
    public String getNom() {
        return nom;
    }
//setter pour jour de la semaine

    public void setNom(String nom) {
        this.nom = nom;
    }

//getter pour l'etat
    public String getEtat() {
        return etat;
    }
//setter pour l'etat

    public void setEtat(String etat) {
        this.etat = etat;
    }

}
