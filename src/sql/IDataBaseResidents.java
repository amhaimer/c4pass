package sql;

import sql.data.Resident;
import java.util.List;

/**
 * interface database resifdetns
 *
 * @author Abdellah
 */
interface IDataBaseResidents {

    /**
     * renvoi la liste des residents
     *
     * @return liste residens
     */
    List<Resident> getResidents();

    /**
     * modifier un résident
     *
     * @param resident objet resident
     */
    void update(Resident resident);

    /**
     * supprimer un résident
     *
     * @param resident objet résident
     */
    void delete(Resident resident);
}
