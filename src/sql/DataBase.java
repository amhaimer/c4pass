package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import sql.data.Porte;
import sql.data.Resident;
import sql.data.Evenement;
import sql.data.ProfilTemporel;
import sql.data.Profil;
import sql.data.JourFerie;
import event.Event;

import java.util.logging.Level;
import java.util.logging.Logger;
import utils.Constante;

/**
 * classe qui fait la connexion avec la base de données
 *
 * @author Abdellah
 */
public class DataBase implements IDataBaseResidents, IDataBaseEvent, IDataBasePorte {

    static private Connection conn = null;
    private Porte porte1 = null;
    private Statement st = null;
    private ResultSet rs = null;
    static boolean b = true;
    static DataBase INSTANCE;

    /**
     * s'assurer d'avoir une seule instance de la classe (singleton)
     *
     * @return objet de la classe
     */
    public static DataBase getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataBase();
        }
        return INSTANCE;
    }

    /**
     * cree la connexion
     */
    public void Conn() {
        try {

            if (b) {
                // DriverManager.setLoginTimeout(10);
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + Constante.NOM_BD + "?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT&connectTimeout=2000&socketTimeout=4000", "java", Constante.PWD_BD);

                b = false;
            }
            if (conn.isClosed() || conn == null) {
                System.out.println("opening the connection");
                //   DriverManager.setLoginTimeout(10);
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + Constante.NOM_BD + "?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT&connectTimeout=2000&socketTimeout=4000", "java", Constante.PWD_BD);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * fermer la connexion
     */
    public void closeconn() {
        try {
            conn.close();
            //conn=null;
        } catch (SQLException e) {
            System.err.print(e.getMessage());
        }
    }

    /**
     * nombre de residetns sur la platine
     *
     * @return entier
     */
    public String getNbResidents() {

        Porte porte;
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select count(*) as nb from residents");
            String nb = "";
            while (rs.next()) {
                nb = rs.getString("nb");
            }
            porte = new Porte("4444");
            porte.setNbResidents(nb);
            return nb;
        } catch (SQLException ex) {
            System.out.println("1");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return "0";
    }

    /**
     * nombre de residents liste blanche
     *
     * @return entier
     */
    public String getNbResidentsListeBlanche() {
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select count(*) as nb from residents where list_rouge='0'");
            String nb = "";
            while (rs.next()) {
                nb = rs.getString("nb");
            }
            return nb;
        } catch (SQLException ex) {
            System.err.print(ex.getMessage());
        }
        return "0";
    }

    /**
     * nombre de residents liste rouge
     *
     * @return entier
     */
    public String getNbResidentsListeRouge() {
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select count(*) as nb from residents where list_rouge!='0'");
            String nb = "";
            while (rs.next()) {
                nb = rs.getString("nb");
            }
            return nb;
        } catch (SQLException ex) {
            System.err.print(ex.getMessage());
        }
        return "0";
    }

    /**
     * renvoi le nombre de badges
     *
     * @return entier
     */
    public int getNbBadge() {
        try {
            Conn();
            st = conn.createStatement();
            int nb = 0;
            for (int i = 1; i < 10; i++) {
                rs = st.executeQuery("select count(*) as nb from residents where clem"
                        + i
                        + " is not null and clem"
                        + i
                        + "!='' and clem"
                        + i
                        + "!='null'");
                while (rs.next()) {
                    nb += rs.getInt("nb");
                }
            }
            return nb;
        } catch (SQLException ex) {
            System.err.print(ex.getMessage());
        }
        return 0;
    }

    /**
     * renvoi le résidents
     *
     * @param icode le code du résident
     * @return l'objet resident
     */
    public Resident getResident(String icode) {

        Resident resident = new Resident();
        try {
            Conn();

            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * FROM residents where code='" + icode + "' limit 1");

            String id;
            String droit;
            String code;
            String name;
            String telephone1;
            String renvoiAppel;
            String telephone2;
            String listRouge;
            String identifiant;
            String techno_appel;
            String validationCle;

            String cle1;
            String cle2;
            String cle3;
            String cle4;
            String cle5;
            String cle6;
            String cle7;
            String cle8;
            String cle9;
            String cle10;
            String clem1;
            String clem2;
            String clem3;
            String clem4;
            String clem5;
            String clem6;
            String clem7;
            String clem8;
            String clem9;
            String clem10;
            String identitel;
            String appelDirect;
            String tempRenvoiAppel;
            String tempCle;
            String professionLiberale;
            String etat;
            String sipphone1;
            String sipphone2;
            String renvoi_sip;
            String renvoi_tel;

            String idVoip;
            String codeRenvoi;
            String bip1;
            String bip2;
            String bip3;
            String id_bluetooth1;
            String id_bluetooth2;
            String id_bluetooth3;
            String tempRadio;
            String tempIdentitooth;
            String tempIdentitelSIP;
            String tempProfessionLiberale;
            String tempListeRouge;
            String profil;
            if (rs.next()) {
                id = rs.getString("id");
                droit = rs.getString("droit");
                code = rs.getString("code");
                name = rs.getString("nom");
                telephone1 = rs.getString("telephone1");
                renvoiAppel = rs.getString("renvoi_appel");
                telephone2 = rs.getString("telephone2");
                listRouge = rs.getString("list_rouge");
                identifiant = rs.getString("Identifiant");
                techno_appel = rs.getString("techno_appel");
                validationCle = rs.getString("validation_cle");
                cle1 = rs.getString("cle1");
                cle2 = rs.getString("cle2");
                cle3 = rs.getString("cle3");
                cle4 = rs.getString("cle4");
                cle5 = rs.getString("cle5");
                cle6 = rs.getString("cle6");
                cle7 = rs.getString("cle7");
                cle8 = rs.getString("cle8");
                cle9 = rs.getString("cle9");
                cle10 = rs.getString("cle10");
                clem1 = rs.getString("clem1");
                clem2 = rs.getString("clem2");
                clem3 = rs.getString("clem3");
                clem4 = rs.getString("clem4");
                clem5 = rs.getString("clem5");
                clem6 = rs.getString("clem6");
                clem7 = rs.getString("clem7");
                clem8 = rs.getString("clem8");
                clem9 = rs.getString("clem9");
                clem10 = rs.getString("clem10");
                identitel = rs.getString("identitel");
                appelDirect = rs.getString("temp_appeldirect");
                tempRenvoiAppel = rs.getString("temp_renvoi_appel");
                tempCle = rs.getString("temp_cle");
                professionLiberale = rs.getString("profession_liberale");
                etat = rs.getString("etat");
                sipphone1 = rs.getString("sipphone1");
                sipphone2 = rs.getString("sipphone2");
                renvoi_sip = rs.getString("tempo_renvoi_sipphone");
                renvoi_tel = rs.getString("tempo_renvoi_telephone");

                idVoip = rs.getString("id_voip");
                codeRenvoi = rs.getString("code_renvoi");
                id_bluetooth1 = rs.getString("id_bluetooth1");
                id_bluetooth2 = rs.getString("id_bluetooth2");
                id_bluetooth3 = rs.getString("id_bluetooth3");
                bip1 = rs.getString("bip1");
                bip2 = rs.getString("bip2");
                bip3 = rs.getString("bip3");
                tempRadio = rs.getString("temp_radio");
                tempIdentitooth = rs.getString("temp_identitooth");
                tempIdentitelSIP = rs.getString("temp_identitelSIP");
                tempProfessionLiberale = rs.getString("temp_profession_liberale");
                tempListeRouge = rs.getString("temp_liste_rouge");
                profil = rs.getString("profil");

                resident.setId(id);
                resident.setDroit(droit);
                resident.setCode(code);
                resident.setName(name);
                resident.setTelephone1(telephone1);
                resident.setRenvoiAppel(renvoiAppel);
                resident.setTelephone2(telephone2);
                resident.setListeRouge(listRouge);
                resident.setIdentifiant(identifiant);
                resident.setTechnoAppel(techno_appel);
                resident.setValidationCle(validationCle);
                resident.setCle1(cle1);
                resident.setCle2(cle2);
                resident.setCle3(cle3);
                resident.setCle4(cle4);
                resident.setCle5(cle5);
                resident.setCle6(cle6);
                resident.setCle7(cle7);
                resident.setCle8(cle8);
                resident.setCle9(cle9);
                resident.setCle10(cle10);
                resident.setClem1(clem1);
                resident.setClem2(clem2);
                resident.setClem3(clem3);
                resident.setClem4(clem4);
                resident.setClem5(clem5);
                resident.setClem6(clem6);
                resident.setClem7(clem7);
                resident.setClem8(clem8);
                resident.setClem9(clem9);
                resident.setClem10(clem10);
                resident.setIdentitel(identitel);
                resident.setAppelDirect(appelDirect);
                resident.setTempRenvoi(tempRenvoiAppel);
                resident.setTempCle(tempCle);
                resident.setProfessionLiberale(professionLiberale);
                resident.setEtat(etat);
                resident.setSipPhoneUn(sipphone1);
                resident.setSipPhoneDeux(sipphone2);
                resident.setRenvoiSip(renvoi_sip);
                resident.setRenvoiTel(renvoi_tel);

                resident.setIdVoip(idVoip);
                resident.setCodeRenvoi(codeRenvoi);
                resident.setId_bluetooth1(id_bluetooth1);
                resident.setId_bluetooth2(id_bluetooth2);
                resident.setId_bluetooth3(id_bluetooth3);
                resident.setBip1(bip1);
                resident.setBip2(bip2);
                resident.setBip3(bip3);
                resident.setTempRadio(tempRadio);
                resident.setTempIdentitooth(tempIdentitooth);
                resident.setTempIdentitelSIP(tempIdentitelSIP);
                resident.setTempProfessionLiberale(tempProfessionLiberale);
                resident.setTempListeRouge(tempListeRouge);
                resident.setProfil(profil);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("2");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return resident;
    }

    /**
     * supprimer un resident
     *
     * @param resident l'objet resident
     */
    @Override
    public void delete(Resident resident) {

        try {

            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * FROM residents where code='" + resident.getCode() + "' limit 1");

            if (rs.next()) {

                st.executeUpdate("delete from residents where code =" + resident.getCode());
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("4");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * rajouter un resident
     *
     * @param code son code
     * @return renvoi l'objet résident qui a ete crée
     */
    public Resident insert(String code) {

        Resident resident = new Resident();
        try {
            Conn();

            // Do something with the Connection
            st = conn.createStatement();
            st.executeUpdate("insert into residents (code,etat) values('" + code + "',0)");
            resident.setCode(code);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("3");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return resident;
    }

    /**
     * modifier les donnes d'un resident
     *
     * @param resident résident
     */
    @Override
    public void update(Resident resident) {

        System.out.println("MAJ RESIDENTS");
        try {
            Conn();

            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT etat FROM residents where code='" + resident.getCode() + "' limit 1");

            String etat = "";
            if (rs.next()) {
                etat = rs.getString("etat");
            }
            if (etat.equals("1")) {
                etat = "-3";
            }
            st.executeUpdate("update residents set "
                    + "droit='" + resident.getDroit()
                    + "',nom='" + resident.getName()
                    + "',telephone1='" + resident.getTelephone1()
                    + "',renvoi_appel='" + resident.getRenvoiAppel()
                    + "',telephone2='" + resident.getTelephone2()
                    + "',list_rouge='" + resident.getListeRouge()
                    + "',Identifiant='" + resident.getIdentifiant()
                    + "',techno_appel='" + resident.getTechnoAppel()
                    + "',validation_cle='" + resident.getValidationCle()
                    + "',cle1='" + resident.getCle1()
                    + "',cle2='" + resident.getCle2()
                    + "',cle3='" + resident.getCle3()
                    + "',cle4='" + resident.getCle4()
                    + "',cle5='" + resident.getCle5()
                    + "',cle6='" + resident.getCle6()
                    + "',cle7='" + resident.getCle7()
                    + "',cle8='" + resident.getCle8()
                    + "',cle9='" + resident.getCle9()
                    + "',cle10='" + resident.getCle10()
                    + "',clem1='" + resident.getClem1()
                    + "',clem2='" + resident.getClem2()
                    + "',clem3='" + resident.getClem3()
                    + "',cle4='" + resident.getClem4()
                    + "',clem5='" + resident.getClem5()
                    + "',clem6='" + resident.getClem6()
                    + "',clem7='" + resident.getClem7()
                    + "',clem8='" + resident.getClem8()
                    + "',clem9='" + resident.getClem9()
                    + "',clem10='" + resident.getClem10()
                    + "',identitel='" + resident.getIdentitel()
                    + "',temp_appeldirect='" + resident.getAppelDirect()
                    + "',temp_renvoi_appel='" + resident.getTempRenvoi()
                    + "',temp_cle='" + resident.getTempCle()
                    + "',profession_liberale='" + resident.getProfessionLiberale()
                    + "',etat='" + etat
                    + "',sipphone1='" + resident.getSipPhoneUn()
                    + "',sipphone2='" + resident.getSipPhoneDeux()
                    + "',tempo_renvoi_sipphone='" + resident.getRenvoiSip()
                    + "',tempo_renvoi_telephone='" + resident.getRenvoiTel()
                    + "',id_voip='" + resident.getIdVoip()
                    + "',code_renvoi='" + resident.getCodeRenvoi()
                    + "',id_bluetooth1='" + resident.getId_bluetooth1()
                    + "',id_bluetooth2='" + resident.getId_bluetooth2()
                    + "',id_bluetooth3='" + resident.getId_bluetooth3()
                    + "',bip1='" + resident.getBip1()
                    + "',bip2='" + resident.getBip2()
                    + "',bip3='" + resident.getBip3()
                    + "',temp_radio='" + resident.getTempRadio()
                    + "',temp_identitooth='" + resident.getTempIdentitooth()
                    + "',temp_identitelSIP='" + resident.getTempIdentitelSIP()
                    + "',temp_profession_liberale='" + resident.getTempProfessionLiberale()
                    + "',temp_liste_rouge='" + resident.getTempListeRouge()
                    + "',profil='" + resident.getProfil()
                    + "' where code=" + resident.getCode());
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("4");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier l'id d'un utilisateur
     *
     * @param ancienId ancien id
     * @param nouveauId nv id
     */
    public void updateId(String ancienId, String nouveauId) {

        try {
            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            st.executeUpdate("update residents set id='" + nouveauId + "' where id=" + ancienId);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("5");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * inserer un résident
     *
     * @param resident l'objet résident
     */
    public void insert(Resident resident) {

        try {
            Conn();
            // Do something with the Connection	
            st = conn.createStatement();
            st.executeUpdate("insert into residents("
                    + "id,"
                    + "droit,"
                    + "code,"
                    + "nom,"
                    + "telephone1,"
                    + "renvoi_appel,"
                    + "telephone2,"
                    + "list_rouge,"
                    + "Identifiant,"
                    + "techno_appel,"
                    + "validation_cle,"
                    + "cle1,"
                    + "cle2,"
                    + "cle3,"
                    + "cle4,"
                    + "cle5,"
                    + "cle6,"
                    + "cle7,"
                    + "cle8,"
                    + "cle9,"
                    + "cle10,"
                    + "clem1,"
                    + "clem2,"
                    + "clem3,"
                    + "clem4,"
                    + "clem5,"
                    + "clem6,"
                    + "clem7,"
                    + "clem8,"
                    + "clem9,"
                    + "clem10,"
                    + "identitel,"
                    + "temp_appeldirect,"
                    + "temp_renvoi_appel,"
                    + "temp_cle,"
                    + "profession_liberale,"
                    + "etat,"
                    + "sipphone1,"
                    + "sipphone2,"
                    + "tempo_renvoi_sipphone,"
                    + "tempo_renvoi_telephone,"
                    + "code_renvoi,"
                    + "id_bluetooth1,"
                    + "id_bluetooth2,"
                    + "id_bluetooth3,"
                    + "bip1,"
                    + "bip2,"
                    + "bip3,"
                    + "temp_radio,"
                    + "temp_identitooth,"
                    + "temp_identitelSIP,"
                    + "temp_profession_liberale,"
                    + "profil,"
                    + "temp_liste_rouge"
                    + ")values ('"
                    + resident.getId() + "','"
                    + resident.getDroit() + "','"
                    + resident.getCode() + "','"
                    + resident.getName() + "','"
                    + resident.getTelephone1() + "','"
                    + resident.getRenvoiAppel() + "','"
                    + resident.getTelephone2() + "','"
                    + resident.getListeRouge() + "','"
                    + resident.getIdentifiant() + "','"
                    + resident.getTechnoAppel() + "','"
                    + resident.getValidationCle() + "','"
                    + resident.getCle1() + "','"
                    + resident.getCle2() + "','"
                    + resident.getCle3() + "','"
                    + resident.getCle4() + "','"
                    + resident.getCle5() + "','"
                    + resident.getCle6() + "','"
                    + resident.getCle7() + "','"
                    + resident.getCle8() + "','"
                    + resident.getCle9() + "','"
                    + resident.getCle10() + "','"
                    + resident.getClem1() + "','"
                    + resident.getClem2() + "','"
                    + resident.getClem3() + "','"
                    + resident.getClem4() + "','"
                    + resident.getClem5() + "','"
                    + resident.getClem6() + "','"
                    + resident.getClem7() + "','"
                    + resident.getClem8() + "','"
                    + resident.getClem9() + "','"
                    + resident.getClem10() + "','"
                    + resident.getIdentitel() + "','"
                    + resident.getAppelDirect() + "','"
                    + resident.getRenvoiAppel() + "','"
                    + resident.getTempCle() + "','"
                    + resident.getProfessionLiberale() + "','1','"
                    + resident.getSipPhoneUn() + "','"
                    + resident.getSipPhoneDeux() + "','"
                    + resident.getRenvoiSip() + "','"
                    + resident.getRenvoiTel() + "','"
                    + resident.getIdVoip() + "','"
                    + resident.getCodeRenvoi() + "','"
                    + resident.getId_bluetooth1() + "','"
                    + resident.getId_bluetooth2() + "','"
                    + resident.getId_bluetooth3() + "','"
                    + resident.getBip1() + "','"
                    + resident.getBip2() + "','"
                    + resident.getBip3() + "','"
                    + resident.getTempRadio() + "','"
                    + resident.getTempIdentitooth() + "','"
                    + resident.getTempIdentitelSIP() + "','"
                    + resident.getTempProfessionLiberale() + "','"
                    + resident.getProfil() + "','"
                    + resident.getTempListeRouge()
                    + "') on duplicate KEY UPDATE "
                    + "droit='" + resident.getDroit()
                    + "',code='" + resident.getCode()
                    + "',nom='" + resident.getName()
                    + "',telephone1='" + resident.getTelephone1()
                    + "',renvoi_appel='" + resident.getRenvoiAppel()
                    + "',telephone2='" + resident.getTelephone2()
                    + "',list_rouge='" + resident.getListeRouge()
                    + "',Identifiant='" + resident.getIdentifiant()
                    + "',techno_appel='" + resident.getTechnoAppel() + "',validation_cle='" + resident.getValidationCle()
                    + "',cle1='" + resident.getCle1()
                    + "',cle2='" + resident.getCle2()
                    + "',cle3='" + resident.getCle3()
                    + "',cle4='" + resident.getCle4()
                    + "',cle5='" + resident.getCle5()
                    + "',cle6='" + resident.getCle6()
                    + "',cle7='" + resident.getCle7()
                    + "',cle8='" + resident.getCle8()
                    + "',cle9='" + resident.getCle9()
                    + "',cle10='" + resident.getCle10()
                    + "',clem1='" + resident.getClem1()
                    + "',clem2='" + resident.getClem2()
                    + "',clem3='" + resident.getClem3()
                    + "',clem4='" + resident.getClem4()
                    + "',clem5='" + resident.getClem5()
                    + "',clem6='" + resident.getClem6()
                    + "',clem7='" + resident.getClem7()
                    + "',clem8='" + resident.getClem8()
                    + "',clem9='" + resident.getClem9()
                    + "',clem10='" + resident.getClem10()
                    + "',identitel='" + resident.getIdentitel()
                    + "',temp_appeldirect='" + resident.getAppelDirect()
                    + "',temp_renvoi_appel='" + resident.getRenvoiAppel()
                    + "',temp_cle='" + resident.getTempCle()
                    + "',profession_liberale='" + resident.getProfessionLiberale()
                    + "',etat='1"
                    + "',tempo_renvoi_sipphone='" + resident.getRenvoiSip()
                    + "',tempo_renvoi_telephone='" + resident.getRenvoiTel()
                    + "',sipphone1='" + resident.getSipPhoneUn()
                    + "',sipphone2='" + resident.getSipPhoneDeux()
                    + "',id_voip='" + resident.getIdVoip()
                    + "',code_renvoi='" + resident.getCodeRenvoi()
                    + "',id_bluetooth1='" + resident.getId_bluetooth1()
                    + "',id_bluetooth2='" + resident.getId_bluetooth2()
                    + "',id_bluetooth3='" + resident.getId_bluetooth3()
                    + "',bip1='" + resident.getBip1()
                    + "',bip2='" + resident.getBip2()
                    + "',bip3='" + resident.getBip3()
                    + "',temp_radio='" + resident.getTempRadio()
                    + "',temp_identitooth='" + resident.getTempIdentitooth()
                    + "',temp_identitelSIP='" + resident.getTempIdentitelSIP()
                    + "',temp_profession_liberale='" + resident.getTempProfessionLiberale()
                    + "',profil='" + resident.getProfil()
                    + "',temp_liste_rouge='" + resident.getTempListeRouge()
                    + "' ");
        } catch (SQLException ex) {
            System.out.println("6");
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier la porte
     *
     * @param porte l'objet porte
     */
    @Override
    public void update(Porte porte) {

        String request = "update porte set "
                + "nom='" + porte.getNomAcces()
                + "',id_interphone='" + porte.getInterphone()
                + "',version='" + porte.getVersion()
                + "',id_teamviewer='" + porte.getIdTeamViewer()
                + "',sim='" + porte.getNumeroSim()
                + "',Tranches_horaires='" + porte.getTranchesHoraires()
                + "',clavier_code1='" + porte.getClavierCodeUn()
                + "',clavier_code2='" + porte.getClavierCodeDeux()
                + "',temps_appel='" + porte.getTempsAppel()
                + "',temps_conversation='" + porte.getTempsCommunication()
                + "',appel_abrege='" + porte.getAppelAbrege()
                + "',haut_parleur='" + porte.getNiveauHP()
                + "',microphone='" + porte.getNiveauMicro()
                + "',mess_bienvenue='" + porte.getMessageBienvenue()
                + "',mess_utilisation='" + porte.getMessageUtilisation()
                + "',affichage_etage='" + porte.getAffichageEtage()
                + "',prior_affichage1='" + porte.getPrioriteAffichageUn()
                + "',prior_affichage2='" + porte.getPrioriteAffichageDeux()
                + "',prior_affichage3='" + porte.getPrioriteAffichageTrois()
                + "',tempo_porte1='" + porte.getTempsOuvertureUn()
                + "',tempo_porte2='" + porte.getTempsOuvertureDeux()
                + "',tempo_porte3='" + porte.getTempsOuvertureTrois()
                + "',tempo_porte4='" + porte.getTempsOuvertureQuatre()
                + "',tempo_porte5='" + porte.getTempsOuvertureCinq()
                + "',tempo_porte6='" + porte.getTempsOuvertureSix()
                + "',tempo_porte7='" + porte.getTempsOuvertureSept()
                + "',tempo_porte8='" + porte.getTempsOuvertureHuit()
                + "',code_DTMF='" + porte.getTouchesDTMF()
                + "',signal_sonore='" + porte.getSignalSonore()
                + "',valid_temp_badge='" + porte.getValidTempBadge()
                + "',code_gestionnaire='" + porte.getCode()
                + "',ipclient='" + porte.getIpClient()
                + "',user_client='" + porte.getUserClient()
                + "',mdp_client='" + porte.getMdpClient()
                + "',iplocal='" + porte.getIpInterne()
                + "',ipexterne='" + porte.getIpExterne()
                + "',user_externe='" + porte.getUserExterne()
                + "',mdp_externe='" + porte.getMdpExterne()
                + "',code_accueil='" + porte.getCodeAccueil()
                + "',gestion_couleur='" + porte.getGestionCouleur()
                + "',valid_visuel_cle='" + porte.getVisuelBadge()
                + "',lecteur='" + porte.getLecteur()
                + "',id_badge='" + porte.getIdBadge()
                + "',langue='" + porte.getLangue()
                + "',id_wifi='" + porte.getIdWifi()
                + "',mdp_wifi='" + porte.getMdpWifi()
                + "',serveur_IP='" + porte.getServeurIP()
                + "',event_auto='" + porte.getEvenementAutomatique()
                + "',valid_connexion_web='" + porte.getValidConnexionWeb()
                + "',time_connexion_web='" + porte.getTimeConnexionWeb()
                + "',valid_web_client='" + porte.getValidWebClient()
                + "',valid_dhcp='" + porte.getValidServeurClientDhcp()
                + "',ipplatine='" + porte.getIpPlatine()
                + "',netmask='" + porte.getNetmask()
                + "',passerelle='" + porte.getPasserelle()
                + "',valid_radio='" + porte.getValidRadio()
                + "',valid_identitooth='" + porte.getValidIdentitooth()
                + "',valid_identitelSIP='" + porte.getValidIdentitelSIP()
                + "',mac_ethernet ='" + porte.getMacEthernet()
                + "',mac_wifi='" + porte.getMacWifi()
                + "',valid_uart='" + porte.getValidUart()
                + "',temp_radio='" + porte.getTempRadio()
                + "',temp_identitooth='" + porte.getTempIdentitooth()
                + "',temp_identitelSIP='" + porte.getTempIdentitelSIP()
                + "',mdp_wifi_pi='" + porte.getMdpWifiPi()
                + "',valid_wifi_pi='" + porte.getValidWifiPi()
                + "',tempo_vpn='" + porte.getTempoVpn()
                + "',tempo_sip='" + porte.getTempoSip()
                + "',valid_vpn='" + porte.getValidVpn()
                + "',valid_sip1='" + porte.getValidSipUn()
                + "',valid_sip2='" + porte.getValidSipDeux()
                + "',tempo_connect='" + porte.getTempoConnect()
                + "',default_gateway='" + porte.getDefaultGateway()
                + "'";

        //System.out.println(request);
        try {
            Conn();
            st = conn.createStatement();
            st.executeUpdate(request);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("7");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier le clavier codée un
     *
     * @param porte la porte à modifier
     */
    public void update_ClavierCodeUn(Porte porte) {

        try {
            st = conn.createStatement();
            st.executeUpdate("update porte set clavier_code1='" + porte.getClavierCodeUn()
                    + "',choix_cc1='" + porte.getChoixClavierCodeUn()
                    + "',temp_cc1='" + porte.getTemporaireClavierCodeUn()
                    + "'");
        } catch (SQLException ex) {
            System.out.println("8");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier le clavier codée un
     *
     * @param porte la porte à modifier
     */
    public void update_ClavierCodeDeux(Porte porte) {

        try {
            st = conn.createStatement();
            st.executeUpdate("update porte set clavier_code2='" + porte.getClavierCodeDeux()
                    + "',choix_cc2='" + porte.getChoixClavierCodeDeux()
                    + "',temp_cc2='" + porte.getTemporaireClavierCodeDeux()
                    + "'"); //le bug etait ici il y avait un where id = '339'
        } catch (SQLException ex) {
            System.out.println("9");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier le badge
     *
     * @param porte la porte à modifier
     */
    public void update_Badge(Porte porte) {

        try {
            st = conn.createStatement();
            st.executeUpdate("update porte set "
                    + "nomporte1='" + porte.getNomPorte1()
                    + "',lecteur_porte1='" + porte.getLecteurPorte1()
                    + "',nomporte2='" + porte.getNomPorte2()
                    + "',lecteur_porte2='" + porte.getLecteurPorte2()
                    + "',nomporte3='" + porte.getNomPorte3()
                    + "',lecteur_porte3='" + porte.getLecteurPorte3()
                    + "',nomporte4='" + porte.getNomPorte4()
                    + "',lecteur_porte4='" + porte.getLecteurPorte4()
                    + "',nomporte5='" + porte.getNomPorte5()
                    + "',lecteur_porte5='" + porte.getLecteurPorte5()
                    + "',nomporte6='" + porte.getNomPorte6()
                    + "',lecteur_porte6='" + porte.getLecteurPorte6()
                    + "',nomporte7='" + porte.getNomPorte7()
                    + "',lecteur_porte7='" + porte.getLecteurPorte7()
                    + "',nomporte8='" + porte.getNomPorte8()
                    + "',lecteur_porte8='" + porte.getLecteurPorte8()
                    + "',valid_temp_badge='" + porte.getValidTempBadge()
                    + "'");
        } catch (SQLException ex) {
            System.out.println("10");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier l'identitel
     *
     * @param porte la porte à modifier
     */
    public void update_Identitel(Porte porte) {

        try {
            st = conn.createStatement();
            st.executeUpdate("update porte set valid_temp_ident='" + porte.getValidTempIdentitel()
                    + "'");
        } catch (SQLException ex) {
            System.out.println("11");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier l'entree libre 1
     *
     * @param porte la porte à modifier
     */
    public void update_EntreLibreUn(Porte porte) {

        System.out.println("Update: " + porte.getDebutEntreLibreUn());
        try {
            st = conn.createStatement();
            st.executeUpdate("update porte set entre_libre1='" + porte.getChoixEntreLibreUn()
                    + "'"); // il y'avais aussi un where id = 339
        } catch (SQLException ex) {
            System.out.println("12");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        System.out.println("Updated: " + porte.getDebutEntreLibreUn());
    }

    /**
     * modifier l'entree libre 2
     *
     * @param porte la porte à modifier
     */
    public void update_EntreLibreDeux(Porte porte) {

        System.out.println("Update: " + porte.getDebutEntreLibreDeux());
        try {
            st = conn.createStatement();
            st.executeUpdate("update porte set entre_libre2='" + porte.getChoixEntreLibreDeux()
                    + "'");
        } catch (SQLException ex) {
            System.out.println("13");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        System.out.println("Updated: " + porte.getDebutEntreLibreDeux());
    }

    /**
     *
     * suppression de la porte
     *
     * @param porte
     */
    @Override
    public void delete(Porte porte) {
    }

    /**
     * renvoi l'equipement
     *
     * @return objet porte
     */
    public Porte getPorte() {

        porte1 = new Porte("4444");
        try {
            Conn();
            // Do something with the Connection

            st = conn.createStatement();
            rs = st.executeQuery("select porte.*,champ_traduction from porte,langue where langue.diminutif=porte.langue");
            String id = "";
            String idResidence = "";
            String nomAcces = "";
            String interphone = "";
            String version = "";
            String idTeamViewer = "";
            String numeroSim = "";
            String tranchesHoraires = "";
            String ClavierCodeUn = "";
            String choixClavierCodeUn = "";
            String temporaireClavierCodeUn = "";
            String ClavierCodeDeux = "";
            String choixClavierCodeDeux = "";
            String temporaireClavierCodeDeux = "";
            String choixEntreLibreUn = "";
            String choixEntreLibreDeux = "";
            String TempsAppel = "";
            String TempsCommunication = "";
            String appelAbrege = "";
            String niveauHP = "";
            String messageBienvenue = "";
            String messageUtilisation = "";
            String affichageEtage = "";
            String prioriteAffichage1 = "";
            String prioriteAffichage2 = "";
            String prioriteAffichage3 = "";
            String TempsOuvertureUn = "";
            String TempsOuvertureDeux = "";
            String TempsOuvertureTrois = "";
            String TempsOuvertureQuatre = "";
            String TempsOuvertureCinq = "";
            String TempsOuvertureSix = "";
            String TempsOuvertureSept = "";
            String TempsOuvertureHuit = "";
            String codeDTMF = "";
            String signalSonore = "";
            String nomPorte1 = "";
            String lecteur_porte1 = "";
            String nomPorte2 = "";
            String lecteur_porte2 = "";
            String nomPorte3 = "";
            String lecteur_porte3 = "";
            String nomPorte4 = "";
            String lecteur_porte4 = "";
            String nomPorte5 = "";
            String lecteur_porte5 = "";
            String nomPorte6 = "";
            String lecteur_porte6 = "";
            String nomPorte7 = "";
            String lecteur_porte7 = "";
            String nomPorte8 = "";
            String lecteur_porte8 = "";
            String validTempBadge = "";
            String validTempIdent = "";
            String code = "";
            String proxyIpClient = "";
            String userClient = "";
            String mdpClient = "";
            String ipInterne = "";
            String userExterne = "";
            String mdpExterne = "";
            String ipExterne = "";
            String codeAccueil = "";
            String gestionCouleur = "";
            String visuelBadge = "";
            String lecteur = "";
            String idBadge = "";
            String langue = "";
            String idWifi = "";
            String mdpWifi = "";
            String saisieManuelle = "";
            String serveurIP = "";
            String evenementAutomatique = "";
            String validConnexionWeb = "";
            String timeConnexionWeb = "";
            String validWebClient = "";
            String validServeurClientDhcp = "";
            String ipPlatine = "";
            String netmask = "";
            String passerelle = "";
            String validRadio = "";
            String validIdentitooth = "";
            String validIdentitelSIP = "";
            String macEthernet = "";
            String macWifi = "";
            String validUart = "";
            String tempRadio = "";
            String tempIdentitooth = "";
            String tempIdentitelSIP = "";
            String validWifiPi = "";
            String mdpWifiPi = "";
            String microphone = "";
            String numPorte = "";

            String tempoVpn = "";
            String tempoSip = "";
            String validVpn = "";
            String validSipUn = "";
            String validSipDeux = "";
            String tempoConnect = "";
            String defaultGateway = "";
            String traduction = "";
            while (rs.next()) {
                id = rs.getString("id");
                idResidence = rs.getString("id_residence");
                nomAcces = rs.getString("nom");
                interphone = rs.getString("id_interphone");
                version = rs.getString("Version");
                idTeamViewer = rs.getString("id_teamviewer");
                numeroSim = rs.getString("sim");
                tranchesHoraires = rs.getString("Tranches_horaires");
                ClavierCodeUn = rs.getString("clavier_code1");
                choixClavierCodeUn = rs.getString("choix_cc1");
                temporaireClavierCodeUn = rs.getString("temp_cc1");
                ClavierCodeDeux = rs.getString("clavier_code2");
                choixClavierCodeDeux = rs.getString("choix_cc2");
                temporaireClavierCodeDeux = rs.getString("temp_cc2");
                choixEntreLibreUn = rs.getString("entre_libre1");
                choixEntreLibreDeux = rs.getString("entre_libre2");
                TempsAppel = rs.getString("temps_appel");
                TempsCommunication = rs.getString("temps_conversation");
                appelAbrege = rs.getString("appel_abrege");
                niveauHP = rs.getString("haut_parleur");
                microphone = rs.getString("microphone");
                messageBienvenue = rs.getString("mess_bienvenue");
                messageUtilisation = rs.getString("mess_utilisation");
                affichageEtage = rs.getString("affichage_etage");
                prioriteAffichage1 = rs.getString("prior_affichage1");
                prioriteAffichage2 = rs.getString("prior_affichage2");
                prioriteAffichage3 = rs.getString("prior_affichage3");
                TempsOuvertureUn = rs.getString("tempo_porte1");
                TempsOuvertureDeux = rs.getString("tempo_porte2");
                TempsOuvertureTrois = rs.getString("tempo_porte3");
                TempsOuvertureQuatre = rs.getString("tempo_porte4");
                TempsOuvertureCinq = rs.getString("tempo_porte5");
                TempsOuvertureSix = rs.getString("tempo_porte6");
                TempsOuvertureSept = rs.getString("tempo_porte7");
                TempsOuvertureHuit = rs.getString("tempo_porte8");
                codeDTMF = rs.getString("code_DTMF");
                signalSonore = rs.getString("signal_sonore");
                nomPorte1 = rs.getString("nomporte1");
                lecteur_porte1 = rs.getString("lecteur_porte1");
                nomPorte2 = rs.getString("nomporte2");
                lecteur_porte2 = rs.getString("lecteur_porte2");
                nomPorte3 = rs.getString("nomporte3");
                lecteur_porte3 = rs.getString("lecteur_porte3");
                nomPorte4 = rs.getString("nomporte4");
                lecteur_porte4 = rs.getString("lecteur_porte4");
                nomPorte5 = rs.getString("nomporte5");
                lecteur_porte5 = rs.getString("lecteur_porte5");
                nomPorte6 = rs.getString("nomporte6");
                lecteur_porte6 = rs.getString("lecteur_porte6");
                nomPorte7 = rs.getString("nomporte7");
                lecteur_porte7 = rs.getString("lecteur_porte7");
                nomPorte8 = rs.getString("nomporte8");
                lecteur_porte8 = rs.getString("lecteur_porte8");
                validTempBadge = rs.getString("valid_temp_badge");
                validTempIdent = rs.getString("valid_temp_ident");
                code = rs.getString("code_gestionnaire");
                proxyIpClient = rs.getString("ipclient");
                userClient = rs.getString("user_client");
                mdpClient = rs.getString("mdp_client");
                ipInterne = rs.getString("iplocal");
                userExterne = rs.getString("user_externe");
                mdpExterne = rs.getString("mdp_externe");
                ipExterne = rs.getString("ipexterne");
                codeAccueil = rs.getString("code_accueil");
                gestionCouleur = rs.getString("gestion_couleur");
                visuelBadge = rs.getString("valid_visuel_cle");
                lecteur = rs.getString("lecteur");
                idBadge = rs.getString("id_badge");
                langue = rs.getString("langue");
                idWifi = rs.getString("id_wifi");
                mdpWifi = rs.getString("mdp_wifi");
                saisieManuelle = rs.getString("saisie_manuelle");
                serveurIP = rs.getString("serveur_IP");
                evenementAutomatique = rs.getString("event_auto");
                validConnexionWeb = rs.getString("valid_connexion_web");
                timeConnexionWeb = rs.getString("time_connexion_web");
                validWebClient = rs.getString("valid_web_client");
                validServeurClientDhcp = rs.getString("valid_dhcp");
                ipPlatine = rs.getString("ipplatine");
                netmask = rs.getString("netmask");
                passerelle = rs.getString("passerelle");
                validRadio = rs.getString("valid_radio");
                validIdentitooth = rs.getString("valid_identitooth");
                validIdentitelSIP = rs.getString("valid_identitelSIP");
                macEthernet = rs.getString("mac_ethernet");
                macWifi = rs.getString("mac_wifi");
                validUart = rs.getString("valid_uart");
                tempRadio = rs.getString("temp_radio");
                tempIdentitooth = rs.getString("temp_identitooth");
                tempIdentitelSIP = rs.getString("temp_identitelSIP");
                validWifiPi = rs.getString("valid_wifi_pi");
                mdpWifiPi = rs.getString("mdp_wifi_pi");
                numPorte = rs.getString("id");
                tempoVpn = rs.getString("tempo_vpn");
                tempoSip = rs.getString("tempo_sip");
                validVpn = rs.getString("valid_vpn");
                validSipUn = rs.getString("valid_sip1");
                validSipDeux = rs.getString("valid_sip2");
                tempoConnect = rs.getString("tempo_connect");
                defaultGateway = rs.getString("default_gateway");
                traduction = rs.getString("champ_traduction");
            }
            porte1 = new Porte(code);
            porte1.setId(id);
            porte1.setIdResidence(idResidence);
            porte1.setNomAcces(nomAcces);
            porte1.setInterphone(interphone);
            porte1.setVersion(version);
            porte1.setIdTeamViewer(idTeamViewer);
            porte1.setNumeroSim(numeroSim);
            porte1.setTranchesHoraires(tranchesHoraires);
            porte1.setClavierCodeUn(ClavierCodeUn);
            porte1.setChoixClavierCodeUn(choixClavierCodeUn);
            porte1.setTemporaireClavierCodeUn(temporaireClavierCodeUn);
            porte1.setClavierCodeDeux(ClavierCodeDeux);
            porte1.setChoixClavierCodeDeux(choixClavierCodeDeux);
            porte1.setTemporaireClavierCodeDeux(temporaireClavierCodeDeux);
            porte1.setChoixEntreLibreUn(choixEntreLibreUn);
            porte1.setChoixEntreLibreDeux(choixEntreLibreDeux);
            porte1.setTempsAppel(TempsAppel);
            porte1.setTempsCommunication(TempsCommunication);
            porte1.setAppelAbrege(appelAbrege);
            porte1.setNiveauHP(niveauHP);
            porte1.setNiveauMicro(microphone);
            porte1.setMessageBienvenue(messageBienvenue);
            porte1.setMessageUtilisation(messageUtilisation);
            porte1.setAffichageEtage(affichageEtage);
            porte1.setPrioriteAffichageUn(prioriteAffichage1);
            porte1.setPrioriteAffichageDeux(prioriteAffichage2);
            porte1.setTempsOuvertureUn(TempsOuvertureUn);
            porte1.setTempsOuvertureDeux(TempsOuvertureDeux);
            porte1.setTempsOuvertureTrois(TempsOuvertureTrois);
            porte1.setTempsOuvertureQuatre(TempsOuvertureQuatre);
            porte1.setTempsOuvertureCinq(TempsOuvertureCinq);
            porte1.setTempsOuvertureSix(TempsOuvertureSix);
            porte1.setTempsOuvertureSept(TempsOuvertureSept);
            porte1.setTempsOuvertureHuit(TempsOuvertureHuit);
            porte1.setTouchesDTMF(codeDTMF);
            porte1.setSignalSonore(signalSonore);
            porte1.setNomPorte1(nomPorte1);
            porte1.setLecteurPorte1(lecteur_porte1);
            porte1.setNomPorte2(nomPorte2);
            porte1.setLecteurPorte2(lecteur_porte2);
            porte1.setNomPorte3(nomPorte3);
            porte1.setLecteurPorte3(lecteur_porte3);
            porte1.setNomPorte4(nomPorte4);
            porte1.setLecteurPorte4(lecteur_porte4);
            porte1.setNomPorte5(nomPorte5);
            porte1.setLecteurPorte5(lecteur_porte5);
            porte1.setNomPorte6(nomPorte6);
            porte1.setLecteurPorte6(lecteur_porte6);
            porte1.setNomPorte7(nomPorte7);
            porte1.setLecteurPorte7(lecteur_porte7);
            porte1.setNomPorte8(nomPorte8);
            porte1.setLecteurPorte8(lecteur_porte8);
            porte1.setValidTempBadge(validTempBadge);
            porte1.setValidTempIdentitel(validTempIdent);

            porte1.setIpClient(proxyIpClient);
            porte1.setUserClient(userClient);
            porte1.setMdpClient(mdpClient);
            porte1.setIpInterne(ipInterne);
            porte1.setUserExterne(userExterne);
            porte1.setMdpExterne(mdpExterne);
            porte1.setIpExterne(ipExterne);
            porte1.setCodeAccueil(codeAccueil);
            porte1.setGestionCouleur(gestionCouleur);
            porte1.setVisuelBadge(visuelBadge);
            porte1.setLecteur(lecteur);
            porte1.setIdBadge(idBadge);
            porte1.setLangue(langue);
            porte1.setIdWifi(idWifi);
            porte1.setMdpWifi(mdpWifi);
            porte1.setSaisieManuelle(saisieManuelle);
            porte1.setServeurIP(serveurIP);
            porte1.setEvenementAutomatique(evenementAutomatique);
            porte1.setValidConnexionWeb(validConnexionWeb);
            porte1.setTimeConnexionWeb(timeConnexionWeb);
            porte1.setValidWebClient(validWebClient);
            porte1.setValidServeurClientDhcp(validServeurClientDhcp);
            porte1.setIpPlatine(ipPlatine);
            porte1.setNetmask(netmask);
            porte1.setPasserelle(passerelle);
            porte1.setValidRadio(validRadio);
            porte1.setValidIdentitooth(validIdentitooth);
            porte1.setValidIdentitelSIP(validIdentitelSIP);
            porte1.setMacEthernet(macEthernet);
            porte1.setMacWifi(macWifi);
            porte1.setValidUart(validUart);
            porte1.setTempRadio(tempRadio);
            porte1.setTempIdentitooth(tempIdentitooth);
            porte1.setTempIdentitelSIP(tempIdentitelSIP);
            porte1.setValidWifiPi(validWifiPi);
            porte1.setMdpWifiPi(mdpWifiPi);
            porte1.setNumPorte(numPorte);
            porte1.setTempoVpn(tempoVpn);
            porte1.setTempoSip(tempoSip);
            porte1.setValidVpn(validVpn);
            porte1.setValidSipUn(validSipUn);
            porte1.setValidSipDeux(validSipDeux);
            porte1.setTempoConnect(tempoConnect);
            porte1.setDefaultGateway(defaultGateway);
            porte1.setTraduction(traduction);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("14");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }
        return porte1;
    }

    /**
     * renvoi la liste des evenements
     *
     * @return liste des evenements
     */
    @Override
    public List<Event> getEvents() {

        return null;
    }

    /**
     * rajouter un evenement
     *
     * @param type type evenement
     * @param Lcd1 affichage part 1
     * @param Lcd2 affichage part 2
     * @param Lcd3 affichage part 3
     * @param Lcd4 affichage part 4
     */
    public void addEvent(String type, String Lcd1, String Lcd2, String Lcd3, String Lcd4) {

        Porte porte = getPorte();
        try {
            st = conn.createStatement();
            //System.out.println("lcd 1 : " + Lcd1);
            //System.out.println("lcd 2 : " + Lcd2);
            //System.out.println("lcd 3 : " + Lcd3);
            //System.out.println("lcd 4 : " + Lcd4);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            if (Lcd1.length() > 16) {
                Lcd1 = Lcd1.substring(0, 15);
            }
            if (Lcd2.length() > 16) {
                Lcd2 = Lcd2.substring(0, 15);
            }
            if (Lcd3.length() > 16) {
                Lcd3 = Lcd3.substring(0, 15);
            }
            if (Lcd4.length() > 16) {
                Lcd4 = Lcd4.substring(0, 15);
            }
            if (type.length() > 16) {
                type = type.substring(0, 15);
            }
            if (!porte.getUserExterne().equals("0")) {
                st.executeUpdate("insert into eventporte (id_porte,type, lcd_ligne1, lcd_ligne2, lcd_ligne3, lcd_ligne4) values ('" + porte.getNumPorte() + "','" + type + "', '" + Lcd1 + "', '" + Lcd2 + "', '" + Lcd3 + "', '" + Lcd4 + "')");
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("15");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * renvoi la liste des utilisateurs sur l'equipement
     *
     * @return list utilisateur
     */
    @Override
    public List<Resident> getResidents() {

        List<Resident> residents = new ArrayList<>();
        try {
            Conn();

            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT id,droit,id_voip,code,nom,telephone1,renvoi_appel,telephone2,list_rouge,Identifiant,techno_appel,validation_cle,cle1,clem1,cle2,clem2,cle3,clem3,cle4,clem4,cle5,clem5,cle6,clem6,cle7,clem7,cle8,clem8,cle9,clem9,cle10,clem10,identitel,temp_appeldirect,temp_renvoi_appel,temp_cle,profession_liberale,is_refresh,etat,sipphone1,sipphone2,tempo_renvoi_sipphone,tempo_renvoi_telephone,code_renvoi,id_bluetooth1,id_bluetooth2,id_bluetooth3,bip1,bip2,bip3,temp_radio,temp_identitooth,temp_identitelSIP,temp_profession_liberale,temp_liste_rouge,profil FROM residents where list_rouge='0' and nom is not null order by nom asc");

            while (rs.next()) {
                String id = rs.getString("id");
                String droit = rs.getString("droit");
                String code = rs.getString("code");
                String nom = rs.getString("nom");
                String telephone1 = rs.getString("telephone1");
                String renvoiAppel = rs.getString("renvoi_appel");
                String telephone2 = rs.getString("telephone2");
                String listRouge = rs.getString("list_rouge");
                String Identifiant = rs.getString("Identifiant");
                String technoAppel = rs.getString("techno_appel");
                String validationCle = rs.getString("validation_cle");
                String cle1 = rs.getString("cle1");
                String clem1 = rs.getString("clem1");
                String cle2 = rs.getString("cle2");
                String clem2 = rs.getString("clem2");
                String cle3 = rs.getString("cle3");
                String clem3 = rs.getString("clem3");
                String cle4 = rs.getString("cle4");
                String clem4 = rs.getString("clem4");
                String cle5 = rs.getString("cle5");
                String clem5 = rs.getString("clem5");
                String cle6 = rs.getString("cle6");
                String clem6 = rs.getString("clem6");
                String cle7 = rs.getString("cle7");
                String clem7 = rs.getString("clem7");
                String cle8 = rs.getString("cle8");
                String clem8 = rs.getString("clem8");
                String cle9 = rs.getString("cle9");
                String clem9 = rs.getString("clem9");
                String cle10 = rs.getString("cle10");
                String clem10 = rs.getString("clem10");
                String identitel = rs.getString("identitel");
                String tempAppelDirect = rs.getString("temp_appeldirect");
                String tempRenvoiAppel = rs.getString("temp_renvoi_appel");
                String tempCle = rs.getString("temp_cle");
                String professionLiberale = rs.getString("profession_liberale");
                String isRefresh = rs.getString("is_refresh");
                String etat = rs.getString("etat");
                String sipPhone1 = rs.getString("sipphone1");
                String sipPhone2 = rs.getString("sipphone2");
                String tempoRenvoiSipphone = rs.getString("tempo_renvoi_sipphone");
                String tempoRenvoiTelephone = rs.getString("tempo_renvoi_telephone");

                String idVoip = rs.getString("id_voip");
                String codeRenvoi = rs.getString("code_renvoi");
                String idBluetooth1 = rs.getString("id_bluetooth1");
                String idBluetooth2 = rs.getString("id_bluetooth2");
                String idBluetooth3 = rs.getString("id_bluetooth3");
                String bip1 = rs.getString("bip1");
                String bip2 = rs.getString("bip2");
                String bip3 = rs.getString("bip3");
                String tempRadio = rs.getString("temp_radio");
                String tempIdentitooth = rs.getString("temp_identitooth");
                String tempIdentitelSIP = rs.getString("temp_identitelSIP");
                String tempProfessionLiberale = rs.getString("temp_profession_liberale");
                String tempListeRouge = rs.getString("temp_liste_rouge");
                String profil = rs.getString("profil");
                residents.add(new Resident(id,
                        droit,
                        code,
                        nom,
                        telephone1,
                        renvoiAppel,
                        telephone2,
                        listRouge,
                        Identifiant,
                        technoAppel,
                        validationCle,
                        cle1,
                        clem1,
                        cle2,
                        clem2,
                        cle3,
                        clem3,
                        cle4,
                        clem4,
                        cle5,
                        clem5,
                        cle6,
                        clem6,
                        cle7,
                        clem7,
                        cle8,
                        clem8,
                        cle9,
                        clem9,
                        cle10,
                        clem10,
                        identitel,
                        tempAppelDirect,
                        tempRenvoiAppel,
                        tempCle,
                        professionLiberale,
                        isRefresh,
                        etat,
                        sipPhone1,
                        sipPhone2,
                        tempoRenvoiSipphone,
                        tempoRenvoiTelephone,
                        idVoip,
                        codeRenvoi,
                        idBluetooth1,
                        idBluetooth2,
                        idBluetooth3,
                        bip1,
                        bip2,
                        bip3,
                        tempRadio,
                        tempIdentitooth,
                        tempIdentitelSIP,
                        tempProfessionLiberale,
                        tempListeRouge,
                        profil));

            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("16");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return residents;
    }

    /**
     * liste des utilisateurs en simultanée
     *
     * @param firstCode code de la premiere fiche
     * @return liste utilisateur
     */
    public List<Resident> getSimuResidents(String firstCode) {
        int i = 0;
        List<Resident> residents = new ArrayList<>();
        String id;
        String droit;
        String code;
        String nom;
        String telephone1;
        String renvoiAppel = "";
        String telephone2;
        String listRouge;
        String Identifiant;
        String technoAppel;
        String validationCle;
        String cle1;
        String clem1;
        String cle2;
        String clem2;
        String cle3;
        String clem3;
        String cle4;
        String clem4;
        String cle5;
        String clem5;
        String cle6;
        String clem6;
        String cle7;
        String clem7;
        String cle8;
        String clem8;
        String cle9;
        String clem9;
        String cle10;
        String clem10;
        String identitel;
        String tempAppelDirect;
        String tempRenvoiAppel;
        String tempCle;
        String professionLiberale;
        String isRefresh;
        String etat;
        String sipPhone1;
        String sipPhone2;
        String tempoRenvoiSipphone;
        String tempoRenvoiTelephone;

        String idVoip;
        String codeRenvoi = "";
        String idBluetooth1;
        String idBluetooth2;
        String idBluetooth3;
        String bip1;
        String bip2;
        String bip3;
        String tempRadio;
        String tempIdentitooth;
        String tempIdentitelSIP;
        String tempProfessionLiberale;
        String tempListeRouge;
        String profil;
        try {
            Conn();

            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT id,droit,id_voip,code,nom,telephone1,renvoi_appel,telephone2,list_rouge,Identifiant,techno_appel,validation_cle,cle1,clem1,cle2,clem2,cle3,clem3,cle4,clem4,cle5,clem5,cle6,clem6,cle7,clem7,cle8,clem8,cle9,clem9,cle10,clem10,identitel,temp_appeldirect,temp_renvoi_appel,temp_cle,profession_liberale,is_refresh,etat,sipphone1,sipphone2,tempo_renvoi_sipphone,tempo_renvoi_telephone,code_renvoi,id_bluetooth1,id_bluetooth2,id_bluetooth3,bip1,bip2,bip3,temp_radio,temp_identitooth,temp_identitelSIP,temp_profession_liberale,temp_liste_rouge,profil FROM residents where code='" + firstCode + "'");

            while (rs.next()) {
                id = rs.getString("id");
                droit = rs.getString("droit");
                code = rs.getString("code");
                nom = rs.getString("nom");
                telephone1 = rs.getString("telephone1");
                renvoiAppel = rs.getString("renvoi_appel");
                telephone2 = rs.getString("telephone2");
                listRouge = rs.getString("list_rouge");
                Identifiant = rs.getString("Identifiant");
                technoAppel = rs.getString("techno_appel");
                validationCle = rs.getString("validation_cle");
                cle1 = rs.getString("cle1");
                clem1 = rs.getString("clem1");
                cle2 = rs.getString("cle2");
                clem2 = rs.getString("clem2");
                cle3 = rs.getString("cle3");
                clem3 = rs.getString("clem3");
                cle4 = rs.getString("cle4");
                clem4 = rs.getString("clem4");
                cle5 = rs.getString("cle5");
                clem5 = rs.getString("clem5");
                cle6 = rs.getString("cle6");
                clem6 = rs.getString("clem6");
                cle7 = rs.getString("cle7");
                clem7 = rs.getString("clem7");
                cle8 = rs.getString("cle8");
                clem8 = rs.getString("clem8");
                cle9 = rs.getString("cle9");
                clem9 = rs.getString("clem9");
                cle10 = rs.getString("cle10");
                clem10 = rs.getString("clem10");
                identitel = rs.getString("identitel");
                tempAppelDirect = rs.getString("temp_appeldirect");
                tempRenvoiAppel = rs.getString("temp_renvoi_appel");
                tempCle = rs.getString("temp_cle");
                professionLiberale = rs.getString("profession_liberale");
                isRefresh = rs.getString("is_refresh");
                etat = rs.getString("etat");
                sipPhone1 = rs.getString("sipphone1");
                sipPhone2 = rs.getString("sipphone2");
                tempoRenvoiSipphone = rs.getString("tempo_renvoi_sipphone");
                tempoRenvoiTelephone = rs.getString("tempo_renvoi_telephone");

                idVoip = rs.getString("id_voip");
                codeRenvoi = rs.getString("code_renvoi");
                idBluetooth1 = rs.getString("id_bluetooth1");
                idBluetooth2 = rs.getString("id_bluetooth2");
                idBluetooth3 = rs.getString("id_bluetooth3");
                bip1 = rs.getString("bip1");
                bip2 = rs.getString("bip2");
                bip3 = rs.getString("bip3");
                tempRadio = rs.getString("temp_radio");
                tempIdentitooth = rs.getString("temp_identitooth");
                tempIdentitelSIP = rs.getString("temp_identitelSIP");
                tempProfessionLiberale = rs.getString("temp_profession_liberale");
                tempListeRouge = rs.getString("temp_liste_rouge");
                profil = rs.getString("profil");
                residents.add(new Resident(id,
                        droit,
                        code,
                        nom,
                        telephone1,
                        renvoiAppel,
                        telephone2,
                        listRouge,
                        Identifiant,
                        technoAppel,
                        validationCle,
                        cle1,
                        clem1,
                        cle2,
                        clem2,
                        cle3,
                        clem3,
                        cle4,
                        clem4,
                        cle5,
                        clem5,
                        cle6,
                        clem6,
                        cle7,
                        clem7,
                        cle8,
                        clem8,
                        cle9,
                        clem9,
                        cle10,
                        clem10,
                        identitel,
                        tempAppelDirect,
                        tempRenvoiAppel,
                        tempCle,
                        professionLiberale,
                        isRefresh,
                        etat,
                        sipPhone1,
                        sipPhone2,
                        tempoRenvoiSipphone,
                        tempoRenvoiTelephone,
                        idVoip,
                        codeRenvoi,
                        idBluetooth1,
                        idBluetooth2,
                        idBluetooth3,
                        bip1,
                        bip2,
                        bip3,
                        tempRadio,
                        tempIdentitooth,
                        tempIdentitelSIP,
                        tempProfessionLiberale,
                        tempListeRouge,
                        profil));
            }
            while ("1".equals(renvoiAppel) && i < 10) {

                rs = st.executeQuery("SELECT id,droit,id_voip,code,nom,telephone1,renvoi_appel,telephone2,list_rouge,Identifiant,techno_appel,validation_cle,cle1,clem1,cle2,clem2,cle3,clem3,cle4,clem4,cle5,clem5,cle6,clem6,cle7,clem7,cle8,clem8,cle9,clem9,cle10,clem10,identitel,temp_appeldirect,temp_renvoi_appel,temp_cle,profession_liberale,is_refresh,etat,sipphone1,sipphone2,tempo_renvoi_sipphone,tempo_renvoi_telephone,code_renvoi,id_bluetooth1,id_bluetooth2,id_bluetooth3,bip1,bip2,bip3,temp_radio,temp_identitooth,temp_identitelSIP,temp_profession_liberale,temp_liste_rouge,profil FROM residents where code='" + codeRenvoi + "'");
                i++;
                while (rs.next()) {
                    id = rs.getString("id");
                    droit = rs.getString("droit");
                    code = rs.getString("code");
                    nom = rs.getString("nom");
                    telephone1 = rs.getString("telephone1");
                    renvoiAppel = rs.getString("renvoi_appel");
                    telephone2 = rs.getString("telephone2");
                    listRouge = rs.getString("list_rouge");
                    Identifiant = rs.getString("Identifiant");
                    technoAppel = rs.getString("techno_appel");
                    validationCle = rs.getString("validation_cle");
                    cle1 = rs.getString("cle1");
                    clem1 = rs.getString("clem1");
                    cle2 = rs.getString("cle2");
                    clem2 = rs.getString("clem2");
                    cle3 = rs.getString("cle3");
                    clem3 = rs.getString("clem3");
                    cle4 = rs.getString("cle4");
                    clem4 = rs.getString("clem4");
                    cle5 = rs.getString("cle5");
                    clem5 = rs.getString("clem5");
                    cle6 = rs.getString("cle6");
                    clem6 = rs.getString("clem6");
                    cle7 = rs.getString("cle7");
                    clem7 = rs.getString("clem7");
                    cle8 = rs.getString("cle8");
                    clem8 = rs.getString("clem8");
                    cle9 = rs.getString("cle9");
                    clem9 = rs.getString("clem9");
                    cle10 = rs.getString("cle10");
                    clem10 = rs.getString("clem10");
                    identitel = rs.getString("identitel");
                    tempAppelDirect = rs.getString("temp_appeldirect");
                    tempRenvoiAppel = rs.getString("temp_renvoi_appel");
                    tempCle = rs.getString("temp_cle");
                    professionLiberale = rs.getString("profession_liberale");
                    isRefresh = rs.getString("is_refresh");
                    etat = rs.getString("etat");
                    sipPhone1 = rs.getString("sipphone1");
                    sipPhone2 = rs.getString("sipphone2");
                    tempoRenvoiSipphone = rs.getString("tempo_renvoi_sipphone");
                    tempoRenvoiTelephone = rs.getString("tempo_renvoi_telephone");

                    idVoip = rs.getString("id_voip");
                    codeRenvoi = rs.getString("code_renvoi");
                    idBluetooth1 = rs.getString("id_bluetooth1");
                    idBluetooth2 = rs.getString("id_bluetooth2");
                    idBluetooth3 = rs.getString("id_bluetooth3");
                    bip1 = rs.getString("bip1");
                    bip2 = rs.getString("bip2");
                    bip3 = rs.getString("bip3");
                    tempRadio = rs.getString("temp_radio");
                    tempIdentitooth = rs.getString("temp_identitooth");
                    tempIdentitelSIP = rs.getString("temp_identitelSIP");
                    tempProfessionLiberale = rs.getString("temp_profession_liberale");
                    tempListeRouge = rs.getString("temp_liste_rouge");
                    profil = rs.getString("profil");
                    boolean SimuMM = false;
                    for (int j = 0; j < residents.size(); j++) {
                        if (code.equals(residents.get(j).getCode())) {
                            SimuMM = true;
                        }
                    }
                    if (!SimuMM) {
                        residents.add(new Resident(id,
                                droit,
                                code,
                                nom,
                                telephone1,
                                renvoiAppel,
                                telephone2,
                                listRouge,
                                Identifiant,
                                technoAppel,
                                validationCle,
                                cle1,
                                clem1,
                                cle2,
                                clem2,
                                cle3,
                                clem3,
                                cle4,
                                clem4,
                                cle5,
                                clem5,
                                cle6,
                                clem6,
                                cle7,
                                clem7,
                                cle8,
                                clem8,
                                cle9,
                                clem9,
                                cle10,
                                clem10,
                                identitel,
                                tempAppelDirect,
                                tempRenvoiAppel,
                                tempCle,
                                professionLiberale,
                                isRefresh,
                                etat,
                                sipPhone1,
                                sipPhone2,
                                tempoRenvoiSipphone,
                                tempoRenvoiTelephone,
                                idVoip,
                                codeRenvoi,
                                idBluetooth1,
                                idBluetooth2,
                                idBluetooth3,
                                bip1,
                                bip2,
                                bip3,
                                tempRadio,
                                tempIdentitooth,
                                tempIdentitelSIP,
                                tempProfessionLiberale,
                                tempListeRouge,
                                profil));
                    }
                }
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("16");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return residents;
    }

    /**
     * renvoi la liste des utilisateur qui sont modifier
     *
     * @return liste des utilisateurs
     */
    public List<Resident> getResidentsMaj() {

        List<Resident> residents = new ArrayList<>();
        try {
            Conn();

            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT id,droit,code,nom,telephone1,renvoi_appel,telephone2,list_rouge,Identifiant,techno_appel,validation_cle,cle1,clem1,cle2,clem2,cle3,clem3,cle4,clem4,cle5,clem5,cle6,clem6,cle7,clem7,cle8,clem8,cle9,clem9,cle10,clem10,identitel,temp_appeldirect,temp_renvoi_appel,temp_cle,profession_liberale,is_refresh,etat,sipphone1,sipphone2,tempo_renvoi_sipphone,tempo_renvoi_telephone,code_renvoi,id_bluetooth1,id_bluetooth2,id_bluetooth3,bip1,bip2,bip3,temp_radio,temp_identitooth,temp_identitelSIP,temp_profession_liberale,temp_liste_rouge,profil FROM residents where etat=0 or etat=-3");

            while (rs.next()) {
                String id = rs.getString("id");
                String droit = rs.getString("droit");
                String code = rs.getString("code");
                String nom = rs.getString("nom");
                String telephone1 = rs.getString("telephone1");
                String renvoiAppel = rs.getString("renvoi_appel");
                String telephone2 = rs.getString("telephone2");
                String listRouge = rs.getString("list_rouge");
                String Identifiant = rs.getString("Identifiant");
                String technoAppel = rs.getString("techno_appel");
                String validationCle = rs.getString("validation_cle");
                String cle1 = rs.getString("cle1");
                String clem1 = rs.getString("clem1");
                String cle2 = rs.getString("cle2");
                String clem2 = rs.getString("clem2");
                String cle3 = rs.getString("cle3");
                String clem3 = rs.getString("clem3");
                String cle4 = rs.getString("cle4");
                String clem4 = rs.getString("clem4");
                String cle5 = rs.getString("cle5");
                String clem5 = rs.getString("clem5");
                String cle6 = rs.getString("cle6");
                String clem6 = rs.getString("clem6");
                String cle7 = rs.getString("cle7");
                String clem7 = rs.getString("clem7");
                String cle8 = rs.getString("cle8");
                String clem8 = rs.getString("clem8");
                String cle9 = rs.getString("cle9");
                String clem9 = rs.getString("clem9");
                String cle10 = rs.getString("cle10");
                String clem10 = rs.getString("clem10");
                String identitel = rs.getString("identitel");
                String tempAppelDirect = rs.getString("temp_appeldirect");
                String tempRenvoiAppel = rs.getString("temp_renvoi_appel");
                String tempCle = rs.getString("temp_cle");
                String professionLiberale = rs.getString("profession_liberale");
                String isRefresh = rs.getString("is_refresh");
                String etat = rs.getString("etat");
                String sipPhone1 = rs.getString("sipphone1");
                String sipPhone2 = rs.getString("sipphone2");
                String tempoRenvoiSipphone = rs.getString("tempo_renvoi_sipphone");
                String tempoRenvoiTelephone = rs.getString("tempo_renvoi_telephone");

                String idVoip = rs.getString("id_voip");
                String codeRenvoi = rs.getString("code_renvoi");
                String idBluetooth1 = rs.getString("id_bluetooth1");
                String idBluetooth2 = rs.getString("id_bluetooth2");
                String idBluetooth3 = rs.getString("id_bluetooth3");
                String bip1 = rs.getString("bip1");
                String bip2 = rs.getString("bip2");
                String bip3 = rs.getString("bip3");
                String tempRadio = rs.getString("temp_radio");
                String tempIdentitooth = rs.getString("temp_identitooth");
                String tempIdentitelSIP = rs.getString("temp_identitelSIP");
                String tempProfessionLiberale = rs.getString("temp_profession_liberale");
                String tempListeRouge = rs.getString("temp_liste_rouge");
                String profil = rs.getString("profil");

                residents.add(new Resident(id,
                        droit,
                        code,
                        nom,
                        telephone1,
                        renvoiAppel,
                        telephone2,
                        listRouge,
                        Identifiant,
                        technoAppel,
                        validationCle,
                        cle1,
                        clem1,
                        cle2,
                        clem2,
                        cle3,
                        clem3,
                        cle4,
                        clem4,
                        cle5,
                        clem5,
                        cle6,
                        clem6,
                        cle7,
                        clem7,
                        cle8,
                        clem8,
                        cle9,
                        clem9,
                        cle10,
                        clem10,
                        identitel,
                        tempAppelDirect,
                        tempRenvoiAppel,
                        tempCle,
                        professionLiberale,
                        isRefresh,
                        etat,
                        sipPhone1,
                        sipPhone2,
                        tempoRenvoiSipphone,
                        tempoRenvoiTelephone,
                        idVoip,
                        codeRenvoi,
                        idBluetooth1,
                        idBluetooth2,
                        idBluetooth3,
                        bip1,
                        bip2,
                        bip3,
                        tempRadio,
                        tempIdentitooth,
                        tempIdentitelSIP,
                        tempProfessionLiberale,
                        tempListeRouge,
                        profil));
            }
            st.executeUpdate("update residents set etat=1 where etat=0 or etat=-3");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("17");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return residents;
    }

    /**
     * renvoi la liste des evenement non envoyé sur le serveur
     *
     * @return liste des evenements
     */
    public List<Evenement> getEvenementsToSend() {

        List<Evenement> events = new ArrayList<>();
        try {
            Conn();
            st = conn.createStatement();
            Porte porte = getPorte();
            st.executeUpdate("update eventporte set id_porte=" + porte.getNumPorte() + "");
            rs = st.executeQuery("SELECT id,id_porte,date_event,type,lcd_ligne1,lcd_ligne2, lcd_ligne3, lcd_ligne4 from eventporte where toSend=0 order by date_event desc");
            while (rs.next()) {
                String id = rs.getString("id");
                String id_porte = rs.getString("id_porte");
                String date_event = rs.getString("date_event");
                String lcd_ligne1 = rs.getString("lcd_ligne1");
                String lcd_ligne2 = rs.getString("lcd_ligne2");
                String lcd_ligne3 = rs.getString("lcd_ligne3");
                String lcd_ligne4 = rs.getString("lcd_ligne4");
                String type = rs.getString("type");
                events.add(new Evenement(id, id_porte, date_event, type,
                        lcd_ligne1, lcd_ligne2, lcd_ligne3, lcd_ligne4));
            }
            st.executeUpdate("update eventporte set toSend=1 where toSend=0");
        } catch (SQLException ex) {
            System.out.println("18");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return events;
    }

    /**
     * renvoi la liste de tous les evenements
     *
     * @return list evenements
     */
    public List<Evenement> getEvenements() {

        List<Evenement> events = new ArrayList<>();
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("SELECT id,id_porte,date_event,type,lcd_ligne1,lcd_ligne2, lcd_ligne3, lcd_ligne4 from eventporte order by date_event desc limit 100");
            while (rs.next()) {
                String id = rs.getString("id");
                String id_porte = rs.getString("id_porte");
                String date_event = rs.getString("date_event");
                String lcd_ligne1 = rs.getString("lcd_ligne1");
                String lcd_ligne2 = rs.getString("lcd_ligne2");
                String lcd_ligne3 = rs.getString("lcd_ligne3");
                String lcd_ligne4 = rs.getString("lcd_ligne4");
                String type = rs.getString("type");
                events.add(new Evenement(id, id_porte, date_event, type,
                        lcd_ligne1, lcd_ligne2, lcd_ligne3, lcd_ligne4));
            }
        } catch (SQLException ex) {
            System.out.println("19");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return events;
    }

    /**
     *
     * renvoi la liste des profils temporel de la porte
     *
     * @return liste des profils
     */
    public List<ProfilTemporel> getProfilTemporel() {

        List<ProfilTemporel> profils = new ArrayList<>();
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * from profil_temporel ");
            while (rs.next()) {
                String id = rs.getString("id");
                String id_profil = rs.getString("id_profil");
                String heure_debut = rs.getString("heure_debut");
                String heure_fin = rs.getString("heure_fin");
                String jour = rs.getString("jour");
                String etat = rs.getString("etat");
                profils.add(new ProfilTemporel(id, id_profil, heure_debut, heure_fin, jour, etat));
            }
        } catch (SQLException ex) {
            System.out.println("20");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return profils;
    }

    /**
     * get profil Temporel par code
     *
     * @param icode l'identifiant du profile Temporel
     * @return le profile Temporel
     */
    public ProfilTemporel getProfilTemporel(String icode) {

        ProfilTemporel profil = new ProfilTemporel();
        try {
            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * FROM profil_temporel where id ='" + icode + "' limit 1");
            String id;
            String idProfil;
            String heureDebut;
            String heureFin;
            String jour;
            String etat;
            if (rs.next()) {
                id = rs.getString("id");
                idProfil = rs.getString("id_profil");
                heureDebut = rs.getString("heure_debut");
                heureFin = rs.getString("heure_fin");
                jour = rs.getString("jour");
                etat = rs.getString("etat");

                profil.setId(id);
                profil.setIdProfil(idProfil);
                profil.setHeureDebut(heureDebut);
                profil.setHeureFin(heureFin);
                profil.setJour(jour);
                profil.setEtat(etat);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("21");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return profil;
    }

    /**
     * inserer un profil temporel
     *
     * @param profil le profil Temporel
     */
    public void insert(ProfilTemporel profil) {
        try {
            Conn();
            // Do something with the Connection	    
            st = conn.createStatement();
            st.executeUpdate("insert into profil_temporel(id,id_profil,heure_debut,heure_fin,jour,etat) values ('"
                    + profil.getId() + "','"
                    + profil.getIdProfil() + "','"
                    + profil.getHeureDebut() + "','"
                    + profil.getHeureFin() + "','"
                    + profil.getJour() + "','"
                    + profil.getEtat() + "') ");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("22");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier le profil Temporel
     *
     * @param profil le nouveau profil Temporel
     */
    public void update(ProfilTemporel profil) {

        String request = "update profil_temporel set heure_debut='" + profil.getHeureDebut()
                + "',heure_fin='" + profil.getHeureFin()
                + "',jour='" + profil.getJour()
                + "' where id='" + profil.getId() + "' ;";

        System.out.println(request);
        try {
            Conn();
            st = conn.createStatement();
            st.executeUpdate(request);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("23");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * renvoi le profil
     *
     * @return le profil
     */
    public List<Profil> getProfil() {

        List<Profil> profils = new ArrayList<>();
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * from profil");
            while (rs.next()) {
                String id = rs.getString("id");
                String id_residence = rs.getString("id_residence");
                String jour_debut = rs.getString("jour_debut");
                String jour_fin = rs.getString("jour_fin");
                String nom = rs.getString("nom");
                String etat = rs.getString("etat");
                profils.add(new Profil(id, id_residence, jour_debut, jour_fin, nom, etat));
            }
        } catch (SQLException ex) {
            System.out.println("24");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return profils;
    }

    /**
     * renvoi un profil
     *
     * @param icode l'identifiant du profile
     * @return le profil
     */
    public Profil getProfil(String icode) {

        Profil profil = new Profil();
        try {
            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * FROM profil where id ='" + icode + "' limit 1");
            String id;
            String idResidence;
            String jourDebut;
            String jourFin;
            String nom;
            String etat;
            if (rs.next()) {
                id = rs.getString("id");
                idResidence = rs.getString("id_residence");
                jourDebut = rs.getString("jour_debut");
                jourFin = rs.getString("jour_fin");
                nom = rs.getString("nom");
                etat = rs.getString("etat");

                profil.setId(id);
                profil.setIdResidence(idResidence);
                profil.setJourDebut(jourDebut);
                profil.setJourFin(jourFin);
                profil.setNom(nom);
                profil.setEtat(etat);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("25");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return profil;
    }

    /**
     * inserer un profil
     *
     * @param profil le profil
     */
    public void insert(Profil profil) {

        try {
            Conn();
            // Do something with the Connection
            System.out.println("insert into profil(id,id_residence,jour_debut,jour_fin,nom,etat) values ('"
                    + profil.getId() + "','"
                    + profil.getIdResidence() + "','"
                    + profil.getJourDebut() + "','"
                    + profil.getJourFin() + "','"
                    + profil.getNom() + "','"
                    + profil.getEtat() + "') ");
            st = conn.createStatement();
            st.executeUpdate("insert into profil(id,id_residence,jour_debut,jour_fin,nom,etat) values ('"
                    + profil.getId() + "','"
                    + profil.getIdResidence() + "','"
                    + profil.getJourDebut() + "','"
                    + profil.getJourFin() + "','"
                    + profil.getNom() + "','"
                    + profil.getEtat() + "')");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("26");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modifier le profil
     *
     * @param profil le profil à modifier
     */
    public void update(Profil profil) {

        String request = "update profil set jour_debut='" + profil.getJourDebut()
                + "',jour_fin='" + profil.getJourFin()
                + "',nom='" + profil.getNom()
                + "' where id='" + profil.getId() + "' ;";

        System.out.println(request);
        try {
            Conn();
            st = conn.createStatement();
            st.executeUpdate(request);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("27");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * renvoi la liste des jours ferier
     *
     * @return liste des jours ferier
     */
    public List<JourFerie> getJourFerie() {

        List<JourFerie> jourFeries = new ArrayList<>();
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * from jour_ferie");
            while (rs.next()) {
                String id = rs.getString("id");
                String id_residence = rs.getString("id_residence");
                String jour_debut = rs.getString("jour_debut");
                String jour_fin = rs.getString("jour_fin");
                String etat = rs.getString("etat");
                jourFeries.add(new JourFerie(id, id_residence, jour_debut, jour_fin, etat));
            }
        } catch (SQLException ex) {
            System.out.println("28");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return jourFeries;
    }

    /**
     * renvoi jour ferier par id
     *
     * @param icode l'id
     * @return jour ferier
     */
    public JourFerie getJourFerie(String icode) {

        JourFerie jourFerie = new JourFerie();
        try {
            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            rs = st.executeQuery("SELECT * FROM jour_ferie where id ='" + icode + "' limit 1");

            String id;
            String idResidence;
            String jourDebut;
            String jourFin;
            String etat;
            if (rs.next()) {
                id = rs.getString("id");
                idResidence = rs.getString("id_residence");
                jourDebut = rs.getString("jour_debut");
                jourFin = rs.getString("jour_fin");
                etat = rs.getString("etat");

                jourFerie.setId(id);
                jourFerie.setIdResidence(idResidence);
                jourFerie.setJourDebut(jourDebut);
                jourFerie.setJourFin(jourFin);
                jourFerie.setEtat(etat);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("29");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return jourFerie;
    }

    /**
     * insertion du jour ferier
     *
     * @param jourFerie le jour ferier
     */
    public void insert(JourFerie jourFerie) {

        try {
            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            st.executeUpdate("insert into jour_ferie(id,id_residence,jour_debut,jour_fin,etat) values ('"
                    + jourFerie.getId() + "','"
                    + jourFerie.getIdResidence() + "','"
                    + jourFerie.getJourDebut() + "','"
                    + jourFerie.getJourFin() + "','"
                    + jourFerie.getEtat() + "') ");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("30");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * modification du jour ferier
     *
     * @param jourFerie le nouveau jour ferier
     */
    public void update(JourFerie jourFerie) {

        String request = "update jour_ferie set jour_debut='" + jourFerie.getJourDebut()
                + "',jour_fin='" + jourFerie.getJourFin()
                + "' where id='" + jourFerie.getId()
                + "' ;";

        System.out.println(request);
        try {
            Conn();
            st = conn.createStatement();
            st.executeUpdate(request);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("31");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * renvoi le nombre de profil temporel
     *
     * @return profil temporel
     */
    public int getNbProfilTemporel() {

        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select count(*) as nb from profil_temporel where jour < '8' ");
            int nb = 0;
            while (rs.next()) {
                nb = rs.getInt("nb");
            }
            return nb;
        } catch (SQLException ex) {
        }
        return 0;
    }

    /**
     * vérifier si une date appartier à une tranche horraire
     *
     * @param id l'id de la tranche horraire
     * @param dateNow datenow
     * @param heureNow heurenow
     * @param dayWeek dayofweek
     * @return true or false
     */
    public boolean dateValide(String id, String dateNow, String heureNow, String dayWeek) {
        try {
            int nb = 0;

            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select count(*) as nb from profil,profil_temporel where profil.id = '" + id + "' and jour_debut <= '" + dateNow + "' and jour_fin >= '"
                    + dateNow + "' and id_profil = '" + id + "' and jour < '8' and jour = '" + dayWeek + "' and heure_debut <= '"
                    + heureNow + "' and heure_fin >= '" + heureNow + "' ");

            while (rs.next()) {
                nb = rs.getInt("nb");
            }
            return nb > 0;
        } catch (SQLException ex) {
            System.err.print(ex.getMessage());
        }
        return false;
    }

    /**
     * renvoi la string traduit
     *
     * @param valeur la veleur d'origine
     * @param language la langue
     * @return la chaine traduit
     */
    public String getTraduction(String valeur, String language) {
        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select " + language + " as traduction from traduction where alias='" + valeur + "'");
            //System.out.println("select " + language + " as traduction from traduction where alias='" + valeur + "'");

            String traduction = "";
            while (rs.next()) {
                traduction = rs.getString("traduction");
            }
            return !traduction.isEmpty() ? traduction.replace("\\n", "\n").replace("’", "'") : valeur;
        } catch (SQLException ex) {
            System.err.print(ex.getMessage());
        }
        return valeur;
    }

    /**
     * mettre à jour la version
     *
     * @param version le numéro de la version
     */
    public void setVersion(String version) {

        try {

            Conn();
            // Do something with the Connection
            st = conn.createStatement();
            System.out.println(version);
            st.executeUpdate("update porte set version='" + version + "'");

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("update version");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    /**
     * renvoi le numéro de la version actuelle
     *
     * @return le numéro de la version
     */
    public String getVersion() {

        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select version  from porte");
            String version = "";
            if (rs.next()) {
                version = rs.getString("version");
            }

            return version;
        } catch (SQLException ex) {
            System.out.println("select version");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return "0";

    }

    /**
     * renvoi la version de la base de données
     *
     * @return le numéro de la version du bd
     */
    public String getDBVersion() {

        try {
            Conn();
            st = conn.createStatement();
            rs = st.executeQuery("select valeur from params where param like '%DBVERSION%'");
            String versiondb = "";
            if (rs.next()) {
                versiondb = rs.getString("valeur");
            }

            return versiondb;
        } catch (SQLException ex) {
            System.out.println("select version");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return "0";

    }
}
