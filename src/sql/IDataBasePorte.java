package sql;

import sql.data.Porte;

/**
 * interface database porte
 *
 * @author Abdellah
 */
interface IDataBasePorte {

    /**
     * modifier la porte avec objet porte
     *
     * @param porte objet porte
     */
    void update(Porte porte);

    /**
     * supprimer porte
     *
     * @param porte objet porte
     */
    void delete(Porte porte);
}
