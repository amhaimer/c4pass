package sip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * classe pour utiliser linphone (obsolete)
 *
 * @author Abdellah
 */
public class SipClient {

    String domain = null;

    //! \brief Contrôle le rétroeclairage du clavier
    //! \details Par default le rétroeclairage est activé
    //! \param b booléen si egale true alors le rétro-éclairage est activé.
    private native void System(String command);

    //! \brief Contrôle le rétroeclairage du clavier
    //! \details Par default le rétroeclairage est activé
    //! \param b booléen si egale true alors le rétro-éclairage est activé.
    public void init() {
        System("linphonecsh init -c ~/.linphonerc");
    }

    public void exit() {
        System("linphonecsh exit");
    }

    //! \brief Contrôle le rétroeclairage du clavier
    //! \details Par default le rétroeclairage est activé
    //! \param b booléen si egale true alors le rétro-éclairage est activé.
    public void registerOVH() {
        System("linphonecsh register --host sip3.ovh.fr --username 0033972564962 --password CBw9Gh3P");
    }

    public void registerClient(String domaine, String user, String mdp) {

        System("linphonecsh register --host " + domaine + " --username " + user + " --password " + mdp + "");
    }

    public void registerAsterisk() {
        System("linphonecsh register --host sip.linphone.org --username andreamouraud --password 50ee2f11");
    }

    public void unregister() {
        System("linphonecsh unregister");
    }

    //! \brief Contrôle le rétroeclairage du clavier
    //! \details Par default le rétroeclairage est activé
    //! \param b booléen si egale true alors le rétro-éclairage est activé.
    public void terminate() {
        System("linphonecsh generic \"terminate\"");
    }

    public void call_asterisk(String tel) {
        System("linphonecsh generic \"call " + tel + "\"");
    }

    public void call_ovh(String tel) {
        System("linphonecsh generic \"call " + tel + "\"");
    }

    //! \brief Contrôle le rétroeclairage du clavier
    //! \details Par default le rétroeclairage est activé
    //! \param b booléen si egale true alors le rétro-éclairage est activé.
    public void status() {

        System.out.println(command("linphonecsh status register"));
    }

    public int statusCall() {
        int stat;
        String s = command("linphonecsh status hook");
        if (s.contains("Call out")) {
            System.out.println("user a decroché");
            stat = 2;
        } else if (s.contains("hook=offhook")) {
            System.out.println("l'utilisateur a racroché ");
            stat = 1;
        } else {
            System.out.println("appel en cours");
            stat = 0;
        }
        return stat;
    }

    public String command(String param) {
        Runtime rt = Runtime.getRuntime();
        Process proc = null;
        try {
            proc = rt.exec(param);
        } catch (IOException e) {
            System.out.println(e);
        }
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String s = " ";
        String m = " ";
        System.out.println("le message :");
        try {
            while ((s = stdInput.readLine()) != null) {

                m += s;
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println(m + "\n");
        return m;

    }
}
