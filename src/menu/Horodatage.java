package menu;

import sql.DataBase;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * classe d'horodatage qui gére les fobnctions des tranches horraires
 *
 * @see check
 * @author Abdellah
 */
public class Horodatage {

    private LocalDate date;
    private String numJour;
    private String dateCourrante;
    private String heureCourrante;
    private String lastProfil;

    private DataBase data = DataBase.getInstance();
    private static Horodatage INSTANCE = null;
    private final DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final SimpleDateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");

    /**
     * enum des causes pourquoi la tranche est invalide
     */
    public enum Cause {
        UNKNOWN, DATE, JOUR, HORA;
    }

    /**
     * vérification de la tanche horraire
     *
     * @param idProfil le profil à vérifier
     * @return true or false selon la validité
     */
    public boolean check(String idProfil) {
        lastProfil = idProfil;
        date = LocalDate.now();
        numJour = String.valueOf(date.getDayOfWeek().getValue());
        dateCourrante = date.format(formatDate);
        heureCourrante = formatHeure.format(Calendar.getInstance().getTime());

        // JOUR FERIER : REGARDER TABLE JOUR FERIER, SI JOUR EST FERIE, PRENDRE NUMERO JOUR ET LE CHANGER PAR (sans doute 8)
        return data.dateValide(idProfil, dateCourrante, heureCourrante, numJour);
    }

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Horodatage getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Horodatage();
        }
        return INSTANCE;
    }
}
