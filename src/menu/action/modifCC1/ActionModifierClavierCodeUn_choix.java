package menu.action.modifCC1;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Clavier Code Un choix
 *
 * @author Abdellah
 */
public class ActionModifierClavierCodeUn_choix implements ActionItem {

    private int currentCursorBegin = 6;

    private int cursor = currentCursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private String langue = "";
    private String digicode1 = "";

    private String varTemp = "0";

    private char input[];
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        currentCursorBegin = 6;

        cursor = currentCursorBegin;
        input = porte.getChoixClavierCodeUn().toCharArray();

        varTemp = porte.getTemporaireClavierCodeUn();

        lcd.setCursor(2, cursor);
        lcd.print(porte.getChoixClavierCodeUn(), false);
        lcd.setCursor(2, cursor);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, currentCursorBegin);
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[0] = '1';
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[0] = '2';
        } else if (event instanceof EventKeyValid) {

            porte.setChoixClavierCodeUn(String.valueOf(input));
            data.update_ClavierCodeUn(porte);
            digicode1 = porte.getChoixClavierCodeUn();
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("deuxdigicodes", langue), data.getTraduction("digicode1", langue), data.getTraduction("choixporte_e", langue), digicode1);

            if (varTemp.equals("0")) {
                lcd.print("PROFIL TEMPOREL-", true);
                lcd.setCursor(2, 0);
                lcd.print("---PERMANENT----", false);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    System.err.print(ex.getMessage());
                }
                menu.father();
            } else {
                lcd.print("PROFIL TEMPOREL-", true);
                lcd.setCursor(2, 0);
                lcd.print("LOGIDIESE: " + varTemp, false);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    System.err.print(ex.getMessage());
                }
                menu.father();
            }
            //menu.next();
        } else if (event instanceof EventKeyB) {
            count = 0;
            menu.father();
        }
    }
}
