package menu.action.modifCC1;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Clavier Code Un code
 *
 * @author Abdellah
 */
public class ActionModifierClavierCodeUn_code implements ActionItem {

    private int currentCursorBegin = 5;

    private int cursor = currentCursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private String langue = "";
    private String digicode1 = "";
    char input[] = new char[4];
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        currentCursorBegin = 5;

        cursor = currentCursorBegin;
        input = new char[4];
        for (int i = 0; i < porte.getClavierCodeUn().length(); i++) {
            input[i] = porte.getClavierCodeUn().charAt(i);
        }
        lcd.setCursor(2, cursor - 2);
        lcd.print(porte.getInterphone().charAt(1) == '3' ? "B " : "V ", false);
        lcd.print(porte.getClavierCodeUn(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - currentCursorBegin] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - currentCursorBegin] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - currentCursorBegin] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - currentCursorBegin] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - currentCursorBegin] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - currentCursorBegin] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - currentCursorBegin] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - currentCursorBegin] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - currentCursorBegin] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - currentCursorBegin] = '9';
            cursor++;
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyRight) {
            cursor++;
            lcd.setCursor(2, cursor);
        }
        if (cursor - currentCursorBegin >= 4) {
            cursor = currentCursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (cursor - currentCursorBegin < 0) {
            cursor += 4;
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyValid) {
            System.out.println("1");
            porte.setClavierCodeUn(String.valueOf(input));
            System.out.println("2");
            data.update_ClavierCodeUn(porte);
            digicode1 = porte.getClavierCodeUn();
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("deuxdigicodes", langue), data.getTraduction("digicode1", langue), data.getTraduction("modifdigicode1_e", langue), digicode1);
            System.out.println("3");
            menu.next();
        } else if (event instanceof EventKeyP) {
            input = "----".toCharArray();
            cursor = currentCursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print("----", false);
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyB) {
            cursor = currentCursorBegin;
            count = 0;
            menu.father();
        }
    }
}
