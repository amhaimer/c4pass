package menu.action.ficheBadge;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Validation Lecteur
 *
 * @author Abdellah
 */
public class ActionValidationLecteur implements ActionItem {

    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private char input[] = new char[8];
    int count = 0;
    private String langue = "";
    Boolean boo;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        resident = data.getResident(ActionEntreeCodeResidentCle.code);
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ",false);
        lcd.print(data.getTraduction("nonoui", langue), false);
        lcd.setCursor(2, 13);
        cursor = 0;

        /*
	** lcd.cursorMode(false);
         */
        for (int i = 0; i < resident.getValidationCle().length(); i++) {
            input[i] = resident.getValidationCle().charAt(i);
        }
        if (input[0] == '1') {
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI", false);
        } else {
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON", false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        boo = true;
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, 13);
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON",false);
            input[cursor] = '0';
            boo = false;
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, 13);
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI",false);
            input[cursor] = '1';
            boo = false;
        } else if (event instanceof EventKeyValid) {

            /*
	    ** count=0;
	    ** cursor = cursorBegin;
             */
            resident.setValidationCle(String.valueOf(input));
            data.update(resident);
            cursor++;

            /*
	    ** menu.father();
             */
        } else if (event instanceof EventKeyB) {
            cursor = 0;
            count = 0;
            menu.father();
        }

        if (cursor == 8) {
            menu.father();
        } else if (cursor < 0) {
            cursor = 0;
        }
        if (cursor == 0 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur1", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 1 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur2", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 2 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur3", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 3 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur4", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 4 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur5", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 5 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur6", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 6 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur7", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        } else if (cursor == 7 && boo) {
            //lcd.print("LECTEUR "+(cursor+1),true);
            lcd.print(data.getTraduction("lecteur8", langue), true);
            lcd.setCursor(2, 0);
            String val = data.getTraduction("non", langue);
            if (input[cursor] == '1') {
                val = data.getTraduction("oui", langue);
            }
            //lcd.print("OUI=1 NON=0: "+val,false);
            lcd.setCursor(2, 0);
            lcd.print(data.getTraduction("nonoui", langue), false);
            lcd.setCursor(2, 13);
            lcd.print(val, false);
        }
    }
}
