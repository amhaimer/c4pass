package menu.action.ficheBadge;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Resident;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Lire Emetteur
 *
 * @author Abdellah
 */
public class ActionLireEmetteur implements ActionItem {

    //! \class ActionEntreeCodeGestionaire action enntrer code gestionaire.
    private final int cursorBegin = 0;

    private int cursor = cursorBegin;

    private Menu menu;
    private Lcd lcd;

    private String[] input1 = new String[10];

    DataBase data = DataBase.getInstance();
    Resident resident;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        resident = data.getResident(ActionEntreeCodeResidentEmetteur.code);
        lcd = Lcd.getInstance();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.print(data.getTraduction("bip1", langue), true);
        lcd.setCursor(2, 0);
        lcd.blinkMode(false);

        //lcd.print("CLE "+(indicateur+1),true);
        /*lcd.setCursor(2,0);
	lcd.print(resident.getBip1(),false);
	for(int i=0;i<resident.getBip1().length();i++) {
	    input[i]=resident.getBip1().charAt(i);
	}
	lcd.saisie(true);*/
 /*
	** lcd.cursorMode (false);
         */
        if (resident.getBip1() != null) {
            input1[0] = resident.getBip1();
        } else {
            input1[0] = " ";
        }
        if (resident.getBip2() != null) {
            input1[1] = resident.getBip2();
        } else {
            input1[1] = " ";
        }
        if (resident.getBip3() != null) {
            input1[2] = resident.getBip3();
        } else {
            input1[2] = " ";
        }

        lcd.print(data.getTraduction("bip1", langue), true);
        lcd.setCursor(2, 0);
        lcd.print(input1[cursor], false);

        /*if ((resident.getBip1()).equals(null)) {
	    input1[0] = " ";
	}
	else {
	    input1[0] = resident.getBip1();
	}
	if ((resident.getBip2()).equals(null)) {
	    input1[1] = " ";
	}
	else {
	    input1[1] = resident.getBip2();
	}
	if ((resident.getBip3()).equals(null)) {
	    input1[2] = " ";
	}
	else {
	    input1[2] = resident.getBip3();
	}*/

 /*
	** lcd.print(input1[0],false);
	** indicateur++;
         */
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    //! \brief Methode implémenter de l'interface ActionItem.
    //! \details Méthode appelée à chaque que un évenement se produit.
    //! \li si \a EventKey[0-9] affiche la numéro appuyé et incrément le curseur , si 4 touche au été appuyé alors test si egale au mot de passe.
    //! \li si \a EventKeyCancel retour Menu Bienvenue ,
    //! \li si \a EventTimer retour Menu Bienvenue
    //!param event Pointeur sur le dernier évenement produit
    /*public void action(Event event) {

	menu = Menu.getInstance();
	lcd = Lcd.getInstance();
	
	if (event instanceof EventKey0) {
	    lcd.print("0",false);
	    input[cursor-cursorBegin]= '0';
	    cursor++;
	}
	if (event instanceof EventKey1) {
	    lcd.print("1",false);
	    input[cursor-cursorBegin]= '1';
	    cursor++;
	}
	else if (event instanceof EventKey2) {
	    lcd.print("2",false);
	    input[cursor-cursorBegin]= '2';
	    cursor++;	
	}
	else if (event instanceof EventKey3) {
	    lcd.print("3",false);
	    input[cursor-cursorBegin]='3' ;
	    cursor++;
	}
	else if (event instanceof EventKey4) {
	    lcd.print("4",false);
	    input[cursor-cursorBegin]= '4';
	    cursor++;
	}
	else if (event instanceof EventKey5) {
	    lcd.print("5",false);
	    input[cursor-cursorBegin]= '5';
	    cursor++;
	}
	else if (event instanceof EventKey6) {
	    lcd.print("6",false);
	    input[cursor-cursorBegin]= '6';
	    cursor++;
	}
	else if (event instanceof EventKey7) {
	    lcd.print("7",false);
	    input[cursor-cursorBegin]= '7';
	    cursor++;
	 }
	else if (event instanceof EventKey8) {
	    lcd.print("8",false);
	    input[cursor-cursorBegin]= '8';
	    cursor++;
	}
	else if (event instanceof EventKey9) {
	    lcd.print("9",false);
	    input[cursor-cursorBegin]= '9';
	    cursor++;
	}
	if (event instanceof EventKeyB) {
	    
	    menu.father();
	}
	
	if (event instanceof EventKeyRight) {
	    cursor ++;
	}
	if (event instanceof EventKeyLeft)  {
	    cursor--;
	}
	if (event instanceof EventKeyValid) {
	    System.out.println(indicateur);
	    if (indicateur < 0) {
		indicateur = 0;
	    }
	    if (indicateur == 0) {
		String Bip1 = new String(input);
		resident.setBip1(Bip1);
		data.update(resident);
		indicateur ++;
		for (int i = 0; i < 10; i++) {
		    input[i] = ' ';
		}
		cursor = cursorBegin;
		lcd.print(data.getTraduction("bip2",langue),true);
		lcd.setCursor(2,0);
		lcd.print(resident.getBip2(),false);
		for(int i=0;i<resident.getBip2().length();i++) {
	    	    input[i]=resident.getBip2().charAt(i);
		}
		lcd.saisie(true);
	    } else if (indicateur == 1) {
		String Bip2 = new String(input);
		resident.setBip2(Bip2);
		data.update(resident);
		indicateur ++;
		for (int i = 0; i < 10; i++) {
		    input[i] = ' ';
		}
		cursor = cursorBegin;
		lcd.print(data.getTraduction("bip3",langue),true);
		lcd.setCursor(2,0);
		lcd.print(resident.getBip3(),false);
		for(int i=0;i<resident.getBip3().length();i++) {
	    	    input[i]=resident.getBip3().charAt(i);
		}
		lcd.saisie(true);
	    } else if (indicateur == 2) {
		String Bip3 = new String(input);
		resident.setBip3(Bip3);
		data.update(resident);
		indicateur ++;
		for (int i = 0; i < 10; i++) {
		    input[i] = ' ';
		}
		cursor = cursorBegin;
	    }
	    if (indicateur == 3 ) {
		menu.next();
	    }
	}
	if (cursor > cursorEnd) {
	    cursor = cursorBegin;
	    lcd.setCursor(2,cursor);
	} 
    }*/
    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyB) {

            /*
	    ** input = "";
             */
            menu.father();
        }

        if (event instanceof EventKeyValid) {
            cursor++;

            /*
	    **	menu.next();
             */
        }
        System.out.println(cursor);
        if (cursor == 3) {
            menu.next();
        }
        if (cursor < 0) {
            cursor = 0;
        }
        switch (cursor) {
            case 0:
                lcd.print(data.getTraduction("bip1", langue), true);
                lcd.setCursor(2, 0);
                lcd.print(input1[cursor], false);
                break;
            case 1:
                lcd.print(data.getTraduction("bip2", langue), true);
                lcd.setCursor(2, 0);
                lcd.print(input1[cursor], false);
                break;
            case 2:
                lcd.print(data.getTraduction("bip3", langue), true);
                lcd.setCursor(2, 0);
                lcd.print(input1[cursor], false);
                break;
            default:
                break;
        }
    }
}
