package menu.action.ficheBadge;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;

/**
 * Action Modifier Liste Rouge
 *
 * @author Abdellah
 */
public class ActionModifierListeRouge implements ActionItem {

    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private char input[] = new char[1];
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResidentCle.code);
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
        lcd.setCursor(2, 13);
        if (resident.getListeRouge() != null) {
            input[0] = resident.getListeRouge().charAt(0);
            if (resident.getListeRouge().equals("1")) {
                lcd.print("OUI", false);
            } else {
                lcd.print("NON", false);
            }
        } else {
            input[0] = '0';
            lcd.print("---", false);
        }

        /*
	** lcd.cursorMode(false);
         */
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print("NON", false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print("OUI", false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;

            resident.setListeRouge(String.valueOf(input));
            data.update(resident);
            menu.next();
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        }
    }
}
