package menu.action.ficheBadge;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Resident;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Affichage Badge
 *
 * @author Abdellah
 */
public class ActionAffichageBadge implements ActionItem {

    //! \class ActionEntreeCodeGestionaire action enntrer code gestionaire.
    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;

    private String[] input = new String[10];
    DataBase data = DataBase.getInstance();
    Resident resident;
    private String langue = "";

    /**
     * Méthode appelée à chaque que l'on entre dans le menu d'enter du code de
     * gestionaire (en appuyant sur la 'V' dans le menu Bienvenue), cette
     * methode initialise le timer pour le retour le buffer pour l'entrer du
     * code, et place le curseur sur l'afficheur lcd.
     */
    @Override
    public void init() {

        resident = data.getResident(ActionEntreeCodeResidentCle.code);
        lcd = Lcd.getInstance();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.blinkMode(false);
        /*
	** lcd.cursorMode (false);
         */

        if (resident.getCle1() != null) {
            input[0] = resident.getCle1();
        } else {
            input[0] = " ";
        }
        if (resident.getCle2() != null) {
            input[1] = resident.getCle2();
        } else {
            input[1] = " ";
        }
        if (resident.getCle3() != null) {
            input[2] = resident.getCle3();
        } else {
            input[2] = " ";
        }
        if (resident.getCle4() != null) {
            input[3] = resident.getCle4();
        } else {
            input[3] = " ";
        }
        if (resident.getCle5() != null) {
            input[4] = resident.getCle5();
        } else {
            input[4] = " ";
        }
        if (resident.getCle6() != null) {
            input[5] = resident.getCle6();
        } else {
            input[5] = " ";
        }
        if (resident.getCle7() != null) {
            input[6] = resident.getCle7();
        } else {
            input[6] = " ";
        }
        if (resident.getCle8() != null) {
            input[7] = resident.getCle8();
        } else {
            input[7] = " ";
        }
        if (resident.getCle9() != null) {
            input[8] = resident.getCle9();
        } else {
            input[8] = " ";
        }
        if (resident.getCle10() != null) {
            input[9] = resident.getCle10();
        } else {
            input[9] = " ";
        }

        /*
	** lcd.print(input[0],false);
	** cursor++;
         */
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyB) {

            /*
	    ** input = "";
             */
            menu.father();
        }

        if (event instanceof EventKeyValid) {
            cursor++;

            /*
	    **	menu.next();
             */
        }
        System.out.println(cursor);
        if (cursor == 10) {
            menu.next();
        }
        if (cursor < 0) {
            cursor = 0;
        }
        if (cursor == 0) {
            lcd.print(data.getTraduction("cle1", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 1) {
            lcd.print(data.getTraduction("cle2", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 2) {
            lcd.print(data.getTraduction("cle3", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 3) {
            lcd.print(data.getTraduction("cle4", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 4) {
            lcd.print(data.getTraduction("cle5", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 5) {
            lcd.print(data.getTraduction("cle6", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 6) {
            lcd.print(data.getTraduction("cle7", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 7) {
            lcd.print(data.getTraduction("cle8", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 8) {
            lcd.print(data.getTraduction("cle9", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        } else if (cursor == 9) {
            lcd.print(data.getTraduction("cle10", langue), true);
            //lcd.print("CLE "+(cursor+1),true);
            lcd.setCursor(2, 0);
            lcd.print(input[cursor], false);
        }
    }
}
