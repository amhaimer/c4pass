package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.DataBase;

/**
 * Action Modifier Bienvenue
 *
 * @author Abdellah
 */
public class ActionModifierBienvenue implements ActionItem {

    private final int cursorBegin = 3;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private char[] input = new char[10];
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private int c;
    private final char[] lettre = " .,1ABC2DEF3GHI4JKL5MNO6PQRS7TUV8WXYZ9-\'#0".toCharArray();
    private Event lastEvent;
    private Item sub1;

    private Item ItemBienvenue;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        sub1 = menu.currentItem.father;
        ItemBienvenue = sub1.childs.get(0);
        System.out.println(ItemBienvenue.toString());
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        input = new char[10];
        for (int i = 0; i < porte.getMessageBienvenue().length(); i++) {
            input[i] = porte.getMessageBienvenue().charAt(i);
        }
        cursor = cursorBegin;
        c = 0;
        lastEvent = new EventKeyValid();
        //lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getMessageBienvenue(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    public void action(Event event) {

        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor - 3] = lettre[0 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[4 + c]), false);
            input[cursor - 3] = lettre[4 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[8 + c]), false);
            input[cursor - 3] = lettre[8 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[12 + c]), false);
            input[cursor - 3] = lettre[12 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[16 + c]), false);
            input[cursor - 3] = lettre[16 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[20 + c]), false);
            input[cursor - 3] = lettre[20 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[24 + c]), false);
            input[cursor - 3] = lettre[24 + c];
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[29 + c]), false);
            input[cursor - 3] = lettre[29 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[33 + c]), false);
            input[cursor - 3] = lettre[33 + c];
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[38 + c]), false);
            input[cursor - 3] = lettre[38 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor <= 9) {
                lcd.setCursor(2, cursor);
            }
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 3) {
                cursor = 12;
            }
            lcd.setCursor(2, cursor);
        }
        if (cursor - 3 > 9) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            String msg = new String(input);
            msg = msg.trim();
            ItemBienvenue.affichage = msg;
            porte.setMessageBienvenue(msg);
            data.update(porte);
            //data.addEvent("SAISIE ENR.PORTE", "Modification du", "Mess. bienvenue"," "," ");
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("messbienvenuel3", langue), msg);
            menu.father();
        }
        if (event instanceof EventKeyP) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print("BIENVENUE ", false);
            lcd.setCursor(2, cursor);
            input = "BIENVENUE ".toCharArray();
        }
        c = c < 4 ? c + 1 : 0;
        lcd.setCursor(2, cursor);
        lastEvent = event;
    }
}
