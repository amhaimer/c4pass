package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;

/**
 * Action Navigate
 *
 * @author Abdellah
 */
public class ActionNavigate implements ActionItem {

    private Menu menu;

    /**
     * init
     */
    @Override
    public void init() {
        Lcd lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * Methode implémenter de l'interface ActionItem. Méthode appelée à chaque
     * que un évenement se produit. EventKeyRight passe a l'item suiavnt
     * EventKeyLeft passe a l'item précédent EventKeyValid entre dans le menu,
     * EventKeyCancel retour au Menu precedent, EventTimer retour Menu
     * preccedent,
     *
     * @param event Pointeur sur le dernier évenement produit
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();

        if (event instanceof EventKeyLeft) {
            menu.previous();
        }
        if (event instanceof EventKeyRight) {
            menu.next();
        }
        if (event instanceof EventKeyValid) {
            menu.child();
        }
        if (event instanceof EventKeyB) {
            menu.father();
        }
    }

    @Override
    public String toString() {
        return "action : navigate \n";
    }

}
