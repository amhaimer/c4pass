package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;

/**
 * action entree clavier code
 *
 * @author Abdellah
 */
public class ActionEntreeClavierCode implements ActionItem {

    private final int cursorBegin = 6;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private PorteManager portemanager;
    private String mdpUn = new String();
    private String mdpDeux = new String();
    private String input = new String();

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        portemanager = PorteManager.getInstance();
        menu = Menu.getInstance();
        //  DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        mdpUn = porte.getClavierCodeUn();
        mdpDeux = porte.getClavierCodeDeux();
        input = "";
        cursor = cursorBegin;
        lcd.print("CODE DIGICODE", true);
        lcd.setCursor(2, 5);
        switch (porte.getInterphone().charAt(1)) {
            case '1':
            case '2':
                lcd.print("V", false);
                break;
            case '3':
                lcd.print("B", false);
                break;
            case '4':
                lcd.print("PRODUIT INADAPTE\n GIBRALTAR", true);
                break;
            default:
                lcd.print("PRODUIT INADAPTE\n INCONNU", true);
                break;
        }
        lcd.saisie(true);
    }
    //! \brief Méthode implémenter de l'interface ActionItem.
    //! \details Méthode appelée à chaque que l'on sort du menu de défilement des residents
    //! \li soit en apuyant sur 'X'
    //! \li soit en entrant un mauvais mot de pass
    //! \li soit si dépassemnt du timer
    //!
    //! Cette méthode suprime les timers utilisés.

    public void exit() {
        input = "";
        cursor = cursorBegin;

        /*
	** lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        //DataBase data = DataBase.getInstance();
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyB) {
            input = "";
            lcd.saisie(false);
            menu.father();
        }
        if (event instanceof EventKey0) {
            cursor++;
            lcd.print("*", false);
            input += "0";
        }
        if (event instanceof EventKey1) {
            cursor++;
            lcd.print("*", false);
            input += "1";
        }
        if (event instanceof EventKey2) {
            cursor++;
            lcd.print("*", false);
            input += "2";
        }
        if (event instanceof EventKey3) {
            cursor++;
            lcd.print("*", false);
            input += "3";
        }
        if (event instanceof EventKey4) {
            cursor++;
            lcd.print("*", false);
            input += "4";
        }
        if (event instanceof EventKey5) {
            cursor++;
            lcd.print("*", false);
            input += "5";
        }
        if (event instanceof EventKey6) {
            cursor++;
            lcd.print("*", false);
            input += "6";
        }
        if (event instanceof EventKey7) {
            cursor++;
            lcd.print("*", false);
            input += "7";
        }
        if (event instanceof EventKey8) {
            cursor++;
            lcd.print("*", false);
            input += "8";
        }
        if (event instanceof EventKey9) {
            cursor++;
            lcd.print("*", false);
            input += "9";
        }
        if (event instanceof EventKeyP) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print("    ", false);
            lcd.setCursor(2, cursor);
            input = "";
        }
        if (input.length() >= 4) {
            lcd.saisie(false);

            portemanager.init();
            portemanager.request(input);
            cursor = cursorBegin;
            input = "";
        }
    }
}
