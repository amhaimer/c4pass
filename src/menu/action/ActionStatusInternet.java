package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;

import java.io.*;

/**
 * Action Status Internet
 *
 * @author Abdellah
 */
public class ActionStatusInternet implements ActionItem {

    private Menu menu;
    private Lcd lcd;

    private Porte porte = Porte.getInstance();

    private Process checkConnection;
    private Process adresseIp;

    /**
     * init
     */
    @Override
    public void init() {

        menu = Menu.getInstance();
        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("TEST EN COURS...", false);
        try {
            checkConnection = Runtime.getRuntime().exec("ping -c 1 -W2 google.com");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            adresseIp = Runtime.getRuntime().exec("hostname -I");
        } catch (IOException e) {
            System.out.println(e);
        }

        BufferedReader tryConn = new BufferedReader(new InputStreamReader(checkConnection.getInputStream()));
        BufferedReader adresse = new BufferedReader(new InputStreamReader(adresseIp.getInputStream()));

        String s;
        String m = "";
        String s1;
        String m1 = "";

        try {
            while ((s = adresse.readLine()) != null) {
                //System.out.println(s);
                m += s;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s1 = tryConn.readLine()) != null) {
                //System.out.println(s1);
                m1 += s1;
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        /*System.out.println("--conn
        
        ection ping--->" + m1);
	System.out.println("--addresse ip--->" + m);
         */
        lcd.setCursor(2, 0);

        if (porte.getValidConnexionWeb().equals("1")) {
            if (m.length() == 0) {
                lcd.print("NC= NON CONNECTE", false);
            } else {
                if (m1.length() == 0) {
                    lcd.print("X=INTERNETABSENT", false);
                } else {
                    lcd.print("@= INTERNET     ", false);
                }
            }
        } else {
            lcd.print("DESACTIVE       ", false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);
        if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
