package menu.action.modifEL1;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Entre Libre Un choix
 *
 * @author Abdellah
 */
public class ActionModifierEntreLibreUn_choix implements ActionItem {

    private Menu menu;
    private String langue = "";
    private String OuiNon = "";
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    //private char input[];
    private String input = null;
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        //input = porte.getChoixEntreLibreUn().toCharArray();
        input = porte.getChoixEntreLibreUn();

        lcd.print(menu.toString(), true);

        langue = porte.getTraduction();
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        lcd.print(data.getTraduction(porte.getChoixEntreLibreUn().equals("-1") ? "nonoui" : "ouinon", langue), false);
        //lcd.setCursor(2,13);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 0);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("nonoui", langue), false);
            //input[0]= '0';
            input = "-1";
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("ouinon", langue), false);
            //   input[0]= '1';
            input = "0";
        } else if (event instanceof EventKeyP) {
            lcd.print("---", false);
            //  input[0]= '-';
            input = "-";
        } else if (event instanceof EventKeyValid) {

            /*   porte.setChoixEntreLibreUn(String.valueOf(input));
	    data.update_EntreLibreUn(porte);		
	    if(input[0] == '0') {
		menu.father();
	    }
	    else {
		menu.next();
	    }
	}*/
            if ("-1".equals(input)) {
                OuiNon = data.getTraduction("non", langue);
            } else if ("0".equals(input)) {
                OuiNon = data.getTraduction("oui", langue);
            }
            data.addEvent(data.getTraduction("menuprog", langue), OuiNon, data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("validerporte1", langue));
            System.out.println(input);
            if (input.equals("-1")) {
                porte.setChoixEntreLibreUn(input);
                data.update_EntreLibreUn(porte);
                menu.father();
            } else {
                input = porte.getChoixEntreLibreUn();
                switch (input) {
                    case "-1":
                        lcd.print(data.getTraduction("activerleprofil", langue), false);
                        //lcd.print("ACTIVEZ LEPROFIL",true);
                        //lcd.setCursor(2,0);
                        //lcd.print(data.getTraduction("logidiese.com",langue),false);
                        //lcd.print("-\"LOGIDIESE.COM\"", false);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            System.err.print(ex.getMessage());
                        }
                        break;

                    case "0":
                        lcd.print(data.getTraduction("profiltemporel", langue), false);
                        //lcd.print("PROFIL TEMPOREL",true);
                        lcd.setCursor(2, 0);
                        lcd.print(data.getTraduction("permanent", langue), false);
                        //lcd.print("---PERMANENT---", false);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            System.err.print(ex.getMessage());
                        }
                        break;

                    default:
                        lcd.print(data.getTraduction("profiltemporaire", langue), true);
                        //lcd.print("PROFILTEMPORAIRE",true);
                        lcd.setCursor(2, 0);
                        lcd.print(data.getTraduction("logidiese:", langue) + input, false);
                        //lcd.print("LOGIDIESE: " + input, false);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            System.err.print(ex.getMessage());
                        }
                        break;

                }
                menu.father();
            }
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
