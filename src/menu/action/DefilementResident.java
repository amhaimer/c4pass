package menu.action;

import event.Event;
import menu.Menu;

import event.eventType.*;
import event.EventManager;
import devices.Lcd;
import java.util.*;
import sql.DataBase;
import sql.data.Resident;
import sip.SipClient;

/**
 * action de Defilement des Resident par le clavier
 *
 * @author Abdellah
 */
public class DefilementResident implements ActionItem {

    devices.TimerLocal timer = null;
    devices.TimerLocal timercallAsterisk = null;
    devices.TimerLocal timercallOVH = null;

    int timecallAsterisk;
    int timecallOVH;
    boolean renvoieAppel;
    boolean nextCall = false;
    boolean previousCall = false;
    Menu menu;
    private Lcd lcd = Lcd.getInstance();
    DataBase data = DataBase.getInstance();
    List<Resident> residents = data.getResidents();
    SipClient sip = new SipClient();

    private int c = 0;

    public static String codeResident = "";
    public static String dInput = new String();
    private Event lastEvent;
    ListIterator itr = residents.listIterator();
    EventManager manager = EventManager.getInstance();
    boolean vide = false;
    // ActionAppelerCodeResident codeAppel = new ActionAppelerCodeResident();
    Resident res = new Resident();

    /**
     * init
     */
    @Override
    public void init() {
        residents = data.getResidents();
        lastEvent = new EventKeyValid();
        itr = residents.listIterator();
        timer = manager.requestTimer(30, devices.TimerLocal.Unit.sec);
        vide = false;
        dInput = "";
        if (itr.hasNext()) {
            res = (Resident) itr.next();
        } else {
            lcd.print("AUCUN RESIDENT\nENREGISTRE", true);
            vide = true;
            return;
        }
        nextCall = true;
        lcd.print(res.getName(), true);
        lcd.setCursor(2, 0);
        lcd.print("CODE D'APPEL:" + res.getCode(), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        manager.releaseTimer(timer);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();

        /*
	** si touche '>' alors montrer prochain résidents
         */
        if (vide == true && !(event instanceof EventTimer)) {
            menu.father();
        } else {
            if (event instanceof EventKeyRight) {
                nextCall = true;
                if (previousCall) {
                    previousCall = false;
                    if (itr.hasNext()) {
                        res = (Resident) itr.next();
                    }
                }
                if (itr.hasNext()) {
                    res = (Resident) itr.next();
                } else {
                    itr = residents.listIterator();
                    res = (Resident) itr.next();
                }
                lcd.print(res.getName(), true);
                lcd.setCursor(2, 0);
                lcd.print("CODE D'APPEL:" + res.getCode(), false);
            } /*
	    ** si touche '<' alors montrer résidents précedents
             */ else if (event instanceof EventKeyLeft) {
                previousCall = true;
                if (nextCall) {
                    nextCall = false;
                    if (itr.hasPrevious()) {
                        res = (Resident) itr.previous();
                    }
                }
                if (itr.hasPrevious()) {
                    res = (Resident) itr.previous();
                } else {
                    while (itr.hasNext()) {
                        res = (Resident) itr.next();
                    }
                }
                lcd.print(res.getName(), true);
                lcd.setCursor(2, 0);
                lcd.print("CODE D'APPEL:" + res.getCode(), false);
            } /*
	    ** si touche 'A' appel du résidents
             */ else if (event instanceof EventKeyValid) {
                codeResident = res.getCode();
                System.out.println(codeResident);
                menu.child();
            } else if (event instanceof EventKeyCancel) {
                sip.terminate();
                sip.unregister();
            } else if (event instanceof EventKeyP) {
                menu.father();
                menu.child();
            } else if (event instanceof EventTimer) {
                exit();
                menu.father();
            } /*
	    ** si touche 'X' retour menu
             */ else if (event instanceof EventKeyB) {
                itr = residents.listIterator();
                menu.father();
            } else {
                /*c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
		if(event instanceof EventKey1){
		    input = String.valueOf(lettre[0+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey2){
		    input = String.valueOf(lettre[3+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey3){
		    input = String.valueOf(lettre[6+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey4){
		    input = String.valueOf(lettre[9+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey5){
		    input = String.valueOf(lettre[12+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey6){
		    input = String.valueOf(lettre[15+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey7)
		    input = String.valueOf(lettre[18+c]);
		if(event instanceof EventKey8) {
		    input = String.valueOf(lettre[22+c]);
		    c = c == 2 ? c + 1 : c;
		}
		if(event instanceof EventKey9)
		    input = String.valueOf(lettre[25+c]);
		if(event instanceof EventKey0) {
		    input = String.valueOf(lettre[29+c]);
		    c = c == 2 ? c + 1 : c;
		}
		boolean found = false;
		itr = residents.listIterator();
		while (itr.hasNext()) {
		    res = (Resident)itr.next();
		    if (res.getName().toUpperCase().startsWith(input)) {
			found = true;
			lcd.print(res.getName(), true);
			lcd.setCursor(2,0);
			lcd.print("CODE D'APPEL:"+ res.getCode(), false);
			break;
		    }
		}
		if (!found)
		    lcd.print("AUCUN NOM POUR\nCETTE LETTRE", true);
		c = c < 3 ? c + 1 : 0;*/

                if (event instanceof EventKey0 || event instanceof EventKey1 || event instanceof EventKey2
                        || event instanceof EventKey3 || event instanceof EventKey4 || event instanceof EventKey5
                        || event instanceof EventKey6 || event instanceof EventKey7 || event instanceof EventKey8
                        || event instanceof EventKey9) {
                    menu.next();
                }
            }
            lastEvent = event;
        }
    }
}
