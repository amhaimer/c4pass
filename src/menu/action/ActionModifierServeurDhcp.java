package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * Action Modifier Serveur Dhcp
 *
 * @author Abdellah
 */
public class ActionModifierServeurDhcp implements ActionItem {

    private Process p2;
    private ProcessBuilder pb2;
    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    private String langue = "";
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getValidServeurClientDhcp().toCharArray();
    int count = 0;
    private Process ip;
    private String s = "";
    private String m = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();

        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("nonoui", langue), false);
        //lcd.print("OUI=1 NON=0: ",false);
        lcd.setCursor(2, 13);
        if (porte.getValidServeurClientDhcp().equals("1")) {
            //lcd.print("OUI", false);
            lcd.print(data.getTraduction("oui", langue), false);
        } else {
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON", false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON",false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI",false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;

            String mode = porte.getValidServeurClientDhcp();
            porte.setValidServeurClientDhcp(String.valueOf(input));
            data.update(porte);
            if (input[0] == '1') {
                if (mode.equals("0")) {
                    try {
                        pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/dhcpcd.conf /etc/dhcpcd.conf.dyn");
                        p2 = pb2.start();
                    } catch (IOException e) {
                        System.out.println("ERROR");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                    }
                    try {
                        pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/dhcpcd.conf.fixe /etc/dhcpcd.conf");
                        p2 = pb2.start();
                    } catch (IOException e) {
                        System.out.println("ERROR");
                    }
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                menu.child();
            } else if (input[0] == '0') {
                if (mode.equals("1")) {
                    try {
                        pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/dhcpcd.conf /etc/dhcpcd.conf.fixe");
                        p2 = pb2.start();
                    } catch (IOException e) {
                        System.out.println("ERROR");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                    }
                    try {
                        pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/dhcpcd.conf.dyn /etc/dhcpcd.conf");
                        p2 = pb2.start();
                    } catch (IOException e) {
                        System.out.println("ERROR");
                    }
                }
                lcd.clearLcd();
                lcd.print(data.getTraduction("configdhcp", langue), false);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/networking restart");
                    p2 = pb2.start();
                    System.out.println("networking restart...");
                } catch (IOException e) {
                    System.out.println("ERROR networking");
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "ifup eth0");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR eth0");
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "ifup wlan0");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR wlan0");
                }

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "ifup eth0");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR eth0");
                }

                m = "";
                s = "";
                try {
                    ip = Runtime.getRuntime().exec("hostname -I");
                } catch (IOException e) {
                    System.out.println(e);
                }

                BufferedReader adresse = new BufferedReader(new InputStreamReader(ip.getInputStream()));

                try {
                    while ((s = adresse.readLine()) != null) {
                        System.out.println(s);
                        m += s;
                    }
                } catch (IOException e) {
                    System.out.println(e);
                }
                System.out.println("-----------------------------------------\n" + m + "\n-----------------------------------------------");
                if (m.length() > 17) {
                    String[] ip = m.split(" ");
                    m = ip[0];
                }
                if (m.length() > 30) {
                    String[] ip = m.split(" ");
                    m = ip[1];
                }

                lcd.clearLcd();
                //lcd.print("ADRESSE DHCP    \nIP:"+m+"      ",false);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }

                lcd.setCursor(1, 0);
                lcd.print("REDEMARRAGE ...  ", false);
                try {
                    pb2 = new ProcessBuilder("sudo", "reboot", "-f");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR rebooting");
                }
                menu.father();
            }
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        } else if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        }
    }
}
