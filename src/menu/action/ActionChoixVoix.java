package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 *
 *
 * @author Abdellah
 */
public class ActionChoixVoix implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private char input[] = new char[1];
    private char choix1, choix2, current;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        Lcd lcd = Lcd.getInstance();
        Menu menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        choix1 = porte.getSignalSonore().charAt(0);
        choix2 = porte.getSignalSonore().charAt(1);
        //lcd.print("OUI=1 NON=0:", false);
        lcd.print(data.getTraduction("nonoui", langue), false);
        current = menu.toString().charAt(11);
        input[0] = current == '1' ? choix1 : choix2;
        if (current == '1') {
            lcd.print(choix1 == '2' || choix1 == '3' ? data.getTraduction("oui", langue) : data.getTraduction("non", langue), false);
        } else {
            lcd.print(choix2 == '2' || choix2 == '3' ? data.getTraduction("oui", langue) : data.getTraduction("non", langue), false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        /* 
	** lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 12);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("non", langue), false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("oui", langue), false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            String value = porte.getSignalSonore();
            String result = new String();
            if (current == '1' && input[0] == '0' && (choix1 == '1' || choix1 == '3')) {
                input[0] = '1';
            } else if (current == '1' && input[0] == '1' && (choix1 == '1' || choix1 == '3')) {
                input[0] = '3';
            } else if (current == '2' && input[0] == '0' && (choix2 == '1' || choix2 == '3')) {
                input[0] = '2';
            } else if (current == '2' && input[0] == '1' && (choix2 == '1' || choix2 == '3')) {
                input[0] = '3';
            }
            result = current == '2' ? value.substring(0, 1) + input[0] + value.substring(2) : input[0] + value.substring(1);
            porte.setSignalSonore(result);
            data.update(porte);
            if (current == '1') {
                menu.next();
                data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("voix1", langue), result, " ");
            } else {
                menu.father();
                data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("voix2", langue), result, " ");
            }
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
