package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * action de modification IP Local
 *
 * @author Abdellah
 */
public class ActionIpLocal implements ActionItem {

    private final int cursorBegin = 0;
    private final int cursorEnd = 15;
    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char[] input = new char[15];

    /**
     * init
     */
    @Override
    public void init() {

        data = DataBase.getInstance();
        porte = Porte.getInstance();
        menu = Menu.getInstance();

        cursor = cursorBegin;
        lcd = Lcd.getInstance();

        lcd.clearLcd();
        lcd.print(" SERVEUR #LOCAL" + "\n", true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getIpInterne(), false);
        lcd.setCursor(2, cursor);
        for (int i = 0; i < porte.getIpInterne().length(); i++) {
            input[i] = porte.getIpInterne().charAt(i);
        }
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        data = DataBase.getInstance();
        porte = Porte.getInstance();

        /*
	** if(c==0)
	** {
	** lcd.print(porte.getIpInterne(),false);
	** lcd.setCursor(2,cursor);
	** }
	** c++;
         */
        if (cursor >= cursorEnd) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }

        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor] = '9';
            cursor++;
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor > 15) {
                cursor = 0;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = (porte.getIpInterne().length() - 1);
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyValid) {
            porte.setIpInterne(String.valueOf(input));
            data.update(porte);
            switch (porte.getServeurIP().charAt(1)) {
                case 'D':
                    menu.father();
                    break;
                case 'C':
                    menu.next();
                    break;
                case 'O':
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    break;
                case 'V':
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();

                    /*
                    ** lcd.print(" SERVEUR VIDEO"+"\n",true);
                    ** lcd.setCursor(2,0);
                    ** lcd.print(" voipdiese.com",true);
                    ** menu.father();
                     */ break;
                default:
                    break;
            }
        } else if (event instanceof EventKeyB) {

            menu.father();
        } else if (event instanceof EventKeyP) {
            for (int i = 0; i < 15; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print(" ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        if ((cursor + 1) % 4 == 0 && cursor < 15 && cursor > 0) {
            lcd.print(".", false);
            input[cursor] = '.';
            cursor++;
        }
    }
}
