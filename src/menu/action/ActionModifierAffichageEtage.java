package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Affichage Etage
 *
 * @author Abdellah
 */
public class ActionModifierAffichageEtage implements ActionItem {

    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getAffichageEtage().toCharArray();
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
        lcd.setCursor(2, 13);
        if (porte.getAffichageEtage().equals("1")) {
            lcd.print("OUI", false);
        } else {
            lcd.print("NON", false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print("NON", false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print("OUI", false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;

            porte.setAffichageEtage(String.valueOf(input));
            data.update(porte);
            menu.father();
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        }
    }
}
