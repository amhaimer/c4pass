package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Appel Texto
 *
 * @author Abdellah
 */
public class ActionModifierAppelTexto implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private String base;
    private boolean active;
    private String Oui_Non = "";
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        base = porte.getAppelAbrege();
        langue = porte.getTraduction();
        active = base.equals("4");
        lcd.print(data.getTraduction("texto", langue), true);
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0:", false);
        //lcd.print(active ? "OUI" : "NON", false);
        lcd.print(active ? data.getTraduction("ouinon", langue) : data.getTraduction("nonoui", langue), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 12);
        if (event instanceof EventKey0) {
            //lcd.print("NON",false);
            lcd.print(data.getTraduction("non", langue), false);
            active = false;
            Oui_Non = data.getTraduction("non", langue);
        } else if (event instanceof EventKey1) {
            //lcd.print("OUI",false);
            lcd.print(data.getTraduction("oui", langue), false);
            active = true;
            Oui_Non = data.getTraduction("oui", langue);
        } else if (event instanceof EventKeyValid) {
            if (active) {
                porte.setAppelAbrege("4");
            } else {
                porte.setAppelAbrege(base.equals("4") ? "3" : base);
            }
            data.update(porte);
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("texto_e", langue), Oui_Non);
            menu.father();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
