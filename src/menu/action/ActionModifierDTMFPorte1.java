package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.DataBase;

/**
 * Action Modifier DTMF Porte1
 *
 * @author Abdellah
 */
public class ActionModifierDTMFPorte1 implements ActionItem {

    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private final char[] touches = "123456789*0#".toCharArray();

    /**
     * init
     */
    @Override
    public void init() {
        int i = -1;
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        while (touches[++i] != porte.getTouchesDTMF().charAt(0));
        cursor = i;
        lcd.print("FLECHER <<>> : ", false);
        lcd.print(String.valueOf(touches[cursor]), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event avenement
     */
    @Override
    public void action(Event event) {

        if (event instanceof EventKeyB) {
            menu.father();
        } else if (event instanceof EventKeyRight) {
            cursor++;
        } else if (event instanceof EventKeyLeft) {
            cursor--;
        } else if (event instanceof EventKeyValid) {
            porte.setTouchesDTMF(touches[cursor] + porte.getTouchesDTMF().substring(1));
            data.update(porte);
            data.addEvent("SAISIE ENR.PORTE", "Modification de", "Note DTMFRelais1", " ", " ");
            menu.next();
        }
        if (cursor == 12) {
            cursor = 0;
        } else if (cursor == -1) {
            cursor = 11;
        }
        lcd.setCursor(2, 15);
        lcd.print(String.valueOf(touches[cursor]), false);
    }
}
