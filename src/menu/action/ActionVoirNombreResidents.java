package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Voir Nombre Residents
 *
 * @author Abdellah
 */
public class ActionVoirNombreResidents implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();

    /**
     * init
     */
    @Override
    public void init() {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (!data.getNbResidents().equals("0")) {
            lcd.print(menu.toString(), true);
            lcd.setCursor(2, 0);
            lcd.print(data.getNbResidents() + "=", false);
            lcd.print(data.getNbResidentsListeBlanche() + "BL+", false);
            lcd.print(data.getNbResidentsListeRouge() + "RO", false);
        } else {
            lcd.print("AUCUN RESIDENT\n ENREGISTRE", true);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);

        if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
