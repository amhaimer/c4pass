package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier User Pwd Sip
 *
 * @author Abdellah
 */
public class ActionModifierUserPwdSip implements ActionItem {

    private final int cursorBegin = 5;
    private final int cursorEnd = 16;
    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    boolean boo;
    Event precevent = new EventKeyValid();
    int i;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char[] input = new char[24];
    private char[] inputUser = new char[12];
    private char[] inputMdp = new char[12];
    private int ligne = 1;
    private int indice;
    private int c = 0;
    char[] lettre = {' ', '.', ',', '@', '-', '_', '1', 'A', 'B', 'C', 'a', 'b', 'c', '2', 'D',
        'E', 'F', 'd', 'e', 'f', '3', 'G', 'H', 'I', 'g', 'h', 'i', '4', 'J', 'K', 'L', 'j', 'k', 'l',
        '5', 'M', 'N', 'O', 'm', 'n', 'o', '6', 'P', 'Q', 'R', 'S', 'p', 'q', 'r', 's', '7', 'T', 'U',
        'V', 't', 'u', 'v', '8', 'W', 'X', 'Y', 'Z', 'w', 'x', 'y', 'z', '9', '-', '\'', '0'};

    /**
     * init
     */
    @Override
    public void init() {

        porte = Porte.getInstance();
        indice = 0;
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        System.out.println(porte.getUserClient());
        lcd.print("user:" + porte.getUserClient() + "\n" + "MDP :" + porte.getMdpClient(), true);
        for (i = 0; i < porte.getUserClient().length(); i++) {
            input[i] = porte.getUserClient().charAt(i);
        }
        for (i = 13; i < porte.getMdpClient().length() + 13; i++) {
            input[i] = porte.getMdpClient().charAt(i - 13);
        }
        lcd.setCursor(ligne, cursor);
        cursor = cursorBegin;
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        boo = true;
        if (event instanceof EventTimer) {
            boo = false;
        }
        if (cursor == cursorEnd || cursor < cursorBegin) {
            cursor = cursorBegin;
        }
        lcd.setCursor(ligne, cursor);
        if (c > 7 || !(precevent.getClass().equals(event.getClass())) && boo) {
            c = 0;
        } else {
            c++;
        }
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[indice] = lettre[0 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[7 + c]), false);
            input[indice] = lettre[7 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[14 + c]), false);
            input[indice] = lettre[14 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[21 + c]), false);
            input[indice] = lettre[21 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[28 + c]), false);
            input[indice] = lettre[28 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[35 + c]), false);
            input[indice] = lettre[35 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[42 + c]), false);
            input[indice] = lettre[42 + c];
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[51 + c]), false);
            input[indice] = lettre[51 + c];
            if (c == 6) {
                c = c + 2;
            }
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[58 + c]), false);
            input[indice] = lettre[58 + c];
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[67 + c]), false);
            input[indice] = lettre[67 + c];
            if (c == 2) {
                c = c + 6;
            }
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            indice++;
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            indice--;
        }
        if (event instanceof EventKeyValid) {
            if (indice < 12) {
                indice = 13;
                cursor = cursorBegin;
                ligne = 2;
            } else {
                for (i = 0; i < 12; i++) {
                    inputUser[i] = input[i];
                    System.out.println(input[i]);
                }
                for (i = 13; i < 24; i++) {
                    inputMdp[i - 13] = input[i];
                    System.out.println(input[i]);
                }
                ligne = 1;
                String user = new String(inputUser);
                String mdp = new String(inputMdp);
                System.out.println(user);
                System.out.println(mdp);
                porte.setUserClient(user.trim());
                porte.setMdpClient(mdp.trim());
                data.update(porte);
                menu.father();
            }
        }
        System.out.println(indice);
        if (!(event instanceof EventTimer)) {
            precevent = event;
        }
    }
}
