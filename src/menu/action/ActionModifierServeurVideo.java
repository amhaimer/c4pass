package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Serveur Video
 *
 * @author Abdellah
 */
public class ActionModifierServeurVideo implements ActionItem {

    private Menu menu;
    private Lcd lcd;

    Event lastEvent;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();

    /**
     * init
     */
    @Override
    public void init() {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        lcd.print(" SERVEUR VIDEO" + "\n", false);
        lcd.setCursor(2, 0);
        lcd.print(" voipdiese.com", false);
        lcd.setCursor(2, 0);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyValid) {
            data = DataBase.getInstance();
            porte = Porte.getInstance();
            switch (porte.getServeurIP().charAt(1)) {
                case 'C':

                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    break;
                case 'O':

                    menu.previous();
                    menu.previous();
                    menu.previous();
                    break;
                case 'D':

                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    menu.previous();
                    break;
                case 'V':
                    menu.father();
                    break;
                default:
                    break;
            }
        }
        if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
