package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import devices.HpMicro;

/**
 * Action Modifier Niveau HP
 *
 * @author Abdellah
 */
public class ActionModifierNiveauHP implements ActionItem {

    private Process p2;
    private ProcessBuilder pb2;
    private final int cursorBegin = 5;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getNiveauHP().toCharArray();

    int count = 0;
    private String langue = "";
    HpMicro hp = HpMicro.getInstance();

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getNiveauHP(), false);

        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursor);
            lcd.print("0", false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursor);
            lcd.print("1", false);
            input[0] = '1';
        } else if (event instanceof EventKey2) {
            lcd.setCursor(2, cursor);
            lcd.print("2", false);
            input[0] = '2';
        } else if (event instanceof EventKey3) {
            lcd.setCursor(2, cursor);
            lcd.print("3", false);
            input[0] = '3';
        } else if (event instanceof EventKey4) {
            lcd.setCursor(2, cursor);
            lcd.print("4", false);
            input[0] = '4';
        } else if (event instanceof EventKey5) {
            lcd.setCursor(2, cursor);
            lcd.print("5", false);
            input[0] = '5';
        } else if (event instanceof EventKey6) {
            lcd.setCursor(2, cursor);
            lcd.print("6", false);
            input[0] = '6';
        } else if (event instanceof EventKey7) {
            lcd.setCursor(2, cursor);
            lcd.print("7", false);
            input[0] = '7';
        } else if (event instanceof EventKey8) {
            lcd.setCursor(2, cursor);
            lcd.print("8", false);
            input[0] = '8';
        } else if (event instanceof EventKey9) {
            lcd.setCursor(2, cursor);
            lcd.print("9", false);
            input[0] = '9';
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyValid) {
            count = 0;
            cursor = cursorBegin;
            porte.setNiveauHP(String.valueOf(input));
            data.update(porte);
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("niveauson", langue), String.valueOf(input));
            //lcd.print("REGLAGE DU HP\nEN COURS...", true);
            lcd.print(data.getTraduction("hp_ecriture", langue), true);
            //hp.set(input[0] - '0');
            /*if (input[0] == '1') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "80%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '2') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "82%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '3') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "84%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '4') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "86%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '5') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "88%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '6') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "90%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '7') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "95%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '8') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "100%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
		else if (input[0] == '9') {
			try {
				pb2 = new ProcessBuilder("sudo", "amixer", "set",  "PCM", "--", "100%");
				p2 = pb2.start();
			}
		 	catch(IOException e) {
				System.out.println("ERROR");
			}
		}
	    /*try {
			pb2 = new ProcessBuilder("sudo", "bash", "-c",  "reboot");
			p2 = pb2.start();
		    }
	    catch(IOException e) {
		System.out.println("ERROR rebooting");
	    }*/
            menu.father();
        } else if (event instanceof EventKeyP) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print(" ", false);
            lcd.setCursor(2, cursor);
            input[0] = ' ';
        } else if (event instanceof EventKeyB) {
            cursor = cursorBegin;
            count = 0;
            menu.father();
        }
    }
}
