package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Temps Communication
 *
 * @author Abdellah
 */
public class ActionModifierTempsCommunication implements ActionItem {

    private final int cursorBegin = 5;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    //private char input[] = porte.getTempsCommunication().toCharArray();
    private char input[];
    int count = 0;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();

        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        input = porte.getTempsCommunication().toCharArray();
        lcd.print(porte.getTempsCommunication(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (cursor > 7) {
            cursor = cursorBegin;
            lcd.setCursor(5, cursor);
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - 5] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - 5] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - 5] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - 5] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - 5] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - 5] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - 5] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - 5] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - 5] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - 5] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {
            count = 0;
            cursor = cursorBegin;
            porte.setTempsCommunication(String.valueOf(input));
            data.update(porte);
            //data.addEvent("SAISIE ENR.PORTE", "Modification du", "Temps comm"," "," ");
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("communication", langue), String.valueOf(input));
            menu.father();
        } else if (event instanceof EventKeyB) {
            cursor = cursorBegin;
            count = 0;
            menu.father();
        }
    }
}
