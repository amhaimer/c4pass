package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * recupération de l'adresse ip de ma platine
 *
 * @author Abdellah
 */
public class ActionAdresseIpWifi implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private Process adresseIp;
    private Process wlan0;

    /**
     * init
     */
    @Override
    public void init() {

        menu = Menu.getInstance();
        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        lcd.print(menu.toString(), true);

        try {
            adresseIp = Runtime.getRuntime().exec("hostname -I");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            wlan0 = Runtime.getRuntime().exec("ifconfig wlan0");
        } catch (IOException e) {
            System.out.println(e);
        }
        lcd.setCursor(2, 0);

        BufferedReader adresse = new BufferedReader(new InputStreamReader(adresseIp.getInputStream()));
        BufferedReader ifaceWlan0 = new BufferedReader(new InputStreamReader(wlan0.getInputStream()));

        String s;
        String m = "";

        String s1;
        String m1 = "";

        try {
            while ((s = adresse.readLine()) != null) {
                System.out.println(s);
                m += s;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s1 = ifaceWlan0.readLine()) != null) {
                System.out.println(s1);
                m1 += s1;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        System.out.println(m + "\n-----------------------------------------------");

        if (m.length() == 0) {
            //pas d'addresse ip	
            System.out.println("\n--------------------m=0---------------------------");
            lcd.print("NC: NON CONNECTE", false);
        } else if (m.length() < 17) {
            System.out.println("\n--------------------m<17---------------------------");
            if (m1.contains(m)) {
                if (m1.contains("169.244.")) {
                    lcd.print("R" + m, false);
                } else {
                    lcd.print("W" + m, false);
                }
                porte.setMdpWifiPi(m);
            } else {
                lcd.print("NC: NON CONNECTE", false);
            }

        } else if (m.length() > 17) {
            System.out.println("\n--------------------m>17---------------------------");
            String[] ip = m.split(" ");
            String ip1 = ip[0];
            String ip2 = ip[1];
            //on affiche ip du routeur
            if (m1.contains(ip1)) {
                if (ip1.contains("169.244.")) {
                    lcd.print("R" + ip1, false);
                } else {
                    lcd.print("W" + ip1, false);
                }
                porte.setMdpWifiPi(ip1);
            } else if (m1.contains(ip2)) {
                if (ip2.contains("169.244.")) {
                    lcd.print("R" + ip2, false);
                } else {
                    lcd.print("W" + ip2, false);
                }
                porte.setMdpWifiPi(ip2);
            }

        }

        data.update(porte);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);

        if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
