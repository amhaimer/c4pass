package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * Action Modifier Net mask
 *
 * @author Abdellah
 */
public class ActionModifierNetmask implements ActionItem {

    private Process p2;
    private ProcessBuilder pb2;
    private final int cursorBegin = 0;
    private final int cursorEnd = 15;
    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char[] input = new char[15];

    private String nbits;
    private String addip;
    private String netmask;

    /**
     * init
     */
    @Override
    public void init() {

        porte = Porte.getInstance();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd = Lcd.getInstance();

        lcd.clearLcd();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getNetmask(), false);
        lcd.setCursor(2, cursor);
        for (int i = 0; i < porte.getNetmask().length(); i++) {
            input[i] = porte.getNetmask().charAt(i);
        }
        String netm = new String(input);
        netmask = netm.replaceAll(" ", "");
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        data = DataBase.getInstance();
        porte = Porte.getInstance();

        /*
	** if(c==0)
	** {
	** lcd.print(porte.getIpInterne(),false);
	** lcd.setCursor(2,cursor);
	** }
	** c++;
         */
        if (cursor >= cursorEnd) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor] = '9';
            cursor++;
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor > 15) {
                cursor = 0;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = (porte.getNetmask().length() - 1);
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyValid) {
            String netm = new String(input);
            netmask = netm.replaceAll(" ", "");
            porte.setNetmask(netmask.trim());
            netmask = netmask.trim();

            switch (netmask) {
                case "255.255.255.255":
                    nbits = "/32";
                    break;
                case "255.255.255.254":
                    nbits = "/31";
                    break;
                case "255.255.255.252":
                    nbits = "/30";
                    break;
                case "255.255.255.248":
                    nbits = "/29";
                    break;
                case "255.255.255.240":
                    nbits = "/28";
                    break;
                case "255.255.255.224":
                    nbits = "/27";
                    break;
                case "255.255.255.192":
                    nbits = "/26";
                    break;
                case "255.255.255.128":
                    nbits = "/25";
                    break;
                case "255.255.255.0":
                    nbits = "/24";
                    break;
                case "255.255.254.0":
                    nbits = "/23";
                    break;
                case "255.255.252.0":
                    nbits = "/22";
                    break;
                case "255.255.248.0":
                    nbits = "/21";
                    break;
                case "255.255.240.0":
                    nbits = "/20";
                    break;
                case "255.255.224.0":
                    nbits = "/19";
                    break;
                case "255.255.192.0":
                    nbits = "/18";
                    break;
                case "255.255.128.0":
                    nbits = "/17";
                    break;
                case "255.255.0.0":
                    nbits = "/16";
                    break;
                case "255.254.0.0":
                    nbits = "/15";
                    break;
                case "255.252.0.0":
                    nbits = "/14";
                    break;
                case "255.248.0.0":
                    nbits = "/13";
                    break;
                case "255.240.0.0":
                    nbits = "/12";
                    break;
                case "255.224.0.0":
                    nbits = "/11";
                    break;
                case "255.192.0.0":
                    nbits = "/10";
                    break;
                case "255.128.0.0":
                    nbits = "/9";
                    break;
                case "255.0.0.0":
                    nbits = "/8";
                    break;
                case "254.0.0.0":
                    nbits = "/7";
                    break;
                case "252.0.0.0":
                    nbits = "/6";
                    break;
                case "248.0.0.0":
                    nbits = "/5";
                    break;
                case "240.0.0.0":
                    nbits = "/4";
                    break;
                case "224.0.0.0":
                    nbits = "/3";
                    break;
                case "192.0.0.0":
                    nbits = "/2";
                    break;
                default:
                    nbits = "/1";
                    break;
            }
            addip = porte.getIpPlatine() + nbits;

            try {
                /*
		** pb2 = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's/.*static ip_adress.static ip_adress="+01210+"\/"+45+"/g' /etc/dhcpcd_save.conf");
                 */
                pb2 = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's#.*static ip_address=.*#static ip_address=" + addip.trim() + "#g' /etc/dhcpcd.conf");
                p2 = pb2.start();
            } catch (IOException e) {
                System.out.println("ERROR");
            }
            data.update(porte);
            menu.next();
        } else if (event instanceof EventKeyB) {

            menu.father();
            menu.father();
        } else if (event instanceof EventKeyP) {
            for (int i = 0; i < 15; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print("               ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        if ((cursor + 1) % 4 == 0 && cursor < 15 && cursor > 0) {
            lcd.print(".", false);
            input[cursor] = '.';
            cursor++;
        }
    }
}
