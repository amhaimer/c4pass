package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Raccourci Appel
 *
 * @author Abdellah
 */
public class ActionModifierRaccourciAppel implements ActionItem {

    private int cursor = 15;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char input[];
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();

        porte = Porte.getInstance();
        langue = porte.getTraduction();
        input = porte.getAppelAbrege().toCharArray();
        lcd.print(data.getTraduction("raccourciappel", porte.getTraduction()), true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getAppelAbrege(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursor);
            lcd.print("0", false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursor);
            lcd.print("1", false);
            input[0] = '1';
        } else if (event instanceof EventKey2) {
            lcd.setCursor(2, cursor);
            lcd.print("2", false);
            input[0] = '2';
        } else if (event instanceof EventKey3) {
            lcd.setCursor(2, cursor);
            lcd.print("3", false);
            input[0] = '3';
        } else if (event instanceof EventKeyValid) {
            porte.setAppelAbrege(String.valueOf(input));
            data.update(porte);
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("raccourciappell3", langue), String.valueOf(input));
            menu.next();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
