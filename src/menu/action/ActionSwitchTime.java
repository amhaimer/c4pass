package menu.action;

import event.Event;
import event.EventManager;
import menu.Menu;
import menu.PorteManager;
import event.eventType.*;
import devices.Lcd;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;
import utils.JsonParse;

import utils.MajResidents;
import devices.HpMicro;
import java.util.Calendar;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static utils.DownloadVersion.RunCMD;

/**
 * l'action qui fait l'affichage de l'acceuil
 *
 * @author Abdellah
 */
public class ActionSwitchTime implements ActionItem {

    public static boolean cnx = false, last_cnx, DHCP;
    public static String HOSTIP, SERVERIP, MASK, SSID, PWD, CHOIXIPBX, DGW;
    public static boolean lstCALL = false;
    public static String premierchiffre = "";

    devices.TimerLocal timer;
    HpMicro hp = HpMicro.getInstance();
    private DataBase data = DataBase.getInstance();

    private int compteur = -2;
    String format1 = new String();
    private Lcd lcd = Lcd.getInstance();

    private Porte porte = Porte.getInstance();
    private EventManager manager = EventManager.getInstance();
    private int delai = Integer.parseInt(porte.getTimeConnexionWeb()) / 4;

    private static String test = "";

    private static String code = "";

    private long registertime = System.currentTimeMillis(), tmp_porteNULL = System.currentTimeMillis() / 1000, vpnTime = System.currentTimeMillis(), debut_time = System.currentTimeMillis() / 1000;

    private Process p2;
    private ProcessBuilder pb2;

    private int tempregitration = Integer.parseInt(porte.getTempoSip()) * 1000;
    //private int tempregitration = Integer.parseInt(porte.getTempoSip())*1000;
    boolean bel = false, checkCnx = false, turn = false, switched = false;
    private static int oldest = 0;
    private int state_co = 0;

    private String valididentitelsip = "";
    private static String change = "";

    private static boolean first_connexion = false;

    /**
     * init
     */
    @Override
    public void init() {

        PorteManager portemanager = PorteManager.getInstance();

        Menu menu = Menu.getInstance();
        manager = EventManager.getInstance();
        Calendar now = Calendar.getInstance();
        data = DataBase.getInstance();
        porte = Porte.getInstance();
        MajResidents majResidents = new MajResidents();
        String hour = String.format("%02d", now.get(Calendar.HOUR_OF_DAY));
        String minute = String.format("%02d", now.get(Calendar.MINUTE));

        String langue = porte.getTraduction();
        lcd.initS();

        int tempconnexionweb = Integer.parseInt(porte.getTimeConnexionWeb());
        int tempconnexion = tempconnexionweb / 4;
        int tempvpn = Integer.parseInt(porte.getTempoVpn()) * 1000;

        if (tempconnexion != delai) {
            manager.releaseTimer(timer);
            delai = Integer.parseInt(porte.getTimeConnexionWeb()) / 4;
            compteur = 0;
        }

        if ((System.currentTimeMillis() / 1000) - debut_time > tempconnexionweb / 2 || !first_connexion) {
            first_connexion = true;
            checkCnx = true;
            debut_time = System.currentTimeMillis() / 1000;
        }
        if (porte.getValidConnexionWeb().equals("1")) {
            if (checkCnx) {
                if (JsonParse.checkOver) {
                    new Thread(() -> {
                        JsonParse.tryConnection();
                    }).start();
                }
                cnx = JsonParse.conn;
                if (cnx) {
                    test = "@";
                    state_co = 1;
                } else {
                    test = "X";
                    state_co = 0;
                }
            }
        } else {
            test = "-";
            state_co = 0;
        }

        portemanager.init();

        if (portemanager.entreeLibre()) {
            /*   if (lstCALL) {
                new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                    @Override
                    public void run() {
                        int tempoporte = Integer.parseInt(porte.getTempsOuvertureUn()) > Integer.parseInt(porte.getTempsOuvertureDeux()) ? Integer.parseInt(porte.getTempsOuvertureUn()) : Integer.parseInt(porte.getTempsOuvertureDeux());

                        for (int i = 0; i < tempoporte*2; i++) {
                            Relais.getInstance().setStatusRelais1(0);
                             Relais.getInstance().setStatusRelais2(0);
                              try {
                            Thread.sleep(500);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(ActionSwitchTime.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        }
                       
                       
                    }
                },
                        10
                );

            }
             */
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(ActionSwitchTime.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (switched) {
            switched = false;
            lcd.print(center(porte.getNomAcces()) + "\n" + "" + center(hour + /*rasp_type*/ ":" + minute + "-" + test /*+ vpn_state + (porte.getNumPorte().length() < 4 ? porte.getNumPorte() : "") + " " + sip1_char + sip2_char + " " + cpu_conso*/), true);

            //lcd.print(pjreg1+pjreg2, false);
            /*  if (checkCnx && turn && porte.getValidConnexionWeb().equals("1")) {
                //cnx = json.tryConnection();
                new Thread(() -> {
                    if (cnx) {
                        JsonParse.updateResidents();
                        majResidents.majPorte(0);
                        checkCnx = false;
                    }
                    turn = false;
                    lcd.setCursor(1, 15);
                    //  lcd.print("L", false);

                    // manager.releaseTimer(timer);
                    delai = Integer.parseInt(porte.getTimeConnexionWeb()) / 4;
                    if (delai == 0) {
                        delai = 1;
                    }
                }).start();

            }*/
        } else {
            switched = true;
            String sub;
            String sub2 = "";
            if (porte.getInterphone().charAt(0) != '4' && Integer.parseInt(porte.getVisuelBadge()) == 0) {
                if (porte.getMessageUtilisation().length() > 16) {
                    if (porte.getMessageUtilisation().charAt(16) == ' ') {
                        sub = porte.getMessageUtilisation().substring(0, 16);
                        sub2 = porte.getMessageUtilisation().substring(16);
                    } else {
                        String tmp1 = porte.getMessageUtilisation().substring(0, 16);
                        int lstspace = lastSpace(tmp1);
                        sub = porte.getMessageUtilisation().substring(0, lstspace);
                        sub2 = porte.getMessageUtilisation().substring(lstspace, porte.getMessageUtilisation().length());
                    }
                } else {
                    sub = porte.getMessageUtilisation().substring(0, porte.getMessageUtilisation().length());
                }
                lcd.print(center(sub), true);
                lcd.setCursor(2, 0);
                if (porte.getMessageUtilisation().length() > 16) {
                    lcd.print(center(sub2), false);
                }
            }
        }

        last_cnx = cnx;

        /*    if (checkCnx && !turn && porte.getValidConnexionWeb().equals("1")) {
            //cnx = json.tryConnection();	
            new Thread(() -> {
                if (cnx) {

                    boolean is_json = true;
                    JsonParse.updatePorte();
                    JsonParse.updateProfil();
                    majResidents.maj();
                    is_json = majResidents.majEvent();
//			if (is_json == false) {
//				test = "PX";
//				state_co = 2;
//			}
                    checkCnx = false;
                    turn = true;

                }
            }).start();
        }*/
        if (state_co != oldest) {
            switch (test) {
                case "@":
                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requete_internet", langue),
                            data.getTraduction("connexion", langue), data.getTraduction("web", langue), data.getTraduction("effectue", langue));
                    break;
                case "X":
                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requete_internet", langue),
                            data.getTraduction("connexion", langue), data.getTraduction("web", langue), data.getTraduction("echouee", langue));
                    break;
                case "PX":
                    data.addEvent(data.getTraduction("assistance", langue), data.getTraduction("requete_internet", langue),
                            data.getTraduction("connexion", langue), data.getTraduction("web", langue), data.getTraduction("impaye", langue));
                    break;
                default:
                    break;
            }
            oldest = state_co;
        }

        if (compteur >= tempconnexionweb) {
            compteur = 0;
        }
        if ((System.currentTimeMillis() - registertime) >= tempregitration) {
            registertime = System.currentTimeMillis();
        }
        if ((System.currentTimeMillis() - vpnTime) >= tempvpn) {
            vpnTime = System.currentTimeMillis();
        }

        //System.out.println(ANSI_BLUE + (System.currentTimeMillis() - registertime)+ ANSI_RESET);
        compteur++;
        format1 = String.format("%02d", compteur);
        timer = manager.requestTimer(2001, devices.TimerLocal.Unit.millis);

        //Update du niveau HP
        change = porte.getValidIdentitelSIP();

        change = porte.getValidIdentitelSIP();
//	System.out.println("\nchange = "+change+"              valididentitelsip = "+valididentitelsip+"\n");
        if (!change.equals(valididentitelsip)) {

        }
        if ((System.currentTimeMillis() / 1000) - tmp_porteNULL > 60) {
            tmp_porteNULL = System.currentTimeMillis() / 1000;
            if (isChnagedDHCPParam()) {
                CHANGEIPPARAM();
            }
            if (isChnagedWIFIParam()) {
                CHANGEIPPARAMWIFI();
            }
            if (isChnagedDGW()) {
                CHANGEDGW();
            }
            if (isChnagedchoixIPBX()) {
                System.exit(1);
            }

            Porte.INSTANCE = null;
            DataBase.getInstance().closeconn();

        }

    }

    /**
     * executer une commande cmd
     *
     * @param cmd la commande à executer
     * @return le retour de la commande
     */
    public static String execCmd(String cmd) {
        try {
            java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(cmd).getInputStream()).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * execution command sans retour
     *
     * @param cmd la commande à executer
     */
    public void cmdVpn(String cmd) {
        try {
            pb2 = new ProcessBuilder("sudo", "bash", "-c", cmd);
            p2 = pb2.start();

        } catch (IOException e) {
            System.out.println("ERROR");
        }
        timerSleep(500);
    }

    /**
     * redemarrer la platine
     */
    public void restartNetworkReboot() {

        System.out.println("reboot");
        cmdVpn("reboot -f");
    }

    /**
     * timer sleep avec gestion d'exception
     *
     * @param time le temps du timer sleep
     */
    public void timerSleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        manager = EventManager.getInstance();
        manager.releaseTimer(timer);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        Menu menu = Menu.getInstance();
        // appel magelan
        DataBase data1 = DataBase.getInstance();
        Porte porte = data1.getPorte();
        String input = new String();
        Lcd lcd = Lcd.getInstance();

        if (event instanceof EventBoutonAppel) {
            String codeAccueil = porte.getCodeAccueil();
            System.out.println("BOUTON APPEL APPUYE");
            if (codeAccueil == null || codeAccueil.length() != 3) {
                lcd.print("-CODE D'ACCUEIL-\n-NON PROGRAMME -", true);
            } else if (data.getResident(codeAccueil).getId() == null) {
                lcd.print("-CODE D'ACCUEIL-\n--" + codeAccueil + " INCONNU --", true);
            } else {
                DefilementResident.codeResident = codeAccueil;
                menu.child();
                menu.next();
                menu.next();
                menu.child();
            }
        }
        if (event instanceof EventKeyLeft || event instanceof EventKeyRight) {
            menu.child();
            menu.next();
            menu.next();
        }
        // taper 1 pour entrer code resident

        if (event instanceof EventKey1 || event instanceof EventKey2 || event instanceof EventKey3
                || event instanceof EventKey4 || event instanceof EventKey5 || event instanceof EventKey6
                || event instanceof EventKey7 || event instanceof EventKey8 || event instanceof EventKey9 || event instanceof EventKey0) {
            menu.child();
            String AppelAbrege = porte.getAppelAbrege();
            if (AppelAbrege.equals("4")) {
                for (int i = 0; i < 2; i++) {
                    menu.next();
                }
            } else {
                for (int i = 0; i < 3; i++) {
                    menu.next();
                }
            }

            menu.itemChanged = false;
            menu.init();
            menu.currentItem.execute(event);
        }

        if (event instanceof EventKeyP) {
            menu.child();
        }
        if (event instanceof EventKeyB) {
            menu.child();
            menu.next();
        }
        if (event instanceof EventTimer) {

            if (event.toString().equals("switch")) {
                menu.previous();
            }
        }
    }

    /**
     * appel avec le code
     *
     * @param event evenement
     */
    public void appelAbrege(Event event) {
        porte = Porte.getInstance();
        if (porte.getAppelAbrege().equals("4")) {
            //appelTexto(event);
            return;
        }
        if (code.length() != Integer.parseInt(porte.getAppelAbrege())) {
            code += event.toString();
            System.out.println(code);
            return;
        }
        while (code.length() < 3) {
            code = "0" + code;
        }

        System.out.println("Final :" + code);
        Menu menu = Menu.getInstance();
        Resident resident = data.getResident(code);
        code = "";
        if (resident.getCode() == null) {
            lcd.print("AUCUN RESIDENT\nENREGISTRE", true);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
            }
            return;
        }
        lcd.print(resident.getListeRouge().equals("0") ? resident.getName() : "", true);
        lcd.setCursor(2, 0);
        lcd.print("CODE D'APPEL:" + resident.getCode(), false);
        DefilementResident.codeResident = resident.getCode();
        menu.child();
        menu.next();
        menu.next();
        menu.child();
    }

    /**
     * renvoi le type du serveur d'appel
     *
     * @param nb serveur d'appel 1 ou 2
     * @return le type de serveur
     */
    public int getTypeTechno(int nb) {
        int type = -1;
        char techno = porte.getServeurIP().charAt(nb == 1 ? 0 : 1);
        if (techno == '-') {
            techno = porte.getServeurIP().charAt(0);
        }
        switch (techno) {
            case 'C':
                type = 0; // client
                break;
            case 'O':
                type = 1; // ovh
                break;
            case 'D':
                type = 2; // asterisk
                break;
            case 'V':
                type = 3; // diese
                break;
            default:
                break;
        }
        return type;
    }

    /**
     * centre la chaine pour l'affichage sur le LCD
     *
     * @param chaine chaine à centré
     * @return chaine en sortie
     */
    private String center(String chaine) {
        String tmp = chaine.trim();
        String tmp2 = tmp;
        for (int j = 0; j < ((16 - tmp2.length()) / 2); j++) {
            tmp = " " + tmp;
        }
        return tmp;
    }

    /**
     * detecter le dernier mot pour ne pas decouper la chaine au milieu
     *
     * @param chaine la chaine à vérifier
     * @return la position du dernier espace
     */
    private int lastSpace(String chaine) {
        String tmp = chaine.trim();
        int counter = 0;
        for (int j = 0; j < tmp.length(); j++) {
            if (tmp.charAt(j) == ' ') {
                counter = j;
            }
        }
        return counter;
    }

    /**
     * vérifie si on'a changer le DHCP en manual ou l'inverse
     *
     * @return true or false
     */
    public boolean isChnagedDHCPParam() {
        return porte.getValidServeurClientDhcp().equals("1") != DHCP
                || !porte.getIpInterne().equals(HOSTIP)
                || !porte.getPasserelle().equals(SERVERIP)
                || !porte.getNetmask().equals(MASK);
    }

    /**
     *
     * suit l'etat du wifi pour voir s'il y'avait des changement
     *
     * @return true or false
     */
    public boolean isChnagedWIFIParam() {
        return !porte.getIdWifi().equals(SSID)
                || !porte.getMdpWifi().equals(PWD);
    }

    /**
     * changez les parametres IP
     */
    private void CHANGEIPPARAM() {
        if (porte.getValidServeurClientDhcp().equals("1")) {
            DHCP = porte.getValidServeurClientDhcp().equals("1");
            RunCMD("sudo cp /etc/dhcpcd.conf.dhcp /etc/dhcpcd.conf");
        } else {
            DHCP = porte.getValidServeurClientDhcp().equals("1");
            HOSTIP = porte.getIpInterne();
            SERVERIP = porte.getPasserelle();
            MASK = porte.getNetmask();
            try {

                List<String> lines = Arrays.asList("# Defaults from Raspberry Pi configuration",
                        "hostname",
                        "clientid",
                        "persistent",
                        "option rapid_commit",
                        "option domain_name_servers, domain_name, domain_search, host_name",
                        "option classless_static_routes",
                        "option ntp_servers",
                        "require dhcp_server_identifier",
                        "slaac private",
                        "nohook lookup-hostname",
                        "interface wlan0",
                        "static ip_address=10.3.141.1/24",
                        "static routers=10.3.141.1",
                        "static domain_name_server=1.1.1.1 8.8.8.8",
                        "interface eth0",
                        "static ip_address=" + porte.getIpInterne() + "/" + porte.getNetmask(),
                        "static routers=" + porte.getPasserelle(),
                        "static domain_name_server=" + porte.getPasserelle());

                Path file = Paths.get("/etc/dhcpcd.conf");
                Files.write(file, lines, Charset.forName("UTF-8"));
            } catch (IOException ex) {
                Logger.getLogger(ActionSwitchTime.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        RunCMD("sudo ifconfig eth0 down");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ActionSwitchTime.class.getName()).log(Level.SEVERE, null, ex);
        }
        RunCMD("sudo ifconfig eth0 up");
    }

    /**
     * changez les parametres wifi
     */
    private void CHANGEIPPARAMWIFI() {

        try {

            List<String> lines = Arrays.asList("driver=nl80211",
                    "ctrl_interface=/var/run/hostapd",
                    "ctrl_interface_group=0",
                    "auth_algs=1",
                    "wpa_key_mgmt=WPA-PSK",
                    "beacon_int=100",
                    "ssid=" + porte.getIdWifi(),
                    "channel=1",
                    "hw_mode=g",
                    "ieee80211n=0",
                    "wpa_passphrase=" + porte.getMdpWifi(),
                    "interface=wlan0",
                    "wpa=1",
                    "wpa_pairwise=TKIP",
                    "country_code=AF",
                    "ignore_broadcast_ssid=0");

            Path file = Paths.get("/etc/hostapd/hostapd.conf");
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException ex) {
            Logger.getLogger(ActionSwitchTime.class.getName()).log(Level.SEVERE, null, ex);
        }
        RunCMD("sudo chmod 755 /etc/hostapd/hostapd.conf");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ActionSwitchTime.class.getName()).log(Level.SEVERE, null, ex);
        }
        RunCMD("sudo reboot -f");
    }

    /**
     * rajouter une gateway
     */
    private void CHANGEDGW() {
        if (DGW != null) {
            RunCMD("sudo route add default gw " + DGW + " eth0");
        }
    }

    /**
     * vérifier l'etat de l'ipbx pour relancer le programme
     *
     * @return true or false
     */
    private boolean isChnagedchoixIPBX() {
        return !porte.getServeurIP().equals(CHOIXIPBX);
    }

    /**
     * vérifier l'etat de la gateway
     *
     * @return true or false
     */
    private boolean isChnagedDGW() {
        return !porte.getDefaultGateway().equals(DGW);
    }
}
