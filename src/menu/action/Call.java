package menu.action;

import devices.Buzzer;
import event.Event;
import menu.Menu;
import menu.PorteManager;
import menu.Horodatage;
import utils.*;
import event.eventType.*;
import event.EventManager;
import devices.Pjsip;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;
import java.io.*;
import devices.Lcd;
import utils.Connection;
import devices.HpMicro;
import devices.Leds;
import devices.Relais;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.JsonParse;

/**
 * la classe qui gére les appels
 *
 * @author Abdellah
 */
public class Call implements ActionItem {

    private Horodatage horodatage = Horodatage.getInstance();
    long currTemps, lastTemps = 0;
    private String format = new String();
    private String format1 = new String();
    private String langue = "", raclangue = "";
    int LastDTMF;
    boolean stopB;
    private devices.TimerLocal timer1;
    private int ouverture1, ouverture2, tempsComm, tempsAppel, code2/*, tempoRenvoi*/;
    private String c4_user, c4_passwd, c4_domain, c4_user1, c4_passwd1, c4_domain1;
    private String nom, code, user, domain, user1, domain1;
    private char techno1, techno2;
    private int dtmf1, dtmf2, dtmf3, dtmf4;
    private String nom2 = "";
    // private static final char Audio = '3', VOIP = '4', SIP = '5';
    private Menu menu;
    private DataBase data = DataBase.getInstance();
    private boolean firsttona = false;
    private boolean simul = false;
    private Porte porte;
    private Resident resident;
    private Resident residentRenvoi;
    private EventManager manager = EventManager.getInstance();
    private Pjsip pjsip = Pjsip.getInstance();
    private Lcd lcd = Lcd.getInstance();
    boolean renvoifiche2 = false;
    public static int confirmed = 0, disconnected = 0;
    private char renvoi_appel[];
    private boolean stat = false;
    private boolean differe = false;
    private Connection connection = new Connection();
    private HpMicro hp = HpMicro.getInstance();
    private int callCount = 0;
    private Leds diodes = Leds.getInstance();
    private boolean init = true, openned = false, appelTermine = false;
    private Sound sound = new Sound();
    JsonParse json = new JsonParse();
    private long tempsDebut = System.currentTimeMillis() / 1000;// initialisation debut appel
    private long tempsActuel = tempsDebut;
    private long appelDebut = System.currentTimeMillis() / 1000; // initialisation debut communication
    private long appelActuel = appelDebut;
    private boolean enCommunication = false;
    public int itt;
    public boolean etat;
    long init_tona = 0;
    Buzzer buz;
    String Uri1 = "", Uri2 = "", Uri3 = "", Uri4 = "";
    int typeUri1 = -1, typeUri2 = -1, typeUri3 = -1, typeUri4 = -1;

    PorteManager Pmanager = PorteManager.getInstance();
    /*
	* Couleurs texte pr Bastien, a supp si nécessaire
     */
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    static int callStatus = -1;
    static boolean hasBeenConnected = false;
    int statusdisconnected = -1;
    private final DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final SimpleDateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");
    private LocalDate date;
    private String tempsCourrant;

    private Process p2;
    private ProcessBuilder pb2;
    int tempvpn;
    Relais relais = Relais.getInstance();
    int statusel = 0;
    String renvoiFiche;
    static boolean pasDecro = false;

    /**
     * initialisation et vérification des parametres d'appels
     */
    @Override
    public void init() {
        // pasDecro = false;
        Porte.INSTANCE = null;
        firsttona = true;
        differe = false;
        renvoifiche2 = false;
        buz = Buzzer.getInstance();
        resident = data.getResident(DefilementResident.codeResident);// les parametres
        //resident2 = data.getResident(resident.getCodeRenvoi());
        menu = Menu.getInstance();
        ActionSwitchTime.lstCALL = true;
        lcd.initS();
        porte = Porte.getInstance();
        stopB = false;
        langue = porte.getTraduction();
        raclangue = porte.getLangue();
        // curentItem = menu.getCurrentItem();
        lcd.print(center(data.getTraduction("appeldu", langue) + resident.getCode()), true);
        //lcd.print("APPEL VERS------\n-------------"+resident.getCode(), true);
        init = true;
        etat = true;
        appelTermine = false;
        callCount = 0;

        hp.VOlumHP(porte.getNiveauHP());// mettre à jour le niveau du HP aprés avoir l'activé 
        hp.VOlumMicro(porte.getNiveauMicro());
        itt = 0;
        pasDecro = true;
        date = LocalDate.now();
        tempsCourrant = date.format(formatDate) + "  " + formatHeure.format(Calendar.getInstance().getTime());
        if (resident.getId() == null) {
            renvoiFiche = "0";
            displayErrorAndGoBack("RESIDENT\n NON TROUVEE", false);
            return;
        }
        if (resident.getTechnoAppel().length() > 0) {
            techno1 = resident.getTechnoAppel().charAt(0);
        }
        tempvpn = Integer.parseInt(porte.getTempoVpn()) * 1000;
        if (resident.getTechnoAppel().length() > 1) {
            techno2 = resident.getTechnoAppel().charAt(1);
            if (resident.getTechnoAppel().charAt(1) == '-') {
                techno2 = resident.getTechnoAppel().charAt(0);
            }
        }
        statusel = 0;
        if (Pmanager.status_el1 && Pmanager.status_el2) {
            statusel = 3;
        } else if (Pmanager.status_el1) {
            statusel = 1;
        } else if (Pmanager.status_el2) {
            statusel = 2;
        }
        /*if(resident.getTechnoAppel().contains("O") || resident.getTechnoAppel().contains("V")){
			cmdVpn("lxterminal -e /home/pi/c4pass/assistance/startvpn");
			try {  Thread.sleep(1000);	}catch (InterruptedException e) {}
			
		}*/
        //System.out.println(ANSI_GREEN+"###" +resident.getCode()+"####"+resident.getTechnoAppel()+ANSI_RESET);
        horodatage = Horodatage.getInstance();
        String profLiberale = resident.getProfessionLiberale();
        if (profLiberale.equals("0") || horodatage.check(profLiberale)) {

            Pmanager.ouverture(1, "PL", "P");
            menu.father();
            menu.init();
            return;
        }
        /*----- Verifier si le micro est branché------*/
 /*if(execCmd("lsusb").equals("") || !execCmd("lsusb").contains("Audio")){
			System.out.println(ANSI_YELLOW+"-- # "+tempsCourrant+" " + resident.getName() +"  "+ data.getTraduction("appel_lance",langue) +"   "+ data.getTraduction("aucunmicro",langue) + "    " +  data.getTraduction("findappel",langue)+ANSI_RESET);
			data.addEvent(data.getTraduction("assistance",langue), data.getTraduction("aucunmicro",langue), "", "", "");
			//displayErrorAndGoBack(data.getTraduction("aucunmicro",langue)+"\n"+ data.getTraduction("findappel", langue), true);
			//return;
		}
		else {
			System.out.println(ANSI_GREEN+"-- # "+tempsCourrant+" " + resident.getName() +"   "+ data.getTraduction("microOK",langue) + "    " +ANSI_RESET);
		}*/

 /*---------------------------------------------------------------------------*/
 /*----- Verifier si asterisK est en cours d'execution------*/
        if (execCmd("pidof asterisk").equals("")) {
            System.out.println("-- # " + tempsCourrant + " " + resident.getName() + "  " + data.getTraduction("appel_lance", langue) + "   " + data.getTraduction("asterisk", langue) + "    " + data.getTraduction("hs", langue));
            displayErrorAndGoBack(data.getTraduction("asterisk", langue) + "\n" + data.getTraduction("hs", langue), true);
            return;
        }
        /*---------------------------------------------------------------------------*/
        data.addEvent(data.getTraduction("appelvoip", langue), resident.getName(), data.getTraduction("appel_lance", langue), data.getTraduction("asterisk", langue), data.getTraduction("hs", langue));

        renvoi_appel = resident.getRenvoiAppel().toCharArray();
        differe = false;
        if (resident.getTechnoAppel().length() < 2) {
            displayErrorAndGoBack("ERREUR SAISIE\nTECHNO APPEL", true);
            return;
        }

        if (!checkTechno()) {
            return;
        }
        renvoiFiche = "0";
        if (resident.getRenvoiAppel().equals("2") && !resident.getCode().equals(resident.getCodeRenvoi())) { // vérifier si il y'a un renvoi d'appel 
            renvoiFiche = resident.getCodeRenvoi();
        }

        /* if (eth0plug.isPlugged()) {
            displayErrorAndGoBack("   DEFAUT DE   \n   CONNEXION", false);
        }*/
        if ((porte.getIpExterne().trim().length() == 0 || porte.getIpExterne().equals("Null")
                || porte.getUserExterne().trim().length() == 0 || porte.getUserExterne().equals("Null")
                || porte.getMdpExterne().trim().length() == 0 || porte.getMdpExterne().equals("Null"))
                && (porte.getServeurIP().charAt(0) == 'O' && porte.getServeurIP().charAt(1) == '-')) {
            displayErrorAndGoBack("SERVEUR AUDIO\n NON PARAMETRE", false);
        } else if ((porte.getIpClient().trim().length() == 0 || porte.getIpClient().equals("Null")
                || porte.getUserClient().trim().length() == 0 || porte.getUserClient().equals("Null")
                || porte.getMdpClient().trim().length() == 0 || porte.getMdpClient().equals("Null"))
                && (porte.getServeurIP().charAt(0) == 'C' && porte.getServeurIP().charAt(1) == '-')) {
            displayErrorAndGoBack("SERVEUR CLIENT\n NON PARAMETRE", false);
        } else if ((porte.getIpInterne().trim().length() == 0 || porte.getIpInterne().equals("Null"))
                && (porte.getServeurIP().charAt(0) == 'D' && porte.getServeurIP().charAt(1) == '-')) {
            displayErrorAndGoBack("SERVEUR INTERNE\n NON PARAMETRE", false);
        } // serveurIP=CC
        else if ((porte.getServeurIP().equals("C-")) && ((resident.getSipPhoneUn().equals("________________"))
                || (resident.getSipPhoneUn().equals("")) || (resident.getSipPhoneUn().equals("0")))) {
            displayErrorAndGoBack("IDENTIFIANT SIP1\nNON ENREGISTRE", true);
        } else if ((porte.getServeurIP().equals("C-"))
                && ((resident.getSipPhoneDeux().equals("________________")) || (resident.getSipPhoneDeux().equals(""))
                || (resident.getSipPhoneDeux().equals("0")))
                && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            displayErrorAndGoBack("IDENTIFIANT SIP2\nNON ENREGISTRE", true);
        } // serveurIP=OO
        else if ((porte.getServeurIP().equals("O-")) && ((resident.getTelephone1().equals("__________"))
                || (resident.getTelephone1().equals("")) || (resident.getTelephone1().equals("0")) || (resident.getTelephone1().length() < 10))) {

            displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);

        } // serveurIP=VV
        else if ((porte.getServeurIP().equals("V-")) && ((resident.getIdVoip().equals("__________"))
                || (resident.getIdVoip().equals("")) || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } // serveeurIP=DV
        else if (((porte.getServeurIP().equals("DV")) || (porte.getServeurIP().equals("VD")))
                && ((resident.getTechnoAppel().equals("VD")) || (resident.getTechnoAppel().equals("V-")))
                && ((resident.getIdVoip().equals("__________")) || (resident.getIdVoip().equals(""))
                || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("DV")) || (porte.getServeurIP().equals("VD")))
                && (resident.getTechnoAppel().equals("DV")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))
                && ((resident.getIdVoip().equals("__________")) || (resident.getIdVoip().equals(""))
                || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } // serveruIP="DC"
        else if (((porte.getServeurIP().equals("DC")) || (porte.getServeurIP().equals("CD")))
                && ((resident.getTechnoAppel().equals("CD")) || (resident.getTechnoAppel().equals("C-")))
                && ((resident.getSipPhoneUn().equals("________________")) || (resident.getSipPhoneUn().equals(""))
                || (resident.getSipPhoneUn().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n SIP1 ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("DC")) || (porte.getServeurIP().equals("CD")))
                && (resident.getTechnoAppel().equals("C-")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            if ((resident.getSipPhoneUn().equals("________________")) || (resident.getSipPhoneUn().equals(""))
                    || (resident.getSipPhoneUn().equals("0"))) {
                displayErrorAndGoBack("AUCUNIDENTIFIANT\n SIP1 ENREGISTRE", true);
            }
        } // serveurIP=DO
        else if (((porte.getServeurIP().equals("DO")) || (porte.getServeurIP().equals("OD")))
                && ((resident.getTechnoAppel().equals("OD")) || (resident.getTechnoAppel().equals("O-")))
                && ((resident.getTelephone1().equals("__________")) || (resident.getTelephone1().equals("")) || (resident.getTelephone1().equals("0")))) {
            displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("DO")) || (porte.getServeurIP().equals("OD")))
                && (resident.getTechnoAppel().contains("O")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            if ((resident.getTelephone1().equals("__________")) || (resident.getTelephone1().equals(""))
                    || (resident.getTelephone1().equals("0"))) {
                displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);
            }
        } else if (((porte.getServeurIP().equals("CV")) || (porte.getServeurIP().equals("VC")))
                && ((resident.getTechnoAppel().equals("VC")) || (resident.getTechnoAppel().equals("V-")))
                && ((resident.getIdVoip().equals("__________")) || (resident.getIdVoip().equals(""))
                || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("CV")) || (porte.getServeurIP().equals("VC")))
                && (resident.getTechnoAppel().equals("CV")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))
                && ((resident.getIdVoip().equals("__________")) || (resident.getIdVoip().equals(""))
                || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("CV")) || (porte.getServeurIP().equals("VC")))
                && ((resident.getTechnoAppel().equals("CV")) || (resident.getTechnoAppel().equals("C-")))
                && ((resident.getSipPhoneUn().equals("________________")) || (resident.getSipPhoneUn().equals(""))
                || (resident.getSipPhoneUn().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n SIP1 ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("CV")) || (porte.getServeurIP().equals("VC")))
                && (resident.getTechnoAppel().equals("C-")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            if ((resident.getSipPhoneUn().equals("________________")) || (resident.getSipPhoneUn().equals(""))
                    || (resident.getSipPhoneUn().equals("0"))) {
                displayErrorAndGoBack("AUCUNIDENTIFIANT\n SIP1 ENREGISTRE", true);
            }
        } else if (((porte.getServeurIP().equals("CO")) || (porte.getServeurIP().equals("OC")))
                && ((resident.getTechnoAppel().equals("OC")) || (resident.getTechnoAppel().equals("O-")))
                && ((resident.getTelephone1().equals("__________")) || (resident.getTelephone1().equals(""))
                || (resident.getTelephone1().equals("0")))) {
            displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("CO")) || (porte.getServeurIP().equals("OC")))
                && (resident.getTechnoAppel().equals("O-")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            if ((resident.getTelephone1().equals("__________")) || (resident.getTelephone1().equals(""))
                    || (resident.getTelephone1().equals("0"))) {
                displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);
            }
        } else if (((porte.getServeurIP().equals("CO")) || (porte.getServeurIP().equals("OC")))
                && ((resident.getTechnoAppel().equals("CO")) || (resident.getTechnoAppel().equals("C-")))
                && ((resident.getSipPhoneUn().equals("________________")) || (resident.getSipPhoneUn().equals(""))
                || (resident.getSipPhoneUn().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n SIP1 ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("CO")) || (porte.getServeurIP().equals("OC")))
                && (resident.getTechnoAppel().equals("C-")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            if ((resident.getSipPhoneUn().equals("________________")) || (resident.getSipPhoneUn().equals(""))
                    || (resident.getSipPhoneUn().equals("0"))) {
                displayErrorAndGoBack("AUCUNIDENTIFIANT\n SIP1 ENREGISTRE", true);
            }
        } else if (((porte.getServeurIP().equals("VO")) || (porte.getServeurIP().equals("OV")))
                && ((resident.getTechnoAppel().equals("VO")) || (resident.getTechnoAppel().equals("V-")))
                && ((resident.getIdVoip().equals("__________")) || (resident.getIdVoip().equals(""))
                || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("VO")) || (porte.getServeurIP().equals("OV")))
                && (resident.getTechnoAppel().equals("OV")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))
                && ((resident.getIdVoip().equals("__________")) || (resident.getIdVoip().equals(""))
                || (resident.getIdVoip().equals("0")))) {
            displayErrorAndGoBack("AUCUNIDENTIFIANT\n VoIP ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("VO")) || (porte.getServeurIP().equals("OV")))
                && ((resident.getTechnoAppel().equals("OV")) || (resident.getTechnoAppel().equals("O-")))
                && ((resident.getTelephone1().equals("__________")) || (resident.getTelephone1().equals(""))
                || (resident.getTelephone1().equals("0")))) {
            displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);
        } else if (((porte.getServeurIP().equals("VO")) || (porte.getServeurIP().equals("OV")))
                && (resident.getTechnoAppel().equals("O-")) && ((renvoi_appel[0] == '1') || (renvoi_appel[0] == '2'))) {
            if ((resident.getTelephone1().equals("__________")) || (resident.getTelephone1().equals(""))
                    || (resident.getTelephone1().equals("0"))) {
                displayErrorAndGoBack("AUCUN TELEPHONE1\n ENREGISTRE", true);
            }
        }
        if (etat) {
            // 2Enregistrement au serveur
            System.out.println(ANSI_GREEN + "###" + resident.getCode() + "####" + resident.getTechnoAppel() + ANSI_RESET);
            init_c4();
            // enregistrement sur le serveur
            if ((porte.getServeurIP().charAt(0) == 'O' || porte.getServeurIP().charAt(1) == 'O' || porte.getServeurIP().charAt(0) == 'V' || porte.getServeurIP().charAt(1) == 'V' || (porte.getServeurIP().equals("C-") && porte.getValidWebClient().equals("1"))
                    || (porte.getServeurIP().equals("CD") && porte.getValidWebClient().equals("1"))
                    || (porte.getServeurIP().equals("DC") && porte.getValidWebClient().equals("1")))) {
                if (Connection.trycnx() == false) {
                    displayErrorAndGoBack("PAS DE CONNEXION\n-FIN DE L'APPEL-", false);
                } else if ((porte.getIpExterne().trim().length() == 0 || porte.getIpExterne().equals("Null")
                        || porte.getUserExterne().trim().length() == 0 || porte.getUserExterne().equals("Null")
                        || porte.getMdpExterne().trim().length() == 0 || porte.getMdpExterne().equals("Null"))
                        && (porte.getServeurIP().charAt(0) == 'O' || porte.getServeurIP().charAt(1) == '-')) {
                    displayErrorAndGoBack("SERVEUR AUDIO\nNON PARAMETRE", false);
                } else {
                    if (!appeler()) {
                        return;
                    }
                    data.addEvent("APPEL VOIP", resident.getName(), "APPEL SIMPLE", " ", " ");
                }
            } else {
                if (!appeler()) {
                    return;
                }
                data.addEvent("APPEL VOIP", resident.getName(), "APPEL SIMPLE", " ", " ");
            }
        }
        timer1 = manager.requestTimer(2, devices.TimerLocal.Unit.sec);
        init = false;
    }

    /**
     * executer une commande cmd
     *
     * @param cmd la commande à executer
     */
    public void cmdVpn(String cmd) {
        try {
            pb2 = new ProcessBuilder("sudo", "bash", "-c", cmd);
            p2 = pb2.start();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }

    /**
     * renvoi la type du techno du serveur
     *
     * @param nb serveur 1 ou 2
     * @return le type de l'ipbx
     */
    private int getTypeTechno(int nb) {
        int type = -1;
        char techno = porte.getServeurIP().charAt(nb == 1 ? 0 : 1);
        if (techno == '-') {
            techno = porte.getServeurIP().charAt(0);
        }

        switch (techno) {
            case 'C':
                type = 0; // client
                break;
            case 'O':
                type = 1; // ovh
                break;
            case 'D':
                type = 2; // asterisk
                break;
            case 'V':
                type = 3; // diese
                break;
            default:
                break;
        }
        return type;
    }

    public boolean appeler() {
        init_appel();
        renvoi_appel = resident.getRenvoiAppel().toCharArray();

// verification de la techno
        if (!checkTechno()) {
            return false;
        }
// Si  V  ou C importer les lib jar	

// Verification de l'existence des comptes au technos 
        System.out.println(ANSI_BLUE + "--#enregistrement techno1" + techno1 + "---------> " + pjsip.connectedToServer(getTypeTechno(techno1)) + ANSI_RESET);
        if (techno2 != '-') {
            System.out.println(ANSI_BLUE + "--#enregistrement techno2" + techno2 + "---------> " + pjsip.connectedToServer(getTypeTechno(techno2)) + ANSI_RESET);
        }
        List<Resident> residents = data.getSimuResidents(resident.getCode());
        for (int i = 0; i < residents.size(); i++) {
            if (!residents.get(i).getProfil().equals("0")) {
                if (!horodatage.check(residents.get(i).getProfil())) {
                    residents.remove(i);
                }
            }
        }
        if (residents.isEmpty()) {
            displayErrorAndGoBack(data.getTraduction("appelhorstranche", langue), true);
            return false;

        }
        List<String> UriCall = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UriCall.add("");
        }
        List<Integer> TypeCall = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TypeCall.add(-1);
        }
        for (int i = 0; i < residents.size(); i++) {

            switch (residents.get(i).getTechnoAppel().charAt(0)) {
                case 'D':
                    UriCall.set(i, setUserDomain1("D", residents.get(i)));
                    TypeCall.set(i, 2);

                    break;
                case 'C':
                    UriCall.set(i, setUserDomain1("C", residents.get(i)));
                    TypeCall.set(i, 0);
                    break;
                case 'O':
                    UriCall.set(i, setUserDomain1("O", residents.get(i)));
                    TypeCall.set(i, 1);
                    break;
                case 'V':
                    UriCall.set(i, setUserDomain1("V", residents.get(i)));
                    TypeCall.set(i, 3);
                    JsonParse.notifIOS(residents.get(i).getIdVoip());
                     {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Call.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                default:
                    break;

            }
        }

        pjsip.makeCall(UriCall.get(0), TypeCall.get(0), UriCall.get(1), TypeCall.get(1), UriCall.get(2), TypeCall.get(2), UriCall.get(3), TypeCall.get(3), UriCall.get(4), TypeCall.get(4), UriCall.get(5), TypeCall.get(5), UriCall.get(6), TypeCall.get(6), UriCall.get(7), TypeCall.get(7), UriCall.get(8), TypeCall.get(8), UriCall.get(9), TypeCall.get(9), statusel);

        // pjsip.makeCall("sip:001@127.0.0.1", 2, "sip:002@127.0.0.1", 2, "sip:003@127.0.0.1", 2, "sip:004@127.0.0.1", 2, "sip:005@127.0.0.1", 2, "sip:006@127.0.0.1", 2, "sip:007@127.0.0.1", 2, "sip:008@127.0.0.1", 2, "sip:009@127.0.0.1", 2, "sip:010@127.0.0.1", 2, statusel);
        //  pjsip.makeCall("sip:001@127.0.0.1", 2, "sip:002@127.0.0.1", 2, "sip:003@127.0.0.1", 2, "sip:004@127.0.0.1", 2, "", -1, "", -1, "", -1, "", -1, "", -1, "", -1, statusel);
        //  pjsip.makeCall("sip:0758397583@94.143.87.216", 1, "", -1, "", -1, "",-1,"", -1, "", -1, "", -1, "", -1, "", -1, "", -1, statusel);
        appelEnCours();
        tempsDebut = System.currentTimeMillis() / 1000;// initialisation debut appel
        tempsActuel = tempsDebut;
        enCommunication = false;
        return true;

    }

    private void appelEnCours() {
        diodes.init();
        diodes.AllumeWHITEBLUE();

        hasBeenConnected = false;
        if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) >= 2) {
            sound.play(raclangue + "_appel_en_cours.wav");
        }
        if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) == 1 || Character.getNumericValue(porte.getSignalSonore().charAt(0)) > 2) {
            buz.buzz(200);
        }
    }

    private void getError() {
        if (techno1 == 'D') {
            data.addEvent("APPEL VOIP", nom, "ERREUR", "NON ENREGISTRE", "FIN DE L APPEL");
            displayErrorAndGoBack("-AUCUN  SERVEUR-\nFIN DE L APPEL", true);
        } else if (!internetAvailable()) {
            data.addEvent("APPEL VOIP", nom, "ERREUR", "AUCUN RESEAU", "FIN DE L APPEL");
            displayErrorAndGoBack("--AUCUN RESEAU--\nFIN DE L APPEL", true);
        } else if (!serverAvailable()) {
            data.addEvent("APPEL VOIP", nom, "ERREUR", "PING SERVEUR HS", "FIN DE L APPEL");
            displayErrorAndGoBack("PING SERVEUR HS\nFIN DE L APPEL", true);
        } else {
            data.addEvent("APPEL VOIP", nom, "ERREUR", "PING OK NON ENREGIST", "FIN DE L APPEL");
            displayErrorAndGoBack("PING OK NON ENREGIST\nFIN DE L APPEL", true);
        }
    }

    /**
     * executer une commande
     *
     * @param cmd la commande à executer
     * @return le resultat de la commande
     */
    public static String execCmd(String cmd) {
        try {
            java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(cmd).getInputStream()).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * pinger le serveur
     *
     * @return true or false
     */
    private boolean serverAvailable() {
        String ping = execCmd("ping -c 1 -W1 " + domain);

        return ping.contains("time=");
    }

    /**
     * vérifier l'etat d'internet
     *
     * @return
     */
    private boolean internetAvailable() {
        String ip = execCmd("hostname -I");
        return !ip.equals("\n");
    }

    /**
     * vérification du serveur d'appel
     *
     * @return true or false
     */
    private boolean checkTechno() {
        String serveurIP = porte.getServeurIP();

        if (serveurIP.indexOf(techno1) == -1 && serveurIP.indexOf(techno2) == -1) {
            data.addEvent("APPEL VOIP", nom, "ERREUR", "ERREUR DE SAISIE", "TECHNO APPEL");
            displayErrorAndGoBack("ERREUR DE SAISIE\nTECHNO APPEL", true);
            return false;
        }
        return true;
    }

    /**
     * renvoi l'uri a appeler
     *
     * @param tech la technologie
     * @param Resid l'utilisateur
     * @return l'uri à appeler
     */
    public String setUserDomain1(String tech, Resident Resid) {
        String adresse = "";

        switch (tech) {
            case "O":
                adresse = Resid.getTelephone1() + "@192.168.1.108";// resident.getTelephone1() + "@94.143.87.216";
                break;
            case "V":
                adresse = Resid.getIdVoip() + "@192.168.1.100";//sip.linphone.org";//192.168.168.103";
                break;
            case "C":
                adresse = Resid.getSipPhoneUn() + "@" + porte.getIpClient();
                break;
            case "D":
                adresse = Resid.getCode() + "@127.0.0.1";
                break;
            default:
                break;
        }
        System.out.println(ANSI_YELLOW + "--#appel vers " + adresse + ANSI_RESET);
        return "sip:" + adresse;
    }

    /**
     * init d'appel
     */
    private void init_appel() {
        statusdisconnected = -1;
        nom = resident.getName();
        code = resident.getCode();
        dtmf1 = porte.getTouchesDTMF().charAt(0);
        dtmf2 = porte.getTouchesDTMF().charAt(1);
        dtmf3 = porte.getTouchesDTMF().charAt(2);
        dtmf4 = '1';
        ouverture1 = Integer.parseInt(porte.getTempsOuvertureUn());
        ouverture2 = Integer.parseInt(porte.getTempsOuvertureDeux());
        tempsComm = Integer.parseInt(porte.getTempsCommunication());
        tempsAppel = Integer.parseInt(porte.getTempsAppel());
        pjsip.init_params(dtmf1, dtmf2, dtmf3, dtmf4, ouverture1, ouverture2, nom, code, tempsComm, tempsAppel);
    }

    /**
     * REMPLISSAGES DES PARAMETRES DE LA PLATINE DALS LA PORTE
     */
    private void init_c4() {
        techno1 = resident.getTechnoAppel().charAt(0);
        if (resident.getTechnoAppel().length() > 1) {
            techno2 = resident.getTechnoAppel().charAt(1);
        }

        switch (techno1) {
            case 'O':
                // si OVH
                c4_user = porte.getUserExterne();
                c4_passwd = porte.getMdpExterne();
                c4_domain = porte.getIpExterne();
                break;
            case 'V':
                c4_user = "1002";
                c4_passwd = "9999";
                c4_domain = "213.246.57.109:5080";
                break;
            case 'C':
                c4_user = porte.getUserClient();
                c4_passwd = porte.getMdpClient();
                c4_domain = porte.getIpClient();
                break;
            case 'D':
                c4_user = "999";
                c4_passwd = "9999";
                c4_domain = porte.getIpInterne();
                break;
            default:
                break;
        }
        techno2 = resident.getTechnoAppel().charAt(1);
        switch (techno2) {
            case 'O':
                c4_user1 = porte.getUserExterne();
                c4_passwd1 = porte.getMdpExterne();
                c4_domain1 = porte.getIpExterne();
                break;
            case 'V':
                c4_user1 = "1002";
                c4_passwd1 = "9999";
                c4_domain1 = "213.246.57.109:5080";
                break;
            case 'C':
                c4_user1 = porte.getUserClient();
                c4_passwd1 = porte.getMdpClient();
                // c4_domain1 = "sip.linphone.org";
                c4_domain1 = porte.getIpClient();
                break;
            case 'D':
                c4_user1 = "999";
                c4_passwd1 = "9999";
                c4_domain1 = porte.getIpInterne();
                break;
            default:
                break;
        }
    }

    /**
     * affichage du message d'erreur
     *
     * @param str le message
     * @param veracite etat lcd
     */
    public void displayErrorAndGoBack(String str, boolean veracite) {
        System.out.println(ANSI_PURPLE + "--###   " + str + "     ####" + ANSI_RESET);
        etat = false;
        lcd.clearLcd();
        lcd.print(str, veracite);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        stopAppel();
        System.out.println(menu.toString());
        if (init) {
            menu.init();
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        hp.exit();
        diodes.eteint();
        manager = EventManager.getInstance();
        manager.releaseTimer(timer1);
    }

    /**
     * fin d'appel
     */
    private void stopAppel() {
        diodes.eteint();


        /*if (openned) {

            if (!Pmanager.status_el1 && LastDTMF == 1) {
                relais.CloseDoor(LastDTMF);
            } else if (!Pmanager.status_el2 && LastDTMF == 2) {
                relais.CloseDoor(LastDTMF);
            }
        }*/
        if (!appelTermine) {
            if (!(!renvoiFiche.equals("0") && pasDecro)) {
                if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) >= 2) {
                    sound.play(raclangue + "_appel_termine.wav");
                }

                /*if(!execCmd("pidof openvpn").equals("") && porte.getValidVpn().equals("1")){
			try {  Thread.sleep(tempvpn);	}catch (InterruptedException e) {}
			cmdVpn("killall openvpn");
		}*/
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }

            }
            confirmed = 0;
            pjsip.endCall();
            disconnected = 0;
            appelTermine = true;
            hp.exit();
            if (!renvoiFiche.equals("0") && pasDecro && !stopB) {
                pasDecro = false;
                lcd.clearLcd();
                lcd.print(center("TRANSFERT"), true);
                lcd.setCursor(2, 0);
                lcd.print(center("D'APPEL"), false);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                DefilementResident.codeResident = renvoiFiche;
                menu.father();
                menu.child();
            } else {
                pasDecro = false;
                menu.father();
                menu.father();
            }
        }
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        String teleph1 = resident.getTelephone1();
        String teleph2 = resident.getTelephone2();
        int double_telep = 0;
        String technoAppel = resident.getTechnoAppel();

        if (pjsip.raccroche()) {
            pjsip.resetRaccroche();
            stopAppel();

            return;
        }
        if (teleph1.equals(teleph2) && !teleph1.contains("_")) {
            double_telep = 1;
        }
        if (double_telep == 1 && technoAppel.contains("O-")) {
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("appelvoip", langue), nom2, data.getTraduction("appel_nonlance", langue), data.getTraduction("telephone1et2", langue), data.getTraduction("doublon", langue));
            lcd.clearLcd();
            lcd.print(data.getTraduction("telephone1et2", langue) + "\n" + data.getTraduction("doublon", langue), true);
            //displayErrorAndGoBack(data.getTraduction("telephone1et2",langue) +"\n"+ data.getTraduction("doublon",langue), false);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }
            stopAppel();
            return;
        }

        if (event instanceof EventKey1 || event instanceof EventKey2 || event instanceof EventKey3
                || event instanceof EventKey4 || event instanceof EventKey5 || event instanceof EventKey6
                || event instanceof EventKey7 || event instanceof EventKey8 || event instanceof EventKey9 || event instanceof EventKey0) {
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("appelvoip", langue), nom2, data.getTraduction("appel_lance", langue), data.getTraduction("annulationpar", langue), data.getTraduction("leclavier", langue));
            lcd.print(data.getTraduction("annulationpar", langue) + "\n" + data.getTraduction("leclavier", langue), true);
            stopB = true;
            stopAppel();

            return;
        }
        // DTMF TOUCHE # ou 0 OUVERTURE PORTE
        if (pjsip.ouverturePorte() != 0) {
            if (!openned) {

                diodes.AllumeWHITEORANGEGREEN();
                if (pjsip.ouverturePorte() == 35) {
                    LastDTMF = 1;
                } else if (pjsip.ouverturePorte() == 48) {
                    LastDTMF = 2;
                }
                // relais.OpenDoor(pjsip.ouverturePorte());
                if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) >= 2) {
                    sound.play(raclangue + "_porte_ouverte.wav");
                }
                if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) == 1 || Character.getNumericValue(porte.getSignalSonore().charAt(0)) > 2) {
                    buz.buzz(200);
                }
                lcd.initS();
                lcd.print(data.getTraduction("porteouverte", langue), true);

            }
            openned = true;
            return;
        }
        // TOUCHE FIN DE L'APPEL
        if (event instanceof EventKeyB || event instanceof EventKeyP || (porte.getInterphone().charAt(1) == '1' && event instanceof EventKey1)) {
            stopB = true;
            stopAppel();

            return;
        }
        // DTMF TOUCHE * AJOUT DE TEMPS
        if (pjsip.addTime()) {
            /*    appelDebut = System.currentTimeMillis() / 1000;
            pjsip.resetAddTime();
            if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) >= 2) {
                sound.play("communication_prolongee.wav");
            }*/
        }
        // CHANGEMENT DE TECHNOSTATUT APPEL
        if (statusdisconnected != pjsip.statusdisconnected()) {
            statusdisconnected = pjsip.statusdisconnected();
            System.out.println(ANSI_RED + "statusdisconnected = " + pjsip.statusdisconnected() + ANSI_RESET);
        }
        // APPEL DECROCHE
        if (pjsip.hasBeenConnected() == 1) {
            hasBeenConnected = true;
        }
        // FIN DE L'APPEL
        if (statusdisconnected == 1) {

            displayErrorAndGoBack(hasBeenConnected ? data.getTraduction("communication", langue) + "\n" + data.getTraduction("terminee", langue) : data.getTraduction("residentnon", langue) + "\n" + data.getTraduction("joignable", langue), false);
            statusdisconnected = 0;
            pasDecro = true;
            if (!hasBeenConnected) {

                data.addEvent(data.getTraduction("appelvoip", langue), resident.getName(), data.getTraduction("appel_lance", langue), data.getTraduction("residentnon", langue), data.getTraduction("joignable", langue));
            }
            // sound.play("appel_termine.wav");
            return;
        } // DEBUT DE COMMUNICATION INITIALISATION DU TEMPS
        else if (!enCommunication && pjsip.onCommunication()) {
            pasDecro = false;

            if (Character.getNumericValue(porte.getSignalSonore().charAt(0)) >= 2 && pjsip.ouverturePorte() == 0) {
                sound.play(raclangue + "_communication_engagee.wav");

            }
            if ((Character.getNumericValue(porte.getSignalSonore().charAt(0)) == 1 || Character.getNumericValue(porte.getSignalSonore().charAt(0)) > 2) && pjsip.ouverturePorte() == 0) {
                buz.buzz(200);
            }

            enCommunication = true;
            appelDebut = System.currentTimeMillis() / 1000; // initialisation debut communication
            appelActuel = appelDebut;
        }
        // APPEL TERMINE PAS REPONDU OU DECROCHE
        if (!hasBeenConnected && (System.currentTimeMillis() / 1000 - tempsDebut > tempsAppel || pjsip.onCommunication() || statusdisconnected == 1)) {
            pasDecro = true;
            if (callCount >= 20) {
                //stopAppel();
                displayErrorAndGoBack("MAXIMUM ATTEINT\n-FIN DE L'APPEL-", false);
                return;
            } else if (renvoi_appel[0] == '2' && !differe) { // Si second appel differé
                differe = true;
                pjsip.endCall();
                manager.releaseTimer(timer1);
                timer1 = manager.requestTimer(2, devices.TimerLocal.Unit.sec);
                callCount++;
                data.addEvent("APPEL VOIP", nom, "APPEL DIFFERE", " ", " ");
                //  appeler();
            } else if (resident.getCodeRenvoi().length() >= 3 && !resident.getCodeRenvoi().equals("-----") && !resident.getCodeRenvoi().contains("-") && !resident.getCodeRenvoi().equals("000")) { // Si renvoie d'appel vers un autre résident	
                resident = data.getResident(resident.getCodeRenvoi());
                if (resident != null) {
                    renvoi_appel = resident.getRenvoiAppel().toCharArray();
                    //tempoRenvoi = Integer.parseInt(resident.getTempoRenvoi());
                    pjsip.endCall();
                    manager.releaseTimer(timer1);
                    timer1 = manager.requestTimer(2, devices.TimerLocal.Unit.sec);
                    callCount++;
                    data.addEvent("APPEL VOIP", nom, "APPEL CASCADE", " ", " ");
                    renvoifiche2 = true;
                    //   appeler();

                } else {
                    pjsip.endCall();
                    manager.releaseTimer(timer1);
                    return;
                }

            } else {
                //stopAppel();
                pasDecro = false;
                displayErrorAndGoBack("- SANS REPONSE -\n-FIN DE L'APPEL-", false);
                return;
            }

            /*if (!pjsip.onCommunication()) {
		stopAppel();
		return;
	    }*/
        }
        // COMMUNICATION TERMINEE PLUS DE TEMPS
        if (hasBeenConnected && tempsComm - (System.currentTimeMillis() / 1000 - appelDebut) < 0) {
            lcd.print(data.getTraduction("findappel", langue), true);

            stopAppel();
            return;
        } // EN APPEL
        else if (!hasBeenConnected && !pjsip.onCommunication() && statusdisconnected == 0) {

            if ((System.currentTimeMillis() / 1000) - init_tona > 3 || firsttona) {

                init_tona = System.currentTimeMillis() / 1000;
                firsttona = false;

                sound.play("tonalite_appel_short.wav");

            }
            /*  if (tempsActuel != System.currentTimeMillis() / 1000) {
		tempsActuel = System.currentTimeMillis() / 1000;
		lcd.print(nom, true);
		lcd.setCursor(2, 0);
		itt++;
		System.out.println("lancement d'appel " + itt  + "   "+callCount);
		lcd.print("APPEL " + code + " " + (tempsActuel - tempsDebut) + "s", false);
	    }*/
        } // COMMUNICATION EN COURS
        else if (enCommunication && (System.currentTimeMillis() / 1000 - appelDebut < tempsComm)) {

            diodes.AllumeWHITEORANGE();
            if (appelActuel != System.currentTimeMillis() / 1000) {
                appelActuel = System.currentTimeMillis() / 1000;
                long tempsRestant = tempsComm - (System.currentTimeMillis() / 1000 - appelDebut);
                /*if (tempsRestant > 9 && pjsip.onCommunication()) {*/
                if (tempsRestant == tempsComm - 1 || openned) {
                    lcd.print(nom + "\n" + code + " " + data.getTraduction("parlez", langue) + " ", true);
                    openned = false;

                }

                /* lcd.setCursor(2, 12);
		    lcd.print(tempsRestant + "s", false);
		}
		else {
		    if (tempsRestant == 9 || openned) {
			// buzzer 200
			lcd.print("COMMUNICATION\nTERMINEE DANS ", true);
			lcd.setCursor(2, 15);
			lcd.print("s", false);
			openned = false;
		    }
		    lcd.setCursor(2, 14);
		    lcd.print(tempsRestant + "", false);
		}
	    }*/
            }
            // GESTION APPELS DIFFERES ET CASCADE

        }
    }

    private String center(String chaine) {
        String tmp = chaine.trim();
        String tmp2 = tmp;
        for (int j = 0; j < ((16 - tmp2.length()) / 2) + 1; j++) {
            tmp = " " + tmp;
        }
        return tmp;
    }
}
