package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Mdp Client
 *
 * @author Abdellah
 */
public class ActionModifierMdpClient implements ActionItem {

    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private int c;
    private char[] input = new char[16];
    Event lastEvent;
    DataBase data = DataBase.getInstance();
    Porte porte;

    private final char[] lettre = " .,:-_1ABCabc2DEFdef3GHIghi4JKLjkl5MNOmno6PQRpqr7STUstu8VWXvwx9XYxy0\'#0".toCharArray();

    /**
     * init
     */
    @Override
    public void init() {
        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        if (porte.getMdpClient() != null) {
            int i;
            lcd.print(porte.getMdpClient(), false);
            for (i = 0; i < porte.getMdpClient().length(); i++) {
                input[i] = porte.getMdpClient().charAt(i);
            }
            while (i < 15) {
                input[i++] = ' ';
            }
            lcd.setCursor(2, 0);
        }
        c = 0;
        lastEvent = new EventKeyValid();
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        /*
     ** lcd.saisie(false);
         */
    }

    /**
     * action evenement
     *
     * @param event
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        System.out.println("user client avant: " + porte.getUserClient());
        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        System.out.println(c);

        if (event instanceof EventKeyB) {
            menu.father();
        } else if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor] = lettre[0 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[7 + c]), false);
            input[cursor] = lettre[7 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[14 + c]), false);
            input[cursor] = lettre[14 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[21 + c]), false);
            input[cursor] = lettre[21 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[28 + c]), false);
            input[cursor] = lettre[28 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[35 + c]), false);
            input[cursor] = lettre[35 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[42 + c]), false);
            input[cursor] = lettre[42 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[49 + c]), false);
            input[cursor] = lettre[49 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[56 + c]), false);
            input[cursor] = lettre[56 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[63 + c]), false);
            input[cursor] = lettre[63 + c];
            c = c == 6 ? c + 1 : c;
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor > 15) {
                cursor = 0;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = (porte.getMdpClient().length() - 1);
            }
            lcd.setCursor(2, cursor);
        } else if (cursor > 15) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyValid) {
            String mdp = new String(input);

            porte.setMdpClient(mdp.trim());
            data.update(porte);
            switch (porte.getServeurIP().charAt(1)) {
                case 'C':
                    menu.father();
                    break;
                case 'O':
                    menu.next();
                    break;
                case 'V':
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();

                    /*
                    ** lcd.clearLcd();
                    ** lcd.print(" SERVEUR VIDEO"+"\n",false);
                    ** lcd.setCursor(2,0);
                    ** lcd.print(" voipdiese.com",false);
                    ** try{Thread.sleep(1500);}
                    ** catch(InterruptedException e){}
                    ** menu.father();
                     */ break;
                case 'D':
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    break;
                default:
                    break;
            }
        }
        if (event instanceof EventKeyP) {
            for (int i = 0; i < 16; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print(" ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        c = c < 7 ? c + 1 : 0;
        lcd.setCursor(2, cursor);
        lastEvent = event;
        System.out.println("cursor: " + cursor);
    }
}
