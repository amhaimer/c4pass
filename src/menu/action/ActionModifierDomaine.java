package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.DataBase;

/**
 * Action Modifier Domaine
 *
 * @author Abdellah
 */
public class ActionModifierDomaine implements ActionItem {

    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private char[] input = new char[16];
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private int c;
    private final char[] lettre = " .,1abc2def3ghi4jkl5mno6pqrs7tuv8wxzz9-\'#0".toCharArray();
    private Event lastEvent;
    private boolean second;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        input = new char[16];
        second = false;
        cursor = cursorBegin;
        c = 0;
        lastEvent = new EventKeyValid();
        if (porte.getInterphone().charAt(0) == '5') {
            for (int i = 0; i < porte.getIpInterne().length() && i < 15; i++) {
                input[i] = porte.getIpExterne().charAt(i);
            }
            lcd.print("SERVEUR SIP\n" + porte.getIpExterne(), true);
        } else {
            for (int i = 0; i < porte.getIpInterne().length() && i < 15; i++) {
                input[i] = porte.getIpInterne().charAt(i);
            }
            lcd.print("SERVEUR VOIP\n" + porte.getIpInterne(), true);
        }
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor] = lettre[0 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[4 + c]), false);
            input[cursor] = lettre[4 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[8 + c]), false);
            input[cursor] = lettre[8 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[12 + c]), false);
            input[cursor] = lettre[12 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[16 + c]), false);
            input[cursor] = lettre[16 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[20 + c]), false);
            input[cursor] = lettre[20 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[24 + c]), false);
            input[cursor] = lettre[24 + c];
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[29 + c]), false);
            input[cursor] = lettre[29 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[33 + c]), false);
            input[cursor] = lettre[33 + c];
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[38 + c]), false);
            input[cursor] = lettre[38 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = 15;
                lcd.setCursor(2, cursor);
            }
            if (cursor > 15) {
                cursor = cursorBegin;
                lcd.setCursor(2, cursor);
            }
            if (event instanceof EventKeyValid) {
                String adr = new String(input);
                adr = adr.trim();
                if (porte.getInterphone().charAt(0) == '5' || second) {
                    porte.setIpExterne(adr);
                    data.addEvent("SAISIE ENR PORTE", "Modification du", "Serv.client SIP", " ", " ");
                } else {
                    porte.setIpInterne(adr);
                    data.addEvent("SAISIE ENR PORTE", "Modification du", "Serveur VOIP", " ", " ");
                }
                data.update(porte);
                data.addEvent("Saisie Gestion", "Nom de domaine", "serveur", adr, " ");
                if (porte.getInterphone().charAt(0) != '3' || second) {
                    menu.father();
                } else {
                    second = true;
                    lcd.print("SERVEUR SIP\n" + porte.getIpExterne(), true);
                    cursor = cursorBegin;
                    lcd.setCursor(2, cursor);
                    for (int i = 0; i < 15; i++) {
                        input[i] = 0;
                    }
                    for (int i = 0; i < porte.getIpExterne().length() && i < 15; i++) {
                        input[i] = porte.getIpExterne().charAt(i);
                    }
                }
            }
            if (event instanceof EventKeyP) {
                cursor = cursorBegin;
                lcd.setCursor(2, cursor);

                /*
		** lcd.print(porte.getIpInterne(), false);
		** lcd.setCursor(2, cursor);
		** input = new char[16];
		** for (int i = 0;i <porte.getIpInterne().length() && i<15;i++)
		** input[i] = porte.getIpInterne().charAt(i);
                 */
            }
            c = c < 4 ? c + 1 : 0;
            lcd.setCursor(2, cursor);
            lastEvent = event;
        }
    }
}
