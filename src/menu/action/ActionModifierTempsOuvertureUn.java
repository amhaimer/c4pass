package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Temps Ouverture Un
 *
 * @author Abdellah
 */
public class ActionModifierTempsOuvertureUn implements ActionItem {

    private final int cursorBegin = 11;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getTempsOuvertureUn().toCharArray();
    int count = 0;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("nomporte1", langue), false);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getTempsOuvertureUn() + " s", false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - 11] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - 11] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - 11] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - 11] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - 11] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - 11] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - 11] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - 11] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - 11] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - 11] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {
            count = 0;
            cursor = cursorBegin;
            porte.setTempsOuvertureUn(String.valueOf(input));
            System.out.println(input);
            data.addEvent(data.getTraduction("Saisie Gestion", langue), data.getTraduction("Duree Ouverture", langue), data.getTraduction("nomporte1", langue) + String.valueOf(input) + "s", " ", " ");
            data.update(porte);
            menu.next();
        } else if (event instanceof EventKeyB) {
            cursor = cursorBegin;
            count = 0;
            menu.father();
        } else if (event instanceof EventKeyP) {
            cursor = cursorBegin;
            input = "05".toCharArray();
            lcd.setCursor(2, cursor);
            lcd.print("05", false);
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor < 13) {
                lcd.setCursor(2, cursor);
            }
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 11) {
                cursor = 12;
            }
            lcd.setCursor(2, cursor);
        }
        if (cursor >= 12) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
    }
}
