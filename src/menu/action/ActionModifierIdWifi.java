//! \author AM
//! \date 18 novembre 2016
//! \file ActionModifieridWifi.java action de modification .
package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * Action Modifier Id Wifi
 *
 * @author Abdellah
 */
public class ActionModifierIdWifi implements ActionItem {

    private Process p;
    private ProcessBuilder pb;
    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    boolean boo;
    Event lastEvent = new EventKeyValid();

    int i;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char[] input = new char[24];
    int indice = 1;

    private int c = 0;
    private final char[] lettre = " .,:-_1ABCabc2DEFdef3GHIghi4JKLjkl5MNOmno6PQRpqr7STUstu8VWXvwx9YZyz0\'#0".toCharArray();

    /*
    ** char[] lettre={' ','.',',','@','-','_','1','A','B','C','a','b','c','2','D',
    ** 'E','F','d','e','f','3','G','H','I','g','h','i','4','J','K','L','j','k','l',
    ** '5','M','N','O','m','n','o','6','P','Q','R','S','p','q','r','s','7','T','U',
    ** 'V','t','u','v','8','W','X','Y','Z','w','x','y','z','9','-','\'','0'};
     */
    /**
     * init
     */
    @Override
    public void init() {

        porte = Porte.getInstance();
        cursor = cursorBegin;
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        System.out.println(porte.getIdWifi());
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        if (porte.getIdWifi() != null) {
            lcd.print(porte.getIdWifi(), false);
            lcd.setCursor(2, cursor);
            for (i = 0; i < porte.getIdWifi().length(); i++) {
                input[i] = porte.getIdWifi().charAt(i);
            }
            while (i < 15) {
                input[i++] = ' ';
            }
        }
        cursor = cursorBegin;
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        /*boo=true;
	  
	  if (event instanceof EventTimer)
{
boo=false;
}
 if(cursor ==cursorEnd || cursor <cursorBegin)
		cursor=cursorBegin;
 lcd.setCursor(2,cursor);
if(c>7 || !(precevent.getClass().equals( event.getClass())) && boo)
	c=0;
	else if(boo)
	c++;

 menu = Menu.getInstance();
 lcd = Lcd.getInstance();
	
	if (event instanceof EventKeyB)
	 {
	//	input = "";
		menu.father();
	 }
	if (event instanceof EventKey1)
	 {
		lcd.print(String.valueOf(lettre[0+c]),false);
		input[indice]= lettre[0+c];
		if(c==6) c=c+2;
	 }
	if (event instanceof EventKey2)
	 {
		lcd.print(String.valueOf(lettre[7+c]),false);
		input[indice]= lettre[7+c];
		if(c==6) c=c+2;
	 }
	if (event instanceof EventKey3)
	 {
		lcd.print(String.valueOf(lettre[14+c]),false);
		input[indice]= lettre[14+c];
		if(c==6) c=c+2;
	 }
	if (event instanceof EventKey4)
	 {
		lcd.print(String.valueOf(lettre[21+c]),false);
		input[indice]= lettre[21+c];
		if(c==6) c=c+2;
	 }
	if (event instanceof EventKey5)
	 {
		lcd.print(String.valueOf(lettre[28+c]),false);
		input[indice]= lettre[28+c];
		if(c==6) c=c+2;
	 }
	if (event instanceof EventKey6)
	 {
		lcd.print(String.valueOf(lettre[35+c]),false);
		input[indice]= lettre[35+c];
		if(c==6) c=c+2;
	 }

	if (event instanceof EventKey7)
	 {
		lcd.print(String.valueOf(lettre[42+c]),false);
		input[indice]= lettre[42+c];
		
	 }

	if (event instanceof EventKey8)
	 {
		lcd.print(String.valueOf(lettre[51+c]),false);
		input[indice]= lettre[51+c];
		if(c==6) c=c+2;
	 }
	if (event instanceof EventKey9)
	 {
		lcd.print(String.valueOf(lettre[58+c]),false);
		input[indice]= lettre[58+c];
		
	 }
	if (event instanceof EventKey0)
	 {
		lcd.print(String.valueOf(lettre[67+c]),false);
		input[indice]= lettre[67+c];
		
		if(c==2) c=c+6;
	 }
if (event instanceof EventKeyRight)
	 {
		cursor ++;
		indice++;
	 }
if (event instanceof EventKeyLeft)
	 {
		cursor--;
		indice--;
		}
         */
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        /*
	** DataBase data=getInstance();
	** Porte porte=Porte.getInstance();
         */
        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;

        /*
	** System.out.println(c);
         */
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor] = lettre[0 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[7 + c]), false);
            input[cursor] = lettre[7 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[14 + c]), false);
            input[cursor] = lettre[14 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[21 + c]), false);
            input[cursor] = lettre[21 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[28 + c]), false);
            input[cursor] = lettre[28 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[35 + c]), false);
            input[cursor] = lettre[35 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[42 + c]), false);
            input[cursor] = lettre[42 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[49 + c]), false);
            input[cursor] = lettre[49 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[56 + c]), false);
            input[cursor] = lettre[56 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[63 + c]), false);
            input[cursor] = lettre[63 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            c = 0;
            if (cursor <= 15) {
                lcd.setCursor(2, cursor);
            }
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            c = 0;
            if (cursor < 0) {
                cursor = (porte.getIdWifi().length() - 1);
            }
        }
        if (event instanceof EventKeyValid) {
            String idWifi = new String(input);

            /*
	    ** System.out.println(idWifi);
             */
            porte.setIdWifi(idWifi.trim());

            try {
                pb = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's/.*ssid.*/ssid=\"" + idWifi.trim() + "\"/g' /etc/wpa_supplicant/wpa_supplicant.conf");
                p = pb.start();
            } catch (IOException e) {
                System.out.println("ERROR");
            }
            data.update(porte);
            menu.next();

            /* if (!(event instanceof EventTimer))
	    ** {
	    ** precevent=event;
	    ** }
             */
        }
        if (event instanceof EventKeyP) {
            for (int i = 0; i < 24; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print("                ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        c = c < 7 ? c + 1 : 0;
        lcd.setCursor(2, cursor);
        lastEvent = event;
    }
}
