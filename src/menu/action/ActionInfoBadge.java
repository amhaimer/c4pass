package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * action info badge
 *
 * @author Abdellah
 */
public class ActionInfoBadge implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();

    /**
     * init
     */
    @Override
    public void init() {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        porte = Porte.getInstance();
        int nb_lecteur = getNbLecteur();
        if (nb_lecteur != 0) {
            lcd.print(nb_lecteur + " LECTEUR" + (nb_lecteur > 1 ? "S : " : " : "), true);
            lcd.print((isUartRunning() ? "OK" : "HS"), false);
            lcd.setCursor(2, 0);
            lcd.print(data.getNbBadge() + " BADGES", false);
        } else {
            lcd.print(" AUCUN LECTEUR\n ENREGISTRE", true);
        }

    }

    /**
     * is uart running
     *
     * @return true or false
     */
    private boolean isUartRunning() {

        try {
            Process p = Runtime.getRuntime().exec("pidof uart");
            BufferedReader pid = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((pid.readLine()) != null) {
                return true;
            }
        } catch (IOException e) {
            return false;
        }
        return false;
    }

    private int getNbLecteur() {

        int nb_lecteur = 0;
        if (porte.getLecteurPorte1().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte2().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte3().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte4().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte5().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte6().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte7().length() > 0) {
            nb_lecteur++;
        }
        if (porte.getLecteurPorte8().length() > 0) {
            nb_lecteur++;
        }
        return nb_lecteur;
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
