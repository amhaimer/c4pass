package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;

import sql.DataBase;

/**
 * compter nombre de residetns
 *
 * @author Abdellah
 */
public class ActionCompterNbResidents implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();

    private String NbResident = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);

        NbResident = data.getNbResidents();
        /*lcd.setCursor(2,0);
	lcd.print(data.getTraduction("ficheresident",langue), false);*/
        //lcd.print("OUI=1 NON=0: ", false);
        //lcd.print(input[0] == '0' ? "NON" : "OUI", false);
        lcd.setCursor(2, 7);
        lcd.print(NbResident, false);

    }

    /**
     * exit
     */
    @Override
    public void exit() {

    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        //lcd.setCursor(2, 13);
        if (event instanceof EventKeyValid) {
            menu.next();

        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
