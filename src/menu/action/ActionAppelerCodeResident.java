package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import utils.UsersManager;
import sql.data.Resident;
import sql.DataBase;
import sql.data.Porte;

/**
 * appler en entrant un code resident et nous permets aussi le defilements
 *
 * @author Abdellah
 */
public class ActionAppelerCodeResident implements ActionItem {

    //! \class ActionEntreeCodeGestionaire action enntrer code gestionaire.
    DataBase data = DataBase.getInstance();
    private String langue = "";
    private Menu menu;
    private Lcd lcd;
    Porte porte;
    devices.TimerLocal timer;
    private char[] idInterphone;
    public static String code = new String();
    public static String codeResident = "";

    public char t_clavier;
    public String input = new String();
    DefilementResident defile = new DefilementResident();
    EventManager manager = EventManager.getInstance();

    /*
    ** pour le mode Megallan
     */
    private PorteManager portemanager;

    /**
     * init
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        portemanager = PorteManager.getInstance();
        input = "";
        porte = Porte.getInstance();
        idInterphone = porte.getInterphone().toCharArray();
        langue = porte.getTraduction();
        timer = manager.requestTimer(30, devices.TimerLocal.Unit.sec);

        t_clavier = porte.getInterphone().charAt(1);

        if (DefilementResident.dInput.length() == 0) {
            input += ActionSwitchTime.premierchiffre;
            if (t_clavier == '4' || t_clavier == '5') {
                input = "";
                input = "00" + ActionSwitchTime.premierchiffre;
                System.out.println(input);

            } else {
                lcd.print(menu.toString(), true);
                lcd.setCursor(2, 0);
                lcd.print(data.getTraduction("dappel", langue) + "      ", false);
                lcd.setCursor(2, 10);
                lcd.saisie(true);
                portemanager = PorteManager.getInstance();
                DataBase data2 = DataBase.getInstance();
                Porte porte = data2.getPorte();
            }
        } else {
            input = DefilementResident.dInput;
            DefilementResident.dInput = "";
        }
        lcd.print(input, false);

        /*
	** }
	** }
         */
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        input = "";

        lcd.saisie(false);
        manager.releaseTimer(timer);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        portemanager = PorteManager.getInstance();
        DataBase data1 = DataBase.getInstance();
        Porte porte = data1.getPorte();

        /*
	** essai pour appeler directement magellan
         */
        if (event instanceof EventKey1 || event instanceof EventKey2 || event instanceof EventKey3
                || event instanceof EventKey4 || event instanceof EventKey5 || event instanceof EventKey6
                || event instanceof EventKey7 || event instanceof EventKey8 || event instanceof EventKey9 || event instanceof EventKey0) {

            if (input.length() >= 3) {
                init();
            }

        }

        if (event instanceof EventKeyB) {
            if (idInterphone[0] == '3' && input.length() >= 3) {
                Resident resident = data.getResident(input);
                System.out.println(resident.getCode());
                UsersManager users = new UsersManager();
                if (resident.getCode() != null) {
                    DefilementResident.codeResident = resident.getCode();
                    menu.father();
                    menu.child();
                    menu.next();
                    menu.next();
                    menu.child();
                }
            } else {
                ActionSwitchTime.premierchiffre = "";
                DefilementResident.dInput = "";
                input = "";
                menu.father();
            }

        }
        if (event instanceof EventKey0) {

            lcd.print("0", false);
            input += "0";
        }
        if (event instanceof EventKey1) {

            /*
	    ** en mode Magellan
             */
 /*
	    ** if (porte.getInterphone().charAt(1) == '1' ||
	    ** porte.getInterphone().charAt(1) == '5'|| porte.getInterphone().charAt(1) == '4')
	    ** {lcd.print("001" , false );
	    ** input="001";
	    ** DataBase data=getInstance();
	    ** Resident resident=data.getResident(input);
	    ** UsersManager users = new UsersManager();
	    ** if(resident.getCode()==null)
	    ** {
	    ** lcd.clearLcd();
	    ** lcd.print("AUCUN RESIDENT\nENREGISTRE",true);
	    ** maintenir l'affichage pendant 1.5s
	    ** try{Thread.sleep(1500);}
	    ** catch(InterruptedException e){}
	    ** timer = manager.requestTimer(3,devices.Timer.Unit.sec);		
	    ** menu.father();
	    ** }
	    ** else {
	    ** lcd.print(resident.getName(), true);
	    ** lcd.setCursor(2,0);
	    ** lcd.print("CODE D'APPEL:"+resident.getCode(), false);
	    ** DefilementResident.codeResident=resident.getCode();	    
	    ** menu.father();
	    ** menu.child();
	    ** menu.next();
	    ** menu.next();
	    ** menu.child();
	    ** menu.next();
	    ** menu.next();
	    ** }
	    ** }
	    ** else {
             */
            lcd.print("1", false);
            input += "1";

            /*
	    ** }
             */
        }
        if (event instanceof EventKey2) {

            /*
	    ** en mode Magellan
             */
            lcd.print("2", false);
            input += "2";
        }
        if (event instanceof EventKey3) {

            lcd.print("3", false);
            input += "3";
        }
        if (event instanceof EventKey4) {

            lcd.print("4", false);
            input += "4";
        }
        if (event instanceof EventKey5) {

            lcd.print("5", false);
            input += "5";
        }
        if (event instanceof EventKey6) {

            lcd.print("6", false);
            input += "6";
        }
        if (event instanceof EventKey7) {

            lcd.print("7", false);
            input += "7";
        }
        if (event instanceof EventKey8) {

            lcd.print("8", false);
            input += "8";
        }
        if (event instanceof EventKey9) {

            lcd.print("9", false);
            input += "9";
        }
        if (event instanceof EventKeyP) {

            lcd.setCursor(2, 10);
            lcd.print(" ", false);
            lcd.setCursor(2, 10);
            input = "";
        }
        if (event instanceof EventTimer) {
            exit();
            menu.father();
        }
        if (input.length() >= 3) {
            System.out.println(input);
            data = DataBase.getInstance();
            Resident resident = data.getResident(input);
            System.out.println(resident.getCode());
            //  UsersManager users = new UsersManager();
            if (resident.getCode() == null) {

                lcd.clearLcd();
                lcd.print(center(data.getTraduction("ceraccourci", langue)) + "\n" + center(data.getTraduction("inexistant", langue)), false);

                lcd.saisie(false);

                /*
		    ** maintenir l'affichage pendant 1.5s
                 */
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                }

                /*
		    ** timer = manager.requestTimer(3,devices.Timer.Unit.sec);
                 */
                menu.father();
            } else {

                lcd.clearLcd();
                if (resident.getListeRouge().equals("0")) {

                    lcd.print(resident.getName(), true);
                }

                lcd.setCursor(2, 0);
                lcd.print("CODE D'APPEL:" + resident.getCode(), false);
                System.out.println("-------------------------------------------------\npasser appel vers " + input + " depuis l'interphone " + porte.getInterphone());

                if (t_clavier == '4' || t_clavier == '5') {
                    DefilementResident.codeResident = resident.getCode();
                    menu.father();
                    menu.child();
                    menu.next();
                    menu.next();
                    menu.child();

                }

                if (event instanceof EventKeyValid) {

                    DefilementResident.codeResident = resident.getCode();
                    menu.father();
                    menu.child();
                    menu.next();
                    menu.next();
                    menu.child();

                }

                if (event instanceof EventKeyLeft || event instanceof EventKeyRight) {

                    menu.father();
                    menu.child();
                    menu.next();
                    menu.next();

                }
            }

        }

    }

    private String center(String chaine) {
        String tmp = chaine.trim();
        String tmp2 = tmp;
        for (int j = 0; j < ((16 - tmp2.length()) / 2); j++) {
            tmp = " " + tmp;
        }
        return tmp;
    }
}
