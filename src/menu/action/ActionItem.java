package menu.action;

import event.Event;

/**
 * interface des actions item
 *
 * @author Abdellah
 */
public interface ActionItem {

    /**
     * init
     */
    void init();

    /**
     * exit
     */
    void exit();

    /**
     * action
     *
     * @param event evenement
     */
    void action(Event event);
}
