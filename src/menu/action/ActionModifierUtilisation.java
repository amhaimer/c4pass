package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.DataBase;

/**
 * Action Modifier Utilisation
 *
 * @author Abdellah
 */
public class ActionModifierUtilisation implements ActionItem {

    private final int cursorBegin = 0;
    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private char[] input = new char[10];
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private int c, line;
    private boolean saisie, standard;
    private final char[] lettre = " .,1ABC2DEF3GHI4JKL5MNO6PQRS7TUV8WXYZ9-\'#0".toCharArray();
    private Event lastEvent;
    private Item sub1, ItemUtilisation;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        sub1 = menu.currentItem.father;
        ItemUtilisation = sub1.childs.get(0);
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        System.out.println(porte.getMessageUtilisation());
        System.out.println(porte.getMessageUtilisation());
        System.out.println(porte.getMessageUtilisation());
        System.out.println(porte.getMessageUtilisation());
        input = new char[32];
        line = 1;
        int i = 0;
        while (i < porte.getMessageUtilisation().length()) {
            input[i] = porte.getMessageUtilisation().charAt(i++);
        }
        while (i < 32) {
            input[i++] = ' ';
        }
        cursor = cursorBegin;
        c = 0;
        saisie = false;
        lastEvent = new EventKeyValid();
        lcd.print(data.getTraduction("messagestandard", porte.getTraduction()), true);
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        lcd.print(data.getTraduction("ouinon", langue), false);
        standard = porte.getMessageUtilisation().equals("CHERCHER AVEC <<OU >> PUIS APPEL");
        lcd.print(porte.getMessageUtilisation().equals("CHERCHER AVEC <<OU >> PUIS APPEL") ? "OUI" : "NON", false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (saisie) {
            if (event instanceof EventKey1) {
                lcd.print(String.valueOf(lettre[0 + c]), false);
                input[cursor] = lettre[0 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey2) {
                lcd.print(String.valueOf(lettre[4 + c]), false);
                input[cursor] = lettre[4 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey3) {
                lcd.print(String.valueOf(lettre[8 + c]), false);
                input[cursor] = lettre[8 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey4) {
                lcd.print(String.valueOf(lettre[12 + c]), false);
                input[cursor] = lettre[12 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey5) {
                lcd.print(String.valueOf(lettre[16 + c]), false);
                input[cursor] = lettre[16 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey6) {
                lcd.print(String.valueOf(lettre[20 + c]), false);
                input[cursor] = lettre[20 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey7) {
                lcd.print(String.valueOf(lettre[24 + c]), false);
                input[cursor] = lettre[24 + c];
            }
            if (event instanceof EventKey8) {
                lcd.print(String.valueOf(lettre[29 + c]), false);
                input[cursor] = lettre[29 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKey9) {
                lcd.print(String.valueOf(lettre[33 + c]), false);
                input[cursor] = lettre[33 + c];
            }
            if (event instanceof EventKey0) {
                lcd.print(String.valueOf(lettre[38 + c]), false);
                input[cursor] = lettre[38 + c];
                c = c == 3 ? c + 1 : c;
            }
            if (event instanceof EventKeyRight) {
                cursor++;
            }
            if (event instanceof EventKeyLeft) {
                cursor--;
                if (cursor < 0) {
                    line = 2;
                    cursor = 31;
                }
            }
            if (cursor == 15) {
                line = 1;
                lcd.setCursor(1, 16);
            }
            if (cursor == 16) {
                lcd.setCursor(2, 0);
                line = 2;
            }
            if (cursor == 32) {
                line = 1;
                cursor = 0;
                lcd.setCursor(1, cursor);
            }
            if (event instanceof EventKeyValid) {
                String msg = new String(input);
                msg = msg.trim();
                String msg_e = msg.substring(0, 16);
                ItemUtilisation.affichage = msg;
                porte.setMessageUtilisation(msg);
                data.update(porte);
                //data.addEvent("SAISIE ENR.PORTE", "Modification du", "Mess.utilisation"," "," ");
                data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("mesutilisationl3", langue), msg_e);
                menu.father();
            }
            if (event instanceof EventKeyP) {
                lcd.saisie(false);
                cursor = 0;
                lcd.print(porte.getMessageUtilisation().substring(0, 16), true);
                lcd.setCursor(2, 0);
                lcd.print(porte.getMessageUtilisation().substring(16), false);
                lcd.setCursor(2, 0);
                line = 1;
                int i = 0;
                while (i < porte.getMessageUtilisation().length()) {
                    input[i] = porte.getMessageUtilisation().charAt(i++);
                }
                while (i < 32) {
                    input[i++] = ' ';
                }
                lcd.saisie(true);
            }
        } else {
            if (event instanceof EventKey1) {
                standard = true;
                lcd.setCursor(2, 13);
                //lcd.print("OUI", false);
                lcd.print(data.getTraduction("oui", langue), false);
            }
            if (event instanceof EventKey0) {
                standard = false;
                lcd.setCursor(2, 13);
                lcd.print(data.getTraduction("non", langue), false);
                //lcd.print("NON", false);
            }
            if (event instanceof EventKeyValid) {
                System.out.println(lastEvent.getClass());
                if (!standard) {
                    saisie = true;
                    lcd.setCursor(1, 0);
                    lcd.clearLcd();
                    if (porte.getMessageUtilisation().length() > 16) {
                        lcd.print(porte.getMessageUtilisation().substring(0, 16), true);
                    } else {
                        lcd.print(porte.getMessageUtilisation().substring(0, porte.getMessageUtilisation().length()), true);
                        lcd.setCursor(2, 0);
                    }
                    if (porte.getMessageUtilisation().length() > 16) {
                        lcd.print(porte.getMessageUtilisation().substring(16), false);
                    }
                    lcd.setCursor(1, 0);
                    lcd.saisie(true);
                } else {
                    ItemUtilisation.affichage = "CHERCHER AVEC <<OU >> PUIS APPEL";
                    porte.setMessageUtilisation("CHERCHER AVEC <<OU >> PUIS APPEL");
                    data.update(porte);
                    data.addEvent("Saisie Gestion", "Menu Utilisation",
                            "MESSAGE STANDARD", "----------------", "----------------");
                    menu.father();
                }
            }
        }
        c = c < 4 ? c + 1 : 0;
        if (line == 1) {
            lcd.setCursor(1, cursor);
        } else {
            lcd.setCursor(2, cursor - 16);
        }
        lastEvent = event;
    }
}
