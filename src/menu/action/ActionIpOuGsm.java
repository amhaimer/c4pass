package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * action de modification du type de produit
 *
 * @author Abdellah
 */
public class ActionIpOuGsm implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[];

    /**
     * init
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        input = porte.getInterphone().toCharArray();
        lcd.print("SIP? VOIP? GSM?\n S=5 V=4 G=3:", true);
        switch (input[0]) {
            case '3':
                lcd.print("G", false);
                break;
            case '4':
                lcd.print("V", false);
                break;
            default:
                lcd.print("S", false);
                lcd.setCursor(2, 13);
                break;
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);

        if (event instanceof EventKey3) {
            lcd.print("G", false);
            input[0] = '3';
        } else if (event instanceof EventKey4) {
            lcd.print("V ", false);
            input[0] = '4';
        } else if (event instanceof EventKey5) {
            lcd.print("S", false);
            input[0] = '5';
        } else if (event instanceof EventKeyValid) {
            porte.setInterphone(String.valueOf(input));
            data.update(porte);
            if (input[0] == '4') {
                menu.next();
                menu.next();
            } else if (input[0] == '3') {
                menu.next();
                menu.next();
                menu.next();
                menu.next();
            }
            menu.next();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
