package menu.action;

import event.Event;
import menu.Menu;
import event.eventType.*;
import event.EventManager;
import devices.Lcd;
import java.util.*;
import sql.DataBase;
import sql.data.Evenement;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 * Action Visualiser Evenements
 *
 * @author Abdellah
 */
public class ActionVisualiserEvenements implements ActionItem {

    boolean more = false;
    int sousmenu;
    boolean nextCall = false;
    boolean previousCall = false;
    Menu menu;
    private Lcd lcd = Lcd.getInstance();
    DataBase data = DataBase.getInstance();
    EventManager manager = EventManager.getInstance();
    List<Evenement> evenements = data.getEvenements();
    ListIterator itr = evenements.listIterator();
    int idx;
    Evenement evenement;
    DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    /**
     * init
     */
    @Override
    public void init() {
        idx = 0;
        this.more = false;
        this.evenements = data.getEvenements();
        this.itr = evenements.listIterator();
        lcd.print("CHERCHER PAR <<>>\nAPPEL = AFFICHER", true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        if (event instanceof EventKeyRight && !more) {
            nextCall = true;
            if (previousCall) {
                previousCall = false;
                if (itr.hasNext()) {
                    evenement = (Evenement) itr.next();
                }
            }
            if (itr.hasNext()) {
                idx++;
                evenement = (Evenement) itr.next();
            } else {
                idx = 1;
                itr = evenements.listIterator();
                evenement = (Evenement) itr.next();
            }
            lcd.print(evenement.getType(), true);
            lcd.setCursor(1, 13);
            lcd.print(String.valueOf(idx), false);
            lcd.setCursor(2, 0);
            Date date = null;
            try {
                date = originalFormat.parse(evenement.getDate());
            } catch (ParseException e) {
                System.err.print(e.getMessage());
            }
            String formattedDate = targetFormat.format(date);
            lcd.print(formattedDate, false);
        }
        if (more && (event instanceof EventKeyRight || event instanceof EventKeyLeft)) {
            if (sousmenu == 1) {
                System.out.println(evenement.getLigne3());
                System.out.println(evenement.getLigne4());
                sousmenu = 2;
                lcd.print(evenement.getLigne3(), true);
                lcd.setCursor(2, 0);
                lcd.print(evenement.getLigne4(), false);
            }
        } else {
            sousmenu = 1;
            lcd.print(evenement.getLigne1(), true);
            lcd.setCursor(2, 0);
            lcd.print(evenement.getLigne2(), false);
        }
        if (event instanceof EventKeyLeft && !more) {
            previousCall = true;
            if (nextCall) {
                nextCall = false;
                if (itr.hasPrevious()) {
                    evenement = (Evenement) itr.previous();
                }
            }
            if (itr.hasPrevious()) {
                idx--;
                evenement = (Evenement) itr.previous();
            } else {
                while (itr.hasNext()) {
                    idx++;
                    evenement = (Evenement) itr.next();
                }
            }
            lcd.print(evenement.getType(), true);
            lcd.setCursor(1, 13);
            lcd.print(String.valueOf(idx), false);
            lcd.setCursor(2, 0);
            Date date = null;
            try {
                date = originalFormat.parse(evenement.getDate());
            } catch (ParseException e) {
                System.err.print(e.getMessage());
            }
            String formattedDate = targetFormat.format(date);
            lcd.print(formattedDate, false);
        }
        if (event instanceof EventKeyValid && !more) {
            sousmenu = 1;
            more = true;
            lcd.print(evenement.getLigne1(), true);
            lcd.setCursor(2, 0);
            lcd.print(evenement.getLigne2(), false);
        }
        if (event instanceof EventKeyB && !more) {
            itr = evenements.listIterator();
            menu.father();
        }
        if (event instanceof EventKeyB && more) {
            more = false;
            lcd.print(evenement.getType(), true);
            lcd.setCursor(1, 13);
            lcd.print(String.valueOf(idx), false);
            lcd.setCursor(2, 0);
            Date date = null;
            try {
                date = originalFormat.parse(evenement.getDate());
            } catch (ParseException e) {
                System.err.print(e.getMessage());
            }
            String formattedDate = targetFormat.format(date);
            lcd.print(formattedDate, false);
        }
    }
}
