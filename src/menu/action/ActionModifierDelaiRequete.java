package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * mofidier le delai de onnexion Web
 *
 * @author Abdellah
 */
public class ActionModifierDelaiRequete implements ActionItem {

    private final int cursorBegin = 10;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char[] input = new char[5];

    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        if (porte.getTimeConnexionWeb() != null) {
            int i;
            lcd.print(porte.getTimeConnexionWeb(), false);
            for (i = 0; i < porte.getTimeConnexionWeb().length(); i++) {
                input[i] = porte.getTimeConnexionWeb().charAt(i);
            }
            while (i < 4) {
                input[i++] = ' ';
            }
        }
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (cursor > 14) {
            cursor = cursorBegin;
            lcd.setCursor(10, cursor);
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - 10] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - 10] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - 10] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - 10] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - 10] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - 10] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - 10] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - 10] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - 10] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - 10] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {
            cursor = cursorBegin;
            count = 0;
            String nom = new String(input);

            porte.setTimeConnexionWeb(nom.trim());
            data.update(porte);
            menu.next();
        } else if (event instanceof EventKeyB) {
            cursor = cursorBegin;
            count = 0;
            menu.father();
        }
        if (event instanceof EventKeyP) {
            for (int i = 0; i < 5; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 10);
            lcd.print(" ", false);
            lcd.setCursor(2, 10);
            cursor = cursorBegin;
        }
    }
}

/*
** public class ActionModifierDelaiRequete implements ActionItem{
**
** private final int cursorBegin = 0;
** private final int cursorEnd = 15 ;
** private int cursor = cursorBegin;
** private Menu menu;
** private Lcd lcd;
** DataBase data=getInstance();
** Porte porte=Porte.getInstance();
** private char[] input=new char[15];
** //private char input[]=porte.getTimeConnexionWeb().toCharArray();
** int count=0;
**
** public void init(){
**	 Lcd lcd = Lcd.getInstance();
**	 Menu menu = Menu.getInstance();
**	 lcd.print(menu.toString(),true);
**	lcd.setCursor(2,2);
**	//lcd.print(porte.getTimeConnexionWeb(),false);
** lcd.print("s:",false);
** lcd.setCursor(2,6);
** lcd.print("m:",false);
** lcd.setCursor(2,10);
** lcd.print("h",false);
**	lcd.setCursor(2,cursor);
**	lcd.saisie(true);
** }
**
** public void exit(){
**	 lcd.saisie(false);
** }
**
**public void action(Event event){
**
** menu = Menu.getInstance();
** lcd = Lcd.getInstance();
**	
**	 if (cursor > 14)
**	 {
**		cursor = cursorBegin;
**		lcd.setCursor(2,cursor);
**
**	 }
** if (cursor > 1)
** {
** // cursor = 4;
** lcd.setCursor(2,4);
** }
**if (cursor > 6)
** {
** cursor = 8;
** lcd.setCursor(2,8);
**}
**
**	 if (event instanceof EventKey0)
**	 {
**		lcd.print("0",false);
**		input[cursor]= '0';
**		cursor++;
**	 }
**	 	
**	else if (event instanceof EventKey1)
**	 {
**		lcd.print("1",false);
**		input[cursor]= '1';
**		cursor++;
**	 }
**	else if (event instanceof EventKey2)
**	 {
**		lcd.print("2",false);
**		input[cursor]= '2';
**		cursor++;	
**	 }
**	else if (event instanceof EventKey3)
**	 {
**		lcd.print("3",false);
**		input[cursor]='3' ;
**		cursor++;
**	 }
**	else if (event instanceof EventKey4)
**	 {
**		
**		lcd.print("4",false);
**		input[cursor]= '4';
**		cursor++;
**	 }
**	else if (event instanceof EventKey5)
**	 {
**		
**		lcd.print("5",false);
**		input[cursor]= '5';
**		cursor++;
**	 }
**	else if (event instanceof EventKey6)
**	 {
**		
**		lcd.print("6",false);
**		input[cursor]= '6';
**		cursor++;
**	 }
**
**	else if (event instanceof EventKey7)
**	 {
**		
**		lcd.print("7",false);
**		input[cursor]= '7';
**		cursor++;
**	 }
**
**	else if (event instanceof EventKey8)
**	 {
**		
**		lcd.print("8",false);
**		input[cursor]= '8';
**		cursor++;
**	 }
**	else if (event instanceof EventKey9)
**	 {
**		
**		lcd.print("9",false);
**		input[cursor]= '9';
**		cursor++;
**	 }
**
**
**	else if (event instanceof EventKeyValid)
**	 {
**		count=0;
**		cursor = cursorBegin;
**		porte.setTimeConnexionWeb(String.valueOf(input));
**		data.update(porte);
**menu.next();
**menu.father();
**}
**else if (event instanceof EventKeyB)
**{
**cursor = cursorBegin;
**count=0;
**menu.father();
**}
**if (event instanceof EventKeyP)
**{
**for(int i=0;i<5;i++)
**input[i]=' ';
**lcd.setCursor(2,10);
**lcd.print(" ",false);
**lcd.setCursor(2,10);
**cursor = cursorBegin;
**}
**}
**}
 */
