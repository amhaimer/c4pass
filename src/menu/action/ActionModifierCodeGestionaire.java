package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * action de modification du code gestionnaire
 *
 * @author Abdellah
 */
public class ActionModifierCodeGestionaire implements ActionItem {

    private final int cursorBegin = 4;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getCode().toCharArray();

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.clearLcd();
        lcd.print("CODEGESTIONNAIRE" + "\n" + " ", true);
        lcd.print(porte.getInterphone().charAt(1) == '3' ? "P" : "A", false);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getCode(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - 4] = '0';
            cursor++;
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - 4] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - 4] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - 4] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - 4] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - 4] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - 4] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - 4] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - 4] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - 4] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {

            porte.setCode(String.valueOf(input));
            data.update(porte);
            data.addEvent("SAISIE ENR.PORTE", "Modification du", "Code Gestion", " ", " ");
            menu.father();
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor - 4 < 0) {
                cursor = 7;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor < 8) {
                lcd.setCursor(2, cursor);
            }
        } else if (event instanceof EventKeyB) {

            menu.father();
        } else if (event instanceof EventKeyP) {
            input = "9999".toCharArray();
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print("9999", false);
            lcd.setCursor(2, cursor);
        }
        if (cursor > 7) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
    }
}
