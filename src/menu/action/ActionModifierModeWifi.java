package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * Action Modifier Mode Wifi
 *
 * @author Abdellah
 */
public class ActionModifierModeWifi implements ActionItem {

    private Process p2;
    private ProcessBuilder pb2;
    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getValidWifiPi().toCharArray();
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
        lcd.setCursor(2, 13);
        if (porte.getValidWifiPi().equals("1")) {
            lcd.print("OUI", false);
        } else {
            lcd.print("NON", false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print("NON", false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print("OUI", false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;

            int mode = Integer.parseInt(porte.getValidWifiPi());
            porte.setValidWifiPi(String.valueOf(input));
            data.update(porte);
            if (input[0] == '1' && mode == 0) {

//mettre les commandes pour activer le routeru wifi
// apres sudo ifdown wlan0
// sudo /etc/init.d/networkin restart 
// sudo /etc/init.d/hostapd start
// sudo /etc/init.d/dnsmasq start
// sudo ifup wlan0
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/network/interfaces /etc/network/interfacesWifi");
                    p2 = pb2.start();
                    System.out.println("rename wifi to 1");
                } catch (IOException e) {
                    System.out.println("ERROR rename");
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                try {

                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/network/interfacesRouter /etc/network/interfaces");
                    p2 = pb2.start();
                    System.out.println("rename 2 to router");
                } catch (IOException e) {
                    System.out.println("ERROR rename");
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/networking restart");
                    p2 = pb2.start();
                    System.out.println("networking restart");
                } catch (IOException e) {
                    System.out.println("ERROR networking");
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/hostapd start");
                    p2 = pb2.start();
                    System.out.println("hostapd start");
                } catch (IOException e) {
                    System.out.println("ERROR hostapd");
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/dnsmasq start");
                    p2 = pb2.start();
                    System.out.println("dnsmasq start");
                } catch (IOException e) {
                    System.out.println("ERROR dnsmasq");
                }
                System.out.println("routeur activé");
                lcd.setCursor(2, 0);
                lcd.print("R: 169.244.222.1", false);

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
                lcd.clearLcd();
                lcd.setCursor(1, 0);
                lcd.print("--MODE ROUTEUR--", false);
                lcd.setCursor(2, 0);
                lcd.print("REDEMARRAGE ...  ", false);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "reboot", "-f");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR rebooting");
                }
                /*  try {
		pb2 = new ProcessBuilder("sudo", "bash", "-c",  "ifdown wlan0");
		p2 = pb2.start();
		System.out.println("wifi down");
	    }
	    catch(IOException e) {
		System.out.println("ERROR ifdown");
	    }
	    try {
		pb2 = new ProcessBuilder("sudo", "bash", "-c",  "ifup wlan0");
		p2 = pb2.start();
		System.out.println("wifi up");
	    }
	    catch(IOException e) {
		System.out.println("ERROR ifup");
	    }*/

            } else if (input[0] == '0' && mode == 1) {

                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/network/interfaces /etc/network/interfacesRouter");
                    p2 = pb2.start();
                    System.out.println("rename routeur to 2");
                } catch (IOException e) {
                    System.out.println("ERROR rename");
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                try {

                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "mv /etc/network/interfacesWifi /etc/network/interfaces");
                    p2 = pb2.start();
                    System.out.println("rename wifi to 1");
                } catch (IOException e) {
                    System.out.println("ERROR rename");
                }

                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/hostapd stop");
                    p2 = pb2.start();
                    System.out.println("hostapd stop");
                } catch (IOException e) {
                    System.out.println("ERROR hostapd");
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/dnsmasq stop");
                    p2 = pb2.start();
                    System.out.println("dnsmasq stop");
                } catch (IOException e) {
                    System.out.println("ERROR dnsmasq");
                }

                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/networking restart");
                    p2 = pb2.start();
                    System.out.println("networking restart...");
                } catch (IOException e) {
                    System.out.println("ERROR networking");
                }

                /*	    try {
		pb2 = new ProcessBuilder("sudo", "bash", "-c",  "ifdown wlan0");
		p2 = pb2.start();
	    }
	    catch(IOException e) {
		System.out.println("ERROR ifdown");
	    }*/
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "bash", "-c", "ifup wlan0");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR ifup");
                }

                menu.child();
                System.out.println("routeur disactivé");
                lcd.clearLcd();
                lcd.setCursor(1, 0);
                lcd.print("MODE WIFI CLIENT", false);
                lcd.setCursor(2, 0);
                lcd.print("REDEMARRAGE ...  ", false);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                try {
                    pb2 = new ProcessBuilder("sudo", "reboot", "-f");
                    p2 = pb2.start();
                } catch (IOException e) {
                    System.out.println("ERROR rebooting");
                }
            } else {
                if (input[0] == '1') {
                    System.out.println("routeur activé");
                    lcd.setCursor(2, 0);
                    lcd.print("R: 169.244.222.1", false);

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                    }
                    menu.father();
                } else {
                    menu.child();
                    System.out.println("routeur disactivé");
                }
            }
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        } else if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        }
    }
}
