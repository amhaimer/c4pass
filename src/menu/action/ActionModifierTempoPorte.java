package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Tempo Porte
 *
 * @author Abdellah
 */
public class ActionModifierTempoPorte implements ActionItem {

    private final int cursorBegin = 14;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte = Porte.getInstance();
    private char input[];
    private String lecteur;
    private String langue = "";

    public ActionModifierTempoPorte(String lecteur) {
        this.lecteur = lecteur;
    }

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString() + lecteur, true);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("temporisation", langue), false);
        input = getTempoPorte(menu.toString().charAt(5) - '0').toCharArray();
        System.out.println(Integer.valueOf(menu.toString().charAt(5) - 48));
        lcd.print(getTempoPorte(menu.toString().charAt(5) - '0'), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    private String getTempoPorte(int id) {

        switch (id) {
            case 1:
                return porte.getTempsOuvertureUn();
            case 2:
                return porte.getTempsOuvertureDeux();
            case 3:
                return porte.getTempsOuvertureTrois();
            case 4:
                return porte.getTempsOuvertureQuatre();
            case 5:
                return porte.getTempsOuvertureCinq();
            case 6:
                return porte.getTempsOuvertureSix();
            case 7:
                return porte.getTempsOuvertureSept();
            default:
                return porte.getTempsOuvertureHuit();
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        /*
	** lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (cursor > 15) {
            cursor = cursorBegin;
            lcd.setCursor(5, cursor);
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - 14] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - 14] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - 14] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - 14] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - 14] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - 14] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - 14] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - 14] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - 14] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - 14] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {
            cursor = cursorBegin;
            set();
            data.update(porte);
            skip();
        } else if (event instanceof EventKeyB) {
            cursor = cursorBegin;
            menu.father();
        }
    }

    /**
     * set value
     */
    private void set() {

        final int current = menu.toString().charAt(5) - '0';
        switch (current) {
            case 1:
                porte.setTempsOuvertureUn(String.valueOf(input));
                break;
            case 2:
                porte.setTempsOuvertureDeux(String.valueOf(input));
                break;
            case 3:
                porte.setTempsOuvertureTrois(String.valueOf(input));
                break;
            case 4:
                porte.setTempsOuvertureQuatre(String.valueOf(input));
                break;
            case 5:
                porte.setTempsOuvertureCinq(String.valueOf(input));
                break;
            case 6:
                porte.setTempsOuvertureSix(String.valueOf(input));
                break;
            case 7:
                porte.setTempsOuvertureSept(String.valueOf(input));
                break;
            default:
                porte.setTempsOuvertureHuit(String.valueOf(input));
                break;
        }
        data.addEvent(data.getTraduction("SAISIE ENR.PORTE", langue), data.getTraduction("Modification de", langue), data.getTraduction("la tempo porte ", langue), current + " ", " ");
    }

    /**
     * skip
     */
    private void skip() {

        final int current = menu.toString().charAt(5) - '0';
        if (current == 1 && !porte.getLecteurPorte2().equals("")) {
            menu.next();
        } else if (current == 2 && !porte.getLecteurPorte3().equals("")) {
            menu.next();
        } else if (current == 3 && !porte.getLecteurPorte4().equals("")) {
            menu.next();
        } else if (current == 4 && !porte.getLecteurPorte5().equals("")) {
            menu.next();
        } else if (current == 5 && !porte.getLecteurPorte6().equals("")) {
            menu.next();
        } else if (current == 6 && !porte.getLecteurPorte7().equals("")) {
            menu.next();
        } else if (current == 7 && !porte.getLecteurPorte8().equals("")) {
            menu.next();
        } else {
            menu.father();
        }
    }
}
