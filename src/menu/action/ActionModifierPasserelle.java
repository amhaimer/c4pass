package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import java.io.*;
import sql.data.Porte;

/**
 * Action Modifier Passerelle
 *
 * @author Abdellah
 */
public class ActionModifierPasserelle implements ActionItem {

    private Process p;
    private ProcessBuilder pb;

    private final int cursorBegin = 0;
    private final int cursorEnd = 15;
    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char[] input = new char[15];

    private String passerelle;

    /**
     * init
     */
    @Override
    public void init() {

        porte = Porte.getInstance();
        menu = Menu.getInstance();

        cursor = cursorBegin;
        lcd = Lcd.getInstance();

        lcd.clearLcd();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getPasserelle(), false);
        lcd.setCursor(2, cursor);
        for (int i = 0; i < porte.getPasserelle().length(); i++) {
            input[i] = porte.getPasserelle().charAt(i);
        }
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        /*
	** if(c==0)
	** {
	**	lcd.print(porte.getIpInterne(),false);
	**	lcd.setCursor(2,cursor);
	** }
	** c++;
         */
        if (cursor >= cursorEnd) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor] = '9';
            cursor++;
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor > 15) {
                cursor = 0;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = (porte.getPasserelle().length() - 1);
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyValid) {
            String ipas = new String(input);
            passerelle = ipas.replaceAll(" ", "");
            porte.setPasserelle(passerelle.trim());
            lcd.clearLcd();
            lcd.print("CONFIGURATION   \n  EN COURS ...  ", false);
            try {
                pb = new ProcessBuilder("sudo", "bash", "-c", "sed -i 's/.*static routers.*/static routers==" + passerelle.trim() + "/g' /etc/dhcpcd.conf");
                p = pb.start();
            } catch (IOException e) {
                System.out.println("ERROR");
            }
            data.update(porte);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            try {
                pb = new ProcessBuilder("sudo", "bash", "-c", "/etc/init.d/networking restart");
                p = pb.start();
                System.out.println("networking restart...");
            } catch (IOException e) {
                System.out.println("ERROR networking");
            }
            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
            }
            try {
                pb = new ProcessBuilder("sudo", "bash", "-c", "ifup eth0");
                p = pb.start();
                System.out.println("ifup eth0");
            } catch (IOException e) {
                System.out.println("ERROR eth0");
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }

            try {
                pb = new ProcessBuilder("sudo", "bash", "-c", "ifup wlan0");
                p = pb.start();
                System.out.println("ifup wlan0");
            } catch (IOException e) {
                System.out.println("ERROR wlan0");

            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
            try {
                pb = new ProcessBuilder("sudo", "bash", "-c", "ifup eth0");
                p = pb.start();
                System.out.println("ifup eth0");
            } catch (IOException e) {
                System.out.println("ERROR eth0");
            }
            lcd.clearLcd();
            lcd.setCursor(1, 0);
            lcd.print("ADRESSE IP FIXE ", false);
            lcd.setCursor(2, 0);
            lcd.print("REDEMARRAGE ...  ", false);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            try {
                pb = new ProcessBuilder("sudo", "reboot", "-f");
                p = pb.start();
            } catch (IOException e) {
                System.out.println("ERROR rebooting");
            }
            menu.father();
        } else if (event instanceof EventKeyB) {

            menu.father();
            menu.father();
        } else if (event instanceof EventKeyP) {
            for (int i = 0; i < 15; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print("               ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        if ((cursor + 1) % 4 == 0 && cursor < 15 && cursor > 0) {
            lcd.print(".", false);
            input[cursor] = '.';
            cursor++;
        }
    }
}
