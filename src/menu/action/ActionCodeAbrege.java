package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * ActionCodeAbrege
 *
 * @author Abdellah
 */
public class ActionCodeAbrege implements ActionItem {

    DataBase data = DataBase.getInstance();
    Porte porte;
    private Menu menu;
    private Lcd lcd;
    private static String code = "";

    /**
     * init
     */
    @Override
    public void init() {
        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 10);
        lcd.saisie(true);
        System.out.println("helooo####################################");
    }

    /**
     * init
     */
    @Override
    public void exit() {
        code = "";
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        menu = Menu.getInstance();

        if (event instanceof EventKey0 || event instanceof EventKey1 || event instanceof EventKey2
                || event instanceof EventKey3 || event instanceof EventKey4 || event instanceof EventKey5
                || event instanceof EventKey6 || event instanceof EventKey7 || event instanceof EventKey8
                || event instanceof EventKey9) {

            lcd.print(event.toString(), false);
            code += event.toString();

            porte = Porte.getInstance();
            if (code.length() != Integer.parseInt(porte.getAppelAbrege())) {
                menu.previous();
                menu.itemChanged = false;
                menu.init();
                menu.currentItem.execute(event);
                return;
            }
            while (code.length() < 3) {
                code = "0" + code;
            }
            menu = Menu.getInstance();
            Resident resident = data.getResident(code);
            code = "";
            if (resident.getCode() == null) {
                lcd.print("AUCUN RESIDENT\nENREGISTRE", true);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                }
                menu.father();
                return;
            }
            lcd.print(resident.getListeRouge().equals("0") ? resident.getName() : "", true);
            lcd.setCursor(2, 0);
            lcd.print("CODE D'APPEL:" + resident.getCode(), false);
            DefilementResident.codeResident = resident.getCode();
            menu.previous();
            menu.child();
        } else if (event instanceof EventKeyB) {
            lcd.saisie(false);
            code = "";
            menu.father();
        }
    }

}
