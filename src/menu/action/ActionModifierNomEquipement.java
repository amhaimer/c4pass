package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.DataBase;

/**
 * Action Modifier Nom Equipement
 *
 * @author Abdellah
 */
public class ActionModifierNomEquipement implements ActionItem {

    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private char[] input = new char[10];
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private int c;
    private final char[] lettre = " .,1ABC2DEF3GHI4JKL5MNO6PQRS7TUV8WXYZ9-\'#0".toCharArray();
    private Event lastEvent;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        input = new char[16];
        for (int i = 0; i < porte.getNomAcces().length(); i++) {
            input[i] = porte.getNomAcces().charAt(i);
        }
        cursor = cursorBegin;
        c = 0;
        lastEvent = new EventKeyValid();
        lcd.setCursor(2, cursor);
        lcd.print(porte.getNomAcces(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor] = lettre[0 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[4 + c]), false);
            input[cursor] = lettre[4 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[8 + c]), false);
            input[cursor] = lettre[8 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[12 + c]), false);
            input[cursor] = lettre[12 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[16 + c]), false);
            input[cursor] = lettre[16 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[20 + c]), false);
            input[cursor] = lettre[20 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[24 + c]), false);
            input[cursor] = lettre[24 + c];
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[29 + c]), false);
            input[cursor] = lettre[29 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[33 + c]), false);
            input[cursor] = lettre[33 + c];
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[38 + c]), false);
            input[cursor] = lettre[38 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor <= 15) {
                lcd.setCursor(2, cursor);
            }
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = 15;
            }
            lcd.setCursor(2, cursor);
        }
        if (cursor > 15) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            String msg = new String(input);
            msg = msg.trim();
            porte.setNomAcces(msg);
            data.update(porte);
            data.addEvent(data.getTraduction("menuprog", langue), data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("nomequipementl3", langue), msg);
            menu.father();
        }
        c = c < 4 ? c + 1 : 0;
        lcd.setCursor(2, cursor);
        lastEvent = event;
    }
}
