package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Tel voip
 *
 * @author Abdellah
 */
public class ActionModifierTelvoip implements ActionItem {

    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    private String langue = "";
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getValidWebClient().toCharArray();
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("nonoui", langue), false);
        //lcd.print("OUI=1 NON=0: ",false);
        lcd.setCursor(2, 13);
        if (porte.getValidWebClient().equals("1")) {
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI", false);
        } else {
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON", false);
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON",false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI",false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;

            porte.setValidWebClient(String.valueOf(input));
            data.update(porte);
            if (input[0] == '1') {
                menu.child();
            } else {
                menu.father();
            }
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        } else if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        }
    }
}
