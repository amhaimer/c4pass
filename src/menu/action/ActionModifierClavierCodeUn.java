package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import java.util.Arrays;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Clavier Code Un
 *
 * @author Abdellah
 */
public class ActionModifierClavierCodeUn implements ActionItem {

    private int currentCursorBegin = 5;
    private int currentCursorEnd = 8;
    private int cursor = currentCursorBegin;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getClavierCodeUn().toCharArray();
    int count = 0;
    private int current = 1;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);
        currentCursorBegin = 5;
        currentCursorEnd = 8;
        cursor = currentCursorBegin;
        input = porte.getClavierCodeUn().toCharArray();
        current = 1;
        lcd.setCursor(2, cursor);
        lcd.print(porte.getClavierCodeUn(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - currentCursorBegin] = '0';
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - currentCursorBegin] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - currentCursorBegin] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - currentCursorBegin] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - currentCursorBegin] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - currentCursorBegin] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - currentCursorBegin] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - currentCursorBegin] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - currentCursorBegin] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - currentCursorBegin] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {
            count = 0;
            //System.out.println("AAAA");
            porte.setClavierCodeUn(String.valueOf(input));
            //System.out.println("BBBB");
            data.update(porte);
            //System.out.println("CCCC");
            if (current == 2) {
                Arrays.fill(input, '\0');
                input = porte.getChoixClavierCodeUn().toCharArray();
                currentCursorBegin = 9;
                currentCursorEnd = 9;
                cursor = currentCursorBegin;
                lcd.print("CHOIX DE LA", true);
                lcd.setCursor(2, 0);
                lcd.print("PORTE : ", false);
                lcd.setCursor(2, 9);
                lcd.print(porte.getChoixClavierCodeUn(), false);
            } else if (current == 3) {
                Arrays.fill(input, '\0');
                input = porte.getTemporaireClavierCodeUn().toCharArray();
                System.out.println("temp: " + porte.getTemporaireClavierCodeUn());
                currentCursorBegin = 14;
                currentCursorEnd = 14;
                cursor = currentCursorBegin;
                lcd.print("TEMPORAIRE", true);
                lcd.setCursor(2, 0);
                lcd.print("NON=0 OUI=1: ", false);
                lcd.print(porte.getTemporaireClavierCodeUn(), false);
            } else if (current == 4) {
                Arrays.fill(input, '\0');
                //input = porte.getDebutClavierCodeUn().toCharArray();
                currentCursorBegin = 14;
                currentCursorEnd = 14;
                cursor = currentCursorBegin;
                lcd.print(" DATE DE DEBUT", true);
                lcd.setCursor(2, 0);
                //lcd.print(porte.getDebutClavierCodeUn(), false);
            } else if (current == 5) {
                Arrays.fill(input, '\0');
                //input = porte.getFinClavierCodeUn().toCharArray();
                currentCursorBegin = 14;
                currentCursorEnd = 14;
                cursor = currentCursorBegin;
                lcd.print(" DATE DE FIN", true);
                lcd.setCursor(2, 0);
                //lcd.print(porte.getFinClavierCodeUn(), false);
            } else if (current == 6) {
                Arrays.fill(input, '\0');
                input = porte.getHoraireUnDebutClavierCodeUn().toCharArray();
                currentCursorBegin = 14;
                currentCursorEnd = 14;
                cursor = currentCursorBegin;
                lcd.print("HORAIRE 1 SEM. 1", true);
                lcd.setCursor(2, 0);
                lcd.print(" " + porte.getHoraireUnDebutClavierCodeUn().substring(0, porte.getHoraireUnFinClavierCodeUn().length() - 3), false);
                lcd.print(" -- ", false);
                lcd.print(porte.getHoraireUnFinClavierCodeUn().substring(0, porte.getHoraireUnFinClavierCodeUn().length() - 3), false);

            } else if (current > 12) {
                menu.father();
            }
        } else if (event instanceof EventKeyB) {
            cursor = currentCursorBegin;
            count = 0;
            menu.father();
        }
        if (cursor > currentCursorEnd) {
            cursor = currentCursorBegin;
            lcd.setCursor(2, cursor);
        }
    }
}
