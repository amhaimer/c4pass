package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Signaux Porte
 *
 * @author Abdellah
 */
public class ActionModifierSignauxPorte implements ActionItem {

    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = new char[1];
    int count = 0;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("nonoui", langue), false);
        //lcd.print("OUI=1 NON=0: ",false);
        if (porte.getVisuelBadge() != null) {
            lcd.print(porte.getVisuelBadge().equals("1") ? data.getTraduction("oui", langue) : data.getTraduction("non", langue), false);
            input = porte.getVisuelBadge().toCharArray();
        } else {
            input[0] = '0';
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("non", langue), false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("oui", langue), false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;

            porte.setVisuelBadge(String.valueOf(input));
            data.addEvent(data.getTraduction("SAISIE ENR.PORTE", langue), data.getTraduction("Validation du", langue), data.getTraduction("Signal visuelCle", langue), " ", " ");
            data.update(porte);
            menu.next();
            //menu.father();
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        }
    }
}
