package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Modifier Duree Decalage
 *
 * @author Abdellah
 */
public class ActionModifierDureeDecalage implements ActionItem {

    private final int cursorBegin = 3;
    private final int cursorEnd = 6;
    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;

    private char[] input = new char[3];
    Event precevent = new EventKeyValid();
    DataBase data = DataBase.getInstance();
    Resident resident;

    /**
     * init
     */
    @Override
    public void init() {
        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursorBegin);
        /*
	lcd.print(resident.getTempoRenvoi()+ " SECONDES",false);
	for(int i=0;i<resident.getTempoRenvoi().length();i++) {
	    input[i] = resident.getTempoRenvoi().charAt(i);
	}
         */
        lcd.setCursor(2, cursorBegin);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            cursor++;
        }
        if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            cursor++;
        }
        if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            cursor++;
        }
        if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            cursor++;
        }
        if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            cursor++;
        }
        if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            cursor++;
        }
        if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            cursor++;
        }
        if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            cursor++;
        }
        if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
            cursor++;
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            cursor++;
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor - cursorBegin < 0) {
                cursor = cursorBegin + 2;
            }
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor - cursorBegin <= 2) {
                lcd.setCursor(2, cursor);
            }
        }
        if (cursor - cursorBegin > 2) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            String nom = new String(input);
            System.out.println(nom);
            /*
	    resident.setTempoRenvoi(nom.trim());
             */
            data.update(resident);
            menu.father();
        }
        if (event instanceof EventKeyP) {
            for (int i = 0; i < cursorEnd - cursorBegin; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, cursorBegin);
            lcd.print(" ", false);
            lcd.setCursor(2, cursorBegin);
            cursor = 0;
        }
    }
}
