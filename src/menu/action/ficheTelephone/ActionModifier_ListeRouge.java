package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Liste Rouge
 *
 * @author Abdellah
 */
public class ActionModifier_ListeRouge implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private char input[];
    private String langue = "";
    private String nom2 = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd.print(menu.toString(), true);

        input = resident.getListeRouge().toCharArray();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        //lcd.print(input[0] == '0' ? "NON" : "OUI", false);
        lcd.print(input[0] == '0' ? data.getTraduction("nonoui", langue) : data.getTraduction("ouinon", langue), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("non", langue), false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("oui", langue), false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            resident.setListeRouge(String.valueOf(input));
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("listerouge", langue));

            menu.next();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
