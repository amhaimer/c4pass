package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Appel Simultane
 *
 * @author Abdellah
 */
public class ActionModifierAppelSimultane implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private char input[];
    private int i;
    private String langue = "";
    private String nom2 = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd.print(menu.toString(), true);
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();

        input = resident.getRenvoiAppel().toCharArray();
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        //lcd.print(input[0] == '0' ? "NON" : "OUI", false);
        lcd.print(input[0] == '0' ? data.getTraduction("nonoui", langue) : data.getTraduction("ouinon", langue), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON",false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI",false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            if (input[0] == '0') {

                /*
		** input[0]= '2';
                 */
                Porte porte = Porte.getInstance();
                resident.setRenvoiAppel("2");
                data.update(resident);

                data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("simultane", langue));
                menu.next();
            }
            if (input[0] == '1') {
                Porte porte = Porte.getInstance();
                resident.setRenvoiAppel(String.valueOf(input));
                data.update(resident);
                nom2 = resident.getName();
                data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("simultane", langue));
                switch (porte.getServeurIP()) {
                    case "D-":
                        lcd.clearLcd();
                        //lcd.print("ID UTILISATEUR\n" , false);
                        lcd.print(data.getTraduction("idutilisateur", langue) + "\n", false);
                        lcd.print(resident.getCode(), false);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                        menu.next();
                        menu.next();
                        menu.next();
                        menu.next();
                        menu.next();
                        break;
                    case "C-":
                        resident.setTechnoAppel("CC");
                        data.update(resident);
                        nom2 = resident.getName();
                        data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("simultane", langue));
                        menu.next();
                        menu.next();
                        menu.next();
                        break;
                    case "O-":
                        resident.setTechnoAppel("OO");
                        data.update(resident);
                        nom2 = resident.getName();
                        data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("simultane", langue));
                        for (i = 0; i < 4; i++) {
                            menu.next();
                        }
                        break;
                    case "V-":
                        resident.setTechnoAppel("VV");
                        data.update(resident);
                        nom2 = resident.getName();
                        data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("simultane", langue));
                        for (i = 0; i < 5; i++) {
                            menu.next();
                        }
                        break;
                    default:
                        menu.next();
                        menu.next();
                        break;
                }
            }
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
