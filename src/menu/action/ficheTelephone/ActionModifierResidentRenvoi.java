package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Modifier Resident Renvoi
 *
 * @author Abdellah
 */
public class ActionModifierResidentRenvoi implements ActionItem {

    private final int cursorBegin = 13;
    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private char input[] = new char[3];
    private String langue = "";
    private String nom2 = "";

    private DataBase data = DataBase.getInstance();
    private Resident resident;

    /**
     * init
     */
    @Override
    public void init() {

        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        //lcd.print("CODE D'APPEL:", false);
        lcd.print(data.getTraduction("code", langue) + data.getTraduction("dappel", langue), false);

        /*
	** lcd.print(resident.getCodeRenvoi(), false);
         */
        System.out.println("code de renvoi:" + resident.getCodeRenvoi());
        if (resident.getCodeRenvoi() != null) {
            int i;
            lcd.print(resident.getCodeRenvoi(), false);
            for (i = 0; i < resident.getCodeRenvoi().length(); i++) {
                input[i] = resident.getCodeRenvoi().charAt(i);
            }
            while (i < 2) {
                input[i++] = ' ';
            }
            lcd.setCursor(2, 0);
        }
        lcd.setCursor(2, cursorBegin);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        /*
	**	lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            cursor++;
        }
        if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            cursor++;
        }
        if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            cursor++;
        }
        if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            cursor++;
        }
        if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            cursor++;
        }
        if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            cursor++;
        }
        if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            cursor++;
        }
        if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            cursor++;
        }
        if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            cursor++;
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor - cursorBegin < 0) {
                cursor = cursorBegin + 2;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor - cursorBegin <= 2) {
                lcd.setCursor(2, cursor);
            }
        }
        if (cursor - cursorBegin > 2) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            String nom = new String(input);
            if (nom == null) {
                nom = " ";
            }
            resident.setCodeRenvoi(nom.trim());

            /*
	    **resident.setCodeRenvoi(String.valueOf(input));
             */
            System.out.println("code de renvoi après validkey:" + resident.getCodeRenvoi());
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("coderenvoir", langue));
            menu.next();
        }
        if (event instanceof EventKeyP) {
            cursor = cursorBegin;
            for (int i = 0; i < 3; i++) {
                input[i] = '-';
            }
            lcd.setCursor(2, 13);
            lcd.print("---", false);
            lcd.setCursor(2, 13);
        }
    }
}
