package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Modifier Temps Renvoi Appel
 *
 * @author Abdellah
 */
public class ActionModifierTempsRenvoiAppel implements ActionItem {

    private final int cursorBegin = 3;
    private final int cursorEnd = 6;
    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;

    private char[] input = new char[3];
    Event precevent = new EventKeyValid();
    DataBase data = DataBase.getInstance();
    Resident resident;
    private int i;
    private String langue = "";
    private String nom2 = "";
    private char[] technoappel = "".toCharArray();
    private char technoappel_g;

    /**
     * init
     */
    @Override
    public void init() {

        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursorBegin);
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        /*
	  lcd.print(resident.getTempoRenvoi()+ " SECONDES",false);
	  for(int i=0;i<resident.getTempoRenvoi().length();i++) {
	  input[i] = resident.getTempoRenvoi().charAt(i);
	  }
         */
        lcd.setCursor(2, cursorBegin);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            cursor++;
        }
        if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            cursor++;
        }
        if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            cursor++;
        }
        if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            cursor++;
        }
        if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            cursor++;
        }
        if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            cursor++;
        }
        if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            cursor++;
        }
        if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            cursor++;
        }
        if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            cursor++;
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor - cursorBegin < 0) {
                cursor = cursorBegin + 2;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor - cursorBegin <= 2) {
                lcd.setCursor(2, cursor);
            }
        }
        if (cursor - cursorBegin > 2) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            Porte porte = Porte.getInstance();
            String nom = new String(input);
            System.out.println(nom);
            technoappel = resident.getTechnoAppel().toCharArray();
            technoappel_g = technoappel[0];
            /*
	    resident.setTempoRenvoi(nom.trim());
             */
            if (technoappel_g == 'C' || technoappel_g == 'c') {
                resident.setRenvoiSip(nom.trim());
            } else if (technoappel_g == 'O' || technoappel_g == 'o') {
                resident.setRenvoiTel(nom.trim());
            }
            data.update(resident);
            nom2 = resident.getName();

            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("dureediffere", langue));
            System.out.println(resident.getRenvoiAppel());

            /*
             ** if (porte.getInterphone().charAt(0) == '3')
             ** for (int i = 0;i<2;i++)
             ** menu.next();
             ** if (porte.getInterphone().charAt(0) == '4')
             ** for (int i = 0;i<5;i++)
             ** menu.next();
             ** if (porte.getInterphone().charAt(0) == '5')
             ** menu.next();
             ** menu.next();
             */
            switch (porte.getServeurIP()) {
                case "D-":
                    lcd.clearLcd();
                    //lcd.print("ID UTILISATEUR\n" , false);
                    lcd.print(data.getTraduction("idutilisateur", langue) + "\n", false);
                    lcd.print(resident.getCode(), false);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    menu.next();
                    menu.next();
                    menu.next();
                    menu.next();
                    break;
                case "C-":
                    resident.setTechnoAppel("CC");
                    data.update(resident);
                    //nom2 = resident.getName();
                    //data.addEvent(data.getTraduction("menuprog",langue),nom2,data.getTraduction("donnee_resident",langue),data.getTraduction("modif_fiche",langue), data.getTraduction("dureediffere",langue));
                    for (i = 0; i < 2; i++) {
                        menu.next();
                    }
                    break;
                case "O-":
                    resident.setTechnoAppel("OO");
                    data.update(resident);
                    //nom2 = resident.getName();
                    //data.addEvent(data.getTraduction("menuprog",langue),nom2,data.getTraduction("donnee_resident",langue),data.getTraduction("modif_fiche",langue), data.getTraduction("dureediffere",langue));
                    for (i = 0; i < 3; i++) {
                        menu.next();
                    }
                    break;
                case "V-":
                    resident.setTechnoAppel("VV");
                    data.update(resident);
                    //nom2 = resident.getName();
                    //data.addEvent(data.getTraduction("menuprog",langue),nom2,data.getTraduction("donnee_resident",langue),data.getTraduction("modif_fiche",langue), data.getTraduction("dureediffere",langue));
                    for (i = 0; i < 4; i++) {
                        menu.next();
                    }
                    break;
                default:
                    menu.next();
                    break;
            }
        }
        if (event instanceof EventKeyP) {
            for (i = 0; i < cursorEnd - cursorBegin; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, cursorBegin);
            lcd.print(" ", false);
            lcd.setCursor(2, cursorBegin);
            cursor = 0;
        }
    }
}
