package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Profession Lib
 *
 * @author Abdellah
 */
public class ActionModifier_ProfessionLib implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private String input = "";
    private String langue = "";
    private String nom2 = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd.print(menu.toString(), true);

        input = resident.getProfessionLiberale();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        //lcd.print(input[0] == '0' ? "NON" : "OUI", false);
        lcd.print("0".equals(input) ? data.getTraduction("ouinon", langue) : data.getTraduction("nonoui", langue), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("non", langue), false);
            input = "-1";
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("oui", langue), false);
            input = "0";
        } else if (event instanceof EventKeyValid) {
            resident.setProfessionLiberale(String.valueOf(input));
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("professionlib", langue));

            menu.next();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
