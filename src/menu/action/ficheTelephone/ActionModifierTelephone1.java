package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Telephone1
 *
 * @author Abdellah
 */
public class ActionModifierTelephone1 implements ActionItem {

    private final int cursorBegin = 3;
    private final int cursorEnd = 12;
    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private char input[] = new char[10];

    private String langue = "";
    private String nom2 = "";

    /**
     * init
     */
    @Override
    public void init() {

        resident = data.getResident(ActionEntreeCodeResident.code);
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);
        lcd.print(resident.getTelephone1(), false);
        lcd.setCursor(2, cursor);
        for (int i = 0; i < resident.getTelephone1().length(); i++) {
            input[i] = resident.getTelephone1().charAt(i);
        }
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        /*
	** lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            cursor++;
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
            cursor++;
        } else if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor - cursorBegin < 0) {
                cursor = cursorBegin + 9;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor - cursorBegin <= 10) {
                lcd.setCursor(2, cursor);
            }
        } else if (event instanceof EventKeyValid) {
            Porte porte = Porte.getInstance();
            String nom = new String(input);
            resident.setTelephone1(nom.trim());
            data.update(resident);
            langue = porte.getTraduction();
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("tel1", langue));

            /*
	    ** if (resident.getValidationRenvoiAppel() !=null)
	    ** {
             */
            menu.next();
            menu.next();

            /*
	    ** }
	    ** else {
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    **
             */
        } else if (event instanceof EventKeyP) {
            for (int i = 0; i < 10; i++) {
                input[i] = ' ';
            }
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print(" ", false);
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyB) {

            menu.father();
        }
        if (cursor > cursorEnd) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
    }
}
