package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Second Appel
 *
 * @author Abdellah
 */
public class ActionModifierSecondAppel implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private Porte porte;
    private char input, techno_appel1;
    private char techno1, techno2;
    private String serveur1 = "", serveur2 = "", technoappel;
    private String langue = "";
    private String nom2 = "";

    /**
     *
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        porte = Porte.getInstance();
        langue = porte.getTraduction();

        /*
	** techno = porte.getInterphone().charAt(0);
         */
        techno1 = porte.getServeurIP().charAt(0);
        techno2 = porte.getServeurIP().charAt(1);
        techno_appel1 = resident.getTechnoAppel().charAt(0);
        lcd.print(menu.toString(), true);


        /*
	** input[] = resident.getTechnoAppel().toCharArray();
         */
        System.out.println(input);
        lcd.setCursor(2, 0);
        switch (techno1) {
            case 'O':
                serveur1 = "AUDIO";
                break;
            case 'C':
                serveur1 = "IPCLIE";
                break;
            case 'V':
                serveur1 = "VIDEO";
                break;
            case 'D':
                serveur1 = "DIESE";
                break;
            default:
                break;
        }
        switch (techno2) {
            case 'O':
                serveur2 = "AUDIO";
                break;
            case 'C':
                serveur2 = "IPCLIE";
                break;
            case 'V':
                serveur2 = "VIDEO";
                break;
            case 'D':
                serveur2 = "DIESE";
                break;
            default:
                break;
        }
        lcd.print("1=" + serveur1 + " " + "2=" + serveur2, false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 8);
        if (event instanceof EventKey1 && (techno1 == 'O')) {

            /*
	    ** lcd.clearLcd();
	    ** lcd.print("APPEL 1 :O", false);
             */
            input = 'O';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            menu.next();
            menu.next();
        } else if (event instanceof EventKey1 && (techno1 == 'V')) {
            input = 'V';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));

            /*
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
             */
            menu.previous();
            menu.previous();
            menu.previous();
            menu.previous();
        } else if (event instanceof EventKey1 && (techno1 == 'C')) {
            input = 'C';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            menu.next();
        } else if (event instanceof EventKey1 && (techno1 == 'D')) {
            input = 'D';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            lcd.clearLcd();
            lcd.print(data.getTraduction("idutilisateur", langue) + "\n", false);
            //lcd.print("ID UTILISATEUR\n" , false);
            lcd.print(resident.getCode(), false);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            menu.next();
            menu.next();
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'O')) {
            input = 'O';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            menu.next();
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'V')) {
            input = 'V';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            menu.previous();
            menu.previous();
            menu.previous();
            menu.previous();
        } else if (event instanceof EventKey2 && (techno2 == 'C')) {
            input = 'C';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'D')) {
            input = 'D';
            technoappel = new StringBuilder().append(techno_appel1).append(input).toString();
            resident.setTechnoAppel(technoappel);

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("d_technoappel", langue));
            lcd.clearLcd();
            lcd.print(data.getTraduction("idutilisateur", langue) + "\n", false);
            //lcd.print("ID UTILISATEUR\n" , false);
            lcd.print(resident.getCode(), false);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            menu.father();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }

    /* 
    **public void action(Event event){
    ** menu = Menu.getInstance();
    ** lcd = Lcd.getInstance();
    ** lcd.setCursor(2, 8);
    ** if (event instanceof EventKey3 && techno != '8')
    ** {
    ** lcd.print("Audio", false);
    ** input[1]= '3';
    ** }
    ** else if (event instanceof EventKey4 && techno != '7')
    ** {
    ** lcd.print("VOIP ", false);
    ** input[1]= '4';
    ** }
    ** else if (event instanceof EventKey5 && techno != '6')
    ** {
    ** lcd.print("SIP ", false);
    ** input[1]= '5';
    ** }	
    ** else if (event instanceof EventKeyValid && input[0] != '9')
    ** {
    ** System.out.println(input);
    ** resident.setTechnoAppel(String.valueOf(input));
    ** data.update(resident);
    ** if (input[1] != '5')
    ** menu.next();
    ** if (input[1] == '4')
    ** menu.next();
    ** menu.next();
    ** }
    ** else if (event instanceof EventKeyB)
    ** menu.father();
    ** }
     */
}
