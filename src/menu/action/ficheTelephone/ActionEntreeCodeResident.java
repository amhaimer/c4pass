package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import utils.UsersManager;
import sql.data.Resident;
import sql.data.Porte;
import sql.DataBase;

/**
 * Action Entree Code Resident
 *
 * @author Abdellah
 */
public class ActionEntreeCodeResident implements ActionItem {

    //! \class ActionEntreeCodeGestionaire action enntrer code gestionaire.
    private Menu menu;
    private Lcd lcd;
    public static String code = new String();

    private String input = new String();
    private String langue = "";
    DataBase data = DataBase.getInstance();
    Porte porte;

    /**
     * Méthode appelée à chaque que l'on entre dans le menu d'enter du code de
     * gestionaire (en appuyant sur la 'V' dans le menu Bienvenue), cette
     * methode initialise le timer pour le retour le buffer pour l'entrer du
     * code, et place le curseur sur l'afficheur lcd.
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();

        /*  
	** lcd.cursorMode(true);
         */
        lcd.blinkMode(true);
        input = "";

        langue = porte.getTraduction();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 10);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        input = "";

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyB) {
            input = "";
            menu.father();
        }
        if (event instanceof EventKey0) {

            lcd.print("0", false);
            input += "0";
        }
        if (event instanceof EventKey1) {

            lcd.print("1", false);
            input += "1";
        }
        if (event instanceof EventKey2) {

            lcd.print("2", false);
            input += "2";
        }
        if (event instanceof EventKey3) {

            lcd.print("3", false);
            input += "3";
        }
        if (event instanceof EventKey4) {

            lcd.print("4", false);
            input += "4";
        }
        if (event instanceof EventKey5) {

            lcd.print("5", false);
            input += "5";
        }
        if (event instanceof EventKey6) {

            lcd.print("6", false);
            input += "6";
        }
        if (event instanceof EventKey7) {

            lcd.print("7", false);
            input += "7";
        }
        if (event instanceof EventKey8) {

            lcd.print("8", false);
            input += "8";
        }
        if (event instanceof EventKey9) {

            lcd.print("9", false);
            input += "9";
        }
        if (event instanceof EventKeyP) {

            lcd.setCursor(2, 10);
            lcd.print(" ", false);
            lcd.setCursor(2, 10);
            input = "";
        }
        if (input.length() >= 3) {
            data = DataBase.getInstance();
            Resident resident = data.getResident(input);
            UsersManager users = new UsersManager();
            if (resident.getCode() == null) {
                resident = data.insert(input);
                users.add(input);
                code = resident.getCode();
                data.addEvent(data.getTraduction("menuprog", langue), code, data.getTraduction("donnee_resident", langue), data.getTraduction("creation_fiche", langue), data.getTraduction("saisiecode", langue));
            }
            code = resident.getCode();
            menu.next();
        }
    }
}
