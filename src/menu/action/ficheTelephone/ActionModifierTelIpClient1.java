package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Modifier Tel Ip Client1
 *
 * @author Abdellah
 */
public class ActionModifierTelIpClient1 implements ActionItem {

    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private String langue = "";
    private int c = 0;
    private char[] input = new char[16];
    private final char[] lettre = "1 .,:-_2ABCabc3DEFdef4GHIghi5JKLjkl6MNOmno7PQRpqr8STUstu9VWXvwx0XYxy\'#0".toCharArray();

    /*
    ** private final char[] lettre=" .,:-_1ABCabc2DEFdef3GHIghi4JKLjkl5MNOmno6PQRpqr7STUstu8VWXvwx9XYxy0\'#0".toCharArray();
    ** private final char[] lettre = " .,1ABC2DEF3GHI4JKL5MNO6PQRS7TUV8WXYZ9-\'#0".toCharArray();
     */
    private Event lastEvent = new EventKeyValid();
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private String nom2 = "";

    /**
     * init
     */
    @Override
    public void init() {
        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);

        /*
	** lcd.print(resident.getSipPhoneUn(),false);
	** for(int i = 0; i < resident.getSipPhoneUn().length(); i++)
	** input[i] = resident.getSipPhoneUn().charAt(i);
	** lcd.setCursor(2,cursorBegin);
	** lcd.saisie(true);
         */
        if (resident.getSipPhoneUn() != null) {
            int i;
            lcd.print(resident.getSipPhoneUn(), false);
            for (i = 0; i < resident.getSipPhoneUn().length(); i++) {
                input[i] = resident.getSipPhoneUn().charAt(i);
            }
            while (i < 15) {
                input[i++] = ' ';
            }
            lcd.setCursor(2, 0);
        }
        c = 0;
        lastEvent = new EventKeyValid();
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        System.out.println(c);
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor] = lettre[0 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[7 + c]), false);
            input[cursor] = lettre[7 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[14 + c]), false);
            input[cursor] = lettre[14 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[21 + c]), false);
            input[cursor] = lettre[21 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[28 + c]), false);
            input[cursor] = lettre[28 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[35 + c]), false);
            input[cursor] = lettre[35 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[42 + c]), false);
            input[cursor] = lettre[42 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[49 + c]), false);
            input[cursor] = lettre[49 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[56 + c]), false);
            input[cursor] = lettre[56 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[63 + c]), false);
            input[cursor] = lettre[63 + c];
            c = c == 6 ? c + 1 : c;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor <= 15) {
                lcd.setCursor(2, cursor);
            }
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = (resident.getSipPhoneUn().length() - 1);
            }
        }
        if (cursor > 15) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            Porte porte = Porte.getInstance();
            String nom = new String(input);
            System.out.println(resident.getRenvoiAppel());
            resident.setSipPhoneUn(nom.trim());
            data.update(resident);
            langue = porte.getTraduction();
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("telmo1", langue));

            /*
	    ** if (resident.getValidationRenvoiAppel() !=null)
	    ** {
             */
            menu.next();
            menu.next();
            menu.next();

            /*
	    ** }
	    ** else {
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** menu.next();
	    ** }
             */
        }
        if (event instanceof EventKeyP) {
            for (int i = 0; i < 16; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print(" ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        c = c < 7 ? c + 1 : 0;
        lcd.setCursor(2, cursor);
        lastEvent = event;
        System.out.println("cursor: " + cursor);
    }
}
