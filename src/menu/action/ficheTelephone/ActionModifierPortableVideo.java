package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Portable Video
 *
 * @author Abdellah
 */
public class ActionModifierPortableVideo implements ActionItem {

    private final int cursorBegin = 3;
    private final int cursorEnd = 12;
    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    Porte porte;
    private char input[] = new char[10];

    private int i = 0;
    private String langue = "";
    private String nom2 = "";

    /**
     * init
     */
    @Override
    public void init() {

        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        porte = Porte.getInstance();
        cursor = cursorBegin;
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, cursor);

        lcd.setCursor(2, cursor);

        /*
	** techno_appel2 = resident.getTechnoAppel().charAt(1);
         */
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        /*
	** lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            cursor++;
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
            cursor++;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor - cursorBegin <= 10) {
                lcd.setCursor(2, cursor);
            }
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor - cursorBegin < 0) {
                cursor = cursor + 9;
            }
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyValid) {

            data.update(resident);
            langue = porte.getTraduction();
            nom2 = resident.getName();
            data.addEvent(data.getTraduction("menuprog", langue), nom2, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("smartphone", langue));
            if (porte.getServeurIP().equals("V-") || resident.getTechnoAppel().charAt(1) == 'V') {
                for (i = 0; i < 7; i++) {
                    menu.next();
                }
            } else {
                menu.next();
            }
        } else if (event instanceof EventKeyP) {
            for (i = 0; i < 10; i++) {
                input[i] = ' ';
            }
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
            lcd.print("          ", false);
            lcd.setCursor(2, cursor);
        } else if (event instanceof EventKeyB) {

            menu.father();
        }
        if (cursor > cursorEnd) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
    }
}
