package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;

/**
 * Action Modifier Premier Appel
 *
 * @author Abdellah
 */
public class ActionModifierPremierAppel implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private Porte porte;
    private char input[];
    private char techno1, techno2;
    private String serveur1 = "", serveur2 = "";
    private String langue = "";
    private String nom = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        nom = resident.getName();

        /*
	** techno = porte.getInterphone().charAt(0);
         */
        techno1 = porte.getServeurIP().charAt(0);
        techno2 = porte.getServeurIP().charAt(1);

        lcd.print(menu.toString(), true);

        input = resident.getTechnoAppel().toCharArray();
        System.out.println(input);
        lcd.setCursor(2, 0);
        switch (techno1) {
            case 'O':
                serveur1 = "AUDIO";
                break;
            case 'C':
                serveur1 = "IPCLIE";
                break;
            case 'V':
                serveur1 = "VIDEO";
                break;
            case 'D':
                serveur1 = "DIESE";
                break;
            default:
                break;
        }
        switch (techno2) {
            case 'O':
                serveur2 = "AUDIO";
                lcd.print("1=" + serveur1 + " " + "2=" + serveur2, false);
                break;
            //lcd.print("1="+ serveur1+" "+"2=" + serveur2 , false);
            /*
         ** lcd.print(techno_appel1,false);
         ** if(techno1 =='O' ||techno2 =='O')
         ** lcd.print("O? 1-0 : ", false);
         ** else if(techno1 == 'V'|| techno2 =='V')
         ** lcd.print("V? 1-0 : ", false);
         ** else if(techno1 == 'C' || techno2 == 'C')
         ** lcd.print("C? 1-0 : ", false);
         ** if (techno == '6')
         ** lcd.print("3-4 : ", false);
         ** else if (techno == '7')
         ** lcd.print("3-5 : ", false);
         ** else if (techno == '8')
         ** lcd.print("4-5 : ", false);
         ** else if (techno == '9')
         ** lcd.print("3-4-5 : ", false);
         ** if (techno_appel1 == '3')
         ** lcd.print("Audio", false);
         ** else if (techno_appel1 == '4')
         ** lcd.print("VOIP", false);
         ** else if (techno_appel1 == '5')
         ** lcd.print("SIP", false);
             */
            case 'C':
                serveur2 = "IPCLIE";
                lcd.print("1=" + serveur1 + " " + "2=" + serveur2, false);
                break;
            case 'V':
                serveur2 = "VIDEO";
                lcd.print("1=" + serveur1 + " " + "2=" + serveur2, false);
                break;
            case 'D':
                serveur2 = "DIESE";
                lcd.print("1=" + serveur1 + " " + "2=" + serveur2, false);
                break;
            case '-':
                serveur2 = " ";
                lcd.print("1=" + serveur1, false);
                break;
            default:
                break;
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 8);
        if (event instanceof EventKey1 && (techno1 == 'O')) {

            /*
	    ** lcd.clearLcd();
	    ** lcd.print("APPEL 1 :O", false);
             */
            input[0] = 'O';
            resident.setTechnoAppel("O ");

            /*
	    ** resident.setTechnoAppel(String.valueOf(input));
             */
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            menu.next();
            menu.next();
        } else if (event instanceof EventKey1 && (techno1 == 'V')) {
            input[0] = 'V';
            resident.setTechnoAppel("V ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            menu.next();
            menu.next();
            menu.next();
        } else if (event instanceof EventKey1 && (techno1 == 'C')) {
            input[0] = 'C';
            resident.setTechnoAppel("C ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            menu.next();
        } else if (event instanceof EventKey1 && (techno1 == 'D')) {
            input[0] = 'D';
            resident.setTechnoAppel("D ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            lcd.clearLcd();
            lcd.print(data.getTraduction("idutilisateur", langue) + "\n", false);
            lcd.print(resident.getCode(), false);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            menu.next();
            menu.next();
            menu.next();
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'O')) {
            input[0] = 'O';
            resident.setTechnoAppel("O ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            menu.next();
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'V')) {
            input[0] = 'V';
            resident.setTechnoAppel("V ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            menu.next();
            menu.next();
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'C')) {
            input[0] = 'C';
            resident.setTechnoAppel("C ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            menu.next();
        } else if (event instanceof EventKey2 && (techno2 == 'D')) {
            input[0] = 'D';
            resident.setTechnoAppel("D ");
            data.update(resident);
            data.addEvent(data.getTraduction("menuprog", langue), nom, data.getTraduction("donnee_resident", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("g_technoappel", langue));
            lcd.clearLcd();
            lcd.print(data.getTraduction("idutilisateur", langue) + "\n", false);
            lcd.print(resident.getCode(), false);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            menu.next();
            menu.next();
            menu.next();
            menu.next();
        } /* 
	** if (event instanceof EventKey3 && techno != '8')
	** {
	** lcd.print("Audio", false);
	** input[0]= '3';
	** }
	** else if (event instanceof EventKey4 && techno != '7')
	** {
	** lcd.print("VOIP ", false);
	** input[0]= '4';
	** }
	** else if (event instanceof EventKey5 && techno != '6')
	** {
	** lcd.print("SIP ", false);
	** input[0]= '5';
	** }
         */ /*
	** else if (event instanceof EventKeyValid )
	** {
	** System.out.println(input);
	** resident.setTechnoAppel(String.valueOf(input));
	** data.update(resident);
	** if (input[0] == 'O')
	** { menu.next();
	** menu.next();}
	** else if (input[0] == 'V')
	** { menu.next();
	** menu.next();
	** menu.next();}
	** else if (input[0] == 'C')
	** { menu.next();}
	** }
	** else if (event instanceof EventKeyValid && input[0] != '9')
	** {
	** System.out.println(input);
	** resident.setTechnoAppel(String.valueOf(input));
	** data.update(resident);
	** if (input[0] == '3')
	** menu.next();
	** else if (input[0] == '4')
	** for (int i = 0;i<2;i++)
	** menu.next();
	** menu.next();
	** }
         */ else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
