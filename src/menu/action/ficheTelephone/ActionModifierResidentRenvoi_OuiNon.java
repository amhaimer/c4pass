package menu.action.ficheTelephone;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Modifier Resident Renvoi OuiNon
 *
 * @author Abdellah
 */
public class ActionModifierResidentRenvoi_OuiNon implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private char input[];
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeResident.code);
        lcd.print(menu.toString(), true);

        input = resident.getRenvoiAppel().toCharArray();
        Porte porte = Porte.getInstance();
        langue = porte.getTraduction();
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        //lcd.print(input[0] == '0' ? "NON" : "OUI", false);
        lcd.print(input[0] == '0' ? data.getTraduction("nonoui", langue) : data.getTraduction("ouinon", langue), false);

    }

    /**
     * exit
     */
    @Override
    public void exit() {

    }

    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("non", langue), false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("oui", langue), false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            if (input[0] == '0') {

                menu.next();
                menu.next();

            } else {
                menu.next();
            }
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
