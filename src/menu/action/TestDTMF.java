package menu.action;

import event.Event;
import menu.Menu;
import event.eventType.*;
import event.EventManager;
import devices.Lcd;
import sql.DataBase;
import sql.data.Resident;
import sql.data.Porte;
import java.io.*;

/**
 * classe pour tester les touches DTMF
 *
 * @author Abdellah
 */
public class TestDTMF implements ActionItem {

    Menu menu;
    Lcd lcd = Lcd.getInstance();
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    Resident resident;
    long i = 0;
    EventManager manager = EventManager.getInstance();

    /**
     * init
     */
    @Override
    public void init() {
        i = 0;
        menu = Menu.getInstance();
        System.out.println(menu.toString());
        lcd.print("APPUYER SUR APPEL", true);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        if (event instanceof EventKeyValid) {
            lcd.print("EN ATTENTE DU #", true);
            try {
                Process dumpcap = Runtime.getRuntime().exec("sudo dumpcap -w /tmp/dumpcap -q");
            } catch (IOException e) {
                return;
            }
            loop:
            {
                while (true) {
                    try {
                        ProcessBuilder psdiese = new ProcessBuilder("/bin/sh", "-c", "sudo hd /tmp/dumpcap | grep -A2 \"80 65\" | grep \"0b 8\"");
                        psdiese.redirectErrorStream(true);
                        Process processdiese = psdiese.start();
                        BufferedReader readerdiese = new BufferedReader(new InputStreamReader(processdiese.getInputStream()));
                        String s;
                        while ((s = readerdiese.readLine()) != null) {
                            lcd.setCursor(2, 0);
                            lcd.print("DIESE TROUVE", false);
                            for (int i = 0; i < 9999999; i++);
                            menu.father();
                            break loop;
                        }
                        ProcessBuilder psstar = new ProcessBuilder("/bin/sh", "-c", "sudo hd /tmp/dumpcap | grep -A2 \"80 65\" | grep -A2 \"0a 8a\"");
                        psstar.redirectErrorStream(true);
                        Process processstar = psstar.start();
                        BufferedReader readerstar = new BufferedReader(new InputStreamReader(processstar.getInputStream()));
                        while ((s = readerstar.readLine()) != null) {
                            lcd.setCursor(2, 0);
                            lcd.print("ETOILE TROUVE", false);
                            for (int i = 0; i < 9999999; i++);
                            menu.father();
                            break loop;
                        }
                    } catch (IOException e) {
                        System.out.println("Fail");
                    }
                }
            }
        }
        if (event instanceof EventKeyB) {
            menu.father();
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }
}
