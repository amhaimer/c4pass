package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 *
 * @author Abdellah
 */
public class ActionModifierLangue implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte = Porte.getInstance();
    private char input[] = new char[1];

    private String langue_saisi = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        porte = Porte.getInstance();
        lcd.print(data.getTraduction("langue", porte.getTraduction()), true);
        input = porte.getTraduction().toCharArray();
        lcd.setCursor(2, 13);
        lcd.print(porte.getTraduction(), false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[0] = '1';
            langue_saisi = "FR";
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[0] = '2';
            langue_saisi = "EN";
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[0] = '3';
            langue_saisi = "AR";
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[0] = '4';
            langue_saisi = "ES";
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[0] = '5';
            langue_saisi = "GR";
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[0] = '6';
            langue_saisi = "IT";
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[0] = '7';
            langue_saisi = "CN";
        } else if (event instanceof EventKeyValid) {
            porte.setLangue(String.valueOf(input));
            data.update(porte);
            data.addEvent(data.getTraduction("menuprog", langue_saisi), data.getTraduction("donnee_porte", langue_saisi), data.getTraduction("modif_fiche", langue_saisi), data.getTraduction("languel3", langue_saisi), langue_saisi);
            menu.father();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
