package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Priorite Affichage
 *
 * @author Abdellah
 */
public class ActionModifierPrioriteAffichage implements ActionItem {

    private final int cursorBegin = 2;

    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input1[] = porte.getPrioriteAffichageUn().toCharArray();
    private char input2[] = porte.getPrioriteAffichageDeux().toCharArray();
    private char input3[] = porte.getPrioriteAffichageTrois().toCharArray();
    int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        String format1 = String.format("%03d", Integer.parseInt(porte.getPrioriteAffichageUn()));
        String format2 = String.format("%03d", Integer.parseInt(porte.getPrioriteAffichageDeux()));
        String format3 = String.format("%03d", Integer.parseInt(porte.getPrioriteAffichageTrois()));
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 2);
        lcd.print(format1, false);
        lcd.setCursor(2, 6);
        lcd.print(format2, false);
        lcd.setCursor(2, 10);
        lcd.print(format3, false);
        lcd.setCursor(2, 2);
        System.out.println(input1);
        System.out.println(input2);
        System.out.println(input3);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyRight) {
            if (cursor >= 0 && cursor <= 2) {
                cursor = 3;
                lcd.setCursor(2, 7);
            } else if (cursor >= 3 && cursor <= 5) {
                cursor = 6;
                lcd.setCursor(2, 11);
            } else if (cursor >= 6 && cursor <= 8) {
                cursor = 0;
                lcd.setCursor(2, 2);
            }
        }
        if (event instanceof EventKeyLeft) {
            if (cursor >= 0 && cursor <= 2) {
                cursor = 6;
                lcd.setCursor(2, 11);
            } else if (cursor >= 3 && cursor <= 5) {
                cursor = 0;
                lcd.setCursor(2, 2);
            } else if (cursor >= 6 && cursor <= 8) {
                cursor = 3;
                lcd.setCursor(2, 7);
            }
        } else if (event instanceof EventKey0) {
            lcd.print("0", false);
            if (cursor > 5) {
                input3[cursor - 6] = '0';
            } else if (cursor > 2) {
                input2[cursor - 3] = '0';
            } else {
                input1[cursor] = '0';
            }
            cursor++;
        } else if (event instanceof EventKey1) {
            lcd.print("1", false);
            if (cursor > 5) {
                input3[cursor - 6] = '1';
            } else if (cursor > 2) {
                input2[cursor - 3] = '1';
            } else {
                input1[cursor] = '1';
            }
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            if (cursor > 5) {
                input3[cursor - 6] = '2';
            } else if (cursor > 2) {
                input2[cursor - 3] = '2';
            } else {
                input1[cursor] = '2';
            }
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            if (cursor > 5) {
                input3[cursor - 6] = '3';
            } else if (cursor > 2) {
                input2[cursor - 3] = '3';
            } else {
                input1[cursor] = '3';
            }
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            if (cursor > 5) {
                input3[cursor - 6] = '4';
            } else if (cursor > 2) {
                input2[cursor - 3] = '4';
            } else {
                input1[cursor] = '4';
            }
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            if (cursor > 5) {
                input3[cursor - 6] = '5';
            } else if (cursor > 2) {
                input2[cursor - 3] = '5';
            } else {
                input1[cursor] = '5';
            }
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            if (cursor > 5) {
                input3[cursor - 6] = '6';
            } else if (cursor > 2) {
                input2[cursor - 3] = '6';
            } else {
                input1[cursor] = '6';
            }
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            if (cursor > 5) {
                input3[cursor - 6] = '7';
            } else if (cursor > 2) {
                input2[cursor - 3] = '7';
            } else {
                input1[cursor] = '7';
            }
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            if (cursor > 5) {
                input3[cursor - 6] = '8';
            } else if (cursor > 2) {
                input2[cursor - 3] = '8';
            } else {
                input1[cursor] = '8';
            }
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            if (cursor > 5) {
                input3[cursor - 6] = '9';
            } else if (cursor > 2) {
                input2[cursor - 3] = '9';
            } else {
                input1[cursor] = '9';
            }
            cursor++;
        }

        if (cursor == 9) {
            cursor = 0;
            lcd.setCursor(2, cursorBegin);
        } else if (cursor == 6) {
            lcd.setCursor(2, 10);
        } else if (cursor == 3) {
            lcd.setCursor(2, 6);
        } else if (event instanceof EventKeyValid) {
            count = 0;
            cursor = cursorBegin;
            porte.setPrioriteAffichageUn(String.valueOf(input1));
            porte.setPrioriteAffichageDeux(String.valueOf(input2));
            porte.setPrioriteAffichageTrois(String.valueOf(input3));
            data.update(porte);
            menu.father();
        } else if (event instanceof EventKeyB) {
            cursor = cursorBegin;
            count = 0;
            menu.father();
        }
    }
}
