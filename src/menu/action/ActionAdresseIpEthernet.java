package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import java.io.*;

/**
 * recupération de l'adresse ip de ma platine
 *
 * @author Abdellah
 */
public class ActionAdresseIpEthernet implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Porte porte;
    private Process adresseIp;
    private Process eth0;
    private Process eth1;
    private Process wlan0;

    /**
     * init
     */
    @Override
    public void init() {

        menu = Menu.getInstance();
        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        lcd.print(menu.toString(), true);

        try {
            adresseIp = Runtime.getRuntime().exec("hostname -I");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            eth0 = Runtime.getRuntime().exec("ifconfig eth0");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            eth1 = Runtime.getRuntime().exec("ifconfig eth1");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            wlan0 = Runtime.getRuntime().exec("ifconfig wlan0");
        } catch (IOException e) {
            System.out.println(e);
        }
        lcd.setCursor(2, 0);
        BufferedReader adresse = new BufferedReader(new InputStreamReader(adresseIp.getInputStream()));
        BufferedReader ifaceEth0 = new BufferedReader(new InputStreamReader(eth0.getInputStream()));
        BufferedReader ifaceEth1 = new BufferedReader(new InputStreamReader(eth1.getInputStream()));
        BufferedReader ifaceWlan0 = new BufferedReader(new InputStreamReader(wlan0.getInputStream()));

        String s;
        String m = "";

        String s1;
        String m1 = "";

        String s2;
        String m2 = "";

        String s3;
        String m3 = "";

        try {
            while ((s = adresse.readLine()) != null) {
                System.out.println(s);
                m += s;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s1 = ifaceEth0.readLine()) != null) {
                System.out.println(s1);
                m1 += s1;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s2 = ifaceEth1.readLine()) != null) {
                System.out.println(s2);
                m2 += s2;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            while ((s3 = ifaceWlan0.readLine()) != null) {
                System.out.println(s3);
                m3 += s3;
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        System.out.println(m + "\n-----------------------------------------------");

        if (m.length() == 0 || (m.length() < 17 && !m1.contains(m))) {
            //pas d'addresse ip	
            System.out.println("\n--------------------m=0---------------------------");
            lcd.print("NC: NON CONNECTE", false);
        } else if (m.length() < 17) {
            //une seule addresse ip	
            System.out.println("\n--------------------m<17---------------------------");
            if (m1.contains(m)) {
                lcd.print("Et:" + m, false);
            } else if (m2.contains(m)) {
                lcd.print("4G:" + m, false);
            } else if (m.contains("169.254.")) {
                lcd.print("NC: NON CONNECTE", false);
            }

        } else if (m.length() > 17) {
            System.out.println("\n--------------------m>17---------------------------");
            String[] ip = m.split(" ");
            String ip1 = ip[0];
            String ip2 = ip[1];
            //on affiche que ip de eth0
            System.out.println("\n--------------------m>17---------------" + ip1 + "------" + ip2 + "------");
            if (m1.contains(ip1)) {
                lcd.print("EW:" + ip1, false);
                m = ip1;
            } else if (m1.contains(ip2)) {
                lcd.print("EW:" + ip2, false);
                m = ip2;
            }
        }

        porte.setIpInterne(m);
        data.update(porte);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 13);

        if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
