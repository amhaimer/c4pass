package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * action effacer fiche
 *
 * @author Abdellah
 */
public class ActionEffacerFiche implements ActionItem {

    private final int cursorBegin = 5;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    public static String code = new String();

    private char input[] = new char[3];
    private String langue = "";
    DataBase data = DataBase.getInstance();
    Porte porte;
    String input_strX = "";

    /**
     * init
     */
    @Override
    public void init() {
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();

        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("codedeuxpoints", langue), false);
        /*  
	** lcd.cursorMode(true);
         */

        lcd.blinkMode(true);
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 5);
        lcd.saisie(true);

        for (int i = 0; i < 3; i++) {
            input[i] = ' ';
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        for (int i = 0; i < 3; i++) {
            input[i] = ' ';
        }
        cursor = cursorBegin;
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyB) {
            for (int i = 0; i < 3; i++) {
                input[i] = ' ';
            }
            menu.father();
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
            if (cursor >= 8) {
                cursor = cursorBegin;
                lcd.setCursor(2, 5);
            } else {
                cursor++;
            }
        }
        if (event instanceof EventKeyP) {
            cursor = cursorBegin;
            lcd.setCursor(2, 5);
            lcd.print(" ", false);
            lcd.setCursor(2, 5);
            for (int i = 0; i < 3; i++) {
                input[i] = ' ';
            }
        }
        if (event instanceof EventKeyValid) {
            String input_str = new String(input);
            DataBase data = DataBase.getInstance();
            Resident resident = data.getResident(input_str.trim());
            if (resident.getCode() == null) {
                lcd.setCursor(2, 0);
                lcd.clearLcd();
                lcd.print(data.getTraduction("inexistant", langue), false);

            } else {
                data.delete(resident);
                data.addEvent(data.getTraduction("menuprog", langue), input_str, data.getTraduction("donnee_resident", langue), data.getTraduction("effacerfiche", langue), " ");
            }
            menu.previous();
        }
    }
}
