package menu.action.modifEL2;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Entre Libre Deux choix
 *
 * @author Abdellah
 */
public class ActionModifierEntreLibreDeux_choix implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private String langue = "";
    //private char input[];
    private String input = null;
    int count = 0;
    private String OuiNon = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        //input = porte.getChoixEntreLibreDeux().toCharArray();
        input = porte.getChoixEntreLibreDeux();
        lcd.print(menu.toString(), true);

        langue = porte.getTraduction();
        lcd.setCursor(2, 0);
        //lcd.print("OUI=1 NON=0: ", false);
        lcd.print(data.getTraduction(porte.getChoixEntreLibreDeux().equals("-1") ? "nonoui" : "ouinon", langue), false);
        //lcd.print(porte.getChoixEntreLibreDeux().equals("-1") ? "NON" : "OUI",false);
        //lcd.setCursor(2,13);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        lcd.setCursor(2, 0);
        if (event instanceof EventKey0) {
            lcd.print(data.getTraduction("nonoui", langue), false);
            //lcd.print("NON",false);
            //   input[0]= '0';
            input = "-1";
        } else if (event instanceof EventKey1) {
            lcd.print(data.getTraduction("ouinon", langue), false);
            // lcd.print("OUI",false);
            //    input[0]= '1';
            input = "0";
        } else if (event instanceof EventKeyP) {
            lcd.print("---", false);
            //    input[0]= '-';
            input = "-";
        } else if (event instanceof EventKeyValid) {
            /*porte.setChoixEntreLibreDeux(String.valueOf(input));
	    data.update_EntreLibreDeux(porte);
	    if(input[0] == '0') {
		menu.father();
	    }
	    else {
		menu.next();
	    }
	}*/
            if ("-1".equals(input)) {
                OuiNon = data.getTraduction("non", langue);
            } else if ("0".equals(input)) {
                OuiNon = data.getTraduction("oui", langue);
            }
            data.addEvent(data.getTraduction("menuprog", langue), OuiNon, data.getTraduction("donnee_porte", langue), data.getTraduction("modif_fiche", langue), data.getTraduction("validerporte2", langue));
            System.out.println(input);
            if (input.equals("-1")) {
                porte.setChoixEntreLibreDeux(input);
                data.update_EntreLibreDeux(porte);
                menu.father();
            } else {
                input = porte.getChoixEntreLibreDeux();
                switch (input) {
                    case "-1":
                        lcd.print(data.getTraduction("activerleprofil", langue), true);
                        //lcd.print("ACTIVEZ LEPROFIL",true);
                        //lcd.setCursor(2,0);
                        //lcd.print(data.getTraduction("logidiese.com",langue),false);
                        //lcd.print("-\"LOGIDIESE.COM\"", false);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            System.err.print(ex.getMessage());
                        }
                        break;

                    case "0":
                        lcd.print(data.getTraduction("profiltemporel", langue), true);
                        //lcd.print("PROFIL TEMPOREL",true);
                        lcd.setCursor(2, 0);
                        lcd.print(data.getTraduction("permanent", langue), false);
                        //lcd.print("---PERMANENT---", false);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            System.err.print(ex.getMessage());
                        }
                        break;

                    default:
                        lcd.print(data.getTraduction("profiltemporaire", langue), true);
                        //lcd.print("PROFILTEMPORAIRE",true);
                        lcd.setCursor(2, 0);
                        lcd.print(data.getTraduction("logidiese:", langue) + input, false);
                        //lcd.print("LOGIDIESE: " + input, false);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            System.err.print(ex.getMessage());
                        }
                        break;

                }
                menu.father();
            }
        } else if (event instanceof EventKeyB) {
            count = 0;
            menu.father();
        }
    }
}
