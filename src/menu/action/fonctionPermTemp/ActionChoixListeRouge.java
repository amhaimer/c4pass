package menu.action.fonctionPermTemp;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;

/**
 * Action Choix Liste Rouge
 *
 * @author Abdellah
 */
public class ActionChoixListeRouge implements ActionItem {

    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private char input[] = new char[8];
    int idx;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeAppel.code);
        input[0] = resident.getListeRouge().charAt(0);
        idx = 0;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
        lcd.print(input[0] == '1' ? "OUI" : "NON", false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, 13);
            lcd.print("NON", false);
            input[cursor] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, 13);
            lcd.print("OUI", false);
            input[cursor] = '1';
        } else if (event instanceof EventKeyValid) {
            if (idx == 0) {
                if (input[0] == '0') {
                    resident.setListeRouge("0");
                    data.update(resident);
                    menu.next();
                } else {
                    idx++;
                    lcd.print("LROUGE TEMPORAIR", true);
                    lcd.setCursor(2, 0);
                    lcd.print("OUI=1 NON=0: ", false);
                }
            } else {
                if (input[0] == '0') {
                    resident.setListeRouge("1");
                } else {
                    resident.setListeRouge("2");
                }
                data.update(resident);
                menu.next();
            }
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
