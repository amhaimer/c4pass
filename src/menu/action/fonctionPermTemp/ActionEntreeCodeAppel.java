package menu.action.fonctionPermTemp;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Entree Code Appel
 *
 * @author Abdellah
 */
public class ActionEntreeCodeAppel implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    public static String code = new String();

    private String input = new String();

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        input = "";

        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 10);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        input = "";

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKeyB) {
            input = "";
            menu.father();
        }
        if (event instanceof EventKey0) {

            lcd.print("0", false);
            input += "0";
        }
        if (event instanceof EventKey1) {

            lcd.print("1", false);
            input += "1";
        }
        if (event instanceof EventKey2) {

            lcd.print("2", false);
            input += "2";
        }
        if (event instanceof EventKey3) {

            lcd.print("3", false);
            input += "3";
        }
        if (event instanceof EventKey4) {

            lcd.print("4", false);
            input += "4";
        }
        if (event instanceof EventKey5) {

            lcd.print("5", false);
            input += "5";
        }
        if (event instanceof EventKey6) {

            lcd.print("6", false);
            input += "6";
        }
        if (event instanceof EventKey7) {

            lcd.print("7", false);
            input += "7";
        }
        if (event instanceof EventKey8) {

            lcd.print("8", false);
            input += "8";
        }
        if (event instanceof EventKey9) {

            lcd.print("9", false);
            input += "9";
        }
        if (event instanceof EventKeyP) {

            lcd.setCursor(2, 10);
            lcd.print(" ", false);
            lcd.setCursor(2, 10);
            input = "";
        }
        if (input.length() >= 3) {
            DataBase data = DataBase.getInstance();
            Resident resident = data.getResident(input);
            if (resident.getCode() == null) {
                resident = data.insert(input);
            }
            code = resident.getCode();
            menu.next();
        }
    }
}
