package menu.action.fonctionPermTemp;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;

/**
 * Action Choix Renvoi Tempo
 *
 * @author Abdellah
 */
public class ActionChoixRenvoiTempo implements ActionItem {

    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private char input[] = new char[1];

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeAppel.code);
        System.out.println("TEST: " + resident.getTempRenvoi());
        input[0] = resident.getTempRenvoi().charAt(0);
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
        lcd.print(input[0] == '1' ? "OUI" : "NON", false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, 13);
            lcd.print("NON", false);
            input[cursor] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, 13);
            lcd.print("OUI", false);
            input[cursor] = '1';
        } else if (event instanceof EventKeyValid) {
            resident.setTempRenvoi(String.valueOf(input));
            data.update(resident);
            menu.next();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
