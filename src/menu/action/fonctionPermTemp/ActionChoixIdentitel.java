package menu.action.fonctionPermTemp;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;

/**
 * Action Choix Identitel
 *
 * @author Abdellah
 */
public class ActionChoixIdentitel implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    private DataBase data = DataBase.getInstance();
    private Resident resident;
    private char input[] = new char[8];
    private int count = 0;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeAppel.code);
        input[0] = '0';
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
        lcd.print(input[0] == '1' ? "OUI" : "NON", false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, 13);
            lcd.print("NON", false);
            input[0] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, 13);
            lcd.print("OUI", false);
            input[0] = '1';
        } else if (event instanceof EventKeyValid) {
            if (count == 0) {
                if (input[0] == '1') {
                    count++;

                    lcd.print("INDENTITEL TEMPOR", true);
                    lcd.setCursor(2, 0);
                    lcd.print("OUI=1 NON=0: ", false);
                } else if (input[0] == '0') {
                    resident.setIdentitel("0");
                    data.update(resident);
                    menu.next();
                }
            } else if (count == 1) {
                if (input[0] == '0') {
                    resident.setIdentitel("1");
                }
                if (input[0] == '1') {
                    resident.setIdentitel("2");
                }
                data.update(resident);
                menu.next();
            }
            System.out.println(resident.getIdentitel());
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
