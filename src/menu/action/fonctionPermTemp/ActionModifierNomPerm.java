package menu.action.fonctionPermTemp;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.data.Porte;
import sql.data.Resident;
import sql.DataBase;

/**
 * Action Modifier Nom Perm
 *
 * @author Abdellah
 */
public class ActionModifierNomPerm implements ActionItem {

    private final int cursorBegin = 0;

    private int cursor = cursorBegin;
    private Menu menu;
    private Lcd lcd;
    private int c;
    private char[] input = new char[16];
    Event lastEvent;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private final char[] lettre = " .,1ABC2DEF3GHI4JKL5MNO6PQRS7TUV8WXYZ9-\'#0".toCharArray();

    /**
     * init
     */
    @Override
    public void init() {

        resident = data.getResident(ActionEntreeCodeAppel.code);
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        if (resident.getName() != null) {
            int i;
            lcd.print(resident.getName(), false);
            for (i = 0; i < resident.getName().length(); i++) {
                input[i] = resident.getName().charAt(i);
            }
            while (i < 15) {
                input[i++] = ' ';
            }
            lcd.setCursor(2, 0);
        }
        c = 0;
        lastEvent = new EventKeyValid();
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        c = (lastEvent.getClass().equals(event.getClass())) ? c : 0;
        System.out.println(c);
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKeyB) {
            menu.father();
        }
        if (event instanceof EventKey1) {
            lcd.print(String.valueOf(lettre[0 + c]), false);
            input[cursor] = lettre[0 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey2) {
            lcd.print(String.valueOf(lettre[4 + c]), false);
            input[cursor] = lettre[4 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey3) {
            lcd.print(String.valueOf(lettre[8 + c]), false);
            input[cursor] = lettre[8 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey4) {
            lcd.print(String.valueOf(lettre[12 + c]), false);
            input[cursor] = lettre[12 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey5) {
            lcd.print(String.valueOf(lettre[16 + c]), false);
            input[cursor] = lettre[16 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey6) {
            lcd.print(String.valueOf(lettre[20 + c]), false);
            input[cursor] = lettre[20 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey7) {
            lcd.print(String.valueOf(lettre[24 + c]), false);
            input[cursor] = lettre[24 + c];
        }
        if (event instanceof EventKey8) {
            lcd.print(String.valueOf(lettre[29 + c]), false);
            input[cursor] = lettre[29 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKey9) {
            lcd.print(String.valueOf(lettre[33 + c]), false);
            input[cursor] = lettre[33 + c];
        }
        if (event instanceof EventKey0) {
            lcd.print(String.valueOf(lettre[38 + c]), false);
            input[cursor] = lettre[38 + c];
            c = c == 3 ? c + 1 : c;
        }
        if (event instanceof EventKeyRight) {
            cursor++;
            if (cursor <= 15) {
                lcd.setCursor(2, cursor);
            }
        }
        if (event instanceof EventKeyLeft) {
            cursor--;
            if (cursor < 0) {
                cursor = (resident.getName().length() - 1);
            }
        }
        if (cursor > 15) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKeyValid) {
            String nom = new String(input);
            if (nom == null) {
                nom = " ";
            }
            resident.setName(nom.trim());
            data.update(resident);
            Porte porte = Porte.getInstance();
            char val[] = porte.getInterphone().toCharArray();
            if (val[0] == '4') {
                for (int i = 0; i < 3; i++) {
                    menu.next();
                }
            } else if (val[0] == '3') {
                for (int i = 0; i < 8; i++) {
                    menu.next();
                }
            }
            menu.next();
        }
        if (event instanceof EventKeyP) {
            for (int i = 0; i < 16; i++) {
                input[i] = ' ';
            }
            lcd.setCursor(2, 0);
            lcd.print(" ", false);
            lcd.setCursor(2, 0);
            cursor = 0;
        }
        c = c < 4 ? c + 1 : 0;
        lcd.setCursor(2, cursor);
        lastEvent = event;
        System.out.println("cursor: " + cursor);
    }
}
