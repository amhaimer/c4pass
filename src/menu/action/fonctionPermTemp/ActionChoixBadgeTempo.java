package menu.action.fonctionPermTemp;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import menu.action.ActionItem;
import sql.DataBase;
import sql.data.Resident;

/**
 * Action Choix BadgeT empo
 *
 * @author Abdellah
 */
public class ActionChoixBadgeTempo implements ActionItem {

    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Resident resident;
    private char input[] = new char[8];
    int count = 0;
    Boolean boo;

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        resident = data.getResident(ActionEntreeCodeAppel.code);
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print("OUI=1 NON=0: ", false);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action evenement
     *
     * @param event
     */
    @Override
    public void action(Event event) {

        boo = true;
        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, 13);
            lcd.print("NON", false);
            input[cursor] = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, 13);
            lcd.print("OUI", false);
            input[cursor] = '1';
        } else if (event instanceof EventKeyValid) {
            resident.setBadgeTemporaire(String.valueOf(input));
            System.out.println("'" + resident.getBadgeTemporaire() + "'");
            data.update(resident);
            menu.next();
        } else if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
