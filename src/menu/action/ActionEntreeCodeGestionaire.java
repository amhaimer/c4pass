package menu.action;

import event.*;
import event.eventType.*;

import menu.*;
import devices.Lcd;
import sql.data.Porte;
import sql.DataBase;
import utils.Cryptage;

/**
 * action de défielment des résidents
 *
 * @author Abdellah
 */
public class ActionEntreeCodeGestionaire implements ActionItem {

    private final int cursorBegin = 6;

    private int cursor = cursorBegin;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private Menu menu;
    private Lcd lcd;
    private String mdp = new String();

    private boolean isFailed = false;
    private String input = new String();
    devices.TimerLocal timer;
    EventManager manager;

    /**
     * init
     */
    @Override
    public void init() {
        porte = Porte.getInstance();
        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        manager = EventManager.getInstance();
        System.out.println(porte.getCode());
        mdp = porte.getCode();
        isFailed = false;
        input = "";
        timer = manager.requestTimer(15, devices.TimerLocal.Unit.sec);
        cursor = cursorBegin;
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 5);
        switch (porte.getInterphone().charAt(1)) {
            case '1':
            case '2':
                lcd.print("A", false);
                lcd.saisie(true);
                break;
            case '3':
                lcd.print("P ", false);
                lcd.saisie(true);
                break;
            case '4':
                lcd.print("PRODUIT INADAPTE\n GIBRALTAR", true);
                isFailed = true;
                break;
            default:
                lcd.print("PRODUIT INADAPTE\n INCONNU", true);
                isFailed = true;
                break;
        }
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        input = "";
        cursor = cursorBegin;

        /*
	** lcd.saisie(false);
         */
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (!isFailed) {
            /*if (event instanceof EventKeyB) {
		lcd.saisie(false);
		input = "";
		menu.father();
	    }*/
            if (event instanceof EventKey0) {
                cursor++;
                lcd.print("*", false);
                input += "0";
            }
            if (event instanceof EventKey1) {
                cursor++;
                lcd.print("*", false);
                input += "1";
            }
            if (event instanceof EventKey2) {
                cursor++;
                lcd.print("*", false);
                input += "2";
            }
            if (event instanceof EventKey3) {
                cursor++;
                lcd.print("*", false);
                input += "3";
            }
            if (event instanceof EventKey4) {
                cursor++;
                lcd.print("*", false);
                input += "4";
            }
            if (event instanceof EventKey5) {
                cursor++;
                lcd.print("*", false);
                input += "5";
            }
            if (event instanceof EventKey6) {
                cursor++;
                lcd.print("*", false);
                input += "6";
            }
            if (event instanceof EventKey7) {
                cursor++;
                lcd.print("*", false);
                input += "7";
            }
            if (event instanceof EventKey8) {
                cursor++;
                lcd.print("*", false);
                input += "8";
            }
            if (event instanceof EventKey9) {
                cursor++;
                lcd.print("*", false);
                input += "9";
            }
            if (event instanceof EventKeyP) {
                cursor = cursorBegin;
                lcd.setCursor(2, cursor);
                lcd.print("    ", false);
                lcd.setCursor(2, cursor);
                input = "";
            }
            if (event instanceof EventTimer || event instanceof EventKeyB) {
                isFailed = false;
                lcd.saisie(false);
                manager.releaseTimer(timer);
                menu.father();
            }
            if (input.length() >= 4) {

                lcd.saisie(false);
                manager.releaseTimer(timer);
                if (input.equals(mdp)) {
                    menu.child();
                    data.addEvent("CODE GESTION", "Code valide", " ", " ", " ");
                } else {

                    String encr = Cryptage.encrypt(mdp);
                    lcd.print(encr, true);
                    data.addEvent("CODE GESTION", "Code invalide", " ", " ", " ");
                    lcd.setCursor(2, 0);
                    lcd.print("Code Invalide", false);
                    isFailed = true;
                    timer = manager.requestTimer(3, devices.TimerLocal.Unit.sec);
                }
                cursor = cursorBegin;
                input = "";
            }
        } else {
            isFailed = false;
            lcd.saisie(false);
            manager.releaseTimer(timer);
            menu.father();
        }
    }
}
