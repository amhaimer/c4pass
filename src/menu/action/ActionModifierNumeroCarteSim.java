package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * Action Modifier Numero Carte Sim
 *
 * @author Abdellah
 */
public class ActionModifierNumeroCarteSim implements ActionItem {

    private final int cursorBegin = 3;
    private final int cursorEnd = 12;
    private int cursor = 0;
    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private char input[] = porte.getNumeroSim().toCharArray();
    private int c = 0;

    /**
     * init
     */
    @Override
    public void init() {

        cursor = cursorBegin;
        lcd = Lcd.getInstance();
        lcd.clearLcd();
        lcd.print("NUMERO CARTE SIM \n", true);
        lcd.setCursor(2, cursor);
        lcd.print(porte.getNumeroSim(), false);
        lcd.setCursor(2, cursor);
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {

        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (c == 0) {
            lcd.print(porte.getNumeroSim(), false);
            lcd.setCursor(2, cursor);
        }
        c++;
        if (cursor > cursorEnd) {
            cursor = cursorBegin;
            lcd.setCursor(2, cursor);
        }
        if (event instanceof EventKey0) {
            lcd.print("0", false);
            input[cursor - cursorBegin] = '0';
            cursor++;
        }
        if (event instanceof EventKey1) {
            lcd.print("1", false);
            input[cursor - cursorBegin] = '1';
            cursor++;
        } else if (event instanceof EventKey2) {
            lcd.print("2", false);
            input[cursor - cursorBegin] = '2';
            cursor++;
        } else if (event instanceof EventKey3) {
            lcd.print("3", false);
            input[cursor - cursorBegin] = '3';
            cursor++;
        } else if (event instanceof EventKey4) {
            lcd.print("4", false);
            input[cursor - cursorBegin] = '4';
            cursor++;
        } else if (event instanceof EventKey5) {
            lcd.print("5", false);
            input[cursor - cursorBegin] = '5';
            cursor++;
        } else if (event instanceof EventKey6) {
            lcd.print("6", false);
            input[cursor - cursorBegin] = '6';
            cursor++;
        } else if (event instanceof EventKey7) {
            lcd.print("7", false);
            input[cursor - cursorBegin] = '7';
            cursor++;
        } else if (event instanceof EventKey8) {
            lcd.print("8", false);
            input[cursor - cursorBegin] = '8';
            cursor++;
        } else if (event instanceof EventKey9) {
            lcd.print("9", false);
            input[cursor - cursorBegin] = '9';
            cursor++;
        } else if (event instanceof EventKeyValid) {
            c = 0;
            porte.setNumeroSim(String.valueOf(input));
            data.update(porte);
            menu.next();
        } else if (event instanceof EventKeyB) {
            c = 0;
            menu.father();
        }
    }
}
