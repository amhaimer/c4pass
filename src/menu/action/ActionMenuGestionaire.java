package menu.action;

import event.*;
import event.eventType.*;
import event.EventManager;
import menu.*;
import devices.Lcd;

/**
 * action menu gestionnaire
 *
 * @author Abdellah
 */
public class ActionMenuGestionaire implements ActionItem {

    private Menu menu;
    EventManager manager;
    devices.TimerLocal timer;

    /**
     * init
     */
    @Override
    public void init() {
        Lcd lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        lcd.print(menu.toString(), true);
        manager = EventManager.getInstance();

    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();

        if (event instanceof EventKeyLeft) {
            // inter = true;
            manager.releaseTimer(timer);
            //    timer = manager.requestTimer(30,devices.Timer.Unit.sec);
            menu.previous();
        }
        if (event instanceof EventKeyRight) {
            //   inter = true;
            manager.releaseTimer(timer);
            //   timer = manager.requestTimer(30,devices.Timer.Unit.sec);
            menu.next();
        }
        if (event instanceof EventKeyValid) {
            //  inter = true;
            manager.releaseTimer(timer);
            //  timer = manager.requestTimer(30,devices.Timer.Unit.sec);
            menu.child();
        }
        if (event instanceof EventKeyB) {
            menu.father();
            menu.father();
        }
        /*if (event instanceof EventTimer) {
		manager.releaseTimer(timer);	
		menu.father();
		menu.father();
	}*/
    }

    @Override
    public String toString() {
        return "action : Menu1 \n";
    }

}
