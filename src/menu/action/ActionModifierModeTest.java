package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;
import devices.HpMicro;

/**
 * Action Modifier Mode Test
 *
 * @author Abdellah
 */
public class ActionModifierModeTest implements ActionItem {

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    private String input = "";
    int count = 0;
    private String langue = "";
    HpMicro hp = HpMicro.getInstance();

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();
        porte = Porte.getInstance();
        langue = porte.getTraduction();
        input = porte.getValidIdentitelSIP();
        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("nonoui", langue), false);
        lcd.setCursor(2, 13);
        System.out.println("--------------------------------" + input);
        lcd.print(input.equals("01") ? data.getTraduction("oui", langue) : data.getTraduction("non", langue), false);

    }

    /**
     * exit
     */
    @Override
    public void exit() {
        lcd.saisie(false);
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();

        if (event instanceof EventKey0) {
            lcd.setCursor(2, 13);
            lcd.print(data.getTraduction("non", langue), false);
            input = "00";
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, 13);
            lcd.print(data.getTraduction("oui", langue), false);
            input = "01";
        } else if (event instanceof EventKeyLeft) {
            menu.previous();
        } else if (event instanceof EventKeyRight) {
            menu.next();
        } else if (event instanceof EventKeyValid) {
            count = 0;

            porte.setValidIdentitelSIP(input);
            data.update(porte);

            menu.father();
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        }
    }
}
