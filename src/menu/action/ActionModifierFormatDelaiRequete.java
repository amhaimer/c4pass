package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * mettre le format de delais commme suivant :: heure:minutes:secondes
 *
 * @author Abdellah
 */
public class ActionModifierFormatDelaiRequete implements ActionItem {

    private final int cursorBegin = 13;

    private Menu menu;
    private Lcd lcd;
    DataBase data = DataBase.getInstance();
    Porte porte;
    private char input;
    int count = 0;
    int ns, nh, nm, nsr;
    private String langue = "";

    /**
     * init
     */
    @Override
    public void init() {

        lcd = Lcd.getInstance();
        menu = Menu.getInstance();

        porte = Porte.getInstance();
        langue = porte.getTraduction();
        ns = Integer.parseInt(porte.getTimeConnexionWeb());
        nh = ns / (60 * 60);
        nm = (ns / 60) % 60;
        nsr = ns % 60;

        lcd.print(menu.toString(), true);
        lcd.print(String.valueOf(nh) + "h" + " " + String.valueOf(nm) + "mn" + " " + String.valueOf(nsr) + "s", false);
        lcd.setCursor(2, 0);
        lcd.print(data.getTraduction("nonoui", langue), false);
        //lcd.print("OUI=1 NON=0: ",false);
        lcd.setCursor(2, 13);

        /*
	** if (porte.getValidConnexionWeb().equals("1"))
	** lcd.print("OUI", false);
	** else
	** lcd.print("NON", false);
         */
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        if (event instanceof EventKey0) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("non", langue), false);
            //lcd.print("NON",false);
            input = '0';
        } else if (event instanceof EventKey1) {
            lcd.setCursor(2, cursorBegin);
            lcd.print(data.getTraduction("oui", langue), false);
            //lcd.print("OUI",false);
            input = '1';
        } else if (event instanceof EventKeyValid) {
            count = 0;


            /*
	    ** porte.setValidConnexionWeb(String.valueOf(input));
	    **data.addEvent(data.getTraduction("menuprog",langue),data.getTraduction("donnee_porte",langue),data.getTraduction("modif_fiche",langue), data.getTraduction("delaiinternet",langue), " ");
	    ** +data.update(porte);
             */
            if (input == '1') {
                menu.father();
            } else {
                menu.previous();
            }
        } else if (event instanceof EventKeyB) {

            count = 0;
            menu.father();
        }
    }
}
