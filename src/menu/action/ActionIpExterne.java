package menu.action;

import event.*;
import event.eventType.*;
import menu.*;
import devices.Lcd;
import sql.DataBase;
import sql.data.Porte;

/**
 * action de modification IP Externe
 *
 * @author Abdellah
 */
public class ActionIpExterne implements ActionItem {

    private Menu menu;
    private Lcd lcd;

    Event lastEvent;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();

    /**
     * init
     */
    @Override
    public void init() {

        data = DataBase.getInstance();
        porte = Porte.getInstance();

        lcd = Lcd.getInstance();
        lcd.clearLcd();
        menu = Menu.getInstance();

        lcd.print(menu.toString(), true);
        lcd.setCursor(2, 0);
        lcd.print(porte.getIpExterne(), false);
        lcd.setCursor(2, 0);

        /*
	** if(porte.getIpExterne()!=null)
	** {
	** int i = 0;
	** lcd.print(porte.getIpExterne(),false);
	** for(i=0;i<porte.getIpExterne().length();i++)
	** input[i]=porte.getIpExterne().charAt(i);
	** while (i < 15)
	** input[i++] = ' ';
	** lcd.setCursor(2,0);
	** }
         */
        lcd.saisie(true);
    }

    /**
     * exit
     */
    @Override
    public void exit() {
    }

    /**
     * action
     *
     * @param event evenement
     */
    @Override
    public void action(Event event) {

        menu = Menu.getInstance();
        lcd = Lcd.getInstance();
        //  data = DataBase.getInstance();
        porte = Porte.getInstance();

        if (event instanceof EventKeyValid) {
            menu.next();
        }
        if (event instanceof EventKeyB) {
            menu.father();
        }
    }
}
