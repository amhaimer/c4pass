package menu;

import devices.Lcd;
import devices.Relais;
import devices.Buzzer;
import sql.data.Porte;
import sql.DataBase;
import event.EventManager;
import java.util.ArrayList;

import java.util.List;
import utils.Sound;

/**
 * gestion de la porte
 *
 * @author Abdellah
 */
public class PorteManager {

    private Menu menu;
    private Lcd lcd = Lcd.getInstance();
    private Relais relais = Relais.getInstance();
    private Buzzer buzzer;
    private Horodatage horodatage = Horodatage.getInstance();
    private String mdpUn = new String();
    private String mdpDeux = new String();
    private devices.TimerLocal timer;
    private static PorteManager INSTANCE = null;
    DataBase data = DataBase.getInstance();
    Porte porte = Porte.getInstance();
    public static boolean status_porte1 = false;
    public static boolean status_porte2 = false;

    public static String eventEL = "";
    public static String lcdShow = "";

    private String tempsOuverture = "X";
    private Sound sound = new Sound();
    private String langue = "";
    public boolean status_el1 = false, status_el2 = false;

    /**
     *
     * init
     */
    public void init() {
        lcd = Lcd.getInstance();
        relais = Relais.getInstance();
        buzzer = Buzzer.getInstance();
        menu = Menu.getInstance();
        //status_porte1 = relais.getStatusUn();
        mdpUn = porte.getClavierCodeUn();
        mdpDeux = porte.getClavierCodeDeux();
        langue = porte.getTraduction();
    }

    /**
     * ouverture de la porte par entrée libre
     *
     * @param _porte porte à ouvrir
     */
    private void elOuvrirPorte(int _porte) {
        if (_porte == 1 && !relais.getStatusRelais1()) {
            relais.OpenDoor(1);
            status_porte1 = true;
            /*  if (porte.getSignalSonore() == null) {
                return;
            }
            if (porte.getSignalSonore().length() < 2) {
                return;
            }
            if (Character.getNumericValue(porte.getSignalSonore().charAt(1)) >= 2) {
                if (langue.equals("francais")) {
                    sound.play("porte_ouverte.wav");
                } else if (langue.equals("anglais")) {
                    //sound.play("OpenDoor.mp3");
                    sound.play("OpenDoor.wav");
                } else {
                    sound.play("porte_ouverte.wav");
                }
            }*/
        }
        if (_porte == 2 && !relais.getStatusRelais2()) {
            relais.OpenDoor(2);
            status_porte2 = true;
        }
    }

    /**
     * fermeture de la porte par entree libre
     *
     * @param porte porte à fermer
     */
    private void elFermerPorte(int porte) {
        if (porte == 1 && status_porte1) {
            relais.CloseDoor(1);
            status_porte1 = false;
        }
        if (porte == 2 && status_porte2) {
            relais.CloseDoor(2);
            status_porte2 = false;
        }
    }

    /**
     * fonction d'entree libre
     *
     * @return true si entree libre valide
     */
    public boolean entreeLibre() {
        porte = Porte.getInstance();
        String elPorte1 = porte.getChoixEntreLibreUn();
        String elPorte2 = porte.getChoixEntreLibreDeux();

        if (elPorte1.equals("-1") && elPorte2.equals("-1")) { // Entree Libre desactivee
            elFermerPorte(1);
            elFermerPorte(2);
            status_el1 = false;
            status_el2 = false;
            return false;
        }

        boolean entreeLibre1 = elPorte1.equals("0") ? true : horodatage.check(elPorte1); // Si Entree Libre est à 0, entree libre permante
        //  System.out.println(horodatage.getCause().toString());
        boolean entreeLibre2 = elPorte2.equals("0") ? true : horodatage.check(elPorte2); // Sinon regarder si l'horodatage est valide
        //     System.out.println(horodatage.getCause().toString());
        if (entreeLibre1 && entreeLibre2) { // Si les deux portes sont en entree libres
            elOuvrirPorte(1);
            elOuvrirPorte(2);
            status_el1 = true;
            status_el2 = true;
            //lcd.print("ENTREE LIBRE\nPORTES OUVERTES", true);
            lcd.print(data.getTraduction("entreelibrelcd", langue) + "\n" + data.getTraduction("portesouvertes", langue), true);
        } else if (entreeLibre1) { // Si uniquement Porte 1 en entree libre
            elOuvrirPorte(1);
            elFermerPorte(2);
            status_el1 = true;
            status_el2 = false;
            lcd.print(data.getTraduction("entreelibrelcd", langue) + "\n" + data.getTraduction("porte1ouverte", langue), true);
        } else if (entreeLibre2) { // Si uniquement Porte 2 en entree libre
            elOuvrirPorte(2);
            elFermerPorte(1);
            status_el1 = false;
            status_el2 = true;
            lcd.print(data.getTraduction("entreelibrelcd", langue) + "\n" + data.getTraduction("porte2ouverte", langue), true);
            //lcd.print("ENTREE LIBRE\nPORTE 2 OUVERTE", true);
        } else {
            if (status_el1) {
                elFermerPorte(1);

            }
            if (status_el2) {
                elFermerPorte(2);
            }
            status_el1 = false;
            status_el2 = false;
            return false;
        }
        return true;
    }

    /*------------------------------------------------------------------------------------------------------------------*/
    /**
     * fermeture d'entree libre
     *
     * @param porteid id dela porte
     * @param horoel id tranche horaire
     */
    public void fermetureEL(int porteid, int horoel) {

        // data = DataBase.getInstance();
        porte = Porte.getInstance();

        if (porteid == 1) {
            relais.CloseDoor(1);
        } else if (porteid == 2) {
            relais.CloseDoor(2);
        }

        switch (horoel) {
            case 8:
                lcdShow = "PORTE " + String.valueOf(porteid) + " FERMEE\nEL" + String.valueOf(porteid) + "T--DATE INVAL";
                if (!eventEL.equals("DI " + String.valueOf(porteid))) {
                    data.addEvent("Entree Libre T", "Porte " + porteid + " Ferme", "DATE INVALIDE", "", "");
                    eventEL = "DI " + String.valueOf(porteid);
                }
                break;
            case 9:
                lcdShow = "PORTE " + String.valueOf(porteid) + " FERMEE\nEL" + String.valueOf(porteid) + "T--JOUR INVAL";
                if (!eventEL.equals("JI " + String.valueOf(porteid))) {
                    data.addEvent("Entree Libre T", "Porte " + porteid + " Ferme", "JOUR INVALIDE", "", "");
                    eventEL = "JI " + String.valueOf(porteid);
                }
                break;
            case 10:
                lcdShow = "PORTE " + String.valueOf(porteid) + " FERMEE\nEL" + String.valueOf(porteid) + "T--HORA INVAL";
                if (!eventEL.equals("HI " + String.valueOf(porteid))) {
                    data.addEvent("Entree Libre T", "Porte " + porteid + " Ferme", "HORAIRE INVALIDE", "", "");
                    eventEL = "HI " + String.valueOf(porteid);
                }
                break;
            default:
                break;
        }

    }

    /**
     * gestion du timer
     *
     * @param id identifiant
     * @param type type
     * @param porteid id de la porte
     * @param mode mode
     */
    /*---------------------------------------------------------------------------------------------------------------------*/
    private void timer(int id, String type, String porteid, String mode) {

        EventManager manager = EventManager.getInstance();
        //   data = DataBase.getInstance();
        porte = Porte.getInstance();
        int tempsOuverture = 10;
        long tempsRestant, lastTemps;
        String format = new String();

        if (porteid.equals("1")) {
            tempsOuverture = Integer.parseInt(porte.getTempsOuvertureUn());
        } else if (porteid.equals("2")) {
            tempsOuverture = Integer.parseInt(porte.getTempsOuvertureDeux());
        }

        lcd.setCursor(2, 0);
        timer = manager.requestTimer(tempsOuverture, devices.TimerLocal.Unit.sec);
        lastTemps = timer.getRemainingSecond();

        while (!timer.isTimerEnd()) {
            tempsRestant = timer.getRemainingSecond();
            lcd.setCursor(2, 0);
            format = String.format("%02d", tempsRestant);
            if (type.equals("CC")) {
                lcd.print("CC" + id + mode + "---------" + format + "s", false);
            } else if (type.equals("BP")) {
                lcd.print("BP" + id + "--------- " + format + "s", false);
            }
            if (type.equals("#1")) {
                lcd.print("# " + id + "----------" + format + "s", false);
            }
            if (type.equals("#2")) {
                lcd.print("# " + id + "----------" + format + "s", false);
            }
            if (lastTemps != tempsRestant) {
                if (porteid.equals("1") && (porte.getSignalSonore().charAt(0) == '1' || porte.getSignalSonore().charAt(0) == '3')) {
                    buzzer.buzz(1);
                } else if (porteid.equals("2") && (porte.getSignalSonore().charAt(1) == '1' || porte.getSignalSonore().charAt(1) == '3')) {
                    buzzer.buzz(1);
                }
                lastTemps = tempsRestant;
            }
            manager.releaseTimer(timer);
        }
    }

    /**
     * ouverture par bouton poussoir
     *
     * @param id id porte
     * @param type type
     * @param mode mode
     */
    public void ouvertureBP(int id, String type, String mode) {
        List<Boolean> list = new ArrayList<>();
        list.add(relais.getStatusRelais1());
        list.add(relais.getStatusRelais2());

        if (!list.get(id - 1)) {
            porte = Porte.getInstance();
            int tempsOuverture = 10;
            long starttime = System.currentTimeMillis() / 1000;

            if (id == 1) {
                tempsOuverture = Integer.parseInt(porte.getTempsOuvertureUn());
            } else {
                tempsOuverture = Integer.parseInt(porte.getTempsOuvertureDeux());
            }

            relais.OpenDoor(id);

            while ((System.currentTimeMillis() / 1000 - starttime) < tempsOuverture) {

            }

            relais.CloseDoor(id);

        }

    }

    /**
     * fermeture des relais
     */
    public void initRel() {
        relais.CloseDoor(1);
        relais.CloseDoor(2);

    }

    /**
     * ouverture de la porte
     *
     * @param id id de la porte à ouvrir
     * @param type type d'ouverture
     * @param mode mode
     */
    public void ouverture(int id, String type, String mode) {

        //data = DataBase.getInstance();
        porte = Porte.getInstance();
        String porteid = new String();

        porteid = id == 1 ? porte.getChoixClavierCodeUn() : porte.getChoixClavierCodeDeux();

        // BUZZER
        if (porteid.equals("1") && (porte.getSignalSonore().charAt(0) == '1' || porte.getSignalSonore().charAt(0) == '3')
                || porteid.equals("2") && (porte.getSignalSonore().charAt(1) == '1' || porte.getSignalSonore().charAt(1) == '3')) {
            buzzer.buzz(400);
        }

        // OUVERTURE PORTE
        relais.OpenDoor(Integer.parseInt(porteid));
        switch (langue) {
            case "fancais":
                sound.play("porte_ouverte.wav");
                break;
            case "anglais":
                //sound.play("OpenDoor.mp3");
                sound.play("OpenDoor.wav");
                break;
            default:
                sound.play("porte_ouverte.wav");
                break;
        }
        //sound.play("porte_ouverte.wav");
        lcd.setCursor(2, 0);
        if (porteid.equals("1")) {
            lcd.print(data.getTraduction("porte1ouverte", langue), true);
        } else if (porteid.equals("2")) {
            lcd.print(data.getTraduction("porte2ouverte", langue), true);
        }

        tempsOuverture = porteid.equals("1") ? porte.getTempsOuvertureUn() : porte.getTempsOuvertureDeux();

        // EVENEMENT
        if (type.equals("BP")) {
            data.addEvent("Bouton Poussoir", "Porte " + porteid + ": Ouverte", "BP" + porteid + " " + tempsOuverture + "s", "", "");
            if (porteid.equals("1")) {
                data.addEvent(data.getTraduction("boutonpoussoir", langue), data.getTraduction("ouvertureporte", langue), " ", data.getTraduction("porte1ouverte", langue), "bp");
            } else if (porteid.equals("2")) {
                data.addEvent(data.getTraduction("boutonpoussoir", langue), data.getTraduction("ouvertureporte", langue), " ", data.getTraduction("porte2ouverte", langue), "bp");
            }
        }

        // ATTENTE
        timer(id, type, porteid, mode);

        // BUZZER
        if (porteid.equals("1") && (porte.getSignalSonore().charAt(0) == '1' || porte.getSignalSonore().charAt(0) == '3')
                || porteid.equals("2") && porte.getSignalSonore().charAt(1) == '1' || porte.getSignalSonore().charAt(1) == '3') {
            buzzer.buzz(800);
        }

        // FERMETURE PORTE
        relais.CloseDoor(Integer.parseInt(porteid));

        if (!type.equals("#1") && !type.equals("#2")) {
            System.out.println("in");
            menu.child();
            menu.father();
        }
    }

    /**
     * fermeture des portes clavier codée
     *
     * @param id identifiant
     * @param horocc tranche horraire
     */
    public void fermetureCC(int id, int horocc) {

        // data = DataBase.getInstance();
        porte = Porte.getInstance();
        String porteid = new String();
        porteid = id == 1 ? porte.getChoixClavierCodeUn() : porte.getChoixClavierCodeDeux();

        if (Integer.parseInt(porteid) == 1) {
            relais.CloseDoor(1);
        } else if (Integer.parseInt(porteid) == 2) {
            relais.CloseDoor(2);
        }
        switch (horocc) {
            case 3:
                //System.out.println("date invalide");
                System.out.println(data.getTraduction("dateinvalide", langue));
                if (porteid.equals("1")) {
                    lcd.print(data.getTraduction("porte1fermee", langue), true);
                } else if (porteid.equals("2")) {
                    lcd.print(data.getTraduction("porte2fermee", langue), true);
                }   //lcd.print("PORTE "+porteid+" FERMEE", true);
                lcd.setCursor(2, 0);
                lcd.print("CC1T-" + data.getTraduction("dateinvalide", langue), false);
                buzzer.buzz(2000);
                if (porteid.equals("1")) {
                    data.addEvent(data.getTraduction("saisieCC", langue), data.getTraduction("porte1fermee", langue), data.getTraduction("dateinvalide", langue), "", "");
                } else if (porteid.equals("2")) {
                    data.addEvent(data.getTraduction("saisieCC", langue), data.getTraduction("porte2fermee", langue), data.getTraduction("dateinvalide", langue), "", "");
                }   //data.addEvent("Saisie CC", "Porte "+ porteid +" Ferme", data.getTraduction("dateinvalide",langue),"","");
                menu.child();
                menu.father();
                break;
            case 4:
                System.out.println("jour invalide");
                lcd.print("PORTE " + porteid + " FERMEE", true);
                lcd.setCursor(2, 0);
                lcd.print("CC1T--JOUR INVAL", false);
                buzzer.buzz(2000);
                data.addEvent("Saisie CC", "Porte " + porteid + " Ferme", "JOUR INVALIDE", "", "");
                menu.child();
                menu.father();
                break;
            case 5:
                System.out.println("horraire invalide");
                lcd.print("PORTE " + porteid + " FERMEE", true);
                lcd.setCursor(2, 0);
                lcd.print("CC1T--HORA INVAL", false);
                buzzer.buzz(2000);
                data.addEvent("Saisie CC", "Porte " + porteid + " Ferme", "HORAIRE INVALIDE", "", "");
                menu.child();
                menu.father();
                break;
            default:
                break;
        }

    }

    /**
     * vérification clavier codé
     *
     * @param input clavier codé
     */
    public void request(String input) {
        // data = DataBase.getInstance();
        porte = Porte.getInstance();

        mdpUn = porte.getClavierCodeUn();
        mdpDeux = porte.getClavierCodeDeux();

        String id = new String();
        int idPorte = -1;

        if (input.equals(mdpUn)) {
            id = porte.getTemporaireClavierCodeUn();
            idPorte = 1;
        } else if (input.equals(mdpDeux)) {
            id = porte.getTemporaireClavierCodeDeux();
            idPorte = 2;
        }

        if (idPorte != -1) {
            if (id.equals("0") || horodatage.check(id)) {
                ouverture(idPorte, "CC", id.equals("0") ? "P" : "T");
                data.addEvent("DIGICODE", "Code : " + input, (id.equals("0") ? "PERMANENT" : "TEMPORAIRE : " + id),
                        "PORTE " + idPorte + " OUVERTE", "CCX" + (id.equals("0") ? "P" : "T") + "---" + tempsOuverture + "s");
            } else {
                //  lcd.print("PORTE " + idPorte + " FERMEE\nCCX-" + horodatage.getCause() + " INVAL", true); // HORAIRE INVALIDE
                buzzer.buzz(2000);
                //  data.addEvent("DIGICODE", "Code : " + input, "TEMPORAIRE : " + id, "PORTE " + idPorte + " FERMEE", "CCX-" + horodatage.getCause() + " INVAL");
            }
        } else {
            lcd.print("  PORTE FERMEE  \n CODE INVALIDE", true);
            buzzer.buzz(2000);
            data.addEvent("DIGICODE", "Code : " + input, "", "PORTE FERMEE", "CODE INVALIDE");
        }
        menu.child();
        menu.father();
    }

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static PorteManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PorteManager();
        }
        return INSTANCE;
    }
}
