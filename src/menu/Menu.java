package menu;

import menu.action.*;
import menu.action.ficheBadge.*;
import menu.action.ficheTelephone.*;
import menu.action.modifCC1.*;
import menu.action.modifCC2.*;
import menu.action.modifEL1.*;
import menu.action.modifEL2.*;
import sql.data.Porte;

/**
 * classe menu qui regroupe tous les items
 *
 * @author Abdellah
 */
public class Menu {

    private static Menu INSTANCE = null;
    private Item root;
    /**
     * l'item en cours
     */
    public Item currentItem = null;
    public boolean itemChanged = true;

    /**
     * constructuer de classe qui rajoute tous les element pour former un menu
     * comme une arbre
     */
    private Menu() {

        // DataBase data = DataBase.getInstance();
        Porte porte = Porte.getInstance();
        root = new Item("demarrage");
        currentItem = root;
        ActionSwitchTime SwitchTime = new ActionSwitchTime();

        /*
	** menu Bienvenue
         */
        Item menuBienvenue = new Item("BIENVENUE", SwitchTime);
        Item menuBienvenue2 = new Item("UTILISATION", SwitchTime);
        root.addElementSubMenu(menuBienvenue);
        root.addElementSubMenu(menuBienvenue2);

        child();

        /*
	** Menu Code Gestionaire
         */
        Item codeGestionaire = new Item("codegestion", new ActionEntreeCodeGestionaire());
        /*
	** Defilement des Noms
         */

        Item defilementResident = new Item(">>", new DefilementResident());

        /*
	** Menu Clavier Code
         */
        Item codeClavierCode = new Item(" CODE" + "\n" + " ", new ActionEntreeClavierCode());
        Item AppelcodeResident = new Item("raccourci", new ActionAppelerCodeResident());
        Item codeAbrege = new Item("codeabrege", new ActionCodeAbrege());

        menuBienvenue.addElementSubMenu(codeGestionaire);
        menuBienvenue2.addElementSubMenu(codeGestionaire);

        menuBienvenue.addElementSubMenu(codeClavierCode);
        menuBienvenue2.addElementSubMenu(codeClavierCode);

        menuBienvenue.addElementSubMenu(defilementResident);
        menuBienvenue2.addElementSubMenu(defilementResident);

        menuBienvenue.addElementSubMenu(AppelcodeResident);
        menuBienvenue2.addElementSubMenu(AppelcodeResident);

        menuBienvenue.addElementSubMenu(codeAbrege);
        menuBienvenue2.addElementSubMenu(codeAbrege);

        /*
	** Appel
         */
        Item call = new Item(">>", new Call());

        defilementResident.addElementSubMenu(call);

        /*
	** MENU ---------------------------------
         */
        Item saisieFiche = new Item("saisieresident", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(saisieFiche);

        /*
	** MODIFIER/CREER FICHE TELEPHONE
         */
        Item modifCreerficheTel = new Item("creerfiche");
        saisieFiche.addElementSubMenu(modifCreerficheTel);

        Item codeResident = new Item("saisiecode", new ActionEntreeCodeResident());
        Item ModifierNom = new Item("saisienom", new ActionModifierNom());
        Item ModifierPremierAppel = new Item("g_technoappel", new ActionModifierPremierAppel());
        Item ModifierTelIpClient1 = new Item("telmo1", new ActionModifierTelIpClient1());
        Item ModifierTelephone1 = new Item("tel1", new ActionModifierTelephone1());
        Item ModifierPortableVideo = new Item("smartphone_menu", new ActionModifierPortableVideo());
//	Item ModifierDoubleAppel = new Item("doubleappel", new ActionModifierDoubleAppel());
        Item ModifierAppelSimultane = new Item("simultane", new ActionModifierAppelSimultane());
        Item ModifierTempsRenvoiAppel = new Item("dureediffere", new ActionModifierTempsRenvoiAppel());
        Item ModifierSecondAppel = new Item("d_technoappel", new ActionModifierSecondAppel());
        Item ModifierTelIpClient2 = new Item("telmo2", new ActionModifierTelIpClient2());
        //Item ModifierTelephone2 = new Item("tel2", new ActionModifierTelephone2());
        //Item ModifierPortableVideo2 = new Item("smartphone2", new ActionModifierPortableVideo2());
        Item ActionModifierResidentRenvoi_OuiNon = new Item("coderenvoi", new ActionModifierResidentRenvoi_OuiNon());
        Item ModifierResidentRenvoi = new Item("coderenvoi", new ActionModifierResidentRenvoi());
        Item ActionModifier_ListeRouge = new Item("listerouge", new ActionModifier_ListeRouge());
        Item ActionModifier_ProfessionLib = new Item("professionlib", new ActionModifier_ProfessionLib());
        modifCreerficheTel.addElementSubMenu(codeResident);
        modifCreerficheTel.addElementSubMenu(ModifierNom);
        modifCreerficheTel.addElementSubMenu(ModifierPremierAppel);
        modifCreerficheTel.addElementSubMenu(ModifierTelIpClient1);
        modifCreerficheTel.addElementSubMenu(ModifierTelephone1);
        modifCreerficheTel.addElementSubMenu(ModifierPortableVideo);
        //modifCreerficheTel.addElementSubMenu(ModifierDoubleAppel);
        modifCreerficheTel.addElementSubMenu(ModifierAppelSimultane);
        modifCreerficheTel.addElementSubMenu(ModifierTempsRenvoiAppel);
        modifCreerficheTel.addElementSubMenu(ModifierSecondAppel);
        modifCreerficheTel.addElementSubMenu(ModifierTelIpClient2);
        //modifCreerficheTel.addElementSubMenu(ModifierTelephone2);
        //modifCreerficheTel.addElementSubMenu(ModifierPortableVideo2);
        modifCreerficheTel.addElementSubMenu(ActionModifierResidentRenvoi_OuiNon);
        modifCreerficheTel.addElementSubMenu(ModifierResidentRenvoi);
        modifCreerficheTel.addElementSubMenu(ActionModifier_ListeRouge);
        modifCreerficheTel.addElementSubMenu(ActionModifier_ProfessionLib);

        /*
	** MODIFIER/CREER FICHE CLE
         */
        Item lireFicheCle = new Item("lirecle");
        saisieFiche.addElementSubMenu(lireFicheCle);

        Item codeResidentCle = new Item("saisiecode", new ActionEntreeCodeResidentCle());
        Item ModifierNomCle = new Item("saisienom", new ActionModifierNomCle());
        Item AffichageBadge = new Item("cle1", new ActionAffichageBadge());
        Item ValidationLecteur = new Item("lecteur1", new ActionValidationLecteur());
        lireFicheCle.addElementSubMenu(codeResidentCle);
        lireFicheCle.addElementSubMenu(ModifierNomCle);
        lireFicheCle.addElementSubMenu(AffichageBadge);
        lireFicheCle.addElementSubMenu(ValidationLecteur);


        /*
	** LIRE FICHE EMETTEUR
         */
        Item lireFicheEmetteur = new Item("lirebip");
        saisieFiche.addElementSubMenu(lireFicheEmetteur);

        Item codeResidentEmetteur = new Item("saisiecode", new ActionEntreeCodeResidentEmetteur());
        Item ModifierNomEmetteur = new Item("saisienom", new ActionModifierNomEmetteur());
        Item LireEmetteur = new Item("bip1", new ActionLireEmetteur());
        lireFicheEmetteur.addElementSubMenu(codeResidentEmetteur);
        lireFicheEmetteur.addElementSubMenu(ModifierNomEmetteur);
        lireFicheEmetteur.addElementSubMenu(LireEmetteur);

        /*
	** FONCTION PERMANENT OU TEMPORAIRE
         */

 /*Item permTemporaire = new Item("FONCTIONS"+"\n"+"PERM./TEMPORAIRE");	
	saisieFiche.addElementSubMenu(permTemporaire);
	
	Item entrerCodeAppel = new Item("SAISIE DU CODE\n D'APPEL:", new ActionEntreeCodeAppel());
	Item modifierNomPerm = new Item("SAISIE DU NOM", new ActionModifierNomPerm());
	Item modifierRenvoiTempo = new Item("RENVOI TEMPO.:", new ActionChoixRenvoiTempo());
	Item modifierAppelTempo = new Item("APPEL TEMPORAIRE", new ActionChoixAppelTempo());
	Item modifierListeRouge = new Item(" LISTE ROUGE", new ActionChoixListeRouge());
	Item modifierIdentitel = new Item("IDENTITEL", new ActionChoixIdentitel());
	Item modifierBadgeTempo = new Item("BADGE TEMPORAIRE", new ActionChoixBadgeTempo());
	Item modifierProfLiberale = new Item("PROF. LIBERALE", new ActionChoixProfLiberale());
	Item modifierCodeAppartement = new Item("CODE APPARTEMENT");
	Item modifierEtage = new Item("SAISIE ETAGE");
	permTemporaire.addElementSubMenu(modifierNomPerm);
	permTemporaire.addElementSubMenu(entrerCodeAppel);	
	permTemporaire.addElementSubMenu(modifierRenvoiTempo);
	permTemporaire.addElementSubMenu(modifierAppelTempo);
	permTemporaire.addElementSubMenu(modifierListeRouge);
	permTemporaire.addElementSubMenu(modifierIdentitel);;
	permTemporaire.addElementSubMenu(modifierBadgeTempo);
	permTemporaire.addElementSubMenu(modifierProfLiberale);
	permTemporaire.addElementSubMenu(modifierCodeAppartement);
	permTemporaire.addElementSubMenu(modifierEtage);*/

 /*
	** Menu effacer une fiche resident
         */
        Item effacer_Fiche = new Item("effacerfiche");
        saisieFiche.addElementSubMenu(effacer_Fiche);

        Item CompterResident = new Item("ficheresident", new ActionCompterNbResidents());
        Item effacerFiche = new Item("effacerfiche", new ActionEffacerFiche());
        effacer_Fiche.addElementSubMenu(CompterResident);
        effacer_Fiche.addElementSubMenu(effacerFiche);

        /*
	** ----------------------------------------------------------
         */

 /*
	** Menu modification des deux digicodes
         */
        Item deuxClavierCodes = new Item("deuxdigicodes", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(deuxClavierCodes);

        /*
	** Menu modification digicode 1
         */
        Item modifCode1 = new Item("modifdigicode1");
        deuxClavierCodes.addElementSubMenu(modifCode1);

        Item modifierCode1_1 = new Item("digicode1", new ActionModifierClavierCodeUn_code());
        Item modifierCode1_2 = new Item("choixporte", new ActionModifierClavierCodeUn_choix());
        modifCode1.addElementSubMenu(modifierCode1_1);
        modifCode1.addElementSubMenu(modifierCode1_2);

        /*
	** Menu modification digicode 2
         */
        Item modifCode2 = new Item("modifdigicode2");
        deuxClavierCodes.addElementSubMenu(modifCode2);

        Item modifierCode2_1 = new Item("digicode2", new ActionModifierClavierCodeDeux_code());
        Item modifierCode2_2 = new Item("choixporte:", new ActionModifierClavierCodeDeux_choix());
        modifCode2.addElementSubMenu(modifierCode2_1);
        modifCode2.addElementSubMenu(modifierCode2_2);

        /*
	** ----------------------------------------------------------
         */

 /*
	** Menu Entree Libre
         */
        Item entreeLibre = new Item("entreeslibres", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(entreeLibre);

        /*
	** Menu entree libre porte 1
         */
        Item validationPorte1 = new Item("validporte1");
        entreeLibre.addElementSubMenu(validationPorte1);

        Item modifierEL1_1 = new Item("validerporte1", new ActionModifierEntreLibreUn_choix());
        validationPorte1.addElementSubMenu(modifierEL1_1);

        /*
	** Menu entree libre porte 2
         */
        Item validationPorte2 = new Item("validporte2");
        entreeLibre.addElementSubMenu(validationPorte2);

        Item modifierEL2_1 = new Item("validerporte2", new ActionModifierEntreLibreDeux_choix());
        validationPorte2.addElementSubMenu(modifierEL2_1);

        /*
	** Menu Parametres d'appel
         */
        Item optionAppel = new Item("parappel", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(optionAppel);

        /*
	** Menu modifier temps d'appel
         */
        Item DureeAppel = new Item("tempsappel");
        optionAppel.addElementSubMenu(DureeAppel);

        Item modifierTempsAppel = new Item("dureeappel", new ActionModifierTempsAppel());
        DureeAppel.addElementSubMenu(modifierTempsAppel);

        /*
	** Menu modifier temps de communication
         */
        Item DureeCommunication = new Item("tempscom");
        optionAppel.addElementSubMenu(DureeCommunication);

        Item modifierTempsCommunication = new Item("communication_menu", new ActionModifierTempsCommunication());
        DureeCommunication.addElementSubMenu(modifierTempsCommunication);

        /*
	** Menu appel abreger
         */
        Item AppelAbrege = new Item("appelabrege");
        optionAppel.addElementSubMenu(AppelAbrege);

        AppelAbrege.addElementSubMenu(new Item("", new ActionModifierRaccourciAppel()));
        AppelAbrege.addElementSubMenu(new Item("texto", new ActionModifierAppelTexto()));

        /*
	** Menu modifier niveau haut parleur
         */
        Item NiveauHP = new Item("son");

        optionAppel.addElementSubMenu(NiveauHP);

        Item modifierNiveauHP = new Item("niveauson", new ActionModifierNiveauHP());
        Item modifierModeTest = new Item("modetest", new ActionModifierModeTest());
        Item modifierSortieCasque = new Item("sortiecasque", new ActionModifierSortieCasque());
        Item modifierSortieHDMI = new Item("sortiehdmi", new ActionModifierSortieHDMI());

        NiveauHP.addElementSubMenu(modifierNiveauHP);
        NiveauHP.addElementSubMenu(modifierModeTest);
        NiveauHP.addElementSubMenu(modifierSortieCasque);
        NiveauHP.addElementSubMenu(modifierSortieHDMI);

        /*
	** Menu modifier code d'appel
         */
        Item codeAppel = new Item("accueil");
        optionAppel.addElementSubMenu(codeAppel);

        Item modifcodeAppel = new Item("saisiecode", new ActionModifierCodeAppel());
        codeAppel.addElementSubMenu(modifcodeAppel);

        /*
	** ----------------------------------------------------------
         */

 /*
	** Menu Option Affichage
         */
        Item optionAffichage = new Item("paraffichage", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(optionAffichage);

        Item choixLangue = new Item("choixlangue");
        optionAffichage.addElementSubMenu(choixLangue);
        Item actionChoixLangue = new Item("", new ActionModifierLangue());
        choixLangue.addElementSubMenu(actionChoixLangue);

        Item nomEquipement = new Item("nomequipement");
        optionAffichage.addElementSubMenu(nomEquipement);
        Item actionNomEquipement = new Item("", new ActionModifierNomEquipement());
        nomEquipement.addElementSubMenu(actionNomEquipement);

        /*
	** Menu modifier message de bienvenue
         */
        Item messageBienvenue = new Item("messagebienvenue");
        optionAffichage.addElementSubMenu(messageBienvenue);
        Item modifBienvenue = new Item("", new ActionModifierBienvenue());
        messageBienvenue.addElementSubMenu(modifBienvenue);

        /*
	** Menu modifier message d'utilisation
         */
        Item messageUtilisation = new Item("messageutilis");
        optionAffichage.addElementSubMenu(messageUtilisation);
        Item modifUtilisation = new Item("", new ActionModifierUtilisation());
        messageUtilisation.addElementSubMenu(modifUtilisation);


        /*
	** menu affichage de l'etage
         */
 /*Item modifAffichageBureau = new Item("AFFICHAGE"+"\n"+"DE L'ETAGE");	
	optionAffichage.addElementSubMenu( modifAffichageBureau);
	
	Item modifAffichageEtage = new Item("AFFICHER ETAGE", new ActionModifierAffichageEtage());
	modifAffichageBureau.addElementSubMenu(modifAffichageEtage);*/
 /*
	** menu priorite affichage
         */
 /*Item prioriteAffichage = new Item("PRIORITE DE L'"+"\n"+"AFFICHAGED'APPEL");	
	optionAffichage.addElementSubMenu(prioriteAffichage);
	
	Item modifPrioriteAffichage = new Item("1 2 3", new ActionModifierPrioriteAffichage());	
	prioriteAffichage.addElementSubMenu(modifPrioriteAffichage);*/
 /*
	** ----------------------------------------------------------
         */
 /*
	** menu gestion portes
         */
        Item gestionPorte = new Item("paraporte", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(gestionPorte);

        /*
	** menu duree ouverture porte
         */
        Item dureePorte = new Item("dureeporte");
        gestionPorte.addElementSubMenu(dureePorte);

        Item modifierDureePorteUn = new Item("tempsporte1", new ActionModifierTempsOuvertureUn());
        Item modifierDureePorteDeux = new Item("tempsporte2", new ActionModifierTempsOuvertureDeux());

        dureePorte.addElementSubMenu(modifierDureePorteUn);
        dureePorte.addElementSubMenu(modifierDureePorteDeux);

        /*
	** menu signaux sonores ouverture porte
         */
        Item signauxSonorePorte = new Item("sonporte");
        gestionPorte.addElementSubMenu(signauxSonorePorte);

        Item buzzerPorte1 = new Item("buzzer1", new ActionChoixBuzzer());
        Item voixPorte1 = new Item("voix1", new ActionChoixVoix());
        Item buzzerPorte2 = new Item("buzzer2", new ActionChoixBuzzer());
        Item voixPorte2 = new Item("voix2", new ActionChoixVoix());
        signauxSonorePorte.addElementSubMenu(buzzerPorte1);
        signauxSonorePorte.addElementSubMenu(voixPorte1);
        signauxSonorePorte.addElementSubMenu(buzzerPorte2);
        signauxSonorePorte.addElementSubMenu(voixPorte2);

        /*
	** ----------------------------------------------------------
         */
 /*
	** menu parametre de gestion d'acces
         */
        Item ParaAcces = new Item("paracces", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(ParaAcces);

        /*
	** Menu Controle d'acces
         */
        Item CASignaux = new Item("visuel", new ActionModifierSignauxPorte());
        Item CAPorte1 = new Item("nomporte1", new ActionModifierTempoPorte(porte.getLecteurPorte1()));
        Item CAPorte2 = new Item("nomporte2", new ActionModifierTempoPorte(porte.getLecteurPorte2()));
        Item CAPorte3 = new Item("nomporte3", new ActionModifierTempoPorte(porte.getLecteurPorte3()));
        Item CAPorte4 = new Item("nomporte4", new ActionModifierTempoPorte(porte.getLecteurPorte4()));
        Item CAPorte5 = new Item("nomporte5", new ActionModifierTempoPorte(porte.getLecteurPorte5()));
        Item CAPorte6 = new Item("nomporte6", new ActionModifierTempoPorte(porte.getLecteurPorte6()));
        Item CAPorte7 = new Item("nomporte7", new ActionModifierTempoPorte(porte.getLecteurPorte7()));
        Item CAPorte8 = new Item("nomporte8", new ActionModifierTempoPorte(porte.getLecteurPorte8()));

        ParaAcces.addElementSubMenu(CASignaux);
        if (!porte.getLecteurPorte1().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte1);
        }
        if (!porte.getLecteurPorte2().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte2);
        }
        if (!porte.getLecteurPorte3().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte3);
        }
        if (!porte.getLecteurPorte4().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte4);
        }
        if (!porte.getLecteurPorte5().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte5);
        }
        if (!porte.getLecteurPorte6().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte6);
        }
        if (!porte.getLecteurPorte7().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte7);
        }
        if (!porte.getLecteurPorte8().equals("")) {
            ParaAcces.addElementSubMenu(CAPorte8);
        }

        /*
	** menu gestion donnees
         */
        Item gestionDonnees = new Item("pardonnees", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(gestionDonnees);

        /*
	** menu parametres de mise en service
         */
        Item modifCode = new Item("codegestionnaire");
        gestionDonnees.addElementSubMenu(modifCode);

        /*
	** Menu gestion par Internet
         */
        Item gestionInternet = new Item("gestioninternet");
        gestionDonnees.addElementSubMenu(gestionInternet);

        Item connexionInternet = new Item("datainternet", new ActionModifierConnexionInternet());
        Item delaiRequete = new Item("delaiinternet", new ActionModifierDelaiRequete());
        Item formatdelaiRequete = new Item("", new ActionModifierFormatDelaiRequete());

        gestionInternet.addElementSubMenu(connexionInternet);
        gestionInternet.addElementSubMenu(delaiRequete);
        gestionInternet.addElementSubMenu(formatdelaiRequete);

        /*
	** ----------------------------------------------------------
         */
 /*
	** menu parametrage reseaux
         */
        Item paraReseaux = new Item("parreseaux", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(paraReseaux);

        /*
	** sous menu configuration ip local
         */

 /*if (porte.getServeurIP().charAt(0) == 'D' ||
	    porte.getServeurIP().charAt(1) == 'D') {	    
	Item parametreIpLocal = new Item("adrlocal");
	paraReseaux.addElementSubMenu(parametreIpLocal);

	Item modifierIpLocal = new Item("IP LOCAL", new ActionModifierIpLocal());	
	parametreIpLocal.addElementSubMenu(modifierIpLocal);
	}*/
 /*
	** sous menu parametrage maitre d'ouvrage
         */
        if (porte.getServeurIP().charAt(0) == 'C'
                || porte.getServeurIP().charAt(1) == 'C') {
            Item paraMaitreOuv = new Item("adrclient");
            paraReseaux.addElementSubMenu(paraMaitreOuv);

            Item serveurMaitreOuv = new Item("ipclient", new ActionModifierIpClient());
            Item userMaitreOuv = new Item("userclient", new ActionModifierUserClient());
            Item mdpMaitreOuv = new Item("mdpclient", new ActionModifierMdpClient());
            paraMaitreOuv.addElementSubMenu(serveurMaitreOuv);
            paraMaitreOuv.addElementSubMenu(userMaitreOuv);
            paraMaitreOuv.addElementSubMenu(mdpMaitreOuv);

            /*Item paraDelaiInternet = new Item("voipinternet");
	    paraReseaux.addElementSubMenu(paraDelaiInternet);
	    
	    Item telVoip = new Item("telipinternet", new ActionModifierTelvoip());
	    paraDelaiInternet.addElementSubMenu(telVoip);*/
        }

        Item serveurDHCP = new Item("modedhcp", new ActionModifierServeurDhcp());
        paraReseaux.addElementSubMenu(serveurDHCP);

        Item adresseIpPlatine = new Item("ipplatinefixe", new ActionModifierIpPlatine());
        Item netmask = new Item("netmask", new ActionModifierNetmask());
        Item passerelle = new Item("passerelle", new ActionModifierPasserelle());

        serveurDHCP.addElementSubMenu(adresseIpPlatine);
        serveurDHCP.addElementSubMenu(netmask);
        serveurDHCP.addElementSubMenu(passerelle);

        if (porte.getServeurIP().charAt(0) == 'O'
                || porte.getServeurIP().charAt(1) == 'O') {
            Item serveurAudio = new Item("adraudio");
            paraReseaux.addElementSubMenu(serveurAudio);

            Item serveurFixe = new Item("ipaudio", new ActionModifierIpExterne());
            Item userFixe = new Item("useraudio", new ActionModifierUserOvh());
            Item mdpFixe = new Item("mdpaudio", new ActionModifierMdpOvh());
            serveurAudio.addElementSubMenu(serveurFixe);
            serveurAudio.addElementSubMenu(userFixe);
            serveurAudio.addElementSubMenu(mdpFixe);
        }

        if (porte.getServeurIP().charAt(0) == 'V'
                || porte.getServeurIP().charAt(1) == 'V') {
            Item serveurVideo = new Item("adraudio");
            paraReseaux.addElementSubMenu(serveurVideo);

            Item serveurSipSmartphone = new Item("ipvideo");
            serveurVideo.addElementSubMenu(serveurSipSmartphone);
        }

        Item parametreWifi = new Item("routeurwifi", new ActionModifierValidWifi());
        paraReseaux.addElementSubMenu(parametreWifi);

        Item identifiantWifi = new Item("identifiantwifi", new ActionModifierIdWifi());
        Item mdpWifi = new Item("motdepassewifi", new ActionModifierMdpWifi());
        parametreWifi.addElementSubMenu(identifiantWifi);
        parametreWifi.addElementSubMenu(mdpWifi);

        /*
	** menu gestion / suivi des evenements
         */
 /*Item gestionEvenement = new Item("evenements", new ActionMenuGestionaire());
	codeGestionaire.addElementSubMenu(gestionEvenement);*/

 /*
	** menu visualiser evenements
         */
 /*Item visualiserEve = new Item("VISUALISER\nEVENEMENT");
	gestionEvenement.addElementSubMenu(visualiserEve);
	
	Item visuEvenement = new Item("Evenements",new ActionVisualiserEvenements());	
	visualiserEve.addElementSubMenu(visuEvenement);*/

 /*
	** menu visualiser appel
         */

 /*Item visualiserAppel = new Item("VISUALISER "+"\n"+"APPEL RECU");
	gestionEvenement.addElementSubMenu(visualiserAppel);*/

 /*
	** menu visualiser cle lecteur
         */

 /*Item typeClavierh = new Item("VISUALISER"+"\n"+"CLE LECTEUR");
	gestionEvenement.addElementSubMenu(typeClavierh);*/
 /*
	** ----------------------------------------------------------
         */
 /*
	** menu info platine
         */
        Item infoPlatine = new Item("informations", new ActionMenuGestionaire());
        codeGestionaire.addElementSubMenu(infoPlatine);

        /*
	Item miseEnService = new Item("PARAMETRES DE"+"\n"+"MISE EN SERVICE", new ActionMenuGestionaire());	
	codeGestionaire.addElementSubMenu(miseEnService);
	
	DataBase data1 = DataBase.getInstance();
	Porte porte1 = data1.getPorte();
	
	String stringNomDomaine = "SERVEUR SIP\n";
	if (porte1.getServeurIP().equals("DD")) {
	    stringNomDomaine +="# LOCAL--# LOCAL";
	}
	else if (porte1.getServeurIP().equals("VV")) {
	    stringNomDomaine +="VIDEO #--VIDEO #";
	}
	else if (porte1.getServeurIP().equals("CC")) {
	    stringNomDomaine += "CLIENT --CLIENT ";
	}
	else if (porte1.getServeurIP().equals("OO")) {
	    stringNomDomaine +="AUDIO --AUDIO ";
	}
	else if ((porte1.getServeurIP().equals("DC")) || (porte1.getServeurIP().equals("CD"))) {
	    stringNomDomaine += "# LOCAL--CLIENT ";
	}
	else if ((porte1.getServeurIP().equals("DV")) || (porte1.getServeurIP().equals("VD"))) {
	    stringNomDomaine += "# LOCAL--VIDEO #";
	}
	else if ((porte1.getServeurIP().equals("DO")) || (porte1.getServeurIP().equals("OD"))) {
	    stringNomDomaine +="#LOCAL --AUDIO ";
	}
	else if ((porte1.getServeurIP().equals("VC")) || (porte1.getServeurIP().equals("CV"))) {
	    stringNomDomaine += "VIDEO #--CLIENT ";
	}
	else if ((porte1.getServeurIP().equals("CO")) || (porte1.getServeurIP().equals("OC"))) {
	    stringNomDomaine += "CLIENT --AUDIO ";
	}
	else if ((porte1.getServeurIP().equals("VO")) || (porte1.getServeurIP().equals("OV"))) {
	    stringNomDomaine += "VIDEO #--AUDIO";
	}
	
	Item DomaineServeur = new Item(stringNomDomaine);	
	Item modifierDuree = new Item("MODIFIER DUREE "+"\n"+"SAISIE AU MENU");	
	
	miseEnService.addElementSubMenu(DomaineServeur);

	Item modifierCodeGestionaire = new Item("CODEGESTIONNAIRE"+"\n"+" ", new ActionModifierCodeGestionaire());
	Item ipInterne1 = new Item("SERVEUR # LOCAL"+"\n"+"", new ActionIpLocal());
	Item ipProxyClient = new Item("SERVEUR CLIENT"+"\n"+"", new ActionProxyIpClient());
	Item ModifierUserClient = new Item("UTILISATEURCLIENT"+"\n"+"", new ActionModifierUserClient());
	Item ModifierMdpClient = new Item("MOT PASSE CLIENT"+"\n"+"", new ActionModifierMdpClient());
	Item ipExterne = new Item("SERVEUR AUDIO"+"\n"+"", new ActionIpExterne());
	Item ModifierMdpOvh = new Item("MOT PASSE AUDIO"+"\n"+"", new ActionModifierMdpOvh());
	Item ModifierUserOvh = new Item("UTILISATEURAUDIO"+"\n"+"", new ActionModifierUserOvh());
	Item ModifierServeurVideo = new Item("SERVEUR VIDEO"+"\n"+"", new ActionModifierServeurVideo());
	
	modifCode.addElementSubMenu(modifierCodeGestionaire);

	if (porte.getServeurIP().charAt(0) == 'C') {
	    DomaineServeur.addElementSubMenu(ipProxyClient);
	    DomaineServeur.addElementSubMenu(ModifierUserClient);
	    DomaineServeur.addElementSubMenu(ModifierMdpClient);
	    DomaineServeur.addElementSubMenu(ipExterne);
	    DomaineServeur.addElementSubMenu(ModifierUserOvh);
	    DomaineServeur.addElementSubMenu(ModifierMdpOvh);
	    DomaineServeur.addElementSubMenu(ModifierServeurVideo);
	    DomaineServeur.addElementSubMenu(ipInterne1);
	}
	else if (porte.getServeurIP().charAt(0) == 'V') {
	    DomaineServeur.addElementSubMenu(ModifierServeurVideo);
	    DomaineServeur.addElementSubMenu(ipInterne1);
	    DomaineServeur.addElementSubMenu(ipProxyClient);
	    DomaineServeur.addElementSubMenu(ModifierUserClient);
	    DomaineServeur.addElementSubMenu(ModifierMdpClient);
	    DomaineServeur.addElementSubMenu(ipExterne);
	    DomaineServeur.addElementSubMenu(ModifierUserOvh);
	    DomaineServeur.addElementSubMenu(ModifierMdpOvh);
	}
	else if (porte.getServeurIP().charAt(0) == 'O') {
	    DomaineServeur.addElementSubMenu(ipExterne);
	    DomaineServeur.addElementSubMenu(ModifierUserOvh);
	    DomaineServeur.addElementSubMenu(ModifierMdpOvh);
	    DomaineServeur.addElementSubMenu(ipInterne1);
	    DomaineServeur.addElementSubMenu(ipProxyClient);
	    DomaineServeur.addElementSubMenu(ModifierUserClient);
	    DomaineServeur.addElementSubMenu(ModifierMdpClient);
	    DomaineServeur.addElementSubMenu(ModifierServeurVideo);
	}
	else if (porte.getServeurIP().charAt(0) == 'D') {
	    DomaineServeur.addElementSubMenu(ipInterne1);
	    DomaineServeur.addElementSubMenu(ipProxyClient);
	    DomaineServeur.addElementSubMenu(ModifierUserClient);
	    DomaineServeur.addElementSubMenu(ModifierMdpClient);
	    DomaineServeur.addElementSubMenu(ipExterne);
	    DomaineServeur.addElementSubMenu(ModifierUserOvh);
	    DomaineServeur.addElementSubMenu(ModifierMdpOvh);
	    DomaineServeur.addElementSubMenu(ModifierServeurVideo);
	}
         */
 /*
	** menu Info Platine
         */
        String MacWifi1 = "xx-xx-xx";
        String MacWifi2 = "xx-xx-xx";
        char t_clavier = porte.getInterphone().charAt(1);
        String MacEth1 = porte.getMacEthernet().substring(0, 6);
        String MacEth2 = porte.getMacEthernet().substring(6);
        if (porte.getMacWifi().length() > 0) {
            MacWifi1 = porte.getMacWifi().substring(0, 6);
            MacWifi2 = porte.getMacWifi().substring(6);
        }

        String ts_techno = new String();
        String ts_clavier = new String();

        switch (porte.getServeurIP()) {
            case "D-":
                ts_techno = " INTERNE ";
                break;
            case "V-":
                ts_techno = " CLOUD ";
                break;
            case "C-":
                ts_techno = "CLIENT -- CLIENT";
                break;
            case "O-":
                ts_techno = "AUDIO -- AUDIO";
                break;
            case "DC":
            case "CD":
                ts_techno = "INTERNE--CLIENT ";
                break;
            case "DV":
            case "VD":
                ts_techno = "INTERNE -- CLOUD";
                break;
            case "DO":
            case "OD":
                ts_techno = "INTERNE -- AUDIO";
                break;
            case "VC":
            case "CV":
                ts_techno = "CLOUD -- CLIENT";
                break;
            case "CO":
            case "OC":
                ts_techno = "CLIENT -- AUDIO ";
                break;
            case "VO":
            case "OV":
                ts_techno = "CLOUD -- AUDIO";
                break;
            default:
                break;
        }
        switch (t_clavier) {
            case '1':
                ts_clavier = "MAGELLAN";
                break;
            case '2':
                ts_clavier = "HAUSSMAN";
                break;
            case '3':
                ts_clavier = "BOSPHORE";
                break;
            case '4':
                ts_clavier = "GIBRALTAR1";
                break;
            case '5':
                ts_clavier = "GIBRALTAR2";
                break;
            default:
                break;
        }

        Item numPorte = new Item("LOGIDIESE.COM\nPORTE: " + porte.getNumPorte());
        Item typeTechno = new Item("TYPE TECHNOLOGIE\n" + ts_techno);
        Item typeClavier = new Item("TYPE DE CLAVIER\n" + ts_clavier);
        Item nbResidents = new Item("FICHES RESIDENTS", new ActionVoirNombreResidents());
        Item coInternet = new Item("STATUT INTERNET", new ActionStatusInternet());
        Item versionC4 = new Item("MACHINE----SOFT\n" + porte.getIdTeamViewer() + "--" + porte.getVersion());
        Item IpEthernet = new Item("IP ETHERNET-4G\n", new ActionAdresseIpEthernet());
        Item IpWifi = new Item("IP WIFI-ROUTERWI\n", new ActionAdresseIpWifi());
        Item MacEthernet = new Item("MAC-ETHER-" + MacEth1 + "\n" + MacEth2);
        Item MacWifi = new Item("MAC-WIFI-" + MacWifi1 + "\n" + MacWifi2);
        Item infoBadge = new Item("lecteurBadge", new ActionInfoBadge());

        infoPlatine.addElementSubMenu(numPorte);
        infoPlatine.addElementSubMenu(typeTechno);
        infoPlatine.addElementSubMenu(typeClavier);
        infoPlatine.addElementSubMenu(nbResidents);
        infoPlatine.addElementSubMenu(infoBadge);
        infoPlatine.addElementSubMenu(IpEthernet);
        infoPlatine.addElementSubMenu(IpWifi);
        infoPlatine.addElementSubMenu(MacEthernet);
        infoPlatine.addElementSubMenu(MacWifi);
        infoPlatine.addElementSubMenu(coInternet);
    }

    /**
     * s'assurer d'avoir une seule instance de la classe
     *
     * @return objet de la classe
     */
    public static Menu getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Menu();
        }
        return INSTANCE;
    }

    /**
     * aller à l'element suivant sur le menu
     */
    public void next() {
        if (currentItem.father == null) { // root
            return;
        }

        Item father = currentItem.father;
        int indexMenu = father.childs.indexOf(currentItem);

        if (father.childs.size() == 1) {
            return;
        }
        if (indexMenu >= (father.childs.size() - 1)) {
            currentItem = father.childs.get(0);
            itemChanged = true;
        } else {
            currentItem.exit();
            currentItem = father.childs.get(indexMenu + 1);
            itemChanged = true;
        }
    }

    /**
     * aller à l'element precedent sur la liste
     */
    public void previous() {

        if (currentItem.father == null) { // root
            return;
        }

        Item father = currentItem.father;
        int indexMenu = father.childs.indexOf(currentItem);

        if (father.childs.size() == 1) {
            return;
        }
        if (indexMenu <= 0) {
            currentItem.exit();
            currentItem = father.childs.get(father.childs.size() - 1);
            itemChanged = true;
        } else {
            currentItem.exit();
            currentItem = father.childs.get(indexMenu - 1);
            itemChanged = true;
        }
    }

    /**
     * aller à l'element fils
     */
    public void child() {
        if (currentItem.childs.isEmpty()) {
            return;
        }
        currentItem.exit();
        currentItem = currentItem.childs.get(0);
        itemChanged = true;
    }

    /**
     * revient à l'element pere
     */
    public void father() {

        if (currentItem.father == null) { // root
            return;
        }
        currentItem.exit();
        currentItem = currentItem.father;
        itemChanged = true;
        System.out.println("XXX");
    }

    /**
     * initialiser la liste en revenant sur l'item do'rigine
     */
    public void root() {
        currentItem = root;
    }

    /**
     * init
     */
    public void init() {
        currentItem.init();
    }

    /**
     * renvoi l'élement en cours
     *
     * @return item l'élement en cours
     */
    public Item getCurrentItem() {
        if (itemChanged) {
            currentItem.init();
            itemChanged = false;
        }
        return currentItem;
    }

    @Override
    public String toString() {
        return currentItem.toString();
    }
}
