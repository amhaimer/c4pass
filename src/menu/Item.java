package menu;

import java.util.*;
import menu.action.*;
import event.Event;
import sql.DataBase;
import sql.data.Porte;

/**
 * les elements du menu
 *
 * @author Abdellah
 */
public class Item {

    private static DataBase data = DataBase.getInstance();
    private static Porte porte;
    public Item father = null;
    public ArrayList<Item> childs;
    public String valeurTraduction;
    public String affichage;
    public ActionItem execute;

    /**
     * Constructeur de la Class Item
     *
     * @param valeurTraduction message à afficher
     */
    public Item(String valeurTraduction) {
        this.valeurTraduction = valeurTraduction;
        childs = new ArrayList<>();
        execute = new ActionNavigate();
    }

    /**
     * 2eme constructeur
     *
     * @param valeurTraduction message à afficher
     * @param ex item à executer
     */
    public Item(String valeurTraduction, ActionItem ex) {
        this.valeurTraduction = valeurTraduction;
        childs = new ArrayList<>();
        execute = ex;
    }

    /**
     * ajouter un element à la liste
     *
     * @param e l'item à rajouter
     */
    public void addElementSubMenu(Item e) {
        e.father = this;
        childs.add(e);
    }

    /**
     * l'action de l'item courant en fonction d'un evement produit
     *
     * @param event l'evenement
     */
    public void execute(Event event) {
        execute.action(event);
    }

    /**
     * init
     */
    public void init() {
        porte = Porte.getInstance();
        if (valeurTraduction != null && !valeurTraduction.isEmpty()) {
            affichage = data.getTraduction(valeurTraduction, porte.getTraduction());
        }
        execute.init();
    }

    /**
     * exit
     */
    public void exit() {
        execute.exit();
    }

    @Override
    public String toString() {
        return affichage;
    }
}
